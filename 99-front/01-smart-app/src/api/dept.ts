import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  DeptInsert: `${prefix}/org/insertDept`,
  DeptDelete: `${prefix}/org/deleteDept`,
  DeptUpdate: `${prefix}/org/updateDept`,
  DeptTree: `${prefix}/org/listTree`,
  OrgInsert: `${prefix}/org/insertOrg`,
  OrgDelete: `${prefix}/org/deleteOrg`,
  OrgUpdate: `${prefix}/org/updateOrg`,
};

// 环境变
export function deptInsert(data: any) {
  return request.post({
    url: Api.DeptInsert,
    data,
  });
}
export function orgInsert(data: any) {
  return request.post({
    url: Api.OrgInsert,
    data,
  });
}

export function deptUpdate(data: any) {
  return request.post({
    url: Api.DeptUpdate,
    data,
  });
}
export function orgUpdate(data: any) {
  return request.post({
    url: Api.OrgUpdate,
    data,
  });
}
export function orgDelete(id: string) {
  return request.post({
    url: `${Api.OrgDelete}/${id}`,
    data: {},
  });
}
export function deptDelete(id: string) {
  return request.post({
    url: `${Api.DeptDelete}/${id}`,
    data: {},
  });
}
export function deptTree() {
  return request.post({
    url: `${Api.DeptTree}`,
  });
}
