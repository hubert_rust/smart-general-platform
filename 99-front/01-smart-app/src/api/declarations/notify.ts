export interface NOTIFY_PARAM {
  operationType?: string; // 操作对象
  message?: string; // 消息名称
  parentTitle? : string;

  data?: any;
}

export interface ACTION_CONTENT {
  content: string;
  value: string;
  data?: any;
  row?: any;
}
export const successInsert = () => {
  return '添加成功!';
};
export const failInsert = (colon: boolean) => {
  if (colon) {
    return '添加失败:';
  }
  return '添加失败';
};
export const successUpdate = () => {
  return '更新成功!';
};
export const failUpdate = (colon: boolean) => {
  if (colon) {
    return '更新失败:';
  }
  return '更新失败';
};

export const successDelete = () => {
  return '删除成功!';
};
export const failDelete = (colon: boolean) => {
  if (colon) {
    return '删除失败:';
  }
  return '删除失败';
};
export const successActive = () => {
  return '激活成功!';
};
export const failActive = (colon: boolean) => {
  if (colon) {
    return '激活失败:';
  }
  return '激活失败';
};
export const successDeactive = () => {
  return '禁用成功!';
};
export const failDeactive = (colon: boolean) => {
  if (colon) {
    return '禁用失败:';
  }
  return '禁用失败';
};

export const failRefresh = (colon: boolean) => {
  if (colon) {
    return '刷新失败:';
  }
  return '刷新失败:';
};
export const failLoad = (colon: boolean) => {
  if (colon) {
    return '加载失败:';
  }
  return '加载失败:';
};
