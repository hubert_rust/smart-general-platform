import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  UserInsert: `${prefix}/user/insert`,
  UserDelete: `${prefix}/user/delete`,
  UserDept: `${prefix}/user/userDept`,
  UserActive: `${prefix}/user/active`,
  UserDeactive: `${prefix}/user/deactive`,
  UserUpdate: `${prefix}/user/update`,
  UserDetail: `${prefix}/user/detail`,
  UserList: `${prefix}/user/list`,
  UserInfo: `${prefix}/user/userInfo`,
  UserMenu: `${prefix}/user/userMenu`,
  UserAppList: `${prefix}/user/apps`,

  // belong
  UserBelongGroup: `${prefix}/user/belongGroup`,
  UserBelongRole: `${prefix}/user/belongRole`,
  UserRemoveBelongRoleSubject: `${prefix}/user/removeRoleSubject`,
  UserGroups: `${prefix}/user/groups`,
  UserRoles: `${prefix}/user/roles`,
  UserUpdateStaticGroup: `${prefix}/user/updateStaticGroup`,
  UserUpdateBelongRole: `${prefix}/user/updateBelongRole`,
  UserRemoveBlongGroup: `${prefix}/user/removeBelongGroup`,

  // 用户应用授权
  UserApplicationPermit: `${prefix}/user/appPermit`,
  UserWorkspaceRole: `${prefix}/user/userSpaceRole`,
  UserMenuList: `${prefix}/user/userMenus`,
  UserApiList: `${prefix}/user/userApis`,

};

export function userMenuList(data: any) {
  return request.post({
    url: `${Api.UserMenuList}`,
    data,
  });
}

export function userApiList(data: any) {
  return request.post({
    url: `${Api.UserApiList}`,
    data,
  });
}
export function userWorkspaceRoleList(uid: string) {
  return request.post({
    url: `${Api.UserWorkspaceRole}/${uid}`,
  });
}
 export function userAppPermitList(uid: string) {
  return request.post({
    url: `${Api.UserApplicationPermit}/${uid}`,
  });
}

export function userApps(data: any) {
  return request.post({
    url: Api.UserAppList,
    data,
  });
}
export function userRemoveBelongRoleSubject(data: any) {
  return request.post({
    url: Api.UserRemoveBelongRoleSubject,
    data,
  });
}
export function userRemoveBelongGroup(data: any) {
  return request.post({
    url: Api.UserRemoveBlongGroup,
    data,
  });
}
export function userUpdateStaticGroup(data: any) {
  return request.post({
    url: Api.UserUpdateStaticGroup,
    data,
  });
}
export function userUpdateBelongRole(data: any) {
  return request.post({
    url: Api.UserUpdateBelongRole,
    data,
  });
}
export function userGroups(uid: string) {
  return request.post({
    url: `${Api.UserGroups}/${uid}`,
  });
}
export function userRoles(uid: string) {
  return request.post({
    url: `${Api.UserRoles}/${uid}`,
  });
}
export function userBelongGroup(uid: string) {
  return request.post({
    url: `${Api.UserBelongGroup}/${uid}`,
  });
}

export function userBelongRole(uid: string) {
  return request.post({
    url: `${Api.UserBelongRole}/${uid}`,
  });
}
// 环境变
export function userDetail(id: string) {
  return request.post({
    url: `${Api.UserDetail}/${id}`,
  });
}
export function userInsert(data: any) {
  return request.post({
    url: Api.UserInsert,
    data,
  });
}
export function userUpdate(data: any) {
  return request.post({
    url: Api.UserUpdate,
    data,
  });
}
// smart-auth: pending
export function userDelete(data: any) {
  return request.post({
    url: `${Api.UserDelete}`,
    data,
  });
}

export function userDept(id: string) {
  return request.post({
    url: `${Api.UserDept}/${id}`,
    data: {},
  });
}

export function userActive(data: any) {
  return request.post({
    url: `${Api.UserActive}`,
    data,
  });
}
export function userDeactive(data: any) {
  return request.post({
    url: `${Api.UserDeactive}`,
    data,
  });
}
export function userList(data: any) {
  return request.post({
    url: `${Api.UserList}`,
    data,
  });
}

export async function getLoginUserInfo() {
  return request.post({
    url: `${Api.UserInfo}`,
  });
}
export function getLoginUserMenu() {
  return request.post({
    url: `${Api.UserMenu}`,
  });
}
