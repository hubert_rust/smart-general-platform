import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  GroupInsert: `${prefix}/group/insert`,
  GroupUserInsert: `${prefix}/group/insertUser`,
  GroupDelete: `${prefix}/group/delete`,
  GroupUserDelete: `${prefix}/group/deleteUser`,
  GroupUpdate: `${prefix}/group/update`,
  GroupDetail: `${prefix}/group/detail`,
  GroupList: `${prefix}/group/list`,
  GroupUserList: `${prefix}/group/listUser`,
};

// 环境变
export function groupDetail(id: string) {
  return request.post({
    url: `${Api.GroupDetail}/${id}`,
  });
}
export function groupInsert(data: any) {
  return request.post({
    url: Api.GroupInsert,
    data,
  });
}

export function groupUserInsert(data: any) {
  return request.post({
    url: Api.GroupUserInsert,
    data,
  });
}
export function groupUpdate(data: any) {
  return request.post({
    url: Api.GroupUpdate,
    data,
  });
}
export function groupDelete(id: string) {
  return request.post({
    url: `${Api.GroupDelete}/${id}`,
    data: {},
  });
}

export function groupUserDelete(data: any) {
  return request.post({
    url: `${Api.GroupUserDelete}`,
    data,
  });
}

export function groupList(data: any) {
  return request.post({
    url: `${Api.GroupList}`,
    data,
  });
}
export function groupListUser(data: any) {
  return request.post({
    url: `${Api.GroupUserList}`,
    data,
  });
}
