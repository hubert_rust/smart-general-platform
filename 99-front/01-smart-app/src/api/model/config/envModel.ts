export interface EnvModel {
  key: string;
  value: string;
  remark: string;
  status: number;
}
