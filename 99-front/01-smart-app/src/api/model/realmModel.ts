export interface RealmModel {
  tidType: string; // tenant, space/realm
  id: string;
  tid?: string;
  tenantName: string;

  remark?: string;
  logo?: string;
  status?: string;
  createBy?: string;
  createTime?: string;
}
