export interface MenuModel {
  voPath?: string;
  voName?: string;
  voOrder?: number;
  voComponent?: string;
  resType?: string;
  voRedirect?: string;
  voTitle?: string;
  voIcon?: string;
  permitCode?: string;
  voHide?: number;
  children?: Array<MenuModel>;
}
