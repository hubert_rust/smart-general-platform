export interface TreeDlgModel {
  keys?: any;
  treeData?: Array<any>[];
  header?: string;

  service?: string;
  key?: string;
  type?: string;
}
