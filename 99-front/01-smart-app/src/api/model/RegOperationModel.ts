export interface RegOperationModel {
  id?: object;
  ids?: Array<string>;
  tid?: string;
  uid?: string;
  opType?: string;
  opObject?: string;
  conditon?: string;
}
export enum RegConsts {
  OP_OBJECT_USER = 'user',
  OP_OBJECT_TENANT = 'tenant',
  CONDITION_SELF = 'self',
  CONDITION_SELFSUB = 'selfsub',
  CONDITION_SUB = 'sub',
  OP_TYPE_RECYCLE = 'recycle',
  OP_TYPE_ACTICE = 'active',
  OP_TYPE_DEACTIVE = 'deactive',
}
