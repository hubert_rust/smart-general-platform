export interface DictModel {
  dictType: string;
  dataName: string;
  dataKey: string;
  dataParent: string;
  dataValue: string;
  dataOrder: number;
  dataExt: string;
  status: number;
  remark: string;
}
