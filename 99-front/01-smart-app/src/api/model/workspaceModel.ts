export interface listWksResult {
  list: Array<WorkspaceModel>;
}
export interface WorkspaceModel {
  name: string;
  domain?: string;
  tidType?: string;
  remark?: string;
  createBy?: string;
  updateBy?: string;
  createTime?: Date;
  updateTime?: Date;
}
