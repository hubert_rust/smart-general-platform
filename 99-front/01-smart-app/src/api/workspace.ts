import { listWksResult, WorkspaceModel } from '@/api/model/workspaceModel';
import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  WksInsert: `${prefix}/wks/insert`,
  WksDetail: `${prefix}/wks/detail`,
  WksUpdate: `${prefix}/wks/update`,
  WksDelete: `${prefix}/wks/delete`,
  WksSwitch: `${prefix}/wks/switch`,
  WksAdminList: `${prefix}/wks/adminList`,
  WksList: `${prefix}/wks/listRealm`,
  WksAddAdmin: `${prefix}/wks/addSpaceAdmin`,
};

export function wksInsert(data: WorkspaceModel) {
  return request.post<WorkspaceModel>({
    url: Api.WksInsert,
    data,
  });
}
export function wksUpdate(data: any) {
  return request.post({
    url: Api.WksUpdate,
    data,
  });
}
export function wksDetail(id: string) {
  return request.post({
    url: `${Api.WksDetail}/${id}`,
  });
}
export function wksDelete(id: string) {
  return request.post({
    url: `${Api.WksDelete}/${id}`,
    data: {},
  });
}
export function wksSwitch(id: string) {
  return request.post({
    url: `${Api.WksSwitch}/${id}`,
    data: {},
  });
}
export function adminList() {
  return request.post({
    url: Api.WksAdminList,
  });
}
export function wksAddAdmin(data: any) {
  return request.post({
    url: Api.WksAddAdmin,
    data,
  });
}
export function wksList(data: any) {
  return request.post({
    url: Api.WksList,
    data,
  });
}
