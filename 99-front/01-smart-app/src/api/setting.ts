import { EnvModel } from '@/api/model/config/envModel';
import { RegModel } from '@/api/model/regModel';
import { RegOperationModel } from '@/api/model/RegOperationModel';
import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  RegCreate: `${prefix}/reg/create`,
  RegResetPwd: `${prefix}/reg/resetPwd`,
  RegActive: `${prefix}/reg/active`,
  RegDeactive: `${prefix}/reg/deactive`,
  RegActiveTenant: `${prefix}/reg/activeTenant`,
  RegDeactiveTenant: `${prefix}/reg/deactiveTenant`,
  updateBaseInfo: `${prefix}/reg/updateBaseInfo`,
  RegRecycle: `${prefix}/reg/recycle`,
  RegList: `${prefix}/reg/list`,
  RegDetail: `${prefix}/reg/detail`,

  EnvInsert: `${prefix}/env/insert`,
  EnvDelete: `${prefix}/env/delete`,
  EnvUpdate: `${prefix}/env/update`,
  EnvList: `${prefix}/env/list`,
};

// 环境变量
export function envInsert(data: EnvModel) {
  return request.post({
    url: Api.EnvInsert,
    data,
  });
}
export function envUpdate(data: EnvModel) {
  return request.post({
    url: Api.EnvUpdate,
    data,
  });
}
export function envDelete(id: string) {
  return request.post({
    url: `${Api.EnvDelete}/${id}`,
    data: {},
  });
}
export function envList(data: any) {
  return request.post({
    url: `${Api.EnvList}`,
    data,
  });
}

// 注册用户
export function createReg(data: RegModel) {
  return request.post({
    url: Api.RegCreate,
    data,
  });
}

export function resetPwdReg(data: RegOperationModel, param: string) {
  return request.post({
    url: Api.RegResetPwd + param,
    data,
  });
}
export function updateBaseInfo(data: any) {
  return request.post({
    url: Api.updateBaseInfo,
    data,
  });
}
export function activeReg(data: RegOperationModel) {
  return request.post({
    url: Api.RegActive,
    data,
  });
}

export function deactiveReg(data: RegOperationModel) {
  return request.post({
    url: Api.RegDeactive,
    data,
  });
}
export function activeRegTenant(data: RegOperationModel) {
  return request.post({
    url: Api.RegActiveTenant,
    data,
  });
}

export function deactiveRegTenant(data: RegOperationModel) {
  return request.post({
    url: Api.RegDeactiveTenant,
    data,
  });
}
export function recyle(data: RegOperationModel) {
  return request.post({
    url: Api.RegRecycle,
    data,
  });
}

export function list(data: any) {
  return request.post({
    url: Api.RegList,
    data,
  });
}
export function regDetail(id: string) {
  return request.post({
    url: `${Api.RegDetail}/${id}`,
  });
}
