import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  PostInsert: `${prefix}/post/insert`,
  PostDelete: `${prefix}/post/delete`,
  PostUpdate: `${prefix}/post/update`,
  PostDetail: `${prefix}/post/detail`,
  PostList: `${prefix}/post/list`,
};

// 环境变
export function postDetail(id: string) {
  return request.post({
    url: `${Api.PostDetail}/${id}`,
  });
}
export function postInsert(data: any) {
  return request.post({
    url: Api.PostInsert,
    data,
  });
}
export function postUpdate(data: any) {
  return request.post({
    url: Api.PostUpdate,
    data,
  });
}
export function postDelete(id: string) {
  return request.post({
    url: `${Api.PostDelete}/${id}`,
    data: {},
  });
}
export function postList(data: any) {
  return request.post({
    url: `${Api.PostList}`,
    data,
  });
}
