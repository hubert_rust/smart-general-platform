import { DictModel } from '@/api/model/dictionary/dictModel';
import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  DictInsert: `${prefix}/dict/insert`,
  DictDelete: `${prefix}/dict/delete`,
  DictUpdate: `${prefix}/dict/update`,
  DictList: `${prefix}/dict/list`,
  DictListByDictType: `${prefix}/dict/listByDictType`,
};

// 环境变量
export function dictInsert(data: DictModel) {
  return request.post({
    url: Api.DictInsert,
    data,
  });
}
export function dictUpdate(data: DictModel) {
  return request.post({
    url: Api.DictUpdate,
    data,
  });
}
export function dictDelete(id: string) {
  return request.post({
    url: `${Api.DictDelete}/${id}`,
    data: {},
  });
}
export function dictList(data: any) {
  return request.post({
    url: `${Api.DictList}`,
    data,
  });
}
export function dictListByType(data: any) {
  return request.post({
    url: `${Api.DictListByDictType}`,
    data,
  });
}
