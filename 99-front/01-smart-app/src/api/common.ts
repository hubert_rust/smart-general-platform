import { request } from '@/utils/request';

export async function session(data: any) {
  return request.post({
    url: '/api/open/identity/session',
    data,
  });
}
export async function exchange(sessionId: string | undefined, confidential: string) {
  let data = {sessionId: sessionId, confidential: confidential}
  return request.post({
    url: '/api/open/identity/exchange',
    data,
  });
}
export async function getSocialList() {
  return request.get({
    url: '/api/open/identity/sources',
  });
}
