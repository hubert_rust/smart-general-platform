import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  RoleInsert: `${prefix}/role/insert`,
  RoleDelete: `${prefix}/role/delete`,
  RoleUpdate: `${prefix}/role/update`,
  RoleUpdateExt: `${prefix}/role/updateExt`,
  RoleDetail: `${prefix}/role/detail`,
  RoleActive: `${prefix}/role/active`,
  RoleDeactive: `${prefix}/role/deactive`,
  RoleList: `${prefix}/role/list`,
  RoleAppList: `${prefix}/role/apps`,
  RoleMenuList: `${prefix}/role/appMenus`,
  RoleMenus: `${prefix}/role/menus`,
  RoleApiList: `${prefix}/role/appApis`,
  RoleSubjectList: `${prefix}/role/subjectList`,
  RoleAddSubject: `${prefix}/role/addSubject`,
  RoleDeleteSubject: `${prefix}/role/deleteSubject`,

  RoleDataScope: `${prefix}/role/dataScope`,
  RoleCloseDataScope: `${prefix}/role/closeDataScope`,

  // 角色授权菜单 api
  RoleSaveMenus: `${prefix}/role/saveMenus`,
  RoleAddApis: `${prefix}/role/addApis`,
  RoleDeleteMenus: `${prefix}/role/deleteMenus`,
  RoleDeleteApis: `${prefix}/role/deleteApis`,
  RoleGrantMenus: `${prefix}/role/grantMenus`,
  RoleGrantApis: `${prefix}/role/grantApis`,

  // 自定义数据权限
  RoleCustomUpdateOrg: `${prefix}/role/customUpdateOrg`,
  RoleCustomAddUser: `${prefix}/role/customAddUser`,
  RoleCustomDeleteUser: `${prefix}/role/customDeleteUser`,
  RoleCustomOrgList: `${prefix}/role/customOrgList`,
  RoleCustomUserList: `${prefix}/role/customUserList`,
};

export function roleCustomUserList(roleId: string, query: any) {
  return request.post({
    url: `${Api.RoleCustomUserList}/${roleId}`,
    data: query,
  });
}
export function roleCustomOrgList(data: any) {
  return request.post({
    url: `${Api.RoleCustomOrgList}`,
    data,
  });
}
export function roleCustomDeleteUser(data: any) {
  return request.post({
    url: `${Api.RoleCustomDeleteUser}`,
    data,
  });
}
export function roleCustomAddUser(data: any) {
  return request.post({
    url: `${Api.RoleCustomAddUser}`,
    data,
  });
}
export function roleCustomUpdateOrg(data: any) {
  return request.post({
    url: `${Api.RoleCustomUpdateOrg}`,
    data,
  });
}

export function roleGrantMenus(roleId: string) {
  return request.post({
    url: `${Api.RoleGrantMenus}/${roleId}`,
  });
}

export function roleGrantApis(roleId: string) {
  return request.post({
    url: `${Api.RoleGrantApis}/${roleId}`,
  });
}

export function roleDeleteMenus(data: any) {
  return request.post({
    url: Api.RoleDeleteMenus,
    data,
  });
}

export function roleDeleteApis(data: any) {
  return request.post({
    url: Api.RoleDeleteApis,
    data,
  });
}

export function roleSaveMenus(data: any) {
  return request.post({
    url: Api.RoleSaveMenus,
    data,
  });
}

export function roleAddApis(data: any) {
  return request.post({
    url: Api.RoleAddApis,
    data,
  });
}

export function roleDataScope(data: any) {
  return request.post({
    url: Api.RoleDataScope,
    data,
  });
}

// 关闭数据权限
export function roleCloseDataScope(data: any) {
  return request.post({
    url: Api.RoleDataScope,
    data,
  });
}

// 环境变
export function roleDetail(id: string) {
  return request.post({
    url: `${Api.RoleDetail}/${id}`,
  });
}

export function roleActive(id: string) {
  return request.post({
    url: `${Api.RoleActive}/${id}`,
  });
}

export function roleDeactive(id: string) {
  return request.post({
    url: `${Api.RoleDeactive}/${id}`,
  });
}

export function roleInsert(data: any) {
  return request.post({
    url: Api.RoleInsert,
    data,
  });
}

export function roleUpdate(data: any) {
  return request.post({
    url: Api.RoleUpdate,
    data,
  });
}

export function roleUpdateExt(data: any) {
  return request.post({
    url: Api.RoleUpdateExt,
    data,
  });
}

export function roleDelete(id: string) {
  return request.post({
    url: `${Api.RoleDelete}/${id}`,
    data: {},
  });
}

export function roleList(data: any) {
  return request.post({
    url: `${Api.RoleList}`,
    data,
  });
}

export function roleApps() {
  return request.post({
    url: `${Api.RoleAppList}`,
  });
}

export function roleMenuList(id: string) {
  return request.post({
    url: `${Api.RoleMenuList}/${id}`,
    data: {},
  });
}
export function roleMenus(data: any) {
  return request.post({
    url: `${Api.RoleMenus}`,
    data,
  });
}
export function roleApiList(data: any) {
  return request.post({
    url: `${Api.RoleApiList}`,
    data,
  });
}

export function roleSubjectList(data: any) {
  return request.post({
    url: `${Api.RoleSubjectList}`,
    data,
  });
}

export function roleAddSubject(data: any) {
  return request.post({
    url: `${Api.RoleAddSubject}`,
    data,
  });
}

export function roleDeleteSubject(data: any) {
  return request.post({
    url: `${Api.RoleDeleteSubject}`,
    data,
  });
}
