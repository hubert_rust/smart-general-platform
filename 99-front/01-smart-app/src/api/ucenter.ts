import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  UserAccessLogList: `${prefix}/ucenter/userAccessList`,
  UserEdit: `${prefix}/ucenter/userEdit`,
  UserCheck: `${prefix}/ucenter/check`,
  UserChange: `${prefix}/ucenter/change`,
  UserAuth: `${prefix}/ucenter/userAuth`,
};

export function userAccessLogList(data: any) {
  return request.post({
    url: `${Api.UserAccessLogList}`,
    data,
  });
}
export function userCenterEdit(data: any) {
  return request.post({
    url: `${Api.UserEdit}`,
    data,
  });
}
export function userCenterCheck(data: any) {
  return request.post({
    url: `${Api.UserCheck}`,
    data,
  });
}
export function userCenterChange(data: any) {
  return request.post({
    url: `${Api.UserChange}`,
    data,
  });
}
export function userCenterAuth() {
  return request.post({
    url: `${Api.UserAuth}`,
  });
}