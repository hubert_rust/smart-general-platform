import { request } from '@/utils/request';

const Api = {
  AppList: '/api/appsso/list',
  AppListMarket: '/api/appsso/listMarket',
  AppAddSso: '/api/appsso/addsso',
  AppDeleteSso: '/api/appsso/deletesso',
  AppSsoDetail: '/api/appsso/ssoDetail',

  SsoApiListURL: '/api/appsso/apiListOfApp',

  // access
  ListAppEmpower: '/api/application/listEmpower',

  // 所有用户可见，不可见
  AllUserEnable: '/api/appsso/allUserEnable',
  AllUserDisable: '/api/appsso/allUserDisable',
};

export function appSsoDetail(clientId: string) {
  return request.post({
    url: `${Api.AppSsoDetail}/${clientId}`,
  });
}

export function allUserDisable(clientId: any) {
  return request.post({
    url: `${Api.AllUserDisable}/${clientId}`,
  });
}

export function allUserEnable(clientId: any) {
  return request.post({
    url: `${Api.AllUserEnable}/${clientId}`,
  });
}

export function ssoApiList(id: any) {
  return request.post({
    url: `${Api.SsoApiListURL}/${id}`,
  });
}
export function queryAppList(data: any) {
  return request.post({
    url: `${Api.AppList}`,
    data,
  });
}
export function queryAppMarketList(data: any) {
  return request.post({
    url: `${Api.AppListMarket}`,
    data,
  });
}
export function appAddSso(clientId: string) {
  return request.post({
    url: `${Api.AppAddSso}/${clientId}`,
  });
}

export function appDeleteSso(clientId: string) {
  return request.post({
    url: `${Api.AppDeleteSso}/${clientId}`,
  });
}
