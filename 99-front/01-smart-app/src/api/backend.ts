import { CommonConfig } from "@/config/global";
import { request } from "@/utils/request";

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  BackendInsert: `${prefix}/backend/insert`,
  BackendDelete: `${prefix}/backend/delete`,
  BackendUpdate: `${prefix}/backend/update`,
  BackendList: `${prefix}/backend/list`,
};

export function backendInsert(data: any) {
  return request.post({
    url: Api.BackendInsert,
    data,
  });
}
export function backendUpdate(data: any) {
  return request.post({
    url: Api.BackendUpdate,
    data,
  });
}
export function backendDelete(id: string) {
  return request.post({
    url: `${Api.BackendDelete}/${id}`,
    data: {},
  });
}
export function backendList(data: any) {
  return request.post({
    url: `${Api.BackendList}`,
    data,
  });
}
