import { request } from '@/utils/request';

const Api = {
  InsertApp: '/api/application/insert',
  UpdateApp: '/api/application/update',
  UpdateAppAuth: '/api/application/updateAuth',
  DeleteApp: '/api/application/delete',
  DetailApp: '/api/application/detail',
  AppList: '/api/application/list',

  SyncToApp: '/api/application/sync',
  SyncApiToApp: '/api/application/syncApi',
  ForceSyncApiToApp: '/api/application/forceSyncApi',

  ActiveApp: '/api/application/activeApp',
  DeactiveApp: '/api/application/deactiveApp',
  ResApiList: '/api/application/resApiList',
  ApiListBackend: '/api/application/backendListOfapp',

  RemoteLoadApi: '/api/application/remoteLoad',

  ProcessTypeSet: '/api/application/processTypeOfapp',
  ReleaseAPI: '/api/application/releaseApi',
  CancelAPI: '/api/application/cancelApi',
  SaveAPI: '/api/application/saveApi',
  UpdateAPI: '/api/application/updateApi',
  DeleteAPI: '/api/application/deleteApi',
  CheckRes: '/api/application/checkRes',
  LinkApi: '/api/application/menuLinkAPi',
  AppShare: '/api/application/share',
  AppUnShare: '/api/application/unshare',

  // access
  ListAppEmpower: '/api/application/listEmpower',
  AddAppEmpower: '/api/application/addEmpower',
  DeleteAppEmpower: '/api/application/deleteEmpower',

  // 菜单绑定接口
  MenuBindApi: '/api/application/menuLinkApi',
  MenuUnbindApi: '/api/application/menuUnlinkApi',

  // 所有用户可见，不可见
  AllUserEnable: '/api/application/allUserEnable',
  AllUserDisable: '/api/application/allUserDisable',
};

export function allUserEnable(clientId: any) {
  return request.post({
    url: `${Api.AllUserEnable}/${clientId}`,
  });
}
export function allUserDisable(clientId: any) {
  return request.post({
    url: `${Api.AllUserDisable}/${clientId}`,
  });
}
export function menuUnbindOnApi(data: any) {
  return request.post({
    url: `${Api.MenuUnbindApi}`,
    data,
  });
}
export function menuBindOnApi(data: any) {
  return request.post({
    url: `${Api.MenuBindApi}`,
    data,
  });
}
export function queryAppEmpower(data: any) {
  return request.post({
    url: `${Api.ListAppEmpower}`,
    data,
  });
}
export function insertAppEmpower(data: any) {
  return request.post({
    url: `${Api.AddAppEmpower}`,
    data,
  });
}
export function removeAppEmpower(data: any) {
  return request.post({
    url: `${Api.DeleteAppEmpower}`,
    data,
  });
}
export function saveApp(data: any) {
  return request.post({
    url: `${Api.InsertApp}`,
    data,
  });
}
export function updateApp(data: any) {
  return request.post({
    url: `${Api.UpdateApp}`,
    data,
  });
}
export function updateAppAuth(data: any) {
  return request.post({
    url: `${Api.UpdateAppAuth}`,
    data,
  });
}
export function unshareApp(id: string) {
  return request.post({
    url: `${Api.AppUnShare}/${id}`,
  });
}
export function shareApp(id: string) {
  return request.post({
    url: `${Api.AppShare}/${id}`,
  });
}
export function detailApp(id: string) {
  return request.post({
    url: `${Api.DetailApp}/${id}`,
  });
}
export function deleteApp(id: string) {
  return request.post({
    url: `${Api.DeleteApp}/${id}`,
  });
}
export function sync2App(client: string, data: any) {
  return request.post({
    url: `${Api.SyncToApp}/${client}`,
    data,
  });
}

export function syncApi2App(client: string, data: any) {
  return request.post({
    url: `${Api.SyncApiToApp}/${client}`,
    data,
  });
}
export function forceSyncApi2App(client: string, data: any) {
  return request.post({
    url: `${Api.ForceSyncApiToApp}/${client}`,
    data,
  });
}
export function appRemoteLoad(client: string) {
  return request.post({
    url: `${Api.RemoteLoadApi}/${client}`,
  });
}

export function activeApp(id: string) {
  return request.post({
    url: `${Api.ActiveApp}/${id}`,
  });
}
export function deactiveApp(id: string) {
  return request.post({
    url: `${Api.DeactiveApp}/${id}`,
  });
}

export function applicationList() {
  return request.post({
    url: `${Api.AppList}`,
  });
}

// 后台对应的接口
export function operationListOfApp(client: string) {
  return request.post({
    url: `${Api.ApiListBackend}/${client}`,
  });
}
// 应用对应的接口
export function apiListApp(client: string) {
  return request.post({
    url: `${Api.ResApiList}/${client}`,
  });
}

export function processTypeOfOp(data: any) {
  return request.post({
    url: `${Api.ProcessTypeSet}`,
    data,
  });
}
export function releaseApi(id: string) {
  return request.post({
    url: `${Api.ReleaseAPI}/${id}`,
  });
}
export function cancelApi(id: string) {
  return request.post({
    url: `${Api.CancelAPI}/${id}`,
  });
}
export function saveApi(data: any) {
  return request.post({
    url: `${Api.SaveAPI}`,
    data,
  });
}
export function updateApi(data: any) {
  return request.post({
    url: `${Api.UpdateAPI}`,
    data,
  });
}

export function deleteApi(id: string) {
  return request.post({
    url: `${Api.DeleteAPI}/${id}`,
  });
}

export function checkRes(client: string) {
  return request.post({
    url: `${Api.CheckRes}/${client}`,
  });
}
