import { request } from '@/utils/request';

const Api = {

  MenuList: '/api/application/menuListOfapp',
  SaveMenu: '/api/application/saveMenu',
  UpdateMenu: '/api/application/updateMenu',
  DeleteMenu: '/api/application/deleteMenu',

  MenuListSSO: '/api/appsso/menuListOfapp',
  MenuApi: '/api/appsso/menuApi',
};

export function menuListOfApp(client: string) {
  return request.post({
    url: `${Api.MenuList}/${client}`,
  });
}


// sso 应用对应的菜单列表
export function menuListOfSsoApp(client: string) {
  return request.post({
    url: `${Api.MenuListSSO}/${client}`,
  });
}
export function menuOfApi(data: any) {
  return request.post({
    url: `${Api.MenuApi}`,
    data,
  });
}
export function saveMenu(data: any) {
  return request.post({
    url: `${Api.SaveMenu}`,
    data,
  });
}
export function updateMenu(data: any) {
  return request.post({
    url: `${Api.UpdateMenu}`,
    data,
  });
}

export function deleteMenu(id: string) {
  return request.post({
    url: `${Api.DeleteMenu}/${id}`,
  });
}
