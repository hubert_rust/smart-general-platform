//export const prefix = 'tdesign-starter';
//export const TOKEN_NAME = 'tdesign-starter';

class CommonConfig {
  public static APP_NAME = '云平台';

  public static HEADER_APP_TYPE = 'Apt';
  public static HEADER_APP_TYPE_VAL = 'mgt';
  public static COOKIE_KEY = 'SMART_TOKEN';
  public static COOKIE_TOKEN = 'Smart-Token';

  public static HEADER_WKS = 'X-Smart-User-Realm';
  public static HEADER_DEVICE_TYPE = 'Device-Type';
  public static HEADER_DEVICE_VAL = 'Desktop';
  // 网关地址
  public static GATEWAPY_URL = 'http://192.168.1.200:8847';

  // 验证码类型
  public static CAPTCHA_TYPE = 'HUTOOL_LINE';

  // 是否开启自动刷新Token机制，true表示开启，false表示不开启
  public static AUTO_REFRESH_TOKEN = true;

  // 使用OpenID connect(OIDC) 协议
  public static USE_OIDC = true;
  public static USE_CRYPTO = true;

  public static URL_PREFIX = '/api';

  public static AUTH_ADDRESS = 'http://auth.abc.com:7070/sso/doLogin';
  // public static AUTH_ADDRESS = 'http://192.168.137.248:7070/sso/doLogin';
  public static ADMIN_CLIENT_ID = '67601992f3574c75809b';
  public static ADMIN_HOME_PAGE = 'http://app.abc.com:3004/dashboard/base';
  public static AUTH_CHECK_TICKET = 'http://auth.abc.com:7070/sso/checkTicket';
  public static AUTH_LOGOUT_URL = 'http://auth.abc.com:7070/sso/signout';
  public static AUTH_CENTER_URL = 'http://auth.abc.com:7070/sso/auth';
  public static OAUTH2_CLIENT_ID = '14a9cf797931430896ad13a6b1855611';

  //SM4加密密钥
  public static OAUTH2_CLIENT_SECRET = 'a05fe1fc50ed42a4990c6c6fc4bec398';

  public static OAUTH2_REDIRECT_URI = 'http://192.168.1.100:3003/authorizationCode';
  public static OAUTH2_TOKEN_ADDR = '/api/oauth2/token';
  public static OAUTH2_REVOKE_ADDR = '/api/oauth2/revoke';
  public static OAUTH2_AUTHORIZATION_CODE_ADDR = '/api/oauth2/authorize';
  public static OAUTH2_LOGIN_SUCESS_REDIRECT_URI = 'http:///192.168.1.100:3003/dashboard';

  public static ROUTE_PATH_LOGIN = '/login';
  public static ROUTE_PATH_HOME = '/dashboard';
  private static INST = new CommonConfig();

  public static getInstance(): CommonConfig {
    return this.INST;
  }
}

export { CommonConfig };
export const APP_NAME = '职培加智慧云平台'; // 项目/应用目名
export const APP_CODE = 'frame'; // 应用code
export const APP_CLIENT_TYPE = ''; // 终端类型-目前用于区分菜单
export const CLIENT_ID = '1029333'; // 头部使用
export const APP_ID = '683000'; // APPID暂时没用
export const APP_RELACE = 'app'; // 部署方式：self-独立使用  app-跟平台一起

export const AES_KEY = 'Y23B56789AHAEQ0Z'; // 加密key
export const AES_IV = 'DYgjCEIMVrj2W9xN'; // 加密iv

export const prefix = 'tdesign-starter';
export const baseUrl = '';
export const storage = 'localStorage';

export const TOKEN_NAME = 'p2';
