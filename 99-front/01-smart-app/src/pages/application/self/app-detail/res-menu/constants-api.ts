import {FormRule, PrimaryTableCol, TableRowData} from 'tdesign-vue-next';

export const api_columns: PrimaryTableCol<TableRowData>[] = [
  /* {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    checkProps: ({ row }) => ({ disabled: false }),
    // 自由调整宽度，如果发现元素看不见，请加大宽度
    width: 50,
    align: 'left',
  }, */
  { width: 130, colKey: 'operationName', title: '接口名', ellipsis: true, align: 'left' },
  { width: 60, colKey: 'requestMode', title: '方法', ellipsis: true, align: 'left' },
  { width: 130, colKey: 'requestUrl', title: '端点', ellipsis: true, align: 'left' },
  { width: 100, colKey: 'processType', title: '访问方式', ellipsis: true, align: 'left' },
  /*{ width: 50, colKey: 'release', title: '状态', ellipsis: true, align: 'left' },*/
  { colKey: 'op', width: 50, title: '操作', align: 'right' },
];

export const menu_columns: PrimaryTableCol<TableRowData>[] = [
  {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    checkProps: ({ row }) => ({ disabled: false }),
    // 自由调整宽度，如果发现元素看不见，请加大宽度
    width: 50,
    align: 'left',
  },
  { width: 180, colKey: 'voTitle', title: '菜单名称', ellipsis: true, align: 'left' },
  { width: 80, colKey: 'voIcon', title: '图标', ellipsis: true, align: 'left' },
  { width: 80, colKey: 'voOrder', title: '顺序', ellipsis: true, align: 'left' },
  { width: 80, colKey: 'resType', title: '类型', align: 'left' },
  { width: 100, colKey: 'linkApi', title: '绑定接口', align: 'left' },
  { width: 260, colKey: 'apiDesc', title: '接口说明', align: 'left' },
  { colKey: 'op', width: 170, title: '操作', align: 'right' },
];

export const FORM_API_RULES: Record<string, FormRule[]> = {
  operationName: [{ required: true, message: '请输入接口名称', type: 'error' }],
  operationType: [{ required: true, message: '请输入类型', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

//smart-auth: pending: appInnerName;
export const INITIAL_API_DATA = {
  id: '',
  appInnerName: 'smart-cloud-upms',
  operationName: '',
  operationType: '',
  parentId: '-1',
  requestUrl: '',
  requestMode: '',
  processType: 'control',
  release: 0,
  remark: '',
  children: [] as Array<any>,
  childrenNode: [] as Array<any>,
  row: {} as any,
};
export const RES_TYPE_OPTIONS = [
  { label: '目录', value: 'catalog' },
  { label: '接口', value: 'api' },
];
export const PROCESS_OPTIONS = [
  { label: '访问控制', value: 'control' },
  { label: '匿名访问', value: 'anonymous' },
  { label: '登录访问', value: 'login' },
];
export const METHOD_OPTONS = [
  { label: 'POST', value: 'POST' },
  { label: 'GET', value: 'GET' },
  { label: 'PUT', value: 'PUT' },
  { label: 'DELETE', value: 'DELETE' },

];
