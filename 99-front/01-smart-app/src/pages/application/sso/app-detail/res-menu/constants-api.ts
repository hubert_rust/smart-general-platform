import { PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const api_columns: PrimaryTableCol<TableRowData>[] = [
  /* {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    checkProps: ({ row }) => ({ disabled: false }),
    // 自由调整宽度，如果发现元素看不见，请加大宽度
    width: 50,
    align: 'left',
  }, */
  { width: 120, colKey: 'operationName', title: '接口名', ellipsis: true, align: 'left' },
  { width: 60, colKey: 'requestMode', title: '方法', ellipsis: true, align: 'left' },
  { width: 120, colKey: 'requestUrl', title: '端点', ellipsis: true, align: 'left' },
  { width: 120, colKey: 'processType', title: '访问方式', ellipsis: true, align: 'left' },
  /* { width: 50, colKey: 'release', title: '状态', ellipsis: true, align: 'left' }, */
  { colKey: 'op', width: 80, title: '操作', align: 'right' },
];

// smart-auth: pending: appInnerName;
export const INITIAL_API_DATA = {
  id: '',
  appInnerName: 'smart-cloud-upms',
  operationName: '',
  operationType: '',
  parentId: '-1',
  requestUrl: '',
  requestMode: '',
  processType: 'control',
  release: 0,
  remark: '',
  row: {} as any,
};
