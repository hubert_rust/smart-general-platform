import { FormRule } from 'tdesign-vue-next';
export const FORM_RULES: Record<string, FormRule[]> = {
  appTag: [{ required: true, message: '请输入用户空间名称', type: 'error' }],
  appType: [{ required: true, message: '请输入用户空间名称', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  appTag: '',
  clientId: '',
  appInnerName: '',
  logo: '',
  appType: 'user',
  remark: '',
};
