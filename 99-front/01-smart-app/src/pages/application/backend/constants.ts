import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '后台名称',
    align: 'left',
    width: 120,
    colKey: 'backendName',
    fixed: 'left',
  },
  {
    title: '内部名称',
    width: 120,
    ellipsis: true,
    colKey: 'appInnerName',
  },

  {
    title: '后台地址',
    width: 80,
    ellipsis: true,
    colKey: 'backendAddr',
  },
  {
    title: '状态',
    width: 80,
    ellipsis: true,
    colKey: 'status',
  },
  {
    title: '说明',
    width: 120,
    ellipsis: true,
    colKey: 'remark',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  backendName: [{ required: true, message: '请输入键值', type: 'error' }],
  appInnerName: [{ required: true, message: '请输入值', type: 'error' }],
  backendAddr: [{ required: true, message: '请输入值', type: 'error' }],

  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  backendName: '',
  backendAddr:'',
  appInnerName: '',
  contextPath: '',
  status: 'active',
  remark: '',
  id: '',
};
