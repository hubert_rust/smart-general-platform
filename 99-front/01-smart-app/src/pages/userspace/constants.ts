import {FormRule} from "tdesign-vue-next";

export interface UserInfoListType {
  title: string;
  content: string;
  span?: number;
}

export const FORM_RULES: Record<string, FormRule[]> = {
  workspaceName: [{ required: true, message: '请输入用户空间名称', type: 'error' }],
};