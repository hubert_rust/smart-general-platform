import { PrimaryTableCol, TableRowData } from 'tdesign-vue-next';
import { FormRule } from 'tdesign-vue-next';

export const MAIN_MENU_OPTION = [{ content: '添加组织', value: 1 }];
export const USER_ACTION_OPTION = [
  { content: '删除', value: 1 },
];
export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
  {
    title: '姓名',
    align: 'left',
    width: 120,
    colKey: 'realName',
    fixed: 'left',
  },
  {
    title: '账号',
    width: 120,
    ellipsis: true,
    colKey: 'account',
  },

];


export const FORM_RULES: Record<string, FormRule[]> = {
  postName: [{ required: true, message: '请输岗位名称', type: 'error' }],
  postCode: [{ required: true, message: '请输岗位编码', type: 'error' }],

  //suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};


export const TREE_DATA = [
  {
    label: '深圳总部',
    value: 0,
    children: [
      {
        label: '总办',
        value: '0-0',
      },
      {
        label: '市场部',
        value: '0-1',
        children: [
          {
            label: '采购1组',
            value: '0-1-0',
          },
          {
            label: '采购2组',
            value: '0-1-1',
          },
        ],
      },
      {
        label: '技术部',
        value: '0-2',
      },
    ],
  },
  {
    label: '北京总部',
    value: 1,
    children: [
      {
        label: '总办',
        value: '1-0',
      },
      {
        label: '市场部',
        value: '1-1',
        children: [
          {
            label: '采购1组',
            value: '1-1-0',
          },
          {
            label: '采购2组',
            value: '1-1-1',
          },
        ],
      },
    ],
  },
  {
    label: '上海总部',
    value: 2,
    children: [
      {
        label: '市场部',
        value: '2-0',
      },
      {
        label: '财务部',
        value: '2-1',
        children: [
          {
            label: '财务1组',
            value: '2-1-0',
          },
          {
            label: '财务2组',
            value: '2-1-1',
          },
        ],
      },
    ],
  },
  {
    label: '湖南',
    value: 3,
  },
  {
    label: '湖北',
    value: 4,
  },
];

export const IMPORT_OPTIONS = [
  { content: '通过Excel导入', value: 1 },
  { content: '通过脚本导入', value: 2 },
  { content: '导入历史', value: 3 },
];
export const EXPORT_OPTIONS = [
  { content: '导出全部用户', value: 1 },
  { content: '导出历史', value: 2 },
];
