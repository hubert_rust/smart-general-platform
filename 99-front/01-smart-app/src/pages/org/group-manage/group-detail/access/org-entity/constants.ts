import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const MAIN_MENU_OPTION = [{ content: '添加组织', value: 1 }];
export const USER_ACTION_OPTION = [
  { content: '禁用账号', value: 1 },
  { content: '删除成员', value: 2 },
  { content: '变更部门', value: 3 },
  { content: '设置岗位', value: 4 },
  { content: '设为主部门', value: 5 },
  { content: '设为部门负责人', value: 6 },
];
export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '合同名称',
    fixed: 'left',
    width: 180,
    ellipsis: true,
    align: 'left',
    colKey: 'name',
  },

  {
    title: '用户ID',
    width: 120,
    ellipsis: true,
    colKey: 'id',
  },
  {
    title: '电话',
    width: 120,
    ellipsis: true,
    colKey: 'phone',
  },
  {
    title: '邮箱',
    width: 80,
    ellipsis: true,
    colKey: 'email',
  },
  {
    title: '管理员类型',
    width: 80,
    ellipsis: true,
    colKey: 'email',
  },
  {
    title: '最后一次登录时间',
    width: 120,
    ellipsis: true,
    colKey: 'lastLoginTime',
  },
  {
    fixed: 'right',
    align: 'right',
    width: 120,
    colKey: 'op',
    title: '操作',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  key: [{ required: true, message: '请输入键值', type: 'error' }],
  value: [{ required: true, message: '请输入值', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};

export const TREE_DATA = [
  {
    label: '深圳总部',
    value: 0,
    children: [
      {
        label: '总办',
        value: '0-0',
      },
      {
        label: '市场部',
        value: '0-1',
        children: [
          {
            label: '采购1组',
            value: '0-1-0',
          },
          {
            label: '采购2组',
            value: '0-1-1',
          },
        ],
      },
      {
        label: '技术部',
        value: '0-2',
      },
    ],
  },
  {
    label: '北京总部',
    value: 1,
    children: [
      {
        label: '总办',
        value: '1-0',
      },
      {
        label: '市场部',
        value: '1-1',
        children: [
          {
            label: '采购1组',
            value: '1-1-0',
          },
          {
            label: '采购2组',
            value: '1-1-1',
          },
        ],
      },
    ],
  },
  {
    label: '上海总部',
    value: 2,
    children: [
      {
        label: '市场部',
        value: '2-0',
      },
      {
        label: '财务部',
        value: '2-1',
        children: [
          {
            label: '财务1组',
            value: '2-1-0',
          },
          {
            label: '财务2组',
            value: '2-1-1',
          },
        ],
      },
    ],
  },
  {
    label: '湖南',
    value: 3,
  },
  {
    label: '湖北',
    value: 4,
  },
];
