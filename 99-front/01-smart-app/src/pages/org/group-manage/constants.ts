import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '用户组名称',
    align: 'left',
    width: 120,
    colKey: 'groupName',
    fixed: 'left',
  },
  {
    title: '唯一标识',
    width: 120,
    ellipsis: true,
    colKey: 'groupTag',
  },

  {
    title: '用户组类型',
    width: 80,
    ellipsis: true,
    colKey: 'groupType',
  },
  {
    title: '说明',
    width: 120,
    ellipsis: true,
    colKey: 'remark',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  groupName: [{ required: true, message: '请输入用户组名称', type: 'error' }],
  groupTag: [{ required: true, message: '请输入用户组唯一标识', type: 'error' }],
};

export const INITIAL_DATA = {
  groupName: '',
  groupTag: '',
  groupType: '',
  remark: '',
  id: '',
};
export const ACTION_OPTION = [{ content: '删除', value: 'groupDelete' }];
