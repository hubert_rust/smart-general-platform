import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const APP_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  {
    colKey: 'row-select',
    type: 'single',
    // 允许单选(Radio)取消行选中
    checkProps: { allowUncheck: true },
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    width: 30,
  },
  {
    title: '角色名称',
    align: 'center',
    width: 120,
    colKey: 'roleName',
    fixed: 'left',
  },
  {
    title: '应用名称',
    align: 'center',
    width: 120,
    colKey: 'appTag',
    fixed: 'left',
  },
  {
    title: '来源',
    align: 'left',
    width: 90,
    colKey: 'clientFrom',
    fixed: 'left',
  },
];
export const MENU_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  /* {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    checkProps: ({ row }: any) => ({ disabled: row.operationType !== 'api' }),
    // 自由调整宽度，如果发现元素看不见，请加大宽度
    width: 20,
    align: 'left',
  }, */
  { width: 200, colKey: 'voTitle', title: '菜单名称', ellipsis: false, align: 'left' },
  /* { width: 60, colKey: 'requestMode', title: '方法', ellipsis: true, align: 'left' },
  { width: 120, colKey: 'requestUrl', title: '端点', ellipsis: true, align: 'left' }, */
  { width: 100, colKey: 'processType', title: '访问方式', ellipsis: true, align: 'center' },
  { colKey: 'grant', width: 100, title: '授权', align: 'center' },
  { colKey: 'op', width: 100, title: '数据权限', align: 'center' },
];
export const API_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  /* {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    checkProps: ({ row }: any) => ({ disabled: row.operationType !== 'api' }),
    // 自由调整宽度，如果发现元素看不见，请加大宽度
    width: 20,
    align: 'left',
  }, */
  { width: 260, colKey: 'operationName', title: '接口名', ellipsis: false, align: 'left' },
  /* { width: 60, colKey: 'requestMode', title: '方法', ellipsis: true, align: 'left' },
  { width: 120, colKey: 'requestUrl', title: '端点', ellipsis: true, align: 'left' }, */
  { width: 80, colKey: 'processType', title: '访问方式', ellipsis: true, align: 'center' },
  { colKey: 'grant', width: 80, title: '授权', align: 'center' },
  { colKey: 'op', width: 80, title: '数据权限', align: 'center' },
];
export const ROLE_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    width: 30,
  },
  {
    title: '角色名称',
    align: 'center',
    width: 120,
    colKey: 'roleName',
    fixed: 'left',
  },
  {
    title: '说明',
    align: 'left',
    width: 120,
    colKey: 'remark',
    fixed: 'left',
  },
];
export const SUBJECT_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    disabled: ({ rowIndex }) => true,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    width: 30,
  },
  {
    title: '授权主体',
    align: 'center',
    width: 120,
    colKey: 'subjectName',
    fixed: 'left',
  },
  {
    title: '类型',
    align: 'left',
    width: 120,
    colKey: 'subjectType',
    fixed: 'left',
  },
];
