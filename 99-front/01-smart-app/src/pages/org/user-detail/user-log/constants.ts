import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '事件类型',
    align: 'left',
    width: 120,
    colKey: 'logType',
    fixed: 'left',
  },
  {
    title: '事件详情',
    width: 120,
    ellipsis: true,
    colKey: 'operationEvent',
  },
  {
    title: '用户',
    width: 120,
    ellipsis: true,
    colKey: 'account',
  },
  {
    title: 'IP',
    width: 80,
    ellipsis: true,
    colKey: 'clientIp',
  },
  {
    title: '角色',
    width: 120,
    ellipsis: true,
    colKey: 'apt',
  },
  {
    title: '时间',
    width: 150,
    ellipsis: true,
    colKey: 'createTime',
  },
  {
    title: '结果',
    align: 'center',
    fixed: 'right',
    width: 70,
    colKey: 'requestReturn',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  key: [{ required: true, message: '请输入键值', type: 'error' }],
  value: [{ required: true, message: '请输入值', type: 'error' }],

  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};
