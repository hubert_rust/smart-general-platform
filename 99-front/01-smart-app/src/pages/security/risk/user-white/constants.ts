import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';
import { computed } from 'vue';

export const IP_BLACK_OPTION = computed(() => {
  return function (data: any) {
    return [{ content: '删除', value: 'delete', row: data }];
  };
});
export const USER_ACTION_OPTION = [{ content: '删除', value: 1 }];

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: 'IP',
    align: 'left',
    width: 120,
    colKey: 'subjectName',
    fixed: 'left',
  },
  {
    title: '限制类型',
    width: 120,
    ellipsis: true,
    colKey: 'processType',
  },
  {
    title: '添加类型',
    width: 120,
    ellipsis: true,
    colKey: 'addType',
  },
  {
    title: '备注',
    width: 120,
    ellipsis: true,
    colKey: 'remark',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  // subjectValue: [{ required: true, message: '请输入键值', type: 'error' }],
  subjectValue: [
    {
      required: true,
      pattern:
        /^(?:(?:1[0-9][0-9]\.)|(?:2[0-4][0-9]\.)|(?:25[0-5]\.)|(?:[1-9][0-9]\.)|(?:[0-9]\.)){3}(?:(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5])|(?:[1-9][0-9])|(?:[0-9]))$/,
      message: '请输入正确的IP地址',
      trigger: 'blur',
      type: 'error',
    },
  ],
  processTypeList: [{ required: true, message: '请输入值', type: 'error' }],

  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  subjectName: '',
  subjectValue: '',
  processTypeList: ['skip_auth'] as Array<string>,
  processType: '',
  addType: 'hand',
  remark: '',
  id: '',
};
