import { PrimaryTableCol, TableRowData } from 'tdesign-vue-next';
import { FormRule } from 'tdesign-vue-next';
export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  //{ colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '账号',
    align: 'left',
    width: 120,
    colKey: 'account',
    fixed: 'left',
  },
  {
    title: '用户ID',
    width: 120,
    ellipsis: true,
    colKey: 'id',
  },
  {
    title: '电话',
    width: 120,
    ellipsis: true,
    colKey: 'phone',
  },
  {
    title: '邮箱',
    width: 80,
    ellipsis: true,
    colKey: 'email',
  },
  {
    title: '管理员类型',
    width: 80,
    ellipsis: true,
    colKey: 'email',
  },
  {
    title: '最后一次登录时间',
    width: 120,
    ellipsis: true,
    colKey: 'lastLoginTime',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];


export const FORM_RULES: Record<string, FormRule[]> = {
  key: [{ required: true, message: '请输入键值', type: 'error' }],
  value: [{ required: true, message: '请输入值', type: 'error' }],
  //suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};
