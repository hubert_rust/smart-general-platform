import { defineStore } from "pinia";

export const shareStore = defineStore('share', {
  state() {
    // 存放的就是模块的变量
    return {
      popApp: false,
      appColor: '#FFF',
      // 组织结构中点选择树形
      orgClickItem: {},
      count: 1,
      treeData: [],
      treeMap: {},
    };
  },
  getters: {
    // 相当于vue里面的计算属性，可以缓存数据
    lickItem() {
      return this.orgClickItem;
    },
  },
  actions: {
    setPopState(state: boolean) {
      this.popApp = state;
      if(state) {
        this.appColor = '#F3F3F3';
      }
      else {
        this.appColor = '#FFF';
      }
    },
    getPopState() {
      return this.popApp;
    },
    getAppColor() {
      return this.appColor;
    },
    // 可以通过actions 方法，改变 state 里面的值。

    setOrgClickItem(node: any) {
      this.orgClickItem = node;
    },
    getOrgClickItem() {
      return this.orgClickItem;
    }
  },
});
