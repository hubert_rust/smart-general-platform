import 'nprogress/nprogress.css'; // progress bar style

import NProgress from 'nprogress'; // progress bar
import { MessagePlugin } from 'tdesign-vue-next';
import { RouteRecordRaw } from 'vue-router';

import { CommonConfig } from '@/config/global';
import router from '@/router';
import { getPermissionStore, useUserStore } from '@/store';
import { getCookie } from '@/utils/cookie';
import { PAGE_NOT_FOUND_ROUTE } from '@/utils/route/constant';

NProgress.configure({ showSpinner: false });

router.beforeEach(async (to, from, next) => {
  NProgress.start();

  const permissionStore = getPermissionStore();
  const { whiteListRouters } = permissionStore;

  const userStore = useUserStore();

  const cook = getCookie(CommonConfig.COOKIE_KEY);

  //
  /*if (to.query && to.query.ticket) {
    const url = `${CommonConfig.AUTH_CHECK_TICKET}?ticket=${to.query.ticket}`;
    const nextUrl = await userStore.checkTicket(url);
    await userStore.getUserInfo();

    if (nextUrl) {
      // get user info: user, menu, workspace
      location.href = nextUrl;
      // next(nextUrl);
      return;
    }
  }*/

  if (cook) {
    const user = userStore.getCurrentUser();
    if (user && user.account) {
    }
    else {
      await userStore.getUserInfo();
    }
    next();
    return;
  }

  // location.href = `${CommonConfig.AUTH_CENTER_URL}?apt=app&client=${CommonConfig.ADMIN_CLIENT_ID}`;
  // return;

  // if (userStore.token) {
  if (userStore.token) {
    if (to.path === '/login') {
      next();
      return;
    }
    try {
       await userStore.getUserInfo();

      const { asyncRoutes } = permissionStore;

      console.log('to', to);
      if (asyncRoutes && asyncRoutes.length === 0) {
        const routeList = await permissionStore.buildAsyncRoutes();
        routeList.forEach((item: RouteRecordRaw) => {
          router.addRoute(item);
        });

        if (to.name === PAGE_NOT_FOUND_ROUTE.name) {
          // 动态添加路由后，此处应当重定向到fullPath，否则会加载404页面内容
          next({ path: to.fullPath, replace: true, query: to.query });
        } else {
          const redirect = decodeURIComponent((from.query.redirect || to.path) as string);
          next(to.path === redirect ? { ...to, replace: true } : { path: redirect });
          return;
        }
      }
      if (router.hasRoute(to.name)) {
        next();
      } else {
        next(`/`);
      }
    } catch (error) {
      MessagePlugin.error(error.message);
      next({
        path: '/login',
        query: { redirect: encodeURIComponent(to.fullPath) },
      });
      NProgress.done();
    }
  } else {
    /* white list router */
    if (whiteListRouters.indexOf(to.path) !== -1) {
      next();
    } else {
      next({
        path: '/login',
        query: { redirect: encodeURIComponent(to.fullPath) },
      });
    }
    NProgress.done();
  }
});

router.afterEach((to) => {
  if (to.path === '/login') {
    const userStore = useUserStore();
    const permissionStore = getPermissionStore();

    userStore.logout();
    permissionStore.restoreRoutes();
  }
  NProgress.done();
});
