export const CONFIG = {
  // 将配置信息放在window对象上,使其变成全局都可以访问的
  // 管理端地址，也就本前端应用访问地址
  HOME_PAGE: 'http://123.60.219.212/mgt',
  // 校验ticket地址
  AUTH_CHECK_TICKET: 'http://www.hicareer.net/idaas/sso/checkTicket',
  // 登出地址
  AUTH_LOGOUT_URL: 'http://www.hicareer.net/idaas/sso/signout',
  // 登录地址
  AUTH_ADDRESS: 'http://123.60.219.212/idaas/sso/doLogin',
  AUTH_CENTER_URL: 'http://www.hicareer.net/abc',
  // 本前端访问后端前缀，参考nginx配置
  URL_PREFIX: '/idaas',
};
