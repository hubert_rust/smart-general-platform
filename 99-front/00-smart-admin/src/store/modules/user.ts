import { defineStore } from 'pinia';
import * as querystring from 'querystring';
import { MessagePlugin } from 'tdesign-vue-next';
import { useRouter } from 'vue-router';

import { failLoad } from '@/api/declarations';
import { MenuModel } from '@/api/model/menuModel';
import { RealmModel } from '@/api/model/realmModel';
import { ssoTicketCheck } from '@/api/oauth';
import { getLoginUserInfo } from '@/api/user';
import { adminList, wksList, wksSwitch } from '@/api/workspace';
import { CommonConfig, TOKEN_NAME } from '@/config/global';
import { store, usePermissionStore } from '@/store';
import type { UserInfo } from '@/types/interface';
import { getCookie } from '@/utils/cookie';

const InitUserInfo: UserInfo = {
  id: '',
  realName: '',
  phone: '',
  email: '',
  gender: '',
  createTime: '',
  name: '', // 用户名，用于展示在页面右上角头像处
  account: '',
  remark: '',
  roles: [], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
};
const InitRealm: RealmModel = {
  id: '',
  tidType: '',
  tenantName: '',
};
export const useUserStore = defineStore('user', {
  state: () => ({
    token: '', // 默认token不走权限
    salt: '',

    cook: '',
    [TOKEN_NAME]: '', // 默认token不走权限
    userInfo: { ...InitUserInfo },
    // 当前用户空间, tidType: tenant, realm
    realm: { ...InitRealm },
    // 代理的
    agents: [] as Array<RealmModel>,
    // 管理员用户空间列表
    selfs: [] as Array<RealmModel>,

    userMenuRouter: [],
    roles: [],
    menus: [] as Array<MenuModel>,
    menuPerMap: [],
  }),
  getters: {
    roles: (state) => {
      return state.userInfo?.roles;
    },
  },
  /* actions: {
    async login(userInfo: Record<string, unknown>) {
      const mockLogin = async (userInfo: Record<string, unknown>) => {
        // 登录请求流程
        console.log(`用户信息:`, userInfo);
        // const { account, password } = userInfo;
        // if (account !== 'td') {
        //   return {
        //     code: 401,
        //     message: '账号不存在',
        //   };
        // }
        // if (['main_', 'dev_'].indexOf(password) === -1) {
        //   return {
        //     code: 401,
        //     message: '密码错误',
        //   };
        // }
        // const token = {
        //   main_: 'main_token',
        //   dev_: 'dev_token',
        // }[password];
        return {
          code: 200,
          message: '登陆成功',
          data: 'main_token',
        };
      };

      const res = await mockLogin(userInfo);
      if (res.code === 200) {
        this.token = res.data;
      } else {
        throw res;
      }
    },
    async getUserInfo() {
      const mockRemoteUserInfo = async (token: string) => {
        if (token === 'main_token') {
          return {
            name: 'Tencent',
            roles: ['all'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
          };
        }
        return {
          name: 'td_dev',
          roles: ['UserIndex', 'DashboardBase', 'login'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
        };
      };
      const res = await mockRemoteUserInfo(this.token);

      this.userInfo = res;
    },
    async logout() {
      this.token = '';
      this.userInfo = { ...InitUserInfo };
    },
  }, */
  actions: {
    getCurrentWks() {
      return this.realm;
    },
    getSelfs() {
      return this.selfs;
    },
    getAgents() {
      return this.agents;
    },
    getRealm() {
      return this.realm?.id;
    },
    // 该用户空间管理员列表
    async adminWksList() {
      const ret = adminList();
      console.log(ret);
    },

    // 废弃
    async realmList() {
      await wksList({ key: '' }).then((res: any) => {
        console.log(res);
        if (res.code === 20000) {
          this.realm = JSON.parse(JSON.stringify(res.data.model));
          this.selfs = JSON.parse(JSON.stringify(res.data.workspaces));
          this.agents = JSON.parse(JSON.stringify(res.data.agents));
          if (!res.data.model) {
            this.realm = { ...InitRealm };
            // const router = useRouter();
            // router.push('/uspace/index');
          }
        } else {
          MessagePlugin.error(failLoad(false));
        }
      });
    },
    async switchRealm(data: RealmModel) {
      this.realm.tenantName = data.tenantName;
      this.realm.id = data.id;
      await wksSwitch(this.realm.id).then((res: any) => {
        if (res.code === 20000) {
          this.realm = JSON.parse(JSON.stringify(res.data.model));
          this.selfs = JSON.parse(JSON.stringify(res.data.workspaces));
          this.agents = JSON.parse(JSON.stringify(res.data.agents));
        } else {
          MessagePlugin.error(failLoad(false));
        }
      });
    },
    getCurrentTid(): string {
      return this.realm?.id;
    },
    getCookie(): string {
      return this.cook;
    },
    // after sso login success, return homepage
    async checkTicket(url: string) {
      const nextUrl = await ssoTicketCheck(url).then((res: any) => {
        let url = window.location.href.split('?')[0];
        const search = window.location.href.split('?')[1];
        const query: any = querystring.parse(search);

        if (res.code === 20000) {
          const cookie = getCookie('SMART_TOKEN');
          this.cook = cookie;
          delete query.ticket;
          const newSearch = querystring.encode(query);
          if (newSearch) {
            url = `${url}?=${newSearch}`;
          }
          console.log('ticket check success');
          return url;
        }
        MessagePlugin.error('ticket校验失败');
        location.href = `${CommonConfig.AUTH_CENTER_URL}?apt=mgt&client=${CommonConfig.ADMIN_CLIENT_ID}`;
        return null;
      });
      return nextUrl;
    },
    // 废弃
    async login(userInfo: Record<string, unknown>) {
      const mockLogin = async (userInfo: Record<string, unknown>) => {
        // 登录请求流程
        console.log(`用户信息:`, userInfo);
        // const { account, password } = userInfo;
        // if (account !== 'td') {
        //   return {
        //     code: 401,
        //     message: '账号不存在',
        //   };
        // }
        // if (['main_', 'dev_'].indexOf(password) === -1) {
        //   return {
        //     code: 401,
        //     message: '密码错误',
        //   };
        // }
        // const token = {
        //   main_: 'main_token',
        //   dev_: 'dev_token',
        // }[password];
        return {
          code: 200,
          message: '登陆成功',
          data: 'main_token',
        };
      };

      const res = await mockLogin(userInfo);
      if (res.code === 200) {
        this.setToken(res.data);
      } else {
        throw res;
      }
    },
    getLocalMenus(): Array<MenuModel> {
      return this.menus;
    },
    /* async getUserInfo() {
      const mockRemoteUserInfo = async (token: string) => {
        if (token === 'main_token') {
          return {
            name: 'Tencent',
            roles: ['all'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
          };
        }
        return {
          name: 'td_dev',
          roles: ['UserIndex', 'DashboardBase', 'login'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
        };
      };
      const res = await mockRemoteUserInfo(this.token);

      this.userInfo = res;
    }, */
    // 登录后使用此接口
    async getUserInfo() {
      await getLoginUserInfo().then((res: any) => {
        console.log(res);
        if (res.code === 20000) {
          this.userInfo = res.data.user;
          this.realm = res.data.currentWks;
          this.agents = res.data.agents;
          this.selfs = res.data.selfs;
          this.menus = res.data.menus;
          if (!this.realm) {
            this.realm = { ...InitRealm };
            location.href = '/uspace/index';
            // const router = useRouter();
            // router.push('/uspace/index');
          }
        } else {
          MessagePlugin.error('加载用户信息失败!');
        }
      });

      /* const mockRemoteUserInfo = async (token: string) => {
        if (token === 'main_token') {
          return {
            name: 'Tencent',
            roles: ['all'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
          };
        }
        return {
          name: 'td_dev',
          roles: ['UserIndex', 'DashboardBase', 'login'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
        };
      };
      const res = await mockRemoteUserInfo(this[TOKEN_NAME]);
*/
      // this.userInfo = res;
    },
    async logout() {
      this.removeToken();
      this.userInfo = { ...InitUserInfo };
    },
    async removeToken() {
      this.setToken('');
    },
    async setToken(token: string) {
      this.token = token;
    },
    getToken(): string {
      return this.token;
    },
    async setSalt(salt: string) {
      this.salt = salt;
    },
    getSalt(): string {
      return this.salt;
    },
  },
  persist: {
    afterRestore: () => {
      const permissionStore = usePermissionStore();
      permissionStore.initRoutes();
    },
    key: 'user',
    paths: ['token', 'salt'],
  },
});
export function getUserStore() {
  return useUserStore(store);
}
