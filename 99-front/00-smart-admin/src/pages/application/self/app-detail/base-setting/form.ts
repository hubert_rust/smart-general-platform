import { FormRule } from 'tdesign-vue-next';
export const FORM_BASE_RULES: Record<string, FormRule[]> = {
  appTag: [{ required: true, message: '请输入应用名称', type: 'error' }],
  appType: [{ required: true, message: '请输入应用类型', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};
export const FORM_RULES: Record<string, FormRule[]> = {
  appTag: [{ required: true, message: '请输入用户空间名称', type: 'error' }],
  appType: [{ required: true, message: '请输入用户空间名称', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  id: '',
  appImg: '',
  appTag: '',
  clientId: '',
  appInnerName: '',
  logo: '',
  appType: 'user',
  remark: '',
  authUri: '',
  redirectUris: '',
  logoutUris: '',
};
