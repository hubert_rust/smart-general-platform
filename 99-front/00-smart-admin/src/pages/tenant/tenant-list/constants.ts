import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const USER_ACTION_OPTION = [
  { content: '登录控制台', value: 1 },
  { content: '删除租户', value: 2 },
  { content: '停用租户', value: 3 },
];
export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '租户名称',
    align: 'left',
    width: 120,
    colKey: 'tenantName',
    fixed: 'left',
  },
  {
    title: '租户ID',
    width: 120,
    ellipsis: true,
    colKey: 'id',
  },
  {
    title: '租户来源',
    width: 120,
    ellipsis: true,
    colKey: 'phone',
  },
  {
    title: '创建者',
    width: 80,
    ellipsis: true,
    colKey: 'email',
  },
  {
    title: '关联应用',
    width: 80,
    ellipsis: true,
    colKey: 'email',
  },
  {
    title: '成员数量',
    width: 120,
    ellipsis: true,
    colKey: 'lastLoginTime',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  key: [{ required: true, message: '请输入键值', type: 'error' }],
  value: [{ required: true, message: '请输入值', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};
