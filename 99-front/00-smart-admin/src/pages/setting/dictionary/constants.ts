import { PrimaryTableCol, TableRowData } from 'tdesign-vue-next';
import { FormRule } from 'tdesign-vue-next';
export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  //{ colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '数据类型',
    align: 'left',
    width: 120,
    colKey: 'dictType',
    fixed: 'left',
  },
  {
    title: '数据标签',
    width: 120,
    ellipsis: true,
    colKey: 'dataName',
  },
  {
    title: '数据键值',
    width: 120,
    ellipsis: true,
    colKey: 'dataKey',
  },
/*  {
    title: '数据值',
    width: 120,
    ellipsis: true,
    colKey: 'dataValue',
  },*/
  {
    title: '状态',
    width: 80,
    ellipsis: true,
    colKey: 'status',
  },
  {
    title: '说明',
    width: 120,
    ellipsis: true,
    colKey: 'remark',
  },

  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];


export const FORM_RULES: Record<string, FormRule[]> = {
  dictType: [{ required: true, message: '请输入数据类型', type: 'error' }],
  dataName: [{ required: true, message: '请输入数据标签', type: 'error' }],
  dataKey: [{ required: true, message: '请输入数据键值', type: 'error' }],


  //suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  dictType: '',
  dataName: '',
  dataKey: '',
  dataValue: '',
  dataOrder: 0,
  dataExt: '',
  defaluted: 0,
  parentI: '-1',
  id: '',
  status: true,
  remark: '',
};
