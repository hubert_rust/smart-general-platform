import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';
import { computed } from 'vue';

export const MAIN_MENU_OPTION = [{ content: '添加组织', value: 1 }];

export const USER_ACTION_OPTION = computed(() => {
  return function (data: any) {
    return [
        { content: '回收', value: 'recycle', row: data },
    ];
  };
});

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '用户',
    align: 'left',
    width: 120,
    colKey: 'account',
    fixed: 'left',
  },
  {
    title: '账号类型',
    width: 120,
    ellipsis: true,
    colKey: 'createType',
  },
  {
    title: '状态',
    width: 80,
    ellipsis: true,
    colKey: 'status',
  },
  {
    title: '最后登录时间',
    width: 120,
    ellipsis: true,
    colKey: 'lastLoginTime',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  account: [{ required: true, message: '请输入用户名', type: 'error' }],
  // phone: [{ required: true, message: '请输入手机号', type: 'error' }],
  // email: [{ required: true, message: '请输入邮箱', type: 'error' }],
  password: [{ required: true, message: '请输入密码', type: 'error' }],
};

export const INITIAL_DATA = {
  account: '',
  createType: '',
  phone: '',
  email: '',
  password: '',
};

export const IMPORT_OPTIONS = [
  { content: '通过Excel导入', value: 1 },
  { content: '通过脚本导入', value: 2 },
  { content: '导入历史', value: 3 },
];
export const EXPORT_OPTIONS = [
  { content: '导出全部用户', value: 1 },
  { content: '导出历史', value: 2 },
];
