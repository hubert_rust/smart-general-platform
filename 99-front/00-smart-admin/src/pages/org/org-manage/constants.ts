import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';
import { computed } from 'vue';

export const MAIN_MENU_OPTION = [{ content: '添加组织', value: 1 }];
export const USER_ACTION_COMPUTED = computed(() => {
  return function (row: any) {
    if (row.status === 'active') {
      return [
        { content: '禁用账号', value: 'deactive', row },
        { content: '删除成员', value: 'delete', row },
        { content: '变更部门', value: 'changeDept', row },
        { content: '设置岗位', value: 'setPost', row },
        /* { content: '设为主部门', value: 'setMainDept', row }, */
        { content: '设为部门负责人', value: 'setDeptHeader', row },
      ];
    }
    if (row.status === 'deactive') {
      return [
        { content: '激活账号', value: 'active', row },
        { content: '删除成员', value: 'delete', row },
        { content: '变更部门', value: 'changeDept', row },
        { content: '设置岗位', value: 'setPost', row },
        { content: '设为主部门', value: 'setMainDept', row },
        { content: '设为部门负责人', value: 'setDeptHeader', row },
      ];
    }
    return [{ content: '删除成员', value: 'delete', row }];
  };
});

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '用户',
    align: 'left',
    width: 120,
    colKey: 'account',
    fixed: 'left',
  },
  {
    title: '姓名',
    width: 120,
    ellipsis: true,
    colKey: 'realName',
  },
  {
    title: '手机号',
    width: 120,
    ellipsis: true,
    colKey: 'phone',
  },
  {
    title: '邮箱',
    width: 120,
    ellipsis: true,
    colKey: 'email',
  },
  {
    title: '状态',
    width: 120,
    ellipsis: true,
    colKey: 'status',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 80,
    colKey: 'op',
    title: '操作',
  },
];

export const USER_FORM_RULES: Record<string, FormRule[]> = {
  mainDeptId: [{ required: true, message: '请选择部门', type: 'error' }],
  account: [{ required: true, message: '请输入用户名', type: 'error' }],
  phone: [{ required: true, message: '请输入手机号', type: 'error' }],
  email: [{ required: true, message: '请输入邮箱', type: 'error' }],
  password: [{ required: true, message: '请输入密码', type: 'error' }],

  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};
export const FORM_RULES: Record<string, FormRule[]> = {
  deptName: [{ required: true, message: '请输入名称', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};

export const TREE_DATA = [
  {
    label: '深圳总部',
    value: 0,
    children: [
      {
        label: '总办',
        value: '0-0',
      },
      {
        label: '市场部',
        value: '0-1',
        children: [
          {
            label: '采购1组',
            value: '0-1-0',
          },
          {
            label: '采购2组',
            value: '0-1-1',
          },
        ],
      },
      {
        label: '技术部',
        value: '0-2',
      },
    ],
  },
  {
    label: '北京总部',
    value: 1,
    children: [
      {
        label: '总办',
        value: '1-0',
      },
      {
        label: '市场部',
        value: '1-1',
        children: [
          {
            label: '采购1组',
            value: '1-1-0',
          },
          {
            label: '采购2组',
            value: '1-1-1',
          },
        ],
      },
    ],
  },
  {
    label: '上海总部',
    value: 2,
    children: [
      {
        label: '市场部',
        value: '2-0',
      },
      {
        label: '财务部',
        value: '2-1',
        children: [
          {
            label: '财务1组',
            value: '2-1-0',
          },
          {
            label: '财务2组',
            value: '2-1-1',
          },
        ],
      },
    ],
  },
  {
    label: '湖南',
    value: 3,
  },
  {
    label: '湖北',
    value: 4,
  },
];
