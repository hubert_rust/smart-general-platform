import {FormRule, PrimaryTableCol, TableRowData} from 'tdesign-vue-next';

export const FORM_RULES: Record<string, FormRule[]> = {
  groupName: [{ required: true, message: '请输入用户组名称', type: 'error' }],
  groupTag: [{ required: true, message: '请输入唯一标识', type: 'error' }],
  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  groupName: '',
  groupTag: '',
  remark: '',
  id: '',
};

export const USER_ROLE_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  { width: 180, colKey: 'roleName', title: '角色名称', ellipsis: true, align: 'left' },
  { width: 180, colKey: 'roleCode', title: '角色编码', ellipsis: true, align: 'left' },
  { width: 170, colKey: 'roleSubject', title: '角色主体', align: 'left' },
  { width: 170, colKey: 'subjectName', title: '角色主体名称', align: 'left' },
  { colKey: 'op', width: 170, title: '操作', align: 'right' },
];
export const USER_GROUP_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  { width: 180, colKey: 'groupName', title: '用户组名称', ellipsis: true, align: 'left' },
  { width: 170, colKey: 'remark', title: '组描述', align: 'left' },
  { colKey: 'op', width: 170, title: '操作', align: 'right' }
];
export const SUBJECT_COLUMNS: PrimaryTableCol<TableRowData>[] = [
  {
    colKey: 'row-select',
    type: 'multiple',
    // 禁用行选中方式一：使用 disabled 禁用行（示例代码有效，勿删）。disabled 参数：{row: RowData; rowIndex: number })
    // 这种方式禁用行选中，当前行会添加行类名 t-table__row--disabled，禁用行文字变灰
    // disabled: ({ rowIndex }) => rowIndex === 1 || rowIndex === 3,

    // 禁用行选中方式二：使用 checkProps 禁用行（示例代码有效，勿删）
    // 这种方式禁用行选中，行文本不会变灰
    checkProps: ({ row }: any) => ({ disabled: false }),
    // 自由调整宽度，如果发现元素看不见，请加大宽度
    width: 50,
    align: 'left',
  },
  { width: 180, colKey: 'subjectName', title: '主体名称', ellipsis: true, align: 'left' },
  { width: 180, colKey: 'subjectType', title: '主体类型', ellipsis: true, align: 'left' },
  { width: 170, colKey: 'createTime', title: '加入时间', align: 'left' },
  { colKey: 'op', width: 170, title: '操作', align: 'right' }
];
