import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '授权应用名称',
    align: 'left',
    width: 120,
    colKey: 'appTag',
    fixed: 'left',
  },
  {
    title: '授权应用类型',
    width: 120,
    ellipsis: true,
    colKey: 'clientFrom',
  },
  {
    title: '授权主体类型',
    width: 120,
    ellipsis: true,
    colKey: 'subjectType',
  },

  {
    title: '授权作用',
    align: 'center',
    fixed: 'right',
    width: 70,
    colKey: 'permit',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  key: [{ required: true, message: '请输入键值', type: 'error' }],
  value: [{ required: true, message: '请输入值', type: 'error' }],

  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};
