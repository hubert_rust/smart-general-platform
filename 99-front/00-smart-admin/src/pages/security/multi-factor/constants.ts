import { FormRule, PrimaryTableCol, TableRowData } from 'tdesign-vue-next';

export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '键值(Key)',
    align: 'left',
    width: 120,
    colKey: 'key',
    fixed: 'left',
  },
  {
    title: '值(Value)',
    width: 120,
    ellipsis: true,
    colKey: 'value',
  },
  {
    title: '说明',
    width: 120,
    ellipsis: true,
    colKey: 'remark',
  },
  {
    title: '状态',
    width: 80,
    ellipsis: true,
    colKey: 'state',
  },
  {
    title: '创建时间',
    width: 120,
    ellipsis: true,
    colKey: 'createTime',
  },
  {
    align: 'center',
    fixed: 'right',
    width: 100,
    colKey: 'op',
    title: '操作',
  },
];

export const FORM_RULES: Record<string, FormRule[]> = {
  key: [{ required: true, message: '请输入键值', type: 'error' }],
  value: [{ required: true, message: '请输入值', type: 'error' }],

  // suggest: [{ required: true, message: '请输入问题描述', type: 'error' }],
};

export const INITIAL_DATA = {
  key: '',
  value: '',
  state: 'active',
  remark: '',
};
