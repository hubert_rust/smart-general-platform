import 'nprogress/nprogress.css'; // progress bar style

import NProgress from 'nprogress'; // progress bar
import { MessagePlugin } from 'tdesign-vue-next';
import { RouteRecordRaw } from 'vue-router';

import { CommonConfig } from '@/config/global';
import router from '@/router';
import { getPermissionStore, useUserStore } from '@/store';
import { getCookie } from '@/utils/cookie';
import { PAGE_NOT_FOUND_ROUTE } from '@/utils/route/constant';

NProgress.configure({ showSpinner: false });

const parseHref = (): string => {
  const val: Array<string> = window.location.href.split('?');
  if (val.length >= 2) {
    const ticket: Array<string> = val[1].split('=');
    if (ticket.length >= 2) {
      return ticket[1];
    }
  }
  return '';
};
router.beforeEach(async (to, from, next) => {
  NProgress.start();

  const permissionStore = getPermissionStore();
  const { whiteListRouters } = permissionStore;

  const userStore = useUserStore();
  const token = userStore.getToken();
  const salt = userStore.getSalt();
  const cook = getCookie(CommonConfig.COOKIE_KEY);
  if (to.path === '/redirect/index') {
    next();
    return;
  }
  if (to.path === '/uspace/index') {
    next();
    return;
  }
  if (CommonConfig.PROTOCOL === 'jwt') {
    if (!token) {
      location.href = CommonConfig.AUTH_CENTER_URL + '?apt=mgt&client=' + CommonConfig.ADMIN_CLIENT_ID;
    }
  }
  const realm = await userStore.getRealm();
  if (realm) {
    next();
    return;
  }

  if (true) {
    /* if (to.path === '/login') {
      next();
      return;
    } */
    try {
      await userStore.getUserInfo();
      const { asyncRoutes } = permissionStore;

      if (asyncRoutes && asyncRoutes.length === 0) {
        const routeList = await permissionStore.buildAsyncRoutes();
        routeList.forEach((item: RouteRecordRaw) => {
          router.addRoute(item);
        });
        console.log('routeList: ', routeList);

        console.log('to, ', to);
        if (to.name === PAGE_NOT_FOUND_ROUTE.name) {
          console.log('to.name === PAGE_NOT_FOUND_ROUTE.name, to.name: ', to);
          // 动态添加路由后，此处应当重定向到fullPath，否则会加载404页面内容
          next({ path: to.fullPath, replace: true, query: to.query });
        } else {
          const redirect = decodeURIComponent((from.query.redirect || to.path) as string);
          console.log('to.name !== PAGE_NOT_FOUND_ROUTE.name, redirect: ', redirect);
          next(to.path === redirect ? { ...to, replace: true } : { path: redirect });
          return;
        }
      }
      if (router.hasRoute(to.name)) {
        console.log('router.hasRoute == true', to);
        next();
      } else {
        console.log('router.hasRoute == false', to);
        next(`/`);
      }
    } catch (error) {
      MessagePlugin.error(error.message);
      next({
        path: '/login',
        query: { redirect: encodeURIComponent(to.fullPath) },
      });
      NProgress.done();
    }
  } else {
    /* white list router */
    if (whiteListRouters.indexOf(to.path) !== -1) {
      next();
    } else {
      next({
        path: '/login',
        query: { redirect: encodeURIComponent(to.fullPath) },
      });
    }
    NProgress.done();
  }
});

router.afterEach((to) => {
  if (to.path === '/login') {
    const userStore = useUserStore();
    const permissionStore = getPermissionStore();

    userStore.logout();
    permissionStore.restoreRoutes();
  }
  NProgress.done();
});
