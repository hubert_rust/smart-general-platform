import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  AppList: `${prefix}/appsso/list`,
  AppListMarket: `${prefix}/appsso/listMarket`,
  AppAddSso: `${prefix}/appsso/addsso`,
  AppDeleteSso: `${prefix}/appsso/deletesso`,
  AppSsoDetail: `${prefix}/appsso/ssoDetail`,

  SsoApiListURL: `${prefix}/appsso/apiListOfApp`,

  // access
  ListAppEmpower: `${prefix}/application/listEmpower`,

  // 所有用户可见，不可见
  AllUserEnable: `${prefix}/appsso/allUserEnable`,
  AllUserDisable: `${prefix}/appsso/allUserDisable`,
};

export function appSsoDetail(clientId: string) {
  return request.post({
    url: `${Api.AppSsoDetail}/${clientId}`,
  });
}

export function allUserDisable(clientId: any) {
  return request.post({
    url: `${Api.AllUserDisable}/${clientId}`,
  });
}

export function allUserEnable(clientId: any) {
  return request.post({
    url: `${Api.AllUserEnable}/${clientId}`,
  });
}

export function ssoApiList(id: any) {
  return request.post({
    url: `${Api.SsoApiListURL}/${id}`,
  });
}
export function queryAppList(data: any) {
  return request.post({
    url: `${Api.AppList}`,
    data,
  });
}
export function queryAppMarketList(data: any) {
  return request.post({
    url: `${Api.AppListMarket}`,
    data,
  });
}
export function appAddSso(clientId: string) {
  return request.post({
    url: `${Api.AppAddSso}/${clientId}`,
  });
}

export function appDeleteSso(clientId: string) {
  return request.post({
    url: `${Api.AppDeleteSso}/${clientId}`,
  });
}
