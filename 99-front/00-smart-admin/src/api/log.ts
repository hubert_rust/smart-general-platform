import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  AdminLogList: `${prefix}/log/adminList`,
  UserLogList: `${prefix}/log/userList`,
};

export function adminLogList(data: any) {
  return request.post({
    url: `${Api.AdminLogList}`,
    data,
  });
}

export function userLogList(data: any) {
  return request.post({
    url: `${Api.UserLogList}`,
    data,
  });
}
