import { request } from '@/utils/request';
import {CommonConfig} from "@/config/global";

const prefix = CommonConfig.URL_PREFIX;
export async function session(data: any) {
  return request.post({
    url: prefix + '/open/identity/session',
    data,
  });
}
export async function exchange(sessionId: string | undefined, confidential: string) {
  let data = {sessionId: sessionId, confidential: confidential}
  return request.post({
    url: prefix + '/open/identity/exchange',
    data,
  });
}
export async function getSocialList() {
  return request.get({
    url: prefix + '/open/identity/sources',
  });
}
