export interface SessionReq {
  clientId: string;
  clientSecret: string;
  sessionId: string;
}
