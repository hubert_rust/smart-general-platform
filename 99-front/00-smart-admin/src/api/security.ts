import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  CommonSecDetail: `${prefix}/commonSecurity/detail`,
  CommonSecBaseSet: `${prefix}/commonSecurity/baseSet`,
  CommonSecLoginSet: `${prefix}/commonSecurity/loginSet`,
  CommonSecAppValidSet: `${prefix}/commonSecurity/appValidSet`,
};

export function commonSecurityAppValidSet(data: any) {
  return request.post({
    url: `${Api.CommonSecAppValidSet}`,
    data,
  });
}
export function commonSecurityLoginSet(data: any) {
  return request.post({
    url: `${Api.CommonSecLoginSet}`,
    data,
  });
}
export function commonSecurityDetail() {
  return request.post({
    url: `${Api.CommonSecDetail}`,
  });
}

export function commonSecurityBaseSet(data: any) {
  return request.post({
    url: `${Api.CommonSecBaseSet}`,
    data,
  });
}
