import { Base64 } from 'js-base64';

import { CommonConfig } from '@/config/global';
import { ContentTypeEnum } from '@/constants';
import { useCryptoStore } from '@/store/crypto';
import { request } from '@/utils/request';

export async function ssoTicketCheck(url: string) {
  return request.post({
    url,
    data: {},
  });
}
export function getBasicHeader(): string {
  return `Basic ${Base64.encode(`${CommonConfig.OAUTH2_CLIENT_ID}:${CommonConfig.OAUTH2_CLIENT_SECRET}`)}`;
}

/* export async function passwordFlow(username: string, password: string, search: string, oidc = true) {
  const crypto = useCryptoStore();
  const url = CommonConfig.AUTH_ADDRESS + search;
  // const url = 'oauth2/token';
  if (true) {
    username = crypto.encrypt(username);
    password = crypto.encrypt(password);
  }

  let data = {};
  if (oidc) {
    data = { username, password, grant_type: 'password', scope: 'openid' };
  } else {
    data = { username, password, grant_type: 'password' };
  }
  return request.post(
    {
      url,
      data,
      headers: {
        Authorization: getBasicHeader(),
        'Content-Type': ContentTypeEnum.FormURLEncoded,
      },
    },
    {},
  );
} */

// 授权码登录
const createAuthorizationCodeAddress = () => {
  return CommonConfig.OAUTH2_AUTHORIZATION_CODE_ADDR;
};

// 暂时没有增加state
const createAuthorizationCodeParams = () => {
  let param = `?response_type=code&client_id=${CommonConfig.OAUTH2_CLIENT_ID}`;
  param += `&client_secret=${CommonConfig.OAUTH2_CLIENT_SECRET}`;
  param += `&redirect_uri=${CommonConfig.OAUTH2_REDIRECT_URI}`;
  param += `&scope=openid`;
  return param;
};

export function authorizationCodeUrl() {
  const url = createAuthorizationCodeAddress() + createAuthorizationCodeParams();
  return url;
}

// 通过授权码code获取token
export function authorizationCodeFlow(code: string, redirectUri: string, state = '', oidc = false) {
  const url = CommonConfig.OAUTH2_TOKEN_ADDR;
  let data = {};
  if (oidc) {
    data = { code, state, redirect_uri: redirectUri, grant_type: 'authorization_code', scope: 'openid' };
  } else {
    data = { code, state, redirect_uri: redirectUri, grant_type: 'authorization_code' };
  }

  return request.post(
    {
      url,
      data,
      headers: {
        Authorization: getBasicHeader(),
        'Content-Type': ContentTypeEnum.FormURLEncoded,
      },
    },
    {},
  );
}

// sso
