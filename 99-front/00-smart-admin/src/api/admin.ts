import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  QueryExternalUser: `${prefix}/admin/queryExternalAdmin`,
  AdminList: `${prefix}/admin/listAdmin`,
  CreateSpaceAdmin: `${prefix}/admin/addAdmin`,
  CreateSpaceExternalAdmin: `${prefix}/admin/addExternalAdmin`,
  RemoveSpaceAdmin: `${prefix}/admin/removeAdmin`,

  QueryAdminMenu: `${prefix}/admin/queryAdminMenu`,
  SetInnerAdminPermit: `${prefix}/admin/setInnerAdminPermit`,
  SetExternalAdminPermit: `${prefix}/admin/setExternalAdminPermit`,
};

export function externalAdminPermit(data: any) {
  return request.post({
    url: Api.SetExternalAdminPermit,
    data,
  });
}
export function innerAdminPermit(data: any) {
  return request.post({
    url: Api.SetInnerAdminPermit,
    data,
  });
}
export function queryAdminMenuRes(data: any) {
  return request.post({
    url: Api.QueryAdminMenu,
    data,
  });
}
export function queryExernalAdmin(data: any) {
  return request.post({
    url: Api.QueryExternalUser,
    data,
  });
}

export function listAdmin(data: any) {
  return request.post({
    url: Api.AdminList,
    data,
  });
}

export function addExternalAdmin(data: any) {
  return request.post({
    url: Api.CreateSpaceExternalAdmin,
    data,
  });
}
export function addAdmin(data: any) {
  return request.post({
    url: Api.CreateSpaceAdmin,
    data,
  });
}
export function removeAdmin(data: any) {
  return request.post({
    url: Api.RemoveSpaceAdmin,
    data,
  });
}
