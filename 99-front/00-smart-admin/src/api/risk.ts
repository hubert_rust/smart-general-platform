import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  RiskInsertIpBlack: `${prefix}/risk/insertIpBlack`,
  RiskDeleteIpBlack: `${prefix}/risk/deleteIpBlack`,
  RiskIpBlackList: `${prefix}/risk/ipBlackList`,

  RiskInsertIpWhite: `${prefix}/risk/insertIpWhite`,
  RiskDeleteIpWhite: `${prefix}/risk/deleteIpWhite`,
  RiskIpWhiteList: `${prefix}/risk/ipWhiteList`,

  RiskInsertUserWhite: `${prefix}/risk/insertUserWhite`,
  RiskDeleteUserWhite: `${prefix}/risk/deleteUserWhite`,
  RiskUserWhiteList: `${prefix}/risk/userWhiteList`,
};

export function riskInsertIpBlack(data: any) {
  return request.post({
    url: `${Api.RiskInsertIpBlack}`,
    data,
  });
}

export function riskDeleteIpBlack(id: string) {
  return request.post({
    url: `${Api.RiskDeleteIpBlack}/${id}`,
  });
}

export function riskIpBlackList(data: any) {
  return request.post({
    url: `${Api.RiskIpBlackList}`,
    data,
  });
}


//
export function riskInsertIpWhite(data: any) {
  return request.post({
    url: `${Api.RiskInsertIpWhite}`,
    data,
  });
}

export function riskDeleteIpWhite(id: string) {
  return request.post({
    url: `${Api.RiskDeleteIpWhite}/${id}`,
  });
}

export function riskIpWhiteList(data: any) {
  return request.post({
    url: `${Api.RiskIpWhiteList}`,
    data,
  });
}

//
export function riskInsertUserWhite(data: any) {
  return request.post({
    url: `${Api.RiskInsertUserWhite}`,
    data,
  });
}

export function riskDeleteUserWhite(id: string) {
  return request.post({
    url: `${Api.RiskDeleteUserWhite}/${id}`,
  });
}

export function riskUserWhiteList(data: any) {
  return request.post({
    url: `${Api.RiskUserWhiteList}`,
    data,
  });
}
