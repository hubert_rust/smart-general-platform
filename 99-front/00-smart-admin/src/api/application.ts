import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  InsertApp: `${prefix}/application/insert`,
  UpdateApp: `${prefix}/application/update`,
  UpdateAppAuth: `${prefix}/application/updateAuth`,
  DeleteApp: `${prefix}/application/delete`,
  DetailApp: `${prefix}/application/detail`,
  AppList: `${prefix}/application/list`,

  SyncToApp: `${prefix}/application/sync`,
  SyncApiToApp: `${prefix}/application/syncApi`,
  ForceSyncApiToApp: `${prefix}/application/forceSyncApi`,

  ActiveApp: `${prefix}/application/activeApp`,
  DeactiveApp: `${prefix}/application/deactiveApp`,
  ResApiList: `${prefix}/application/resApiList`,
  ApiListBackend: `${prefix}/application/backendListOfapp`,

  RemoteLoadApi: `${prefix}/application/remoteLoad`,

  ProcessTypeSet: `${prefix}/application/processTypeOfapp`,
  ReleaseAPI: `${prefix}/application/releaseApi`,
  CancelAPI: `${prefix}/application/cancelApi`,
  SaveAPI: `${prefix}/application/saveApi`,
  UpdateAPI: `${prefix}/application/updateApi`,
  DeleteAPI: `${prefix}/application/deleteApi`,
  CheckRes: `${prefix}/application/checkRes`,
  LinkApi: `${prefix}/application/menuLinkAPi`,
  AppShare: `${prefix}/application/share`,
  AppUnShare: `${prefix}/application/unshare`,

  // access
  ListAppEmpower: `${prefix}/application/listEmpower`,
  AddAppEmpower: `${prefix}/application/addEmpower`,
  DeleteAppEmpower: `${prefix}/application/deleteEmpower`,

  // 菜单绑定接口
  MenuBindApi: `${prefix}/application/menuLinkApi`,
  MenuUnbindApi: `${prefix}/application/menuUnlinkApi`,

  // 所有用户可见，不可见
  AllUserEnable: `${prefix}/application/allUserEnable`,
  AllUserDisable: `${prefix}/application/allUserDisable`,
};

export function allUserEnable(clientId: any) {
  return request.post({
    url: `${Api.AllUserEnable}/${clientId}`,
  });
}
export function allUserDisable(clientId: any) {
  return request.post({
    url: `${Api.AllUserDisable}/${clientId}`,
  });
}
export function menuUnbindOnApi(data: any) {
  return request.post({
    url: `${Api.MenuUnbindApi}`,
    data,
  });
}
export function menuBindOnApi(data: any) {
  return request.post({
    url: `${Api.MenuBindApi}`,
    data,
  });
}
export function queryAppEmpower(data: any) {
  return request.post({
    url: `${Api.ListAppEmpower}`,
    data,
  });
}
export function insertAppEmpower(data: any) {
  return request.post({
    url: `${Api.AddAppEmpower}`,
    data,
  });
}
export function removeAppEmpower(data: any) {
  return request.post({
    url: `${Api.DeleteAppEmpower}`,
    data,
  });
}
export function saveApp(data: any) {
  return request.post({
    url: `${Api.InsertApp}`,
    data,
  });
}
export function updateApp(data: any) {
  return request.post({
    url: `${Api.UpdateApp}`,
    data,
  });
}
export function updateAppAuth(data: any) {
  return request.post({
    url: `${Api.UpdateAppAuth}`,
    data,
  });
}
export function unshareApp(id: string) {
  return request.post({
    url: `${Api.AppUnShare}/${id}`,
  });
}
export function shareApp(id: string) {
  return request.post({
    url: `${Api.AppShare}/${id}`,
  });
}
export function detailApp(id: string) {
  return request.post({
    url: `${Api.DetailApp}/${id}`,
  });
}
export function deleteApp(id: string) {
  return request.post({
    url: `${Api.DeleteApp}/${id}`,
  });
}
export function sync2App(client: string, data: any) {
  return request.post({
    url: `${Api.SyncToApp}/${client}`,
    data,
  });
}

export function syncApi2App(client: string, data: any) {
  return request.post({
    url: `${Api.SyncApiToApp}/${client}`,
    data,
  });
}
export function forceSyncApi2App(client: string, data: any) {
  return request.post({
    url: `${Api.ForceSyncApiToApp}/${client}`,
    data,
  });
}
export function appRemoteLoad(client: string) {
  return request.post({
    url: `${Api.RemoteLoadApi}/${client}`,
  });
}

export function activeApp(id: string) {
  return request.post({
    url: `${Api.ActiveApp}/${id}`,
  });
}
export function deactiveApp(id: string) {
  return request.post({
    url: `${Api.DeactiveApp}/${id}`,
  });
}

export function applicationList() {
  return request.post({
    url: `${Api.AppList}`,
  });
}

// 后台对应的接口
export function operationListOfApp(client: string) {
  return request.post({
    url: `${Api.ApiListBackend}/${client}`,
  });
}
// 应用对应的接口
export function apiListApp(client: string) {
  return request.post({
    url: `${Api.ResApiList}/${client}`,
  });
}

export function processTypeOfOp(data: any) {
  return request.post({
    url: `${Api.ProcessTypeSet}`,
    data,
  });
}
export function releaseApi(id: string) {
  return request.post({
    url: `${Api.ReleaseAPI}/${id}`,
  });
}
export function cancelApi(id: string) {
  return request.post({
    url: `${Api.CancelAPI}/${id}`,
  });
}
export function saveApi(data: any) {
  return request.post({
    url: `${Api.SaveAPI}`,
    data,
  });
}
export function updateApi(data: any) {
  return request.post({
    url: `${Api.UpdateAPI}`,
    data,
  });
}

export function deleteApi(id: string) {
  return request.post({
    url: `${Api.DeleteAPI}/${id}`,
  });
}

export function checkRes(client: string) {
  return request.post({
    url: `${Api.CheckRes}/${client}`,
  });
}
