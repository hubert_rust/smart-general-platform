export default [
  {
    path: '/redirect',
    name: 'redirect',
    component: () => import('@/layouts/blank.vue'),
    redirect: '/redirect',
    children: [
      {
        path: 'index',
        name: 'redirectIndex',

        // component: () => import('@/layouts/blank.vue'),
        component: () => import('@/pages/redirect/index.vue'),
        meta: { title: '返回' },
      },
    ],
  },
  {
    path: '/ucenter',
    name: 'ucenter',
    component: () => import('@/layouts/blank.vue'),
    redirect: '/ucenter',
    children: [
      {
        path: 'index',
        name: 'ucenterIndex',

        // component: () => import('@/layouts/blank.vue'),
        component: () => import('@/pages/user/ucenter.vue'),
        meta: { title: '个人中心' },
      },
    ],
  },

  {
    path: '/uspace',
    name: 'uspace',
    component: () => import('@/layouts/blank.vue'),
    redirect: '/uspace',
    children: [
      {
        path: 'index',
        name: 'uspaceIndex',
        component: () => import('@/pages/userspace/index.vue'),
        meta: { title: '用户空间' },
      },
    ],
  },
];
/*

export default [
  {
    path: '/application',
    name: 'application',
    component: Layout,
    redirect: '/application/index',
    meta: { title: '应用管理', icon: 'app' },
    children: [
      {
        path: 'backend',
        name: 'backend',
        component: () => import('@/pages/application/backend/index.vue'),
        meta: { title: '后台管理' },
      },
      {
        path: 'self',
        name: 'self',
        component: () => import('@/pages/application/self/index.vue'),
        meta: { title: '自建应用' },
      },
      {
        path: 'sso',
        name: 'sso',
        component: () => import('@/pages/application/sso/index.vue'),
        meta: { title: 'SSO应用' },
      },
    ],
  },
  {
    path: '/org',
    name: 'org',
    component: Layout,
    redirect: '/org/index',
    meta: { title: '用户管理', icon: 'usergroup' },
    children: [
      {
        path: 'userIndex',
        name: 'userIndex',
        component: () => import('@/pages/org/user-manage/index.vue'),
        meta: { title: '用户列表' },
      },
      {
        path: 'orgIndex',
        name: 'orgIndex',
        component: () => import('@/pages/org/org-manage/index.vue'),
        meta: { title: '组织管理' },
      },
      {
        path: 'postIndex',
        name: 'postIndex',
        component: () => import('@/pages/org/post-manage/index.vue'),
        meta: { title: '岗位管理' },
      },

      {
        path: 'groupIndex',
        name: 'groupIndex',
        component: () => import('@/pages/org/group-manage/index.vue'),
        meta: { title: '用户组管理' },
      },
    ],
  },
  {
    path: '/tenant',
    name: 'tenant',
    component: Layout,
    redirect: '/tenant/index',
    meta: { title: '租户管理', icon: 'server' },
    children: [
      {
        path: 'tlist',
        name: 'tlist',
        component: () => import('@/pages/tenant/tenant-list/index.vue'),
        meta: { title: '租户列表' },
      },
      {
        path: 'tdlist',
        name: 'tdlist',
        component: () => import('@/pages/tenant/tenant-admin/index.vue'),
        meta: { title: '租户管理员' },
      },
    ],
  },
  {
    path: '/permission',
    name: 'permission',
    component: Layout,
    redirect: '/permission/index',
    meta: { title: '权限管理', icon: 'lock-on' },
    children: [
      {
        path: 'roleList',
        name: 'roleList',
        component: () => import('@/pages/permission/role/index.vue'),
        meta: { title: '角色管理' },
      },
      {
        path: 'tlist',
        name: 'tlist',
        component: () => import('@/pages/tenant/tenant-list/index.vue'),
        meta: { title: '租户列表' },
      },
      {
        path: 'tdlist',
        name: 'tdlist',
        component: () => import('@/pages/tenant/tenant-admin/index.vue'),
        meta: { title: '租户管理员' },
      },
      {
        path: 'reslist',
        name: 'reslist',
        component: () => import('@/pages/permission/resource/index.vue'),
        meta: { title: '资源权限' },
      },
    ],
  },
  {
    path: '/safety',
    name: 'safety',
    component: Layout,
    redirect: '/safety/index',
    meta: { title: '安全管理', icon: 'secured' },
    children: [
      {
        path: 'mfa',
        name: 'mfa',
        component: () => import('@/pages/security/multi-factor/index.vue'),
        meta: { title: '多因素认证' },
      },
    ],
  },
  {
    path: '/logs',
    name: 'logs',
    component: Layout,
    redirect: '/logs/index',
    meta: { title: '审计日志', icon: 'user-avatar' },
    children: [
      {
        path: 'admin',
        name: 'admin',
        component: () => import('@/pages/audit/admin-log/index.vue'),
        meta: { title: '管理员日志' },
      },
      {
        path: 'member',
        name: 'member',
        component: () => import('@/pages/audit/user-log/index.vue'),
        meta: { title: '用户日志' },
      },
    ],
  },
  {
    path: '/setting',
    name: 'setting',
    component: Layout,
    redirect: '/wks/index',
    meta: { title: '基本配置', icon: 'logo-codepen' },
    children: [
      {
        path: 'index',
        name: 'WksIndex',
        component: () => import('@/pages/setting/workspace/index.vue'),
        meta: { title: '用户空间设置' },
      },
      {
        path: 'envIndex',
        name: 'envIndex',
        component: () => import('@/pages/setting/env-setting/index.vue'),
        meta: { title: '环境变量' },
      },
      {
        path: 'dictIndex',
        name: 'dictIndex',
        component: () => import('@/pages/setting/dictionary/index.vue'),
        meta: { title: '字典管理' },
      },
      {
        path: 'adminIndex',
        name: 'adminIndex',
        component: () => import('@/pages/setting/admin/index.vue'),
        meta: { title: '管理员' },
      },
      {
        path: 'regIndex',
        name: 'regIndex',
        component: () => import('@/pages/setting/reg-account/index.vue'),
        meta: { title: '注册账号' },
      },
    ],
  },
  {
    path: '/user',
    name: 'user',
    component: Layout,
    redirect: '/user/index',
    meta: { title: '个人页', icon: 'user-circle' },
    children: [
      {
        path: 'index',
        name: 'UserIndex',
        component: () => import('@/pages/user/index.vue'),
        meta: { title: '个人中心' },
      },
    ],
  },
  {
    path: '/loginRedirect',
    name: 'loginRedirect',
    redirect: '/login',
    meta: { title: '登录页', icon: shallowRef(LogoutIcon) },
    component: () => import('@/layouts/blank.vue'),
    children: [
      {
        path: 'index',
        redirect: '/login',
        component: () => import('@/layouts/blank.vue'),
        meta: { title: '登录中心' },
      },
    ],
  },
];
*/
