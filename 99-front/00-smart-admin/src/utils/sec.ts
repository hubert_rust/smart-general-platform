import Bcryptjs from 'bcryptjs';
import CryptoJS from 'crypto-js';
import md5 from 'js-md5';

import { AES_IV, AES_KEY } from '@/config/global';

// const privateKey = '';

// export function cryptoEncodeECB(val) {
//   const key = CryptoJS.enc.Utf8.parse(AES_KEY);
//   const content = CryptoJS.AES.encrypt(val, key, {
//     mode: CryptoJS.mode.ECB,
//     padding: CryptoJS.pad.Pkcs7
//   });
//   return content.ciphertext.toString();
// }

// aes解密密: any: any
export function cryptoDecodeCBC(val: any) {
  const baseResult = CryptoJS.enc.Hex.parse(val); // Base64解密
  const ciphertext = CryptoJS.enc.Base64.stringify(baseResult); // Base64解密
  const decryptResult = CryptoJS.AES.decrypt(ciphertext, CryptoJS.enc.Utf8.parse(AES_KEY), {
    //  AES解密
    iv: CryptoJS.enc.Utf8.parse(AES_IV),
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  const resData = decryptResult.toString(CryptoJS.enc.Utf8).toString();
  return resData;
}
// aes加密
export function cryptoEncodeCBC(val: any) {
  const key = CryptoJS.enc.Utf8.parse(AES_KEY);
  const aesIv = CryptoJS.enc.Utf8.parse(AES_IV);
  const content = CryptoJS.AES.encrypt(val, key, { iv: aesIv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
  return content.ciphertext.toString();
}
export function md5Digest(val: any) {
  return md5(val);
}
export function bcryptEncode(pwd: any) {
  const salt = Bcryptjs.genSaltSync(11);
  const bcrypt = Bcryptjs.hashSync(pwd, salt);
  return bcrypt;
}
