import { request } from '@/utils/request';

export async function session(data: any) {
  return request.post({
    url: '/open/identity/session',
    data,
  });
}
export async function exchange(sessionId: string | undefined, confidential: string) {
  let data = {sessionId: sessionId, confidential: confidential}
  return request.post({
    url: '/open/identity/exchange',
    data,
  });
}
export async function getSocialList() {
  return request.get({
    url: '/open/identity/sources',
  });
}
export async function getGrantType() {
  return request.post({
    url: '/open/grantType',
  });
}
