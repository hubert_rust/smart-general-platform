import { Base64 } from 'js-base64';

import { LoginModel } from '@/api/model/LoginModel';
import { CommonConfig } from '@/config/global';
import { ContentTypeEnum } from '@/constants';
import { useCryptoStore } from '@/store/crypto';
import { request } from '@/utils/request';

export function getBasicHeader(): string {
  return `Basic ${Base64.encode(`${CommonConfig.OAUTH2_CLIENT_ID}:${CommonConfig.OAUTH2_CLIENT_SECRET}`)}`;
}

// 授权码登录,输入账号密码后，发送POST，/login请求
export async function authCodeLogin(data: any) {
  const url = 'login';
  return request.post(
    {
      url,
      data,
      headers: {
        Authorization: getBasicHeader(),
        'Content-Type': ContentTypeEnum.FormURLEncoded,
      },
    },
    {},
  );
}

export async function passwordFlow(post: LoginModel, search: string, oidc = true) {
  const crypto = useCryptoStore();
  // const url = CommonConfig.AUTH_ADDRESS + search;
  const loginAddr = document.getElementById('loginAddr') as HTMLInputElement;
  console.log(loginAddr.value);
  let url: any = loginAddr.value + search;
  // 单独部署，不和后台放在一起
  if (!loginAddr.value) {
    // @ts-ignore
    url = `${window.config.BASE_URL}${search}`;
  }
  const data = JSON.parse(JSON.stringify(post));
  if (true) {
    const username = crypto.encrypt(data.username);
    const password = crypto.encrypt(data.password);
    data.username = username;
    data.password = password;
    data.agent = 'desktop';
    // @ts-ignore
    data.protocol = window.config.PROTOCOL;
    data.loginType = 'normal';
  }

  /* let data = {};
        if (oidc) {
          data = { username, password, grant_type: 'password', scope: 'openid' };
        } else {
          data = { username, password, grant_type: 'password' };
        } */
  return request.post(
    {
      url,
      data,
      headers: {
        Authorization: getBasicHeader(),
        'Content-Type': ContentTypeEnum.Json,
      },
    },
    {},
  );
}
