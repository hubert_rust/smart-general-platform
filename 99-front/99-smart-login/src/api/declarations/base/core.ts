import { BaseSysEntity } from '@/api/declarations/crud';
import { ApplicationEnum } from '@/api/enums';

export interface BaseAppEntity extends BaseSysEntity {
  appSecret: string;
  appName: string;
  appCode: string;
  applicationType: ApplicationEnum;
}

export interface BaseCmdbEntity extends BaseSysEntity {
  purpose: string;
  contacts: string;
  phoneNumber: string;
}

