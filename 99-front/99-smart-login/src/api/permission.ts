import type { MenuListResult } from '@/api/model/permissionModel';
import { request } from '@/utils/request';
import { PermissionSpaceModel, listPermissionSpaceResult } from "@/api/model/permissionSpace";

const Api = {
  CreatePermissionSpace: '/api/permission/create',
  ListPermissionSpace: '/api/permission/list',
  MenuList: '/get-menu-list',
};

//权限空间
export function createPermissionSpace(data: PermissionSpaceModel) {
  return request.post<PermissionSpaceModel>({
    url: Api.CreatePermissionSpace,
    data,
  });
}
export function listPermissionSpace() {
  return request.post<listPermissionSpaceResult>({
    url: Api.ListPermissionSpace,
  });
}
export function getMenuList() {
  return request.get<MenuListResult>({
    url: Api.MenuList,
  });
}
