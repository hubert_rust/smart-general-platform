export interface listPermissionSpaceResult {
  list: Array<PermissionSpaceModel>;
}
export interface PermissionSpaceModel {
  spaceName: string;
  spaceCode: string,
  remark: string;
  createBy: string;
  updateBy: string;
  createTime: Date;
  updateTime: Date;
}
