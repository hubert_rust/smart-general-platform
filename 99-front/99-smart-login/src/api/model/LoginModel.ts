export interface LoginModel {
  symmetric?: string;
  agent?: string; // Desktop
  username: string;
  password: string;

  authType?: string;
  // 验证码等信息
  captcha?: string; //

  verificationCategory?: string;
  otpCaptcha?: string; //
  remeberMe?: boolean;

  loginType?: string;
}
