import 'nprogress/nprogress.css'; // progress bar style

import NProgress from 'nprogress'; // progress bar
import { MessagePlugin } from 'tdesign-vue-next';
import { RouteRecordRaw } from 'vue-router';

import { TOKEN_NAME } from '@/config/global';
import router from '@/router';

NProgress.configure({ showSpinner: false });

router.beforeEach(async (to, from, next) => {
  next();
  return;
});

router.afterEach((to) => {
  if (to.path === '/login') {
    console.log('router.afterEach')
  }
  NProgress.done();
});
