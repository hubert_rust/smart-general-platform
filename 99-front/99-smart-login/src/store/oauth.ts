import jwt_decode from 'jwt-decode';
import { defineStore, Store } from 'pinia';

import type {
  OAuth2IdToken,
  OAuth2Token,
} from '@/api/declarations/request/http';
import type {
  AccessPrincipal,
  SocialSource,
} from '@/api/declarations/secure/authentication'
import { CommonConfig } from '@/config/global';
import { useCryptoStore } from '@/store/crypto';
import { moment } from '@/utils/plugins';
import { UserErrorStatus } from "@/api/declarations";
import {passwordFlow} from "@/api/oauth";

export const oauthStore = defineStore('Oauth', {
  state: () => ({
    access_token: '',
    expires_in: 0,
    refresh_token: '',
    license: '',
    openid: '',
    idToken: '',
    scope: '',
    token_type: '',
    errorTimes: 0,
    remainTimes: 0,
    locked: false,
    userId: '',
    userName: '',
    employeeId: '',
    avatar: '',
    roles: [] as Array<string>,
  }),

  getters: {
    isNotExpired: (state) => {
      const expires = moment().add(state.expires_in, 'seconds').valueOf();
      const flag = moment(expires).add(1, 'seconds').diff(moment(), 'seconds');
      return flag !== 0;
    },
    token(): string {
      if (CommonConfig.AUTO_REFRESH_TOKEN) {
        return this.access_token;
      }
      if (this.isNotExpired) {
        return this.access_token;
      }
      return '';
    },
  },

  actions: {
    getBearerToken(): string {
      return `Bearer ${this.token}`;
    },

    getAuthorizationHeader(): Record<string, string> {
      return { Authorization: this.getBearerToken(), 'X-Herodotus-Open-Id': this.userId };
    },

    setTokenInfo(data: OAuth2Token): void {
      this.access_token = data.access_token;
      this.expires_in = data.expires_in;
      this.refresh_token = data.refresh_token;
      this.license = data.license;
      this.scope = data.scope;
      this.token_type = data.token_type;
      if (data.id_token) {
        this.idToken = data.id_token;
        const jwt: OAuth2IdToken = jwt_decode(this.idToken);
        this.userId = jwt.openid;
        this.userName = jwt.sub;
        this.avatar = jwt.avatar;
        this.employeeId = jwt.employeeId;
        this.roles = jwt.roles;
      } else if (data.openid) {
        const crypto = useCryptoStore();
        this.openid = data.openid;
        const openid = crypto.decrypt(this.openid);
        const details = JSON.parse(openid);
        this.userId = details.userId;
        this.userName = details.userName;
        this.roles = details.roles;
        this.avatar = details.avatar;
        this.employeeId = details.employeeId;
      } else {
        console.error('Cannot fetch the use info from backend.');
      }
    },

    setUserErrorStatus(data: UserErrorStatus): void {
      this.remainTimes = data.remainTimes;
      this.errorTimes = data.errorTimes;
      this.locked = data.locked;
    },

    signIn(username: string, password: string) {
      const crypto = useCryptoStore();
      if (CommonConfig.USE_CRYPTO) {
        username = crypto.encrypt(username);
        password = crypto.encrypt(password);
      }
      /*return new Promise<boolean>((resolve, reject) => {
        api
          .oauth2()
          .passwordFlow(username, password, CommonConfig.UseCrypto)
          .then((response: any) => {
            if (response) {
              const data = response as OAuth2Token;
              this.setTokenInfo(data);
            }

            if (this.access_token) {
              resolve(true);
            } else {
              resolve(false);
            }
          })
          .catch((error: any) => {
            this.setErrorPrompt(error, username);
            reject(error);
          });
      });*/
    },
    refreshToken() {
      /*return new Promise<boolean>((resolve, reject) => {
        api
          .oauth2()
          .refreshTokenFlow(this.refresh_token, CommonConfig.UseCrypto)
          .then((response: any) => {
            if (response) {
              const data = response as OAuth2Token;
              this.setTokenInfo(data);
            }

            if (this.access_token) {
              resolve(true);
            } else {
              resolve(false);
            }
          })
          .catch((error: any) => {
            reject(error);
          });
      });*/
    },
    signOut(accessToken = '') {
      const token = accessToken || this.access_token;
      /*return new Promise<AxiosHttpResult>((resolve, reject) => {
        api
          .oauth2()
          .signOut(token)
          .then((response: any) => {
            resolve(response);
          })
          .catch((error: any) => {
            reject(error);
          });
      });*/
    },
    /*
    authorizationCode(code: string, state = '') {
      return new Promise<boolean>((resolve, reject) => {
        api
          .oauth2()
          .authorizationCodeFlow(code, variables.getRedirectUri(), state, variables.isUseCrypto())
          .then((response) => {
            if (response) {
              const data = response as OAuth2Token;
              this.setTokenInfo(data);
            }

            if (this.access_token) {
              resolve(true);
            } else {
              resolve(false);
            }
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    smsSignIn(mobile: string, code: string) {
      const crypto = useCryptoStore();
      if (variables.isUseCrypto()) {
        mobile = crypto.encrypt(mobile);
        code = crypto.encrypt(code);
      }
      return new Promise<boolean>((resolve, reject) => {
        api
          .oauth2()
          .socialCredentialsFlowBySms(mobile, code, variables.isUseCrypto())
          .then((response) => {
            if (response) {
              const data = response as unknown as OAuth2Token;
              this.setTokenInfo(data);
            }

            if (this.access_token) {
              resolve(true);
            } else {
              resolve(false);
            }
          })
          .catch((error) => {
            this.setErrorPrompt(error, mobile);
            reject(error);
          });
      });
    },

    socialSignIn(source: SocialSource, accessPrincipal: AccessPrincipal) {
      return new Promise<boolean>((resolve, reject) => {
        api
          .oauth2()
          .socialCredentialsFlowByJustAuth(source, accessPrincipal, variables.isUseCrypto())
          .then((response) => {
            if (response) {
              const data = response as OAuth2Token;
              this.setTokenInfo(data);
            }

            if (this.access_token) {
              resolve(true);
            } else {
              resolve(false);
            }
          })
          .catch((error) => {
            if (error.code && [40106, 40111].includes(error.code)) reject(error);
          });
      });
    }, */
  },
  persist: true,
});
