import { defineStore } from 'pinia';
import cookies from 'vue-cookies';

import { exchange, session } from '@/api/common';
import { Session } from '@/api/declarations';
import { CommonConfig } from '@/config/global';
import { SM2Utils, SM4Utils } from '@/utils/security/crypto';

const SECRET_KEY: string = CommonConfig.OAUTH2_CLIENT_SECRET;

export const useCryptoStore = defineStore('Crypto', {
  state: () => ({
    sessionId: '',
    key: '',
    state: '',
  }),

  actions: {
    setSessinId(sessionId: string) {
      this.sessionId = sessionId;
    },

    setKey(key: string) {
      this.key = SM4Utils.encrypt(key, SECRET_KEY);
    },

    getKey() {
      //return SM4Utils.decrypt(this.key, SECRET_KEY);
      return SECRET_KEY;
    },

    encrypt(content: string) {
      const key = this.getKey();
      return SM4Utils.encrypt(content, key);
    },

    decrypt(content: string) {
      const key = this.getKey();
      return SM4Utils.decrypt(content, key);
    },

    preLogin() {
      const data = {
        clientId: CommonConfig.OAUTH2_CLIENT_ID,
        clientSecret: CommonConfig.OAUTH2_CLIENT_SECRET,
        sessionId: '',
      };

      // 授权码模式,客户端发起授权,
      if (CommonConfig.GRANT_TYPE_AUTHORIZATION_CODE) {

      } else {
        session(data)
          .then((resp) => {
            if (resp.code === 20000) {
              const backendPublicKey = resp.data.publicKey;
              const pair = SM2Utils.createKeyPair();
              const encryptData = SM2Utils.encrypt(pair.publicKey, backendPublicKey);
              const { sessionId } = resp.data;

              exchange(sessionId, encryptData)
                .then((res) => {
                  if (res.code === 20000) {
                    const confidential = res.data as unknown as string;
                    const key = SM2Utils.decrypt(confidential, pair.privateKey);
                    this.setSessinId(sessionId);
                    this.setKey(key);
                  }
                  console.log(res);
                })
                .catch((error) => {
                  console.log(error);
                });
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    },
  },
  persist: {
    storage: sessionStorage,
  },
});
