import { FormRule } from 'tdesign-vue-next';


export const FORM_RULES: Record<string, FormRule[]> = {
  libName: [{ required: true, message: '请输资源库名称', type: 'error' }],
};
