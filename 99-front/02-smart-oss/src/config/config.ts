import { CommonConfig } from '@/config/global';

export const CONFIG = {
  // 将配置信息放在window对象上,使其变成全局都可以访问的
  // 管理端地址，也就本前端应用访问地址
  HOME_PAGE: 'http://123.60.219.212/oss',
  // 校验ticket地址
  AUTH_CHECK_TICKET: 'http://www.hicareer.net/sso/checkTicket',
  // 登出地址
  AUTH_LOGOUT_URL: 'http://www.hicareer.net/sso/signout',
  // 登录地址
  AUTH_LOGIN_ADDRESS: 'http://123.60.219.212/oss/sso/doLogin',
  AUTH_CENTER_URL: 'http://www.hicareer.net/sso/auth',
  // 前端登录地址
  AUTH_LOGIN_URL: 'http://123.60.219.212/sso/login',
  // 本前端访问后端前缀，参考nginx配置
  URL_PREFIX: '/oss',
};
export const DEV_CONFIG = {
  // 将配置信息放在window对象上,使其变成全局都可以访问的
  // 管理端地址，也就本前端应用访问地址
  HOME_PAGE: 'http://xsmart.com:3008/oss/',
  // 校验ticket地址
  AUTH_CHECK_TICKET: 'http://xsmart.com:7070/sso/checkTicket',
  // 登出地址
  AUTH_LOGOUT_URL: 'http://xsmart.com:7070/sso/signout',
  // 登录地址
  AUTH_LOGIN_ADDRESS: 'http://xsmart.com:7070/sso/doLogin',
  AUTH_CENTER_URL: 'http://xsmart.com:7070/sso/auth',
  // 前端登录地址
  AUTH_LOGIN_URL: `http://xsmart.com:7070/sso/login`,
  // 本前端访问后端前缀，参考nginx配置
  URL_PREFIX: '/api',
};
