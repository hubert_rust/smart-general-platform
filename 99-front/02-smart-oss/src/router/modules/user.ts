import { LogoutIcon } from 'tdesign-icons-vue-next';
import { shallowRef } from 'vue';

import Layout from '@/layouts/index.vue';

export default [
  {
    path: '/ucenter',
    name: 'ucenter',
    component: () => import('@/layouts/blank.vue'),
    redirect: '/ucenter',
    children: [
      {
        path: 'index',
        name: 'ucenterIndex',

        // component: () => import('@/layouts/blank.vue'),
        component: () => import('@/pages/user/ucenter.vue'),
        meta: { title: '个人中心' },
      },
    ],
  },
  {
    path: '/redirect',
    name: 'redirect',
    component: () => import('@/layouts/blank.vue'),
    redirect: '/redirect',
    children: [
      {
        path: 'index',
        name: 'redirectIndex',

        // component: () => import('@/layouts/blank.vue'),
        component: () => import('@/pages/redirect/index.vue'),
        meta: { title: '返回' },
      },
    ],
  },
  {
    path: '/fav',
    name: 'fav',
    component: Layout,
    redirect: '/fav/index',
    meta: { single: true, title: { zh_CN: '收藏夹', en_US: 'User Center' }, icon: 'star' },
    children: [
      {
        path: 'index',
        name: 'FavIndex',
        component: () => import('@/pages/fav/index.vue'),
        meta: { title: { zh_CN: '个人中心', en_US: 'User Center' } },
      },
    ],
  },

  /* {
    path: '/pub',
    name: 'pub',
    component: Layout,
    redirect: '/pub/index',
    meta: { single: true, title: { zh_CN: '已发布的资料库', en_US: 'User Center' }, icon: 'relativity' },
    children: [
      {
        path: 'index',
        name: 'pubIndex',
        component: () => import('@/pages/release/index.vue'),
        meta: { title: { zh_CN: '个人中心', en_US: 'User Center' } },
      },
    ],
  }, */
  /*  {
    path: '/shareManage',
    name: 'shareManage',
    component: Layout,
    redirect: '/shareManage/index',
    meta: { title: { zh_CN: '共享管理', en_US: 'User Center' }, icon: 'setting-1' },
    children: [
      {
        path: 'index',
        name: 'FileIndex',
        component: () => import('@/pages/user/index.vue'),
        meta: { title: { zh_CN: '#资料库', en_US: 'User Center' } },
      },
      {
        path: 'catalog',
        name: 'CatalogIndex',
        component: () => import('@/pages/user/index.vue'),
        meta: { title: { zh_CN: '#文件夹', en_US: 'User Center' } },
      },
      {
        path: 'linkIndex',
        name: 'LinkIndex',
        // component: () => import('@/pages/user/index.vue'),
        component: () => import('@/pages/dashboard/base/index.vue'),
        meta: { title: { zh_CN: '#链接', en_US: 'User Center' } },
      },
    ],
  }, */
  {
    path: '/act',
    name: 'act',
    component: Layout,
    redirect: '/act/index',
    meta: { single: true, title: { zh_CN: '文件活动', en_US: 'User Center' }, icon: 'tree-list' },
    children: [
      {
        path: 'index',
        name: 'ActIndex',
        component: () => import('@/pages/act/index.vue'),
        meta: { title: { zh_CN: '个人中心', en_US: 'User Center' } },
      },
    ],
  },
  {
    path: '/recycle',
    name: 'recycle',
    component: Layout,
    redirect: '/recycle/index',
    meta: { single: true, title: { zh_CN: '回收站', en_US: 'User Center' }, icon: 'delete-time' },
    children: [
      {
        path: 'index',
        name: 'RecycleIndex',
        component: () => import('@/pages/recycle/index.vue'),
        meta: { title: { zh_CN: '回收站', en_US: 'User Center' } },
      },
    ],
  },
];
