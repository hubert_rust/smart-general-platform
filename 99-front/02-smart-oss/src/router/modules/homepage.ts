import { DashboardIcon, GitRepositoryIcon, ShareIcon, UsergroupIcon, UserIcon } from 'tdesign-icons-vue-next';
import { shallowRef } from 'vue';

import Layout from '@/layouts/index.vue';

export default [
  {
    path: '/lib',
    component: Layout,
    redirect: '/lib/index',
    name: 'lib',
    meta: {
      single: true,
      title: {
        zh_CN: '我的资料库',
        en_US: 'Dashboard',
      },
      icon: shallowRef(UserIcon),
      orderNo: 0,
    },
    children: [

      {
        path: 'index',
        name: 'LibIndex',
        component: () => import('@/pages/lib/index.vue'),
        meta: {
          title: {
            zh_CN: '概览仪表盘',
            en_US: 'Overview',
          },
        },
      },

      {
        path: 'index/:id',
        name: 'LibIndexId',
        component: () => import('@/pages/lib/index.vue'),
        meta: {
          title: {
            zh_CN: '概览仪表盘',
            en_US: 'Overview',
          },
        },
      },
    ],
  },
  {
    path: '/tome',
    component: Layout,
    redirect: '/tome/index',
    name: 'tome',
    meta: {
      single: true,
      title: {
        zh_CN: '共享给我的',
        en_US: '111',
      },
      icon: shallowRef(ShareIcon),
      orderNo: 0,
    },
    children: [
      {
        path: 'index',
        name: 'TomeIndex',
        component: () => import('@/pages/tome/index.vue'),
        meta: {
          title: {
            zh_CN: '统计报表',
            en_US: 'Dashboard Detail',
          },
        },
      },
    ],
  },
  /* {
    path: '/group',
    component: Layout,
    redirect: '/group/1',
    name: 'group',
    meta: {
      title: {
        zh_CN: '群组共享',
        en_US: '111',
      },
      icon: shallowRef(UsergroupIcon),
      orderNo: 0,
    },
    children: [
      {
        path: '1',
        name: 'groupIndex',
        component: () => import('@/pages/group/index.vue'),
        meta: {
          title: {
            zh_CN: '#所有群组',
            en_US: 'Dashboard Detail',
          },
        },
      },

      {
        path: '2',
        name: 'xx',
        component: () => import('@/pages/group/index.vue'),
        meta: {
          title: {
            zh_CN: '#xx',
            en_US: 'Dashboard Detail',
          },
        },

      },
    ],
  }, */
  /* {
    path: '/dept',
    component: Layout,
    redirect: '/dept/1',
    name: 'dept',
    meta: {
      title: {
        zh_CN: '组织共享',
        en_US: '111',
      },
      icon: shallowRef(UsergroupIcon),
      orderNo: 0,
    },
    children: [
      {
        path: '1',
        name: 'deptIndex1',
        component: () => import('@/pages/group/index.vue'),
        meta: {
          title: {
            zh_CN: '#部门1',
            en_US: 'Dashboard Detail',
          },
        },
      },

      {
        path: '2',
        name: 'xx',
        component: () => import('@/pages/group/index.vue'),
        meta: {
          title: {
            zh_CN: '#xx',
            en_US: 'Dashboard Detail',
          },
        },

      },
    ],
  }, */
  /*  {
    path: '/server',
    component: Layout,
    redirect: '/server/index',
    name: 'server',
    meta: {
      single: true,
      title: {
        zh_CN: '其他服务器共享',
        en_US: '111',
      },
      icon: shallowRef(GitRepositoryIcon),
      orderNo: 0,
    },
    children: [
      {
        path: 'index',
        name: 'serverIndex',
        component: () => import('@/pages/dashboard/detail/index.vue'),
        meta: {
          title: {
            zh_CN: '统计报表',
            en_US: 'Dashboard Detail',
          },
        },
      },
    ],
  }, */
];
