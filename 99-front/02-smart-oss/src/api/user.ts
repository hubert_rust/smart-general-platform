import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;

const Api = {


  UserInfo: `${prefix}/user/userInfo`,
  UserMenu: `${prefix}/user/userMenu`,
  UserAppList: `${prefix}/user/apps`,



};







export async function getLoginUserInfo() {
  return request.post({
    url: `${Api.UserInfo}`,
  });
}
export function getLoginUserMenu() {
  return request.post({
    url: `${Api.UserMenu}`,
  });
}
