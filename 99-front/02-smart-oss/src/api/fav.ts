import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  MyFav: `${prefix}/fav/myFav`,
  RemoveFavApi: `${prefix}/fav/cancelStar`,
};

export function myFavApi() {
  return request.post({
    url: `${Api.MyFav}`,
  });
}

export function removeFavApi(id: any) {
  return request.post({
    url: `${Api.RemoveFavApi}/${id}`,
  });
}
