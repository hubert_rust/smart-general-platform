import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  MyRecycle: `${prefix}/recycle/myRecycle`,
  RecycleRestore: `${prefix}/recycle/restore`,
  RemoveApi: `${prefix}/recycle/removeFile`,
};

export function myRecycleApi() {
  return request.post({
    url: `${Api.MyRecycle}`,
  });
}
export function restoreApi(list: any) {
  return request.post({
    url: `${Api.RecycleRestore}`,
    data: list,
  });
}
export function removeApi(list: any) {
  return request.post({
    url: `${Api.RemoveApi}`,
    data: list,
  });
}

