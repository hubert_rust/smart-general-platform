
import { request } from '@/utils/request';

export async function ssoTicketCheck(url: string) {
  return request.post({
    url,
    data: {},
  });
}

// sso
