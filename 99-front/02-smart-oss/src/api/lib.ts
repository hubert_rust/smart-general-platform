import { CommonConfig } from '@/config/global';
import { request } from '@/utils/request';

const prefix = CommonConfig.URL_PREFIX;
const Api = {
  CreateLib: `${prefix}/lib/addLib`,
  CreateFolder: `${prefix}/lib/addFolder`,
  LibRename: `${prefix}/lib/rename`,
  RemoveFile: `${prefix}/lib/removeFile`,
  RemoveLib: `${prefix}/lib/removeLib`,
  MyLib: `${prefix}/lib/mylib`,
  MyFav: `${prefix}/lib/myFav`,
  LibBrowse: `${prefix}/lib/libBrowse`,
  AddStar: `${prefix}/lib/addStar`,
  CancelStar: `${prefix}/lib/cancelStar`,
};
export function removeLibApi(list: any) {
  return request.post({
    url: `${Api.RemoveLib}`,
    data: list,
  });
}
export function removeFileApi(list: any) {
  return request.post({
    url: `${Api.RemoveFile}`,
    data: list,
  });
}
export function addStarApi(id: any) {
  return request.post({
    url: `${Api.AddStar}/${id}`,
  });
}
export function cancelStarApi(id: any) {
  return request.post({
    url: `${Api.CancelStar}/${id}`,
  });
}
export function libAdd(data: any) {
  return request.post({
    url: `${Api.CreateLib}`,
    data,
  });
}
export function folderAdd(data: any) {
  return request.post({
    url: `${Api.CreateFolder}`,
    data,
  });
}
export function myFavApi() {
  return request.post({
    url: `${Api.MyFav}`,
  });
}

export function mylibApi() {
  return request.post({
    url: `${Api.MyLib}`,
  });
}
export function libBrowseApi(id: string) {
  return request.post({
    url: `${Api.LibBrowse}/${id}`,
  });
}
export function libRename(data: any) {
  return request.post({
    url: `${Api.LibRename}`,
    data,
  });
}
