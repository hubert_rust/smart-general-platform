export function getCookie(key: string) {
  const items = document.cookie.split(';');
  for (let i = 0; i < items.length; i++) {
    const cooks = items[i].split('=');
    const name = cooks[0];
    if (key === name) {
      return cooks[1];
    }
  }
  return null;
}
