import 'nprogress/nprogress.css'; // progress bar style

import NProgress from 'nprogress'; // progress bar
import { MessagePlugin } from 'tdesign-vue-next';
import { RouteRecordRaw } from 'vue-router';

import { CommonConfig } from '@/config/global';
import router from '@/router';
import { commonStore, getPermissionStore, useUserStore } from '@/store';
import { getCookie } from '@/utils/cookie';
import { PAGE_NOT_FOUND_ROUTE } from '@/utils/route/constant';

NProgress.configure({ showSpinner: false });
const parseHref = (): string => {
  const val: Array<string> = window.location.href.split('?');
  if (val.length >= 2) {
    const ticket: Array<string> = val[1].split('=');
    if (ticket.length >= 2) {
      return ticket[1];
    }
  }
  return '';
};
router.beforeEach(async (to, from, next) => {
  NProgress.start();

  const permissionStore = getPermissionStore();
  const { whiteListRouters } = permissionStore;

  const userStore = useUserStore();
  const token = userStore.getToken();
  const salt = userStore.getSalt();
  const cook = getCookie(CommonConfig.COOKIE_KEY);

  if (to.path === '/redirect/index') {
    next();
    return;
  }
  if (CommonConfig.PROTOCOL === 'jwt') {
    if (!token) {
      location.href = `${CommonConfig.AUTH_LOGIN_URL}?apt=user&client=${CommonConfig.ADMIN_CLIENT_ID}`;
    }
    else {
      if (userStore.getCurrentUser().account){
        next();
        return;
      }
    }
  }

  if (true) {
    try {
      // await userStore.getUserInfo();

      const { asyncRoutes } = permissionStore;

      if (asyncRoutes && asyncRoutes.length === 0) {
        const routeList = await permissionStore.buildAsyncRoutes();
        routeList.forEach((item: RouteRecordRaw) => {
          router.addRoute(item);
        });

        if (to.name === PAGE_NOT_FOUND_ROUTE.name) {
          // 动态添加路由后，此处应当重定向到fullPath，否则会加载404页面内容
          next({ path: to.fullPath, replace: true, query: to.query });
        } else {
          const redirect = decodeURIComponent((from.query.redirect || to.path) as string);
          next(to.path === redirect ? { ...to, replace: true } : { path: redirect });
          return;
        }
      }
      if (router.hasRoute(to.name)) {
        next();
      } else {
        next(`/`);
      }
    } catch (error) {
      MessagePlugin.error(error.message);
      if (CommonConfig.PROTOCOL === 'jwt') {
        if (true) {
          location.href = `${CommonConfig.AUTH_LOGIN_URL}?apt=user&client=${CommonConfig.ADMIN_CLIENT_ID}`;
        }
      }
      /* next({
        path: '/login',
        query: { redirect: encodeURIComponent(to.fullPath) },
      }); */
      NProgress.done();
    }
  }
  /* white list router */
  if (whiteListRouters.indexOf(to.path) !== -1) {
    next();
  } else {
    next({
      path: '/login',
      query: { redirect: encodeURIComponent(to.fullPath) },
    });
  }
  NProgress.done();
});

router.afterEach((to) => {
  const common = commonStore();
  if (to.path === '/lib/index') {
    common.setCurrentMenu('lib');
    console.log('>>>> permission, lib');
  } else if (to.path === '/tome/index') {
    common.setCurrentMenu('tome');
    console.log('>>>> permission, tome');
  }
  if (to.path === '/login') {
    const userStore = useUserStore();
    const permissionStore = getPermissionStore();

    userStore.logout();
    permissionStore.restoreRoutes();
  }
  NProgress.done();
});
