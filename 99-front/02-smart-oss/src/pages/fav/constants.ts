import { FormRule, PrimaryTableCol, TableRowData } from "tdesign-vue-next";


export const FORM_RULES: Record<string, FormRule[]> = {
  libName: [{ required: true, message: '请输资源库名称', type: 'error' }],
};
export const FORM_FOLDER_RULES: Record<string, FormRule[]> = {
  fileName: [{ required: true, message: '请输文件夹名称', type: 'error' }],
};
export const COLUMNS: PrimaryTableCol<TableRowData>[] = [
  // { colKey: 'row-select', type: 'multiple', width: 64, fixed: 'left' },
  {
    title: '',
    align: 'left',
    width: 35,
    colKey: 'star',
    fixed: 'left',
  },
  {
    title: '名称',
    align: 'left',
    width: 260,
    colKey: 'fileName',
    fixed: 'left',
  },

  {
    title: '资料库',
    align: 'left',
    width: 80,
    colKey: 'libName',
    fixed: 'left',
  },
  {
    title: '更新时间',
    align: 'left',
    width: 100,
    colKey: 'logType',
    fixed: 'left',
  },
  {
    title: '',
    align: 'left',
    width: 60,
    colKey: 'commandVisible',
    fixed: 'left',
  },
]
export const NEW_OPTS = [
  { content: '新建文件', value: 1 },
  { content: '操作二', value: 2 },
  { content: '操作三', value: 3, divider: true },
  { content: '操作四', value: 4 },
];
export const UPLOAD_OPTS = [
  { content: '操作一', value: 1 },
  { content: '操作二', value: 2 },
  { content: '操作三', value: 3, divider: true },
  { content: '操作四', value: 4 },
];
