import { defineStore } from 'pinia';

import { exchange, session } from '@/api/common';
import { CommonConfig } from '@/config/global';
import { SM2Utils, SM4Utils } from '@/utils/security/crypto';

const SECRET_KEY: string = CommonConfig.OAUTH2_CLIENT_SECRET;

export const useCryptoStore = defineStore('Crypto', {
  state: () => ({
    sessionId: '',
    key: '',
    state: '',
  }),

  actions: {
    setSessinId(sessionId: string) {
      this.sessionId = sessionId;
    },

    setKey(key: string) {
      this.key = SM4Utils.encrypt(key, SECRET_KEY);
    },

    getKey() {
      return SM4Utils.decrypt(this.key, SECRET_KEY);
    },

    encrypt(content: string) {
      // smart-auth
      // const key = this.getKey();
      const key = SECRET_KEY;
      return SM4Utils.encrypt(content, key);
    },

    decrypt(content: string) {
      // smart-auth
      // const key = this.getKey();
      const key = SECRET_KEY;
      return SM4Utils.decrypt(content, key);
    },

    preLogin() {
      // smart-auth
      if (true) return;
      const data = {
        clientId: CommonConfig.OAUTH2_CLIENT_ID,
        clientSecret: CommonConfig.OAUTH2_CLIENT_SECRET,
        sessionId: '',
      };
      session(data)
        .then((resp) => {
          if (resp.code === 20000) {
            const backendPublicKey = resp.data.publicKey;
            const pair = SM2Utils.createKeyPair();
            const encryptData = SM2Utils.encrypt(pair.publicKey, backendPublicKey);
            const { sessionId } = resp.data;

            exchange(sessionId, encryptData)
              .then((res) => {
                if (res.code === 20000) {
                  const confidential = res.data as unknown as string;
                  const key = SM2Utils.decrypt(confidential, pair.privateKey);
                  this.setSessinId(sessionId);
                  this.setKey(key);
                }
                console.log(res);
              })
              .catch((error) => {
                console.log(error);
              });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    },
  },
  persist: {
    storage: sessionStorage,
  },
});
