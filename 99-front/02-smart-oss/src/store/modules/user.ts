import { defineStore } from 'pinia';
import * as querystring from 'querystring';
import { MessagePlugin } from 'tdesign-vue-next';

import { ssoTicketCheck } from '@/api/oauth';
import { getLoginUserInfo } from '@/api/user';
import { CommonConfig } from '@/config/global';
import { store, usePermissionStore } from '@/store';
import type { UserInfo } from '@/types/interface';
import { getCookie } from '@/utils/cookie';

const InitUserInfo: UserInfo = {
  account: '', // 用户名，用于展示在页面右上角头像处
  realName: '',
  gender: '',
  name: '',
  roles: [], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
};

export const useUserStore = defineStore('user', {
  state: () => ({
    salt: '',
    token: '', // 默认token不走权限
    userInfo: { ...InitUserInfo },
  }),
  getters: {
    roles: (state) => {
      return state.userInfo?.roles;
    },
  },
  actions: {
    getCurrentUser() {
      return this.userInfo;
    },
    async checkTicket(url: string) {
      const nextUrl = await ssoTicketCheck(url).then((res: any) => {
        let url = window.location.href.split('?')[0];
        const search = window.location.href.split('?')[1];
        const query: any = querystring.parse(search);

        if (res.code === 20000) {
          const cookie = getCookie('SMART_TOKEN');
          this.cook = cookie;
          delete query.ticket;
          const newSearch = querystring.encode(query);
          if (newSearch) {
            url = `${url}?=${newSearch}`;
          }
          console.log('ticket check success');
          return url;
        }
        MessagePlugin.error('ticket校验失败');
        location.href = `${CommonConfig.AUTH_CENTER_URL}?apt=mgt&client=${CommonConfig.ADMIN_CLIENT_ID}`;
        return null;
      });
      return nextUrl;
    },

    async login(userInfo: Record<string, unknown>) {
      const mockLogin = async (userInfo: Record<string, unknown>) => {
        // 登录请求流程
        console.log(`用户信息:`, userInfo);
        // const { account, password } = userInfo;
        // if (account !== 'td') {
        //   return {
        //     code: 401,
        //     message: '账号不存在',
        //   };
        // }
        // if (['main_', 'dev_'].indexOf(password) === -1) {
        //   return {
        //     code: 401,
        //     message: '密码错误',
        //   };
        // }
        // const token = {
        //   main_: 'main_token',
        //   dev_: 'dev_token',
        // }[password];
        return {
          code: 200,
          message: '登录成功',
          data: 'main_token',
        };
      };

      const res = await mockLogin(userInfo);
      if (res.code === 200) {
        this.token = res.data;
      } else {
        throw res;
      }
    },
    // 登录后使用此接口
    async getUserInfo() {
      await getLoginUserInfo().then((res: any) => {
        console.log(res);
        if (res.code === 20000) {
          this.userInfo = res.data.user;
          this.realm = res.data.currentWks;
          this.agents = res.data.agents;
          this.selfs = res.data.selfs;
          this.menus = res.data.menus;
        } else {
          MessagePlugin.error('加载用户信息失败!');
        }
      });

      /* const mockRemoteUserInfo = async (token: string) => {
        if (token === 'main_token') {
          return {
            name: 'Tencent',
            roles: ['all'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
          };
        }
        return {
          name: 'td_dev',
          roles: ['UserIndex', 'DashboardBase', 'login'], // 前端权限模型使用 如果使用请配置modules/permission-fe.ts使用
        };
      };
      const res = await mockRemoteUserInfo(this[TOKEN_NAME]);
*/
      // this.userInfo = res;
    },
    async logout() {
      this.token = '';
      this.userInfo = { ...InitUserInfo };
    },
    async setToken(token: string) {
      this.token = token;
    },
    getToken(): string {
      return this.token;
    },
    async setSalt(salt: string) {
      this.salt = salt;
    },
    getSalt(): string {
      return this.salt;
    },
  },
  persist: {
    afterRestore: () => {
      const permissionStore = usePermissionStore();
      permissionStore.initRoutes();
    },
    key: 'user',
    paths: ['token', 'salt'],
  },
});

export function getUserStore() {
  return useUserStore(store);
}
