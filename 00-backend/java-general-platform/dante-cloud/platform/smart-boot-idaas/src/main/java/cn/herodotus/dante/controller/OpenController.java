package cn.herodotus.dante.controller;

import cn.herodotus.dante.realm.entity.ConfigEntity;
import cn.herodotus.dante.realm.entity.DeptEntity;
import cn.herodotus.dante.realm.entity.GroupEntity;
import cn.herodotus.dante.realm.service.ConfigService;
import cn.herodotus.dante.realm.service.open.OpenApiService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/open")
@Tag(name = "开放接口")
@Validated
public class OpenController {
    @Resource
    private ConfigService configService;
    @Resource
    private OpenApiService openApiService;

    @Operation(summary = "平台认证模式", description = "平台认证模式")
    @PostMapping("/grantType")
    public Result<ConfigEntity> authenticationMode() {
        ConfigEntity entity = configService.authorizationGrantType();
        return Result.success(Feedback.OK, entity);
    }

    @Operation(summary = "用户所在群组列表", description = "用户所在群组列表")
    @PostMapping("/myGroups")
    public Result<List<GroupEntity>> myGroups() {
        List<GroupEntity> list = openApiService.myGroups();
        return Result.success(Feedback.OK, list);
    }
    @Operation(summary = "用户所在群组列表", description = "用户所在群组列表")
    @PostMapping("/deptTree")
    public Result<List<DeptEntity>> deptTree() {
        List<DeptEntity> Tree = openApiService.deptTree();
        return Result.success(Feedback.OK, Tree);
    }
}
