package cn.herodotus.dante.aspect;

import cn.herodotus.dante.core.help.Browser;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.SmartLogEntity;
import cn.herodotus.dante.redis.stream.core.MessagePublisher;
import cn.herodotus.dante.redis.stream.core.bean.CommonMessage;
import cn.herodotus.dante.starter.SpringMVCUtil;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Sets;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Slf4j
@Aspect
@Component
public class CommonControllerAspect {
    private static final Set<String> NOT_LOG_SET = Sets.newHashSet("initConverter", "initBinder", "handleUser", "get");

    @Value("${message.filter}")
    private String filters;
    private Set<String> filterSets = Sets.newHashSet();
    @Resource
    private MessagePublisher messagePublisher;

    @PostConstruct
    public void initFilter() {
        if (Strings.isNotEmpty(filters)) {
            String[] arr = filters.split(",");
            Arrays.stream(arr).forEach(v-> filterSets.add(v));
        }
    }

    public boolean noSetLog() {
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        String requestUrl = String.valueOf(request.getAttribute("org.springframework.web.servlet.HandlerMapping.bestMatchingPattern"));
        for (String filterSet : filterSets) {
            if (requestUrl.contains(filterSet)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 配置切入点,该方法无方法体,主要为方便同类中其他方法使用此处配置的切入点
     */

    //这里配置需要被记录的拦截规则
    @Pointcut("execution(* cn..controller..*Controller.*(..))")
    public void aspect() {
        log.info("============>>>execute aspect ...");
    }

    //这里配置不需要记录的拦截规则
    @Pointcut("!execution(* cn..controller..SystemInfoController.* (..)) ")
    public void notAspect() {
    }

    @Around("aspect() && notAspect()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        String methodName = pjp.getSignature().getName();
        if (NOT_LOG_SET.contains(methodName)) { //不需要记录日志的直接执行方法
            return pjp.proceed();
        }
        log.info("{}方法执行时执行记录.....", methodName);
        /**
         * 1.获取request信息
         * 2.从header中取出登录用户信息
         */
        Object result = null;// result的值就是被拦截方法的返回值
        Long st = System.currentTimeMillis();
        try {
            result = pjp.proceed();
            Long end = System.currentTimeMillis();
            if (!noSetLog()) {
                requestMessage(pjp, result, (end - st), RealmConstant.SUCCESS);
            }
            log.info(">>> time: {}", end - st);
            return result;
        } catch (Exception e) {
            log.error("log proceed error", e);
            if (!noSetLog()) {
                requestMessage(pjp,
                        e,
                        System.currentTimeMillis() - st,
                        RealmConstant.FAIL);
            }
            throw e;
        }
    }

    @AfterThrowing(pointcut = "aspect() && notAspect()", throwing = "e")
    public void throwss(JoinPoint joinPoint, Exception e) {
        log.error("方法异常时执行.....");
    }


    /*
     * @Author root
     * @Description 发送到消息队列
     * @Date 8:33 2023/9/28
     * @Param
     * @return
     **/
    private void requestMessage(ProceedingJoinPoint pjp,
                                Object result,
                                long span,
                                String requestResult) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        // 过滤
        String methodName = signature.getMethod().getName();
        for (String filterSet : filterSets) {
            if (methodName.contains(filterSet)) {
                return;
            }
        }

        long start = System.currentTimeMillis();
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();

        SmartLogEntity entity = new SmartLogEntity();
        if (RealmConstant.FAIL.equals(requestResult)) {
            entity.setRequestReturn(RealmConstant.FAIL);
            entity.setRequestResult("");
        }
        else {
            entity.setRequestReturn(RealmConstant.SUCCESS);
            entity.setRequestResult(JSON.toJSONString(result));
        }

        Operation annotation = signature.getMethod().getAnnotation(Operation.class);
        if (Objects.nonNull(annotation)) {
            entity.setOperationEvent(annotation.summary());
        }

        if (Objects.nonNull(pjp.getArgs()) && pjp.getArgs().length>0) {
            entity.setRequestParam(JSON.toJSONString(pjp.getArgs()[0]));
        }

        entity.setDeviceType(CommonSecurityContextHolder.getAgent());
        entity.setUid(CommonSecurityContextHolder.getUid());
        entity.setTid(CommonSecurityContextHolder.getTid());
        entity.setApt(CommonSecurityContextHolder.getApt());
        entity.setLogType(RealmConstant.LOG_TYPE_OPERATION);
        if (Objects.nonNull(CommonSecurityContextHolder.getUser())) {
            entity.setAccount(CommonSecurityContextHolder.getUser().getUsername());
        }

        String ip = SpringMVCUtil.getIpAddress(request);
        entity.setClientIp(ip);
        Browser browser = Browser.resolveBrowser(request);
        entity.setBrowse(browser.getName());
        entity.setDeviceSystem(browser.getPlatform());
        entity.setRequestMethod(request.getMethod());
        entity.setRequestUrl(request.getRequestURI());
        entity.setCreateBy(entity.getUid());
        entity.setClientId(CommonSecurityContextHolder.getCid());
        entity.setFinishTime(new Date());
        entity.setSpan((int)span);

        CommonMessage message = new CommonMessage();
        message.setMessageType(RealmConstant.MESSAGE_TYPE_LOG);
        message.setObject(entity);
        messagePublisher.publish(JSON.toJSONString(message));
        long end = System.currentTimeMillis();

        log.info(">>> requestMessage: {}", end - start);
    }

}
