package cn.herodotus.dante.dao;

import cn.herodotus.dante.entity.DemoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author root
 * @description TODO
 * @date 2023/11/15 16:47
 */
@Mapper
public interface DemoMapper  extends BaseMapper<DemoEntity> {
}
