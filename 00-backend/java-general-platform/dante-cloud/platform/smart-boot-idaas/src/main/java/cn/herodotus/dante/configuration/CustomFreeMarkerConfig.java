package cn.herodotus.dante.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.io.IOException;

@Configuration
public class CustomFreeMarkerConfig {


    @Value("${freemarker.template}")
    private String tplPath;

    @Bean(name = "viewResolver")
    public ViewResolver getViewResolver() {
        FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
        viewResolver.setCache(ApplicationContainer.INST.isCache());
        //viewResolver.setPrefix("/templates/");
        viewResolver.setPrefix("");
        viewResolver.setSuffix(".ftl");
        viewResolver.setOrder(1);
        viewResolver.setContentType("text/html;charset=UTF-8");
        return viewResolver;
    }


    @Bean
    @DependsOn({"applicationContainer"})
    public FreeMarkerConfigurer freemarkerConfig() throws IOException {

        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
        configurer.setTemplateLoaderPaths("file:" + ApplicationContainer.INST.getTplPath());
        configurer.setDefaultEncoding("UTF-8");
        return configurer;
    }
}
