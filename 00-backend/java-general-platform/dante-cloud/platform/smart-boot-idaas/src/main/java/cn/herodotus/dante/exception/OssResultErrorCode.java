package cn.herodotus.dante.exception;


import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "响应结果状态", description = "自定义错误码以及对应的、友好的错误信息")
public enum OssResultErrorCode {
    NAME_INVALID(OssErroCode.NAME_INVALID, "名称不能为空"),
    AUTH_SERVER_INFO(OssErroCode.AUTH_SERVER_INFO, "鉴权服务器: "),
    PARAM_INVALID(OssErroCode.PARAM_INVALID, "参数非法: ");


    @Schema(title = "结果代码")
    private final int code;
    @Schema(title = "结果信息")
    private final String message;


    OssResultErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
