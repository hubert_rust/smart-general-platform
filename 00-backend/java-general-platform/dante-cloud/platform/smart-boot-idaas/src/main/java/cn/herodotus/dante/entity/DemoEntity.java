package cn.herodotus.dante.entity;

import cn.herodotus.engine.data.mybatis.plus.entity.TenantEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author root
 * @description TODO
 * @date 2023/11/15 16:46
 */

@Data
@TableName(value = "tb_demo")
public class DemoEntity extends TenantEntity {


    private String name;
    private Integer age;

}
