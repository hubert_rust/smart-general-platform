package cn.herodotus.dante.controller;


import cn.herodotus.dante.realm.service.DictionaryService;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ServerOpenController {

    @Resource
    private DictionaryService dictionaryService;


    @Operation(summary = "认证中心地址", description = "认证中心地址")
    @PostMapping("/authAddress/{mode}")
    public Result getAuthAddr(@PathVariable("mode") String mode) {
        //smart-auth, tid
        /*String tid = "0";
        Specification<DictionaryEntity> spec = (root, query, cb)-> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("tid"), tid));
            predicates.add(cb.equal(root.get(""), identity));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
        }
        dictionaryService.findAll(spec);*/
        return null;
    }
}
