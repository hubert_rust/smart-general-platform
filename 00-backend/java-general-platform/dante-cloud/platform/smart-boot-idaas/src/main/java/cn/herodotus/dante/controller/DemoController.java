package cn.herodotus.dante.controller;

import cn.herodotus.dante.dao.DemoMapper;
import cn.herodotus.dante.entity.DemoEntity;
import cn.herodotus.dante.service.DemoServiceImpl;
import cn.herodotus.engine.data.core.dto.SingleResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * @author root
 * @description TODO
 * @date 2023/11/15 17:28
 */

@RestController
@RequestMapping("/demo")
@Tag(name = "开放接口")
@Validated
public class DemoController {

    @Resource
    private DemoMapper demoMapper;

    @Resource
    private DemoServiceImpl demoService;
    @PostMapping("insert")
    public SingleResponse insert(@RequestBody DemoEntity entity) {
        try {
            // entity.setId(IDGenUitil.getUUID(32));
            demoService.save(entity);
            return SingleResponse.buildSuccess();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        }
    }
}
