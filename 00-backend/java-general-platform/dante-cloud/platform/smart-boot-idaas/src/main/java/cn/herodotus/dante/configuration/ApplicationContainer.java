package cn.herodotus.dante.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApplicationContainer implements InitializingBean, DisposableBean {

    public static ApplicationContainer INST;

    @Value("${freemarker.template}")
    private String tplPath;
    @Value("${freemarker.cache}")
    private boolean cache;

    public String getTplPath() {
        return tplPath;
    }

    public boolean isCache() {
        return cache;
    }

    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ApplicationContainer.INST = this;

    }
}
