package cn.herodotus.dante.service;

import cn.herodotus.dante.dao.DemoMapper;
import cn.herodotus.dante.entity.DemoEntity;
import cn.herodotus.engine.data.mybatis.plus.service.BServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author root
 * @description TODO
 * @date 2023/11/15 16:47
 *
 */
@Service
public class DemoServiceImpl extends BServiceImpl<DemoMapper, DemoEntity> {

}
