(function () {
  // 将配置信息放在window对象上,使其变成全局都可以访问的
  window.config = {
    // BASE_URL: 'http://auth.abc.com:7070/sso/doLogin',
    // BASE_URL: 'http://123.60.219.212/sso/doLogin',
    BASE_URL: 'http://xsmart.com:7070/sso/doLogin',
    // BASE_URL: 'http://www.hicareer.net/sso/doLogin',
    PROTOCOL: 'jwt',
    DEBUG: 1,
  };
})();
// export default { baseUrl: 'http://192.168.137.248:7070/sso/doLogin' };
