2023-10-20:
user应用是应用容器，
oss是应用。
场景一、user先登录，获取到cookie；
其他应用打开的时候，sso/auth?redirect=http://oss, 带的cookie，后台重定向后oss域下也设置了cookie。

场景二、


2023-09-20:
自建应用，访问授权，是否所有用户可见
sso应用，访问授权，是否所有用户可见

?????怎么解决自建应用和SSO应用的授权
例如: 自建应用对用户A授权
然后该应用发布到SSO
用户A在应用授权中，对SSO应用是否可见

如果该SSO应用对用户A授权，在用户登录后，同时看到2个应用还是？
解决办法，对自建应用授权和SSO应用授权取合集。

#####应用说明：
应用分自建应用和SSO应用
最终，root和master下发布到sso的应用，整个平台所有用户在应用市场均可见
默认，all_show = true

2023-09-19: 
管理员：专门维护管理员表，只有管理员才能登录平台后台


2023-09-18: pending
？？？？？？root中管理员发布的sso应用，所有租户，用户空间可见
？？？？？？在其他master管理员中发布的sso应用，该注册账号下的所有用户空间可见

2023-09-04: pending
？？？？？？组织，部门的innerCode路径有问题，需要测试

2023-08-30: pending
？？？？？？配置菜单的时候，需要增加字段，2023-08-23

2023-08-28: pending
？？？？？？组织机构需要能查看授权的应用和角色

2023-08-23: 在管理端，通过root用户配置菜单的时候，需要分类多选
平台管理员，用户空间管理员，租户管理员
root是超级管理员，不用权限判断就直接获取所有菜单
root也可以分配分配账号，分配的时候，通过判断是平台管理员，就获取有"平台管理员"属性的菜单
同理，如果是用户空间管理员，获取的菜单就是带有"平台管理员"属性的菜单


应用发布通道：
1、root发布sso应用， 每个用户空间管理员都能看到，然后添加到
2、管理员发布sso应用，



2023-08-21:
??? 前端获取用户信息，如果没有返回currentWks，需要前端创建一个，否则，前端可能会一致获取

2023-08-08: 角色权限
用户角色如果是部门继承的角色，在用户详情中，【用户归属】中列表中不能删除撤销角色。


角色管理包括: 基本信息，权限管理，扩展字段管理
基本信息包括：角色信息，扩展字段，授权主体
权限管理包括：授权资源

数据权限在给角色授权接口权限的时候，显示数据权限

应用权限管理：这个是在用户登录后显示应用列表的时候用到，在接口访问的时候暂不校验。

#应用管理:
应用在哪里发布？ root账户发布的应用所有用户空间都能看到，，
parentId = -1的租户可以发布应用，只有该账户下的所有用户空间才看到

授权主体: 用户登录后查询到: roleList, groupList, deptList, 保存到redis
role  subject_type   identity  
r1    user           123
r1    group          789
r1    dept           1123

用户登录，通过dept，group，user查询到属于的角色，通过角色获取权限


role <-> authSubject <-> 
2023-08-04: 上传文件，5.0使用的jwttoken，6.0无法使用5.0的文件服务？？？

2023-08-04: 通过应用查询对应的菜单
clientId->查询应用，
菜单对应的是app，保存appid,  接口对应的是后台
///
平台开发类: 
例如SSO Server, 支付平台, 消息中台等

登录页面单独部署，vue前后端分离页面，
登录采用刷新页面POST提交方式，登录成功后重定向(redirect)。
登录地址，需要有clientId(即applicationId),
例如: http://192.168.1.100:7007/auth/123ab?redirect=http://192.168.1.100:3003/dashboard/base&back=/abc/123

登录成功后,sa-token返回session,
登录成功后，通过response.redirect()返回，





问题：
登录使用无刷post请求，后端redirect能正常到redirect页面？不能
表单submit提交是可以的。
无刷get请求，后端redirect能正常到redirect页面？不能

需要返回密码错误，用户不存在等提示信息，优选前后端分离方案：
需要在OPTIONS和消息请求中增加header: Access-Control-Allow-Origin  值为: server.allow-origin
统一返回json接口，
登录成功后，返回消息中写入cookie

但是登录成功，通过ticket获取信息时候（或者授权码），建议用刷新请求

////登录页面放到resourse下面
发生请求采用无刷post接口
前后端分离单点登录，需要先到/sso/auth，

一个请求怎么判断

////各应用前端（前后端分离）
场景1: 双击应用图标，先向sso中心发送/sso/auth请求(带client和redirect)，
sso中心redirect携带ticket，前端获取到ticket（在哪里获取，在首页页面），发送post请求，校验ticket
成功返回后，

//自建应用和SSO应用区别
自建应用，提供独立访问地址，单独访问，在一个用户空间有效，用户空间管理员发布
SSO应用登录平台(前端访问地址)都可以看到，谁来发布？
1: 平台管理员root, 可以选择tenant，可以发送到全部tenant(app需要标识)
2: parentId =-1的tenant可以向其他租户/空间设置应用

应用定义即属性：

属性包括: 应用菜单，应用对应接口



![image-20230802083107134](C:\Users\root\AppData\Roaming\Typora\typora-user-images\image-20230802083107134.png)



//

