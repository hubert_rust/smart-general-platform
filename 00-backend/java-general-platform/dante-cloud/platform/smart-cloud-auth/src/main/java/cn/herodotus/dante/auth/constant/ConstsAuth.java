package cn.herodotus.dante.auth.constant;

import static cn.herodotus.engine.assistant.core.definition.constants.BaseConstants.CACHE_TOKEN_BASE_PREFIX;

public interface ConstsAuth {
    String CACHE_NAME_TOKEN_SIGN_IN_FAILURE_LIMITED = CACHE_TOKEN_BASE_PREFIX + "sign_in:failure_limited:";
    String CACHE_NAME_TOKEN_LOCKED_USER_DETAIL = CACHE_TOKEN_BASE_PREFIX + "locked:user_details:";


    String PROTOCOL_STRATEGY_DEFAULT = "defaultProtocolStrategy";
    String PROTOCOL_STRATEGY_JWT = "jwtProtocolStrategy";
}
