package cn.herodotus.dante.auth.provider.impl;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.core.LoginCredential;
import cn.herodotus.dante.auth.exception.AuthErrorKey;
import cn.herodotus.dante.auth.provider.AbstractAuthenticationProvider;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.core.exception.AuthException;
import cn.herodotus.dante.realm.entity.UserAuthEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;

import java.util.Objects;

/**
 * @author root
 * @description TODO
 * @date 2023/8/14 9:09
 */
public class NormalAuthenticationProvider extends AbstractAuthenticationProvider {
    @Override
    public String getProviderName() {
        return  "normal" + PROVIDER_SUFFIX;
    }

    public NormalAuthenticationProvider(
            CommonAuthenticationRealm commonAuthenticationRealm,
            SymmetricCryptoProcessor symmetricCryptoProcessor) {
        this.commonAuthenticationRealm = commonAuthenticationRealm;
        this.symmetricCryptoProcessor = symmetricCryptoProcessor;
    }

    @Override
    public CommonUser doAuthenticate(LoginCredential authentication, CommonApplication application) {
        emptyUsernameValid(authentication.getUsername());
        emptyPasswordValid(authentication.getPassword());

        //获取用户信息，用户账户信息
        String secKey = ProtectConstant.SM4_SECRET;
        String account = symmetricCryptoProcessor.decrypt(authentication.getUsername(), secKey);
        UserAuthEntity userAuth
                = commonAuthenticationRealm.findUserAuth(account);
        if (Objects.isNull(userAuth)) {
            throw new AuthException("authException", AuthErrorKey.USER_NOT_EXIST);
        }
        if (!authentication.getPassword().equals(userAuth.getCredential())) {
            throw new AuthException("authException", AuthErrorKey.LOGIN_PASSWORD_ERROR);
        }
        // 用户状态
        UserEntity user = commonAuthenticationRealm.loadUserData(userAuth.getUid());

        // 登录限制

        // 登录日志
        CommonUser commonUser = new CommonUser();
        commonUser.setUser(user);
        commonUser.setUid(user.getId());
        commonUser.setTid(user.getTid());
        commonUser.setClientId(CommonSecurityContextHolder.getCid());
        //commonUser.setUser(user);

        return commonUser;
    }
}
