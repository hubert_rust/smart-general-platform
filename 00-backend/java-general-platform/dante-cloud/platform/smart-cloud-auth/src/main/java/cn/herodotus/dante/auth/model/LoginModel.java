package cn.herodotus.dante.auth.model;

import lombok.Data;

import java.io.Serializable;


@Data
public class LoginModel implements Serializable {
    private String agent;    //pc, wxmicro,
    //mobile: 短消息
    //password,
    //wxqrcode: 微信二维码,
    private String authType;
    private String username;
    private String password;
    private String captcha;
    private Boolean rememberMe;

    private String state;
    private String code;
}
