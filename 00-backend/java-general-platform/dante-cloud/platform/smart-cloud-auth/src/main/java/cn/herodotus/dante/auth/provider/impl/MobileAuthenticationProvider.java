package cn.herodotus.dante.auth.provider.impl;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.core.LoginCredential;
import cn.herodotus.dante.auth.provider.AbstractAuthenticationProvider;

/**
 * @author root
 * @description TODO
 * @date 2023/8/14 9:09
 */
public class MobileAuthenticationProvider extends AbstractAuthenticationProvider {
    @Override
    public String getProviderName() {
        return  "mobile" + PROVIDER_SUFFIX;
    }

    @Override
    public CommonUser doAuthenticate(LoginCredential authentication,
                                     CommonApplication application) {
        return null;
    }
}
