package cn.herodotus.dante.auth.exception;

import cn.herodotus.dante.core.exception.ErrorCodeDefine;

public interface AuthErroCode {

    int BASE = ErrorCodeDefine.AUTH_STAET;
    int USER_NOT_EXIST = BASE + 1;

    int LOGIN_USERNAME_NOT_EMPTY = BASE + 2;
    int LOGIN_PASSWORD_NOT_EMPTY = BASE + 3;
    int LOGIN_PASSWORD_ERROR = BASE + 4;
    int USER_NOT_LOGIN = BASE + 5;
    int INVALID_APP = BASE + 6;
    int JWT_PARSE_FAIL = BASE + 7;



}
