package cn.herodotus.dante.auth.protocols.common;

import cn.herodotus.dante.auth.core.LoginCredential;

/**
 * @author root
 * @description TODO
 * @date 2023/11/17 11:15
 */
public interface IProtocolStrategy {
    Object process(LoginCredential loginCredential);
}
