package cn.herodotus.dante.auth.event;

import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.SmartLogEntity;
import cn.herodotus.dante.redis.stream.core.bean.CommonMessage;
import cn.herodotus.dante.redis.stream.core.bean.ICommonMessageProcess;
import com.alibaba.fastjson2.JSON;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author root
 * @description TODO
 * @date 2023/8/16 10:58
 */
@Slf4j
@Component
public class CommonMessageProc implements ICommonMessageProcess {

    @Resource
    private CommonAuthenticationRealm commonAuthenticationRealm;
    @Override
    public void messageProc(String message) {
        log.info("[{}], message: {}",
                this.getClass().getSimpleName(), message);

        CommonMessage commonMessage
                = JSON.parseObject((String)JSON.parse(message), CommonMessage.class);
        if (RealmConstant.MESSAGE_TYPE_LOG.equals(commonMessage.getMessageType())) {
            SmartLogEntity smartLog
                    = (SmartLogEntity) JSON.to(SmartLogEntity.class, commonMessage.getObject());
            commonAuthenticationRealm.insertLoginLog(smartLog);
        }
    }
}
