package cn.herodotus.dante.auth.exception;


import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "响应结果状态", description = "自定义错误码以及对应的、友好的错误信息")
public enum AuthResultErrorCode {
    USER_NOT_EXIST(AuthErroCode.USER_NOT_EXIST, "用户不存在"),
    LOGIN_USERNAME_NOT_EMPTY(AuthErroCode.LOGIN_USERNAME_NOT_EMPTY, "用户账户不能为空"),
    LOGIN_PASSWOD_NOT_EMPTY(AuthErroCode.LOGIN_PASSWORD_NOT_EMPTY, "密码不能为空"),
    USER_NOT_LOGIN(AuthErroCode.USER_NOT_LOGIN, "用户未登录"),
    INVALID_APP(AuthErroCode.INVALID_APP, "非法应用"),
    JWT_PARSE_FAIL(AuthErroCode.JWT_PARSE_FAIL, "jwt解析失败: "),
    LOGIN_PASSWOD_ERROR(AuthErroCode.LOGIN_PASSWORD_ERROR, "密码不正确");



    @Schema(title = "结果代码")
    private final int code;
    @Schema(title = "结果信息")
    private final String message;


    AuthResultErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
