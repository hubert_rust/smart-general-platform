package cn.herodotus.dante.auth.core;

import cn.herodotus.dante.core.auth.CommonBaseUser;
import cn.herodotus.dante.core.auth.CommonCredentialsContainer;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import lombok.Data;

@Data
public class CommonUser extends CommonBaseUser implements CommonUserDetails, CommonCredentialsContainer {

    private UserEntity user;

}
