package cn.herodotus.dante.auth.core;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.engine.assistant.core.domain.CommonAuthentication;
import cn.herodotus.engine.assistant.core.domain.CommonGrantedAuthority;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;

@Data
public class LoginCredential implements CommonAuthentication {

    //String congress;
    // 客户端(PC等)发送的密钥相关信息，暂时保留

    // 应用类型默认为管理端

    // 登录必填
    String loginType = "normal";
    // 登录必填
    String authType;   //account, email, phone
    // 登录必填
    String protocol;   // default, jwt, auth2
    String phone;
    // 登录必填
    String agent;    //Desktop
    // 登录必填
    String username;
    // 登录必填
    String password;
    // 登录必填
    String redirect;
    String back;
    // 登录必填
    String client;
    String appType = RealmConstant.APP_TYPE_MGT;

    String accountAttr;
    String symmetric;

    String state;

    // 验证码等信息
    String captcha;    //

    //smart-auth: pending, 需要修改变量名
    String captchaCatalog;
    String otpCaptcha; //
    String remeberMe;
    String jwtToken;
    String onlineTicket;
    String provider;
    String code;
    String message = "success";
    //String instId;


    ArrayList<CommonGrantedAuthority> grantedAuthority;
    boolean authenticated;
    boolean roleAdministrators;
    String mobile;
    /**
     * BasicAuthentication.
     */
    public LoginCredential() {
    }

    /**
     * BasicAuthentication.
     */
    public LoginCredential(String username,String password,String authType) {
        this.username = username;
        this.password = password;
        this.authType = authType;
    }

    public ArrayList<CommonGrantedAuthority> getGrantedAuthority() {
        return grantedAuthority;
    }

    public void setGrantedAuthority(ArrayList<CommonGrantedAuthority> grantedAuthority) {
        this.grantedAuthority = grantedAuthority;
    }
    @Override
    public String getName() {
        return "Login Credential";
    }

    @Override
    public Collection<? extends CommonGrantedAuthority> getAuthorities() {
        return grantedAuthority;
    }

    @Override
    public Object getCredentials() {
        return this.getPassword();
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this.getUsername();
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
        this.authenticated = authenticated;

    }


}
