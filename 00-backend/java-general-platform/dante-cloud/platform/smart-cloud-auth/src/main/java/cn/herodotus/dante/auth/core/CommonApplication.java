package cn.herodotus.dante.auth.core;

import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/8/15 8:51
 */
@Data
public class CommonApplication implements Serializable {
    private String appName;
    private String clientId;
    private String appType;
    private String clientSecret;
    private String protocol;
    private String appAttr;
    private String authUri;
    private String logoutUris;
    private String redirectUris ;
    private List<String> redirectList = Lists.newArrayList();

}
