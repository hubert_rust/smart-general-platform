/*
 * Copyright [2022] [MaxKey of copyright http://www.maxkey.top]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.herodotus.dante.auth.provider;


import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.core.LoginCredential;

import java.util.concurrent.ConcurrentHashMap;


public class AuthenticationProviderFactory extends AbstractAuthenticationProvider {

	public AuthenticationProviderFactory() {
	}

	private  static ConcurrentHashMap<String,AbstractAuthenticationProvider> providers =
    									new ConcurrentHashMap<String,AbstractAuthenticationProvider>();
    
    @Override
    public CommonUser authenticate(LoginCredential authentication,
								   CommonApplication application){
    	if(authentication.getAuthType().equalsIgnoreCase("trusted")) {
    		//risk remove
    		return null;
    	}
    	AbstractAuthenticationProvider provider = providers.get(authentication.getAuthType() + PROVIDER_SUFFIX);
    	
    	return provider == null ? null : provider.doAuthenticate(authentication, application);
    }
    
    @Override
    public CommonUser authenticate(LoginCredential authentication,
								   CommonApplication application,
								   boolean trusted){
    	AbstractAuthenticationProvider provider = providers.get(AuthType.TRUSTED + PROVIDER_SUFFIX);
    	return provider.doAuthenticate(authentication, application);
    }
    
    public void addAuthenticationProvider(AbstractAuthenticationProvider provider) {
    	providers.put(provider.getProviderName(), provider);
    }

	@Override
	public String getProviderName() {
		return "AuthenticationProviderFactory";
	}

	@Override
	public CommonUser doAuthenticate(LoginCredential authentication, CommonApplication application) {
		//AuthenticationProvider Factory do nothing 
		return null;
	}
}
