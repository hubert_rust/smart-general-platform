package cn.herodotus.dante.auth.exception;

public interface AuthErrorKey {
    String REG_USER_EXIST = "regUserExist";
    String USER_NOT_EXIST = "userNoExit";

    String TYPE_NOT_MATCH = "typeNotMatch";

    String PARAM_INVALID = "paramInvalid";

    String LOGIN_USERNAME_NOT_EMPTY = "loginUserNameNotEmpty";
    String LOGIN_PASSWORD_NOT_EMPTY = "loginPasswordNotEmpty";
    String LOGIN_PASSWORD_ERROR = "loginPasswordError";
    String USER_NOT_LOGIN = "userNotLogin";
    String INVALID_APP = "invalidApp";
    String JWT_PARSE_FAIL = "jwtParseFail";
}
