/*
 * Copyright [2020] [MaxKey of copyright http://www.maxkey.top]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 *
 */

package cn.herodotus.dante.auth.protocols.base;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.core.constant.ConstsProtocols;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Tag(name = "1-2认证总地址文档模块")
@RestController
public class AuthorizeEndpoint extends AuthorizeBaseEndpoint {


    //all single sign on url
    @Operation(summary = "认证总地址接口", description = "参数应用ID，分发到不同应用的认证地址", method = "GET")
    @PostMapping("/auth/{clientId}")
    public Result authorize(
            HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable("clientId") String id) {

        CommonApplication app = getApp(null);
        if (ConstsProtocols.DEFAULT.equalsIgnoreCase(app.getProtocol())) {

        }
        else if (ConstsProtocols.JWT.equalsIgnoreCase(app.getProtocol())) {

        }
        else if (ConstsProtocols.CAS.equals(app.getProtocol())) {

        }
        else if (ConstsProtocols.OAUTH21.equals(app.getProtocol())) {

        }



        return Result.failure("不支持该协议:" + app.getProtocol());
    }

    @RequestMapping("/authz/refused")
    public ModelAndView refused() {
        ModelAndView modelAndView = new ModelAndView("authorize/authorize_refused");
		/*Apps app = (Apps)WebContext.getAttribute(WebConstants.AUTHORIZE_SIGN_ON_APP);
		app.transIconBase64();
		modelAndView.addObject("model", app);*/
        return modelAndView;
    }

}
