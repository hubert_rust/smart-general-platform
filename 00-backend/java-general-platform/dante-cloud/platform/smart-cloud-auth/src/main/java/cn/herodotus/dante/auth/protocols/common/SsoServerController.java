package cn.herodotus.dante.auth.protocols.common;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.LoginCredential;
import cn.herodotus.dante.auth.protocols.base.AuthorizeBaseEndpoint;
import cn.herodotus.dante.auth.provider.AuthenticationProviderFactory;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.configuration.ServerApplicationContainer;
import cn.herodotus.dante.core.constant.ConstsProtocols;
import cn.herodotus.dante.core.context.SaHolder;
import cn.herodotus.dante.core.context.model.SaRequest;
import cn.herodotus.dante.core.sign.SaSignUtil;
import cn.herodotus.dante.core.stp.StpUtil;
import cn.herodotus.dante.core.util.SaResult;
import cn.herodotus.dante.sso.config.SaSsoConfig;
import cn.herodotus.dante.sso.sso.SaSsoManager;
import cn.herodotus.dante.sso.sso.SaSsoProcessor;
import cn.herodotus.dante.sso.sso.name.ParamName;
import cn.herodotus.engine.assistant.core.domain.Result;
import com.dtflys.forest.Forest;
import com.google.common.base.Strings;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Slf4j
@RestController
public class SsoServerController extends AuthorizeBaseEndpoint {

	// 登录地址
	@Value("${sa-token.sso.login-url}")
	private String loginUrl;

	// 登出地址
	@Value("${sa-token.sso.logout-url}")
	private String logoutUrl;

	// auth中心
	@Value("${sa-token.sso.auth-url}")
	private String authUrl;

	// ticket校验中心
	@Value("${sa-token.sso.ticket-url}")
	private String checkTicketUrl;

	@Resource
	AuthenticationProviderFactory authenticationProviderFactory;
	@Resource
	CommonAuthenticationRealm commonAuthenticationRealm;

	@Resource
	private ProtocolStrategyContext protocolStrategyContext;

	/*
	 * SSO-Server端：处理所有SSO相关请求 
	 * 		http://{host}:{port}/sso/auth			-- 单点登录授权地址，接受参数：redirect=授权重定向地址 
	 * 		http://{host}:{port}/sso/doLogin		-- 账号密码登录接口，接受参数：name、pwd 
	 * 		http://{host}:{port}/sso/checkTicket	-- Ticket校验接口（isHttp=true时打开），接受参数：ticket=ticket码、ssoLogoutCall=单点注销回调地址 [可选] 
	 * 		http://{host}:{port}/sso/signout		-- 单点注销地址（isSlo=true时打开），接受参数：loginId=账号id、secretkey=接口调用秘钥 
	 */

	//认证服务器总入口，通过client区分协议




	@RequestMapping("/sso/signout")
	public Object signout() {
		return SaSsoProcessor.instance.serverDister();
	}

	@RequestMapping("/sso/checkTicket")
	public Object checkTicket() {
		return SaSsoProcessor.instance.serverDister();
	}
	@RequestMapping("/sso/doLogin")
	@ResponseBody
	public Object ssoLogin(@RequestBody LoginCredential loginCredential) {
		return protocolStrategyContext.process(loginCredential);

		/*CommonApplication app = getApp(loginCredential);
		CommonUser commonUser
				= authenticationProviderFactory.authenticate(loginCredential, app);


		String redirect = loginCredential.getRedirect();
		if (!Strings.isNullOrEmpty(redirect)) {
			for (String val : app.getRedirectList()) {
				if (redirect.indexOf(val) == 0) {

				}
			}
		}

		ResponseModel model = ResponseModel.builder()
				.uid(commonUser.getUid())
				.tid(commonUser.getTid())
				.appType(loginCredential.getAppType())
				.client(app.getClientId())
				.authUrl(authUrl)
				.logoutUrl(app.getLogoutUris())
				.redirect(redirect)
				.back(loginCredential.getBack())
				.build();

		StpUtil.login(commonUser.getUser().getId());
		return Result.success(Feedback.OK, model);
		// return loginProcess(loginCredential);*/
	}
	/*@RequestMapping("/sso/doLogin")
	@ResponseBody
	public Object ssoLoginParam(@RequestParam("username") String username,
								@RequestParam("password") String password,
								@RequestParam("agent") String agent,
								@RequestParam("authType") String authType,
								@RequestParam("identityType") String identityType,
								@RequestParam("captcha") String captcha,
								@RequestParam("captchaCatalog") String captchaCatalog,
								@RequestParam("symmetric") String symmetric,
								@RequestParam("phone") String phone) {
		LoginCredential loginCredential = new LoginCredential();
		loginCredential.setUsername(username);
		loginCredential.setPassword(password);
		loginCredential.setAgent(agent);
		loginCredential.setAuthType(authType);
		loginCredential.setCaptcha(captcha);
		loginCredential.setCaptchaCatalog(captchaCatalog);
		loginCredential.setSymmetric(symmetric);
		loginCredential.setIdentityType(identityType);
		loginCredential.setPhone(phone);

		return loginProcess(loginCredential);
	}*/


	// smart-auth
	/*@RequestMapping("/sso/doLogin")
	@ResponseBody
	public Object ssoLogin() {
		return SaSsoProcessor.instance.serverDister();
	}*/
	@RequestMapping("/sso/auth")
	@ResponseBody
	public Object ssoRequest(HttpServletRequest request, HttpServletResponse response) {
		CommonApplication app = getApp(null);
		log.info(">>> /sso/auth");

		String  uid
				= SaSsoProcessor.instance.ssoTemplate.getStpLogic().isLoginEnhance();
		if (Strings.isNullOrEmpty(uid)) {
			// 未登录
			try {
				ParamName paramName = SaSsoProcessor.instance.ssoTemplate.paramName;
				SaRequest req = SaHolder.getRequest();
				SaSsoConfig cfg = SaSsoManager.getConfig();
				String apt = req.getParam(paramName.appType);

				//smart-auth
				String redirect = req.getParam(paramName.redirect);
				if (Strings.isNullOrEmpty(redirect)) {
					redirect = ServerApplicationContainer.INST.getDefaultRedirect(apt);
				}
				redirect = (cfg.getAuthAddr() + "?apt="+ apt +"&client=" + app.getClientId() +"&redirect=" + redirect);
				// 跳转到登录页面，
				response.sendRedirect(redirect);
				return "";
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		if (ConstsProtocols.DEFAULT.equalsIgnoreCase(app.getProtocol())) {
			log.info(">>> /sso/auth, default");
			return SaSsoProcessor.instance.ssoAuth();
			//return SaSsoProcessor.instance.serverDister();
		}
		else if (ConstsProtocols.JWT.equalsIgnoreCase(app.getProtocol())) {

		}
		else if (ConstsProtocols.CAS.equals(app.getProtocol())) {

		}
		else if (ConstsProtocols.OAUTH21.equals(app.getProtocol())) {

		}

		return SaSsoProcessor.instance.serverDister();
	}
	// 配置SSO相关参数 
	@Autowired
	private void configSso(SaSsoConfig sso) {
		
		// 配置：未登录时返回的View 
		sso.setNotLoginView(() -> {
			ModelAndView model = new ModelAndView("sa-login.html");
			model.addObject("loginAddr", loginUrl);
			return model;
		});
		
		// 配置：登录处理函数 
		sso.setDoLoginHandle((name, pwd) -> {
			// 此处仅做模拟登录，真实环境应该查询数据进行登录 
			if("sa".equals(name) && "123456".equals(pwd)) {
				StpUtil.login(10001);
				return SaResult.ok("登录成功！").setData(StpUtil.getTokenValue());
			}
			//smart-auth
			StpUtil.login(10001);
			return Result.success("登录成功", StpUtil.getTokenValue());
			//return SaResult.ok("登录成功！").setData(StpUtil.getTokenValue());
			//return Result.failure("密码不正确", 1000, 500);
			//return SaResult.error("登录失败！");
		});
		
		// 配置 Http 请求处理器 （在模式三的单点注销功能下用到，如不需要可以注释掉） 
		sso.setSendHttp(url -> {
			try {
				// 发起 http 请求 
				System.out.println("------ 发起请求：" + url);
				return Forest.get(url).executeAsString();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		});
	}

	// 示例：获取数据接口（用于在模式三下，为 client 端开放拉取数据的接口）
	@RequestMapping("/sso/getData")
	public SaResult getData(String apiType, String loginId) {
		System.out.println("---------------- 获取数据 ----------------");
		System.out.println("apiType=" + apiType);
		System.out.println("loginId=" + loginId);

		// 校验签名：只有拥有正确秘钥发起的请求才能通过校验
		SaSignUtil.checkRequest(SaHolder.getRequest());

		// 自定义返回结果（模拟）
		return SaResult.ok()
				.set("id", loginId)
				.set("name", "LinXiaoYu")
				.set("sex", "女")
				.set("age", 18);
	}

}
