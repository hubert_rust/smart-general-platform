package cn.herodotus.dante.auth.realm;

import cn.herodotus.dante.core.help.Browser;
import cn.herodotus.dante.realm.entity.RoleEntity;
import cn.herodotus.dante.realm.entity.SmartLogEntity;
import cn.herodotus.dante.realm.entity.UserAuthEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.service.SmartLogService;
import cn.herodotus.dante.realm.service.UserAuthService;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.engine.assistant.core.domain.CommonGrantedAuthority;
import com.google.common.collect.Lists;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/8/14 9:37
 */
@Slf4j
public class CommonAuthenticationRealm {

    @Resource
    private SmartLogService smartLogService;
    @Resource
    private UserService userService;
    @Resource
    private UserAuthService userAuthService;

    public CommonAuthenticationRealm() {
    }

    public UserAuthEntity findUserAuth(String username) {
        return userAuthService.findByIdentity(username);
    }

    public UserEntity loadUserData(String uid) {
        return userService.findById(uid);
    }

    // 用户可使用应用列表
    public List<CommonGrantedAuthority> queryAuthorizedApps(String uid) {
        List<CommonGrantedAuthority> list = Lists.newArrayList();
        return list;
    }

    // 用户可使用角色列表
    public List<RoleEntity> queryAuthorizedRoles(String uid) {
        List<RoleEntity> list = Lists.newArrayList();
        return list;
    }

    public boolean insertLoginLog(SmartLogEntity smartLog) {
/*
        SmartLogEntity smartLogEntity = new SmartLogEntity();
        Browser browser = Browser.resolveBrowser();
        smartLogEntity.setBrowse(browser.getName());
        smartLogEntity.setDeviceSystem(browser.getPlatform());
*/
        smartLogService.save(smartLog);

        return true;
    }

    public boolean insertOperationLog() {
        SmartLogEntity smartLogEntity = new SmartLogEntity();
        Browser browser = Browser.resolveBrowser();
        smartLogEntity.setBrowse(browser.getName());
        smartLogEntity.setDeviceSystem(browser.getPlatform());
        smartLogService.save(smartLogEntity);
        return true;
    }


}
