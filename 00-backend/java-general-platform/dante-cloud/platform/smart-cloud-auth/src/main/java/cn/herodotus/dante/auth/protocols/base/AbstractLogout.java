package cn.herodotus.dante.auth.protocols.base;

import cn.herodotus.dante.core.web.HttpRequestAdapter;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.engine.assistant.core.domain.CommonAuthentication;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public abstract class AbstractLogout {
    public abstract void sendRequest(CommonAuthentication authentication, ApplicationEntity logoutApp) ;

    public void postMessage(String url, Map<String, Object> paramMap) {
        log.debug("post logout message to url {}" , url);
        (new HttpRequestAdapter()).post(url , paramMap);
    }
}
