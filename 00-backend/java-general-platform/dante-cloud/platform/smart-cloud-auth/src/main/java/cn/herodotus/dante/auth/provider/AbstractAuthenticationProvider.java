/*
 * Copyright [2022] [MaxKey of copyright http://www.maxkey.top]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 

package cn.herodotus.dante.auth.provider;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.core.LoginCredential;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.core.exception.CommonLoginException;
import cn.herodotus.dante.exception.LoginErrorKey;
import cn.herodotus.dante.realm.entity.UserAuthEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.engine.assistant.core.domain.CommonGrantedAuthority;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * login Authentication abstract class.
 * 
 * @author Crystal.Sea
 *
 */
public abstract class AbstractAuthenticationProvider {
    private static final Logger _logger = 
            LoggerFactory.getLogger(AbstractAuthenticationProvider.class);

    public static String PROVIDER_SUFFIX = "AuthenticationProvider";
    
    public class AuthType{
    	public final static String NORMAL 	= "normal";
    	public final static String TFA 		= "tfa";
    	public final static String MOBILE 	= "mobile";
    	public final static String TRUSTED 	= "trusted";
    }

    @Resource
    protected CommonAuthenticationRealm commonAuthenticationRealm;
    protected SymmetricCryptoProcessor symmetricCryptoProcessor;

    public static ArrayList<CommonGrantedAuthority> grantedAdministratorsAuthoritys = new ArrayList<CommonGrantedAuthority>();
    
    static {
        //grantedAdministratorsAuthoritys.add(new SimpleGrantedAuthority("ROLE_ADMINISTRATORS"));
    }

    public abstract String getProviderName();

    public abstract CommonUser doAuthenticate(LoginCredential authentication,
                                              CommonApplication application);
    
    @SuppressWarnings("rawtypes")
    public boolean supports(Class authentication) {
        //return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
        return true;
    }

    public CommonUser authenticate(LoginCredential authentication,
                                   CommonApplication application){
    	return null;
    }
    
    public CommonUser authenticate(LoginCredential authentication,
                                   CommonApplication application,
                                   boolean trusted) {
    	return null;
    }
    

    public UserAuthEntity findUserAuth(String username) {
        return commonAuthenticationRealm.findUserAuth(username);
    }

    public UserEntity loadUserData(String uid) {
        return commonAuthenticationRealm.loadUserData(uid);
    }


    /**
     * check input password empty.
     * 
     * @param password String
     * @return
     */
    protected boolean emptyPasswordValid(String password) {
        if (null == password || "".equals(password)) {
            //throw new BadCredentialsException(WebContext.getI18nValue("login.error.password.null"));
        }
        return true;
    }

    /**
     * check input username or password empty.
     * 
     * @param email String
     * @return
     */
    protected boolean emptyEmailValid(String email) {
        if (null == email || "".equals(email)) {
            //throw new BadCredentialsException("login.error.email.null");
        }
        return true;
    }

    /**
     * check input username empty.
     * 
     * @param username String
     * @return
     */
    protected boolean emptyUsernameValid(String username) {
        if (null == username || "".equals(username)) {
            throw new CommonLoginException(LoginErrorKey.LOGIN_PARAM_INVALID, "用户名不能为空值");
        }
        return true;
    }

    protected boolean statusValid(LoginCredential loginCredential /*, UserInfo userInfo*/) {
        /*if (null == userInfo) {
            String i18nMessage = WebContext.getI18nValue("login.error.username");
            _logger.debug("login user  " + loginCredential.getUsername() + " not in this System ." + i18nMessage);
            UserInfo loginUser = new UserInfo(loginCredential.getUsername());
            loginUser.setId(loginUser.generateId());
            loginUser.setUsername(loginCredential.getUsername());
            loginUser.setDisplayName("not exist");
            loginUser.setLoginCount(0);
            authenticationRealm.insertLoginHistory(
            			loginUser, 
            			ConstsLoginType.LOCAL, 
            			"",
            			i18nMessage,
            			WebConstants.LOGIN_RESULT.USER_NOT_EXIST);
            throw new BadCredentialsException(i18nMessage);
        }else {
        	if(userInfo.getIsLocked()==ConstsStatus.LOCK) {
        		authenticationRealm.insertLoginHistory( 
        				userInfo, 
                        loginCredential.getAuthType(), 
                        loginCredential.getProvider(), 
                        loginCredential.getCode(), 
                        WebConstants.LOGIN_RESULT.USER_LOCKED
                    );
        	}else if(userInfo.getStatus()!=ConstsStatus.ACTIVE) {
        		authenticationRealm.insertLoginHistory( 
        				userInfo, 
                        loginCredential.getAuthType(), 
                        loginCredential.getProvider(), 
                        loginCredential.getCode(), 
                        WebConstants.LOGIN_RESULT.USER_INACTIVE
                    );
        	}
        }*/
        return true;
    }

}
