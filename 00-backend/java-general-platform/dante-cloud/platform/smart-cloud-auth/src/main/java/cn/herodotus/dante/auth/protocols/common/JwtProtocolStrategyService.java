package cn.herodotus.dante.auth.protocols.common;

import cn.herodotus.dante.auth.constant.ConstsAuth;
import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.core.LoginCredential;
import cn.herodotus.dante.auth.exception.AuthErrorKey;
import cn.herodotus.dante.auth.protocols.base.AuthorizeBaseEndpoint;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.common.util.NanoIdUtils;
import cn.herodotus.dante.core.SaManager;
import cn.herodotus.dante.core.constant.ExceptionConst;
import cn.herodotus.dante.core.exception.AuthException;
import cn.herodotus.dante.core.stp.StpUtil;
import cn.herodotus.dante.core.web.ContextUtil;
import cn.herodotus.dante.jwt.SaJwtTemplate;
import cn.herodotus.dante.jwt.SaJwtUtil;
import cn.herodotus.dante.jwt.exception.SaJwtException;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.CommonSecurityEntity;
import cn.herodotus.dante.realm.entity.TokenSaltEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.response.ResponseModel;
import cn.herodotus.dante.realm.service.TokenSaltService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hutool.json.jwt.JWT;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author root
 * @description TODO
 * @date 2023/11/17 11:16
 */
@Slf4j
@Service(ConstsAuth.PROTOCOL_STRATEGY_JWT)
public class JwtProtocolStrategyService extends AuthorizeBaseEndpoint implements IProtocolStrategy {
    @Resource
    protected CommonAuthenticationRealm commonAuthenticationRealm;
    @Resource
    protected SymmetricCryptoProcessor symmetricCryptoProcessor;
    @Resource
    protected TokenSaltService tokenSaltService;

    public CommonUserDetails parseJwt(String token, ApplicationEntity application) {
        try {
            String secKey = ProtectConstant.SM4_SECRET;
            String accessToken = symmetricCryptoProcessor.decrypt(token, secKey);


            JWT jwt = SaJwtUtil.parseToken(accessToken, "normal", application.getClientSecret(), true);
            String uid = jwt.getPayload().getClaim(SaJwtTemplate.LOGIN_ID).toString();
            UserEntity user = accountsService.findById(uid);
            CommonUser commonUser = new CommonUser();
            commonUser.setClientId(application.getClientId());
            commonUser.setTid(user.getTid());
            commonUser.setUsername(user.getAccount());
            commonUser.setMainDeptId(user.getMainDeptId());
            // smart-auth: pending
            commonUser.setUser(user);
            commonUser.setUid(uid);
            commonUser.setTid(user.getTid());
            commonUser.setRealName(user.getRealName());
            CommonSecurityContextHolder.setUser(commonUser);

            return commonUser;
        } catch (SaJwtException ex) {
            ex.printStackTrace();
            throw new AuthException(ExceptionConst.EXCEPTION_AUTH, AuthErrorKey.JWT_PARSE_FAIL, ex.getMessage());
        }
    }
    @Override
    public Object process(LoginCredential loginCredential) {
        CommonApplication app = getApp(loginCredential);
        CommonUser commonUser
                = authenticationProviderFactory.authenticate(loginCredential, app);
        if (!app.getProtocol().equals("jwt")) {
            log.error(">>> JwtProtocolStrategyService, not match app protocol: {}", app.getProtocol());
        }

        CommonSecurityEntity securityEntity
                = commonSecurityService.commonSecurityDetail(commonUser.getTid());

        String salt = NanoIdUtils.randomNanoId();
        String token = SaJwtUtil.createToken(loginCredential.getLoginType(),
                commonUser.getUid(),
                loginCredential.getAgent(),
                securityEntity.getCookieTokenSecond(),
                null,
                app.getClientSecret());
        String secKey = ProtectConstant.SM4_SECRET;
        String accessToken = symmetricCryptoProcessor.encrypt(token, secKey);

        //
        TokenSaltEntity tokenSaltEntity = new TokenSaltEntity();
        tokenSaltEntity.setTid(commonUser.getTid());
        tokenSaltEntity.setDev(loginCredential.getAgent());
        tokenSaltEntity.setUid(commonUser.getUid());
        tokenSaltEntity.setSalt(salt);
        tokenSaltService.insert(tokenSaltEntity);

        ResponseModel model = ResponseModel.builder()
                .uid(commonUser.getUid())
                .tid(commonUser.getTid())
                .appType(loginCredential.getAppType())
                .client(app.getClientId())
                .authUrl(app.getAuthUri())
                .logoutUrl(app.getLogoutUris())
                .redirect(app.getRedirectList().get(0))
                .back(loginCredential.getBack())
                .accessToken(accessToken)
                .protocol("jwt")
                .slt(salt)
                .loginType(loginCredential.getLoginType())
                .build();

        StpUtil.login(commonUser.getUser().getId());
        return Result.success(Feedback.OK, model);
    }
}
