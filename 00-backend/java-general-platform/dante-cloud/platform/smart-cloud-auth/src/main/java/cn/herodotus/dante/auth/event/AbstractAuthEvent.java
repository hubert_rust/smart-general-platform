package cn.herodotus.dante.auth.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.context.ApplicationEvent;
@Data
@EqualsAndHashCode(callSuper=false)
public abstract class AbstractAuthEvent extends ApplicationEvent {
    public AbstractAuthEvent(Object source) {
        super(source);
    }

    @Override
    public Object getSource() {
        return super.getSource();
    }
    private String identity; //登录账户
    private String client;   //appid
    private String userId;
}

