package cn.herodotus.dante.auth.protocols.common;

import cn.herodotus.dante.auth.core.LoginCredential;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author root
 * @description TODO
 * @date 2023/11/17 11:19
 */
@Component
public class ProtocolStrategyContext {
    private Map<String, IProtocolStrategy> map = Maps.newHashMap();

    @Autowired
    public ProtocolStrategyContext(Map<String, IProtocolStrategy> strategyMap) {
        this.map.clear();
        strategyMap.forEach(this.map::put);
    }

    public Object process(LoginCredential loginCredential) {
        String protocol = loginCredential.getProtocol() + "ProtocolStrategy";
        return this.map.get(protocol).process(loginCredential);
    }
}
