package cn.herodotus.dante.auth.protocols.common;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.core.LoginCredential;
import cn.herodotus.dante.auth.protocols.base.AuthorizeBaseEndpoint;
import cn.herodotus.dante.core.stp.StpUtil;
import cn.herodotus.dante.realm.model.response.ResponseModel;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import com.google.common.base.Strings;
import org.springframework.stereotype.Service;

/**
 * @author root
 * @description TODO
 * @date 2023/11/17 11:16
 */
@Service("defaultProtocolStrategy")
public class DefaultProtocolStrategyService extends AuthorizeBaseEndpoint implements IProtocolStrategy{
    @Override
    public Object process(LoginCredential loginCredential) {
        CommonApplication app = getApp(loginCredential);
        CommonUser commonUser
                = authenticationProviderFactory.authenticate(loginCredential, app);

        String redirect = loginCredential.getRedirect();
        if (!Strings.isNullOrEmpty(redirect)) {
            for (String val : app.getRedirectList()) {
                if (redirect.indexOf(val) == 0) {

                }
            }
        }

        ResponseModel model = ResponseModel.builder()
                .uid(commonUser.getUid())
                .tid(commonUser.getTid())
                .appType(loginCredential.getAppType())
                .client(app.getClientId())
                .authUrl(app.getAuthUri())
                .logoutUrl(app.getLogoutUris())
                .redirect(redirect)
                .protocol("default")
                .back(loginCredential.getBack())
                .build();

        StpUtil.login(commonUser.getUser().getId());
        return Result.success(Feedback.OK, model);
    }
}
