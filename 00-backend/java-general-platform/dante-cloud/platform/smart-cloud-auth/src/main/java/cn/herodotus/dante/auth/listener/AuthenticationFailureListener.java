/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.auth.listener;

import cn.herodotus.dante.auth.common.AccountStatusManager;
import cn.herodotus.dante.auth.event.AbstractAuthEvent;
import cn.herodotus.dante.auth.stamp.LockedUserDetailsStampManager;
import cn.herodotus.dante.auth.stamp.SignInFailureLimitedStampManager;
import cn.herodotus.engine.cache.core.exception.MaximumLimitExceededException;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * <p>Description: 登出成功监听 </p>
 *
 */
@Component
public class AuthenticationFailureListener implements ApplicationListener<AbstractAuthEvent> {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationFailureListener.class);

    @Resource
    private AccountStatusManager accountStatusManager;
    @Resource
    private SignInFailureLimitedStampManager stampManager;
    @Resource
    private LockedUserDetailsStampManager lockedUserDetailsStampManager;

    @Override
    public void onApplicationEvent(AbstractAuthEvent event) {

        log.debug("[Herodotus] |- User sign in catch failure event : [{}].", event.getClass().getName());

        String username = event.getIdentity();

        if (event instanceof AbstractAuthEvent) {

            if (StringUtils.isNotBlank(username)) {

                log.debug("[Herodotus] |- Parse the username in failure event is [{}].", username);

                int maxTimes = stampManager.getMaxTimes();
                Duration expire = stampManager.getExpire();
                try {
                    int times = stampManager.counting(username, maxTimes, expire, true, "AuthenticationFailureListener");
                    log.debug("[Herodotus] |- Sign in user input password error [{}] items", times);
                } catch (MaximumLimitExceededException e) {
                    log.warn("[Herodotus] |- User [{}] password error [{}] items, LOCK ACCOUNT!", username, maxTimes);
                    accountStatusManager.lock(username);
                }
            }
        }
    }

}
