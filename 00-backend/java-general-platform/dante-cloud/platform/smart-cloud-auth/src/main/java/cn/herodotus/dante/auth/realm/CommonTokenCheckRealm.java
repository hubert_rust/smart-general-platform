package cn.herodotus.dante.auth.realm;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.protocols.base.AuthorizeBaseEndpoint;
import cn.herodotus.dante.core.SaManager;
import cn.herodotus.dante.core.constant.ConfigConstant;
import cn.herodotus.dante.core.web.ContextUtil;
import cn.herodotus.dante.jwt.SaJwtUtil;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.service.IAuthCheck;
import cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author root
 * @description: IDaas 使用
 * @date 2023/8/16 15:38
 */
public class CommonTokenCheckRealm  implements IAuthCheck {
    @Resource
    protected ApplicationService appsService;
    private CommonAuthenticationRealm commonAuthenticationRealm;

    public CommonTokenCheckRealm(CommonAuthenticationRealm commonAuthenticationRealm) {
        this.commonAuthenticationRealm = commonAuthenticationRealm;
    }

    @Override
    public CommonUserDetails tokenCheck(String object, HttpServletRequest request, HttpServletResponse response) {
        String tokenName = SaManager.getConfig().getTokenName();

        String token = ContextUtil.getParam(request, HttpHeaders.HTTP_SMART_TOKEN);
        String salt = ContextUtil.getParam(request, HttpHeaders.HTTP_SLT);
        String cid = ContextUtil.getParam(request, HttpHeaders.HTTP_CID);

        ApplicationEntity entity = appsService.findById(cid);

        Long time = SaJwtUtil.getTimeout(token, "normal", entity.getClientSecret());
        if (time<=0) {

        }
        UserEntity user = commonAuthenticationRealm.loadUserData(object);
        String client = request.getHeader(ConfigConstant.HEADER_CLIENT_ID);
        CommonUser commonUser = new CommonUser();
        commonUser.setClientId(client);
        commonUser.setTid(user.getTid());
        commonUser.setUsername(user.getAccount());
        commonUser.setMainDeptId(user.getMainDeptId());
        // smart-auth: pending
        commonUser.setUser(user);
        commonUser.setUid(object);
        return commonUser;
    }
}
