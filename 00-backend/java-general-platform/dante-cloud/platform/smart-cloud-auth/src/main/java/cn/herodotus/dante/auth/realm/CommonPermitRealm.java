package cn.herodotus.dante.auth.realm;

import cn.herodotus.dante.model.AuthTokenCheck;
import cn.herodotus.dante.service.IPermitInterceptor;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author root
 * @description TODO
 * @date 2023/8/16 15:48
 */
public class CommonPermitRealm implements IPermitInterceptor {

    private CommonAuthenticationRealm commonAuthenticationRealm;

    public CommonPermitRealm(CommonAuthenticationRealm commonAuthenticationRealm) {
        this.commonAuthenticationRealm = commonAuthenticationRealm;
    }

    @Override
    public Boolean permit(HttpServletRequest request,
                          HttpServletResponse response,
                          AuthTokenCheck authTokenCheck,
                          CommonUserDetails requestBaseUser) {

        return true;
    }
}
