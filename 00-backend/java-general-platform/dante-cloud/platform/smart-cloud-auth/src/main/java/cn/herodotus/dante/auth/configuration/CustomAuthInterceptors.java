package cn.herodotus.dante.auth.configuration;


import cn.herodotus.dante.auth.core.CommonUser;
import cn.herodotus.dante.auth.exception.AuthErrorKey;
import cn.herodotus.dante.auth.protocols.common.JwtProtocolStrategyService;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.core.SaManager;
import cn.herodotus.dante.core.constant.ConfigConstant;
import cn.herodotus.dante.core.constant.ExceptionConst;
import cn.herodotus.dante.core.exception.AuthException;
import cn.herodotus.dante.core.web.ContextUtil;
import cn.herodotus.dante.jwt.SaJwtUtil;
import cn.herodotus.dante.jwt.exception.SaJwtException;
import cn.herodotus.dante.model.AuthTokenCheck;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.service.IAuthCheck;
import cn.herodotus.dante.service.IPermitInterceptor;
import cn.herodotus.dante.sso.sso.SaSsoProcessor;
import cn.herodotus.dante.starter.SpringMVCUtil;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import com.google.common.base.Strings;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hutool.json.jwt.JWT;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import java.lang.reflect.Method;



//HandlerInterceptorAdapter需要继承，HandlerInterceptor需要实现  
//建议使用HandlerInterceptorAdapter，因为可以按需进行方法的覆盖。

@Slf4j
@Component
public class CustomAuthInterceptors implements HandlerInterceptor {//HandlerInterceptorAdapter {
    IAuthCheck authCheck;
    IPermitInterceptor permitInterceptor;
    ApplicationService applicationService;
    CommonAuthenticationRealm commonAuthenticationRealm;

    JwtProtocolStrategyService jwtProtocolStrategyService;
    public CustomAuthInterceptors(IAuthCheck authCheck,
                                  IPermitInterceptor permitInterceptor,
                                  ApplicationService applicationService,
                                  JwtProtocolStrategyService jwtProtocolStrategyService) {
        this.authCheck = authCheck;
        this.permitInterceptor = permitInterceptor;
        this.applicationService = applicationService;
        this.jwtProtocolStrategyService = jwtProtocolStrategyService;
    }

    /*
     * 预处理回调方法,实现处理器的预处理。
     * 第三个参数为响应的处理器,自定义Controller,返回值为true表示继续流程（如调用下一个拦截器或处理器）
     * 或者接着执行postHandle()和afterCompletion()；false表示流程中断，不会继续调用其他的拦截器或处理器，中断执行
     * */
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object object) throws Exception {
        if (object instanceof ResourceHttpRequestHandler) {
            return true;
        }

        try {
            String ip = SpringMVCUtil.getIpAddress(request);
            String url = request.getQueryString() != null ?
                    request.getRequestURL().toString() + "?" + request.getQueryString() :
                    request.getRequestURL().toString();

            String client = ContextUtil.getParam(HttpHeaders.HTTP_CID);
            ApplicationEntity application = applicationService.findById(client);
            String uid = null;
            if (application.getProtocol().equals("jwt")) {
                log.info("[CustomAuthIntercpetors], URL: {}, isLogin: {}， IP: {}", url, uid, ip);
                jwtProcessFilter(request, response, application);
                return true;
            }
            else {
                uid = SaSsoProcessor.instance.ssoTemplate.getStpLogic().isLoginEnhance();
            }

            log.info("[CustomAuthIntercpetors], URL: {}, isLogin: {}， IP: {}", url, uid, ip);
            if (Strings.isNullOrEmpty(uid)) {
                throw new AuthException("authException", AuthErrorKey.USER_NOT_LOGIN);
                //String authAddr = AuthCommonContainer.INST.getAuthAddr(request);
                // response.sendRedirect(authAddr);
                // return false;
                // 跳转登录界面，需要带redirect
            }

            //spring boot 2.0开始对静态资源也进行了拦截，当拦截器拦截到请求之后，但controller里并没有对应的请求时，该请求会被当成是对静态资源的请求。
            //此时的handler就成为 ResourceHttpRequestHandler，就会抛出ResourceHttpRequestHandler cannot be cast to HandlerMethod
            //在拦截器排除静态资源的请求路径（不完全解决）registry.addInterceptor(new JJRUserLoginInterceptor()).addPathPatterns("/xx/**").excludePathPatterns("/xx/**")
            boolean isCheckAdmin = false;//是否管理员鉴权
            Method method = null;
            if (object instanceof HandlerMethod) {
                method = ((HandlerMethod) object).getMethod();

                if (url.endsWith("error")) {
                    return false;
                }
                //不是内部流程鉴权，才会进行token鉴权
                //http://xxxxxxxxxx/contextpath/error走此处
                //boolean bneed = method.isAnnotationPresent(NeedToken.class);//是否不鉴定Token

            } else if (object instanceof ResourceHttpRequestHandler) {//静态资源请求
                //客户端请求的api找不到，错误的api等也走此处，之后会触发contextpath/error(该消息走上面的流程)
                //本地file://（如简历模板）  classpath等文件的请求，此时不需要Token，直接返回True
                //对于需要用户信息的静态数据，需要遇到后考虑
                // 简历预览: freemarker模板渲染, 这种静态资源请求不鉴权
                return true;
            } else {
            }

            //鉴权前，先把Token和slt填写上，用于填写headers信息进行鉴权
            //小程序预览token,slt通过url携带

            processFilter(uid, request, response);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private boolean jwtProcessFilter(HttpServletRequest request,
                                  HttpServletResponse response,
                                  ApplicationEntity application) {
        String tokenName = SaManager.getConfig().getTokenName();
        String token = ContextUtil.getParam(request, HttpHeaders.HTTP_JWT_TOKEN);
        CommonUserDetails user = jwtProtocolStrategyService.parseJwt(token, application);
        CommonSecurityContextHolder.setUser(user);
        AuthTokenCheck tokenCheck = new AuthTokenCheck();
        tokenCheck.setToken(user.getUid());
        tokenCheck.setClientId(user.getClientId());
        String bestMatching = String.valueOf(request.getAttribute("org.springframework.web.servlet.HandlerMapping.bestMatchingPattern"));
        tokenCheck.setRequestUrl(bestMatching);
        tokenCheck.setRequestMethod(request.getMethod().toUpperCase());
        permitInterceptor.permit(request, response, tokenCheck, user);
        return true;
    }
    private void processFilter(String uid, HttpServletRequest request,
                               HttpServletResponse response) {
        CommonUserDetails user = authCheck.tokenCheck(uid, request, response);
        CommonSecurityContextHolder.setUser(user);
        AuthTokenCheck tokenCheck = new AuthTokenCheck();
        tokenCheck.setToken(uid);
        tokenCheck.setClientId(user.getClientId());
        String bestMatching = String.valueOf(request.getAttribute("org.springframework.web.servlet.HandlerMapping.bestMatchingPattern"));
        tokenCheck.setRequestUrl(bestMatching);
        tokenCheck.setRequestMethod(request.getMethod().toUpperCase());
        permitInterceptor.permit(request, response, tokenCheck, user);
    }

    /*
     * 后处理回调方法，实现处理器的后处理（DispatcherServlet进行视图返回渲染之前进行调用）
     * 此时我们可以通过modelAndView（模型和视图对象）对模型数据进行处理或对视图进行处理，
     * modelAndView也可能为null。
     * */
    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object o, ModelAndView modelAndView) throws Exception {
    	/*if (response.getStatus() == 500) {
			modelAndView.setViewName("/errorpage/500");
            request.getRequestDispatcher("500.html").forward(request, response);
		} else if (response.getStatus() == 404) {
			modelAndView.setViewName("/errorpage/404");
            request.getRequestDispatcher("404.html").forward(request, response);
		}*/
    }

    /*
     * 整个请求处理完毕回调方法,该方法也是需要当前对应的Interceptor的preHandle()的返回值为true时才会执行.
     * 也就是在DispatcherServlet渲染了对应的视图之后执行。
     * 用于进行资源清理。整个请求处理完毕回调方法。
     * 如性能监控中我们可以在此记录结束时间并输出消耗时间，还可以进行一些资源清理，
     * 类似于try-catch-finally中的finally，但仅调用处理器执行链中
     * */
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {

        CommonSecurityContextHolder.clearUserDetail();
    }
}
