package cn.herodotus.dante.auth.core;

import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;

public interface CommonUserDetailsService {

    CommonUserDetails loadUserByUsername(String username) ;
}
