package cn.herodotus.dante.auth.configuration;

import cn.herodotus.dante.auth.provider.AbstractAuthenticationProvider;
import cn.herodotus.dante.auth.provider.AuthenticationProviderFactory;
import cn.herodotus.dante.auth.provider.impl.NormalAuthenticationProvider;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.auth.realm.CommonPermitRealm;
import cn.herodotus.dante.auth.realm.CommonTokenCheckRealm;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;

/**
 * @author root
 * @description TODO
 * @date 2023/8/14 9:06
 */
@Slf4j
@AutoConfiguration
public class CustomAuthProviderAutoConfiguration implements InitializingBean {




    @Bean
    @DependsOn(value = {
            "smartLogService",
            "userService",
            "userAuthService",
    } )
    public CommonAuthenticationRealm commonAuthenticationRealm() {
        log.info("init CommonAuthenticationRealm");
        return new CommonAuthenticationRealm();
    }

    @Bean
    public CommonTokenCheckRealm commonTokenCheckRealm(
            CommonAuthenticationRealm commonAuthenticationRealm
    ) {
        return new CommonTokenCheckRealm(commonAuthenticationRealm);
    }
    @Bean
    public CommonPermitRealm commonPermitRealm(
            CommonAuthenticationRealm commonAuthenticationRealm
    ) {
        return new CommonPermitRealm(commonAuthenticationRealm);
    }
    @Bean
    @DependsOn(value = {"commonAuthenticationRealm"})
    public AuthenticationProviderFactory authenticationProvider(AbstractAuthenticationProvider normalAuthenticationProvider) {
        AuthenticationProviderFactory factory = new AuthenticationProviderFactory();
        factory.addAuthenticationProvider(normalAuthenticationProvider);
        return factory;
    }

    @Bean
    @DependsOn(value = {"commonAuthenticationRealm"})
    public AbstractAuthenticationProvider normalAuthenticationProvider(
            CommonAuthenticationRealm commonAuthenticationRealm,
            SymmetricCryptoProcessor symmetricCryptoProcessor) {
        log.info("init NormalAuthenticationProvider");
        return new NormalAuthenticationProvider(
                commonAuthenticationRealm,
                symmetricCryptoProcessor
        );
    }



    @Override
    public void afterPropertiesSet() throws Exception {

    }


}
