package cn.herodotus.dante.auth.configuration;

import cn.herodotus.dante.auth.protocols.common.JwtProtocolStrategyService;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.service.IAuthCheck;
import cn.herodotus.dante.service.IPermitInterceptor;
import cn.herodotus.engine.cache.redisson.annotation.EnableHerodotusRedisson;
import com.google.common.collect.Lists;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration(proxyBeanMethods = false)
@EntityScan( basePackages = {
        "cn.herodotus.dante.realm.entity"
})
@EnableJpaRepositories(
        basePackages = "cn.herodotus.dante.realm.repository"
)
@ComponentScan(basePackages = {
        "cn.herodotus.dante.realm.controller",
        "cn.herodotus.dante.realm.service",
})
@EnableHerodotusRedisson
@AutoConfiguration
@Slf4j
@EnableConfigurationProperties(AuthPermitProperties.class)
public class CustomAuthConfiguration implements WebMvcConfigurer {
    @Resource
    AuthPermitProperties authPermitProperties;
    @Resource
    private IPermitInterceptor permitInterceptor;
    @Resource
    private IAuthCheck authCheck;
    @Resource
    private ApplicationService applicationService;
    @Resource
    private JwtProtocolStrategyService jwtProtocolStrategyService;
    @PostConstruct
    public void postConstruct() {
        log.info("[Herodotus] |- Service [CustomAuthConfiguration] Auto Configure.");
    }

    @Bean
    public CustomAuthInterceptors customAuthInterceptors() {
        return new CustomAuthInterceptors(authCheck, permitInterceptor,
                applicationService,
                jwtProtocolStrategyService);
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptorRegistry = registry.addInterceptor(customAuthInterceptors())
                .order(100)
                .addPathPatterns("/**");

        List<String> permits = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(authPermitProperties.getPermit())) {
            permits.addAll(authPermitProperties.getPermit());
        }
        for (String s : permits) {
            interceptorRegistry = ((InterceptorRegistration) interceptorRegistry).excludePathPatterns(s);
        }
    }
}
