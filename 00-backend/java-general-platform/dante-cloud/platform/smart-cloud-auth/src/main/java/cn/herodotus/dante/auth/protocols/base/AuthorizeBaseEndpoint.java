/*
 * Copyright [2020] [MaxKey of copyright http://www.maxkey.top]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 

/**
 * 
 */
package cn.herodotus.dante.auth.protocols.base;

import cn.herodotus.dante.auth.core.CommonApplication;
import cn.herodotus.dante.auth.core.LoginCredential;
import cn.herodotus.dante.auth.exception.AuthErrorKey;
import cn.herodotus.dante.auth.protocols.base.name.BaseParamName;
import cn.herodotus.dante.auth.provider.AuthenticationProviderFactory;
import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.configuration.ServerApplicationContainer;
import cn.herodotus.dante.core.constant.ExceptionConst;
import cn.herodotus.dante.core.context.SaHolder;
import cn.herodotus.dante.core.context.model.SaRequest;
import cn.herodotus.dante.core.exception.AuthException;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.CommonSecurityService;
import cn.herodotus.dante.realm.service.UserAuthService;
import cn.herodotus.dante.realm.service.UserService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Crystal.Sea
 *
 */
@Slf4j
public class AuthorizeBaseEndpoint {

	@Resource
	protected ApplicationService appsService;
	@Resource
	protected AuthenticationProviderFactory authenticationProviderFactory;
	@Resource
	protected CommonAuthenticationRealm commonAuthenticationRealm;
	@Resource
	protected CommonSecurityService commonSecurityService;
	@Resource
	protected UserService accountsService;
	@Resource
	protected UserAuthService userAuthService;
		
	protected CommonApplication getApp(LoginCredential loginCredential){
		SaRequest req = SaHolder.getRequest();
		String client = "";
		if (Objects.nonNull(loginCredential)
				&& Strings.isNotEmpty(loginCredential.getClient())) {
			client = loginCredential.getClient();
		}
		if (!Strings.isNotEmpty(client)) {
			client = req.getParam(BaseParamName.REQ_PARAM_CLIENT);
		}

		String appType = req.getParam(BaseParamName.REQ_PARAM_TYPE);
		if (Strings.isEmpty(client)) {
			client = ServerApplicationContainer.INST.getDefaultClient(appType);
		}
		ApplicationEntity entity = appsService.findById(client);
		if (Objects.isNull(entity)) {
			throw new AuthException(ExceptionConst.EXCEPTION_AUTH, AuthErrorKey.INVALID_APP);
		}
		CommonApplication commonApplication = new CommonApplication();
		BeanUtils.copyProperties(entity, commonApplication);
		if (Strings.isNotEmpty(commonApplication.getRedirectUris())) {
			String[] redirects = commonApplication.getRedirectUris().split(";");
			commonApplication.getRedirectList().addAll(Arrays.stream(redirects).toList());
		}
		return commonApplication;
	}

	public ModelAndView initCredentialView(String appId,String redirect_uri){
		/*String initCredentialURL =
				"" + 
				applicationConfig.getFrontendUri() + 
				"/#/authz/credential?appId=%s&redirect_uri=%s";
		
		initCredentialURL = String.format(initCredentialURL,appId, redirect_uri);
		_logger.debug("redirect to {}.",initCredentialURL);
		ModelAndView  modelAndView =new ModelAndView("redirect");
		modelAndView.addObject("redirect_uri", initCredentialURL);
		return modelAndView;*/
		return null;
	}
	
}
