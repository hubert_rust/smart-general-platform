package cn.herodotus.dante.auth.protocols.common;

import cn.herodotus.dante.auth.realm.CommonAuthenticationRealm;
import cn.herodotus.dante.core.auth.CommonBaseUser;
import cn.herodotus.dante.core.constant.CoreConst;
import cn.herodotus.dante.core.domain.IntrospectModel;
import cn.herodotus.dante.core.web.ContextUtil;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.permission.UserApiPermissionService;
import cn.herodotus.dante.sso.sso.SaSsoProcessor;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

import static cn.herodotus.dante.core.constant.CoreConst.INTROSPECT_INVALID_REQ;

@Tag(name = "应用接口鉴权")
@RestController
@RequestMapping("/sso")
public class DefaultAuthController {

    @Resource
    private ApplicationService applicationService;
    @Resource
    private UserApiPermissionService userApiPermissionService;
    @Resource
    private CommonAuthenticationRealm commonAuthenticationRealm;
    @Resource
    private SymmetricCryptoProcessor symmetricCryptoProcessor;
    @Resource
    private JwtProtocolStrategyService jwtProtocolStrategyService;

    @Operation(summary = "应用访问接口鉴权", description = "应用访问接口鉴权")
    @PostMapping("introspect/{nonce}")
    public Result<CommonUserDetails> introspect(@PathVariable String nonce,
                                                @RequestBody IntrospectModel model,
                                                HttpServletRequest request) {
        ApplicationEntity application = applicationService.findById(model.getClientId());
        if (Objects.isNull(application)) {
            return Result.failure("非法应用: 应用未注册", INTROSPECT_INVALID_REQ, null);
        }
        String val = model.getNonce();
        val = symmetricCryptoProcessor.decrypt(val, ProtectConstant.SM4_SECRET);
        if (val.equals(nonce)) {
            if (application.getProtocol().equals("jwt")) {
                return jwtProcess(model, application);
            } else if (application.getProtocol().equals("default")) {
                return defaultProtocol(model, application);
            }
        }

        return Result.failure("非法请求", INTROSPECT_INVALID_REQ, null);
    }

    private Result<CommonUserDetails> jwtProcess(IntrospectModel model,
                                                 ApplicationEntity application) {
        String accessToken = ContextUtil.getParam(HttpHeaders.HTTP_JWT_TOKEN);
        CommonUserDetails user = jwtProtocolStrategyService.parseJwt(accessToken, application);


        boolean access = userApiPermissionService.userApiAccess(application.getClientId(),
                user.getTid(), user.getUid(), model.getDigest());
        if (access) {
            return Result.success("接口鉴权成功", CoreConst.SUCCESS_CODE, user);
        } else {
            return Result.failure("用户无权限", CoreConst.INPROSPECT_NOT_PERMIT, null);
        }
    }

    private Result<CommonUserDetails> defaultProtocol(IntrospectModel model,
                                                      ApplicationEntity application) {
        String uid
                = SaSsoProcessor.instance.ssoTemplate.getStpLogic().isLoginEnhance();

        if (Strings.isEmpty(uid)) {
            return Result.failure("用户未登录", CoreConst.INTROSPECT_NOT_LOGIN, null);
        }

        UserEntity user = commonAuthenticationRealm.loadUserData(uid);

        boolean access = userApiPermissionService.userApiAccess(model.getClientId(),
                user.getTid(), uid, model.getDigest());
        if (access) {
            CommonBaseUser commonUser = new CommonBaseUser();
            commonUser.setUid(user.getId());
            commonUser.setTid(user.getTid());
            commonUser.setUsername(user.getRealName());
            commonUser.setMainDeptId(user.getMainDeptId());
            CommonSecurityContextHolder.setUser(commonUser);

            return Result.success("接口鉴权成功", CoreConst.SUCCESS_CODE, commonUser);
        } else {
            return Result.failure("用户无权限", CoreConst.INPROSPECT_NOT_PERMIT, null);
        }
    }
}
