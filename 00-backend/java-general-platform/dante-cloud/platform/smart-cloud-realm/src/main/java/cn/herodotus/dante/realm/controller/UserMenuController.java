package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.ResApiEntity;
import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.dante.realm.model.RolePermitModel;
import cn.herodotus.dante.realm.model.user.UserWorkspaceRoleModel;
import cn.herodotus.dante.realm.service.RoleService;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.dante.realm.service.group.RoleManagerServiceImpl;
import cn.herodotus.dante.realm.service.group.RoleMenuResManagerServiceImpl;
import cn.herodotus.dante.realm.service.group.UserPermitResServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Tag(name = "用户管理-资源权限")
public class UserMenuController {
    @Resource
    private UserService userService;
    @Resource
    private RoleService roleService;
    @Resource
    private RoleManagerServiceImpl roleManagerService;
    @Resource
    private RoleMenuResManagerServiceImpl roleMenuResManagerService;
    @Resource
    private UserPermitResServiceImpl userPermitResService;



    @Operation(summary = "用户应用菜权限", description = "用户应用菜单权限")
    @PostMapping("/userMenus")
    public Result<List<ResMenuEntity>> userMenus(@RequestBody RolePermitModel model) {
        List<ResMenuEntity> ret = roleMenuResManagerService.userMenus(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "用户应用API权限", description = "用户应用API权限")
    @PostMapping("/userApis")
    public Result<List<ResApiEntity>> userApis(@RequestBody RolePermitModel model) {
        List<ResApiEntity> ret = roleMenuResManagerService.userApis(model);
        return Result.success(Feedback.OK, ret);
    }
 /*   @Operation(summary = "保存资源权限", description = "保存资源权限")
    @PostMapping("/saveMenus")
    public Result<Boolean> saveMenus(@RequestBody RoleResourceModel model) {
        Boolean ret = roleMenuResManagerService.saveMenus(model);
        return Result.success(Feedback.OK, ret);
    }*/
    @Operation(summary = "用户角色", description = "用户角色")
    @PostMapping("/userSpaceRole/{uid}")
    public Result<List<UserWorkspaceRoleModel>> userSpaceRole(@PathVariable String uid) {
        List<UserWorkspaceRoleModel> ret = userPermitResService.userSpaceRoles(uid);
        return Result.success(Feedback.OK, ret);
    }

}
