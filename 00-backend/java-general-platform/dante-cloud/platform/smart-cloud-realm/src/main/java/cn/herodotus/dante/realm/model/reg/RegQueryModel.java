package cn.herodotus.dante.realm.model.reg;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.Sort;

@EqualsAndHashCode(callSuper=false)
@Data
public class RegQueryModel extends BaseQueryModel {

    private String identify;

    public Sort.Direction getDirection() {
        if (RealmConstant.DIR_ASC.equals(getDir())) {
            return Sort.Direction.ASC;
        }
        else if (RealmConstant.DIR_DESC.equals(getDir())) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }

}
