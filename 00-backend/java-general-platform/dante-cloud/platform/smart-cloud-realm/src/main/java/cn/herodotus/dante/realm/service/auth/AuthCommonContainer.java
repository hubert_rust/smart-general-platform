package cn.herodotus.dante.realm.service.auth;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.DictionaryEntity;
import cn.herodotus.dante.realm.repository.DictionaryRepository;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.DictionaryService;
import cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author root
 * @description TODO
 * @date 2023/10/7 14:33
 */
@Slf4j
@Component
public class AuthCommonContainer implements InitializingBean, DisposableBean {

    public static AuthCommonContainer INST;
    @Resource
    private DictionaryService dictionaryService;
    @Resource
    private ApplicationService applicationService;

    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        AuthCommonContainer.INST = this;
    }

    private String getDefaultAuthAddr() {
        DictionaryEntity entity = ((DictionaryRepository) dictionaryService.getRepository())
                .findByTidAndDictTypeAndDataKey(RealmConstant.TID_VALUE_DEFAULT,
                        "auth_api", "auth");
        return Objects.nonNull(entity) ? entity.getDataValue() : null;
    }

    public String getAuthAddr(HttpServletRequest request) {
        String apt = request.getHeader(HttpHeaders.HTTP_APT);
        String client = request.getHeader(HttpHeaders.HTTP_CID);
        if (Strings.isEmpty(client)) {
            String defaultAuthAddr = getDefaultAuthAddr();
            if (Strings.isEmpty(defaultAuthAddr)) {
                log.error(">>> AuthCommonServiceImpl, getAuthAddr, please config auth addr");
            }
            return defaultAuthAddr;

        } else {
            ApplicationEntity applicationEntity = applicationService.findById(client);
            if (Objects.isNull(applicationEntity)) {
                log.error(">>> AuthCommonServiceImpl, getAuthAddr, " +
                        "application not exist: {}", client);
                return null;
            }
            if (Strings.isEmpty(applicationEntity.getAuthUri())) {
                log.error(">>> AuthCommonServiceImpl, getAuthAddr, " +
                        "please config application auth addr: {}", client);
                return null;
            }
            return applicationEntity.getAuthUri();
        }
    }
}
