package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import cn.herodotus.dante.realm.model.user.UserDeptModel;
import com.google.common.collect.Lists;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "sys_account" )
@Data
@EqualsAndHashCode(callSuper=true)
public class UserEntity extends CommonTenantEntity {

    private String account;
    @Column(name = "post_id", length = 16)
    private String postId;
    @Column(name = "create_type", length = 16)
    private String createType;

    @Column(name = "real_name", length = 32)
    private String realName;

    @Column(name = "gender", length = 16)
    private String gender;

    @Column(name = "profile", length = 255)
    private String profile;

    @Column(name = "last_login_ip", length = 64)
    private String lastLoginIp;

    @Column(name = "last_login_time")
    private Date lastLoginTime;

    @Column(name = "status", length = 16)
    private String status;

    @Column(name = "account_attr", length = 16)
    private String accountAttr;  //root: 第一个用户，master: 每个注册用户

    @Column(name = "remark", length = 32)
    private String remark;

    @Column(name = "main_dept_id", length = 64)
    private String mainDeptId;
    @Column(name = "main_dept_code", length = 64)
    private String mainDeptCode;

    // 登录后显示的用户空间
    @Column(name = "hit_tid", length = 64)
    private String hitTid;
    private String phone;
    private String email;

    @Transient
    private  Boolean agent;  //1 表示代理， 0 表示不是代理
    @Transient
    private List<UserDeptModel> depts = Lists.newArrayList();
    @Transient
    private List<UserAuthEntity> userAuths = Lists.newArrayList();
}
