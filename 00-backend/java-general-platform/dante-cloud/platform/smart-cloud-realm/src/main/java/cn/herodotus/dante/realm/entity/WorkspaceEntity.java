package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Deprecated
@Entity
@Table(name = "sys_workspace" )
@Data
@EqualsAndHashCode(callSuper=true)
public class WorkspaceEntity extends CommonEntity {
    @NotBlank(message = "用户空间名称不能为空")
    @Column(name = "workspace_name", length = 64, unique = true)
    private String workspaceName;
    @Column(name = "workspace_type", length = 64, unique = true)
    private String workspaceType;
    @Column(name = "remark", length = 64, unique = true)
    private String remark;

    @Transient
    private Integer hit = 0;
}
