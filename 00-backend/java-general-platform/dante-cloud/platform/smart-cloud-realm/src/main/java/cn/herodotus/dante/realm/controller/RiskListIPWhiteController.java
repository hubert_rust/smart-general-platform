package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.constant.RiskListConstant;
import cn.herodotus.dante.realm.entity.RiskListEnity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.service.RiskListService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/risk")
@Tag(name = "风险名单-IP 白名单")
public class RiskListIPWhiteController {

    @Resource
    private RiskListService riskListService;

    @Operation(summary = "添加 IP 白名单", description = "添加 IP 白名单")
    @PostMapping("/insertIpWhite")
    public Result<RiskListEnity> insertIpWhite(@Valid @RequestBody RiskListEnity entity) {
        RiskListEnity ret = riskListService.addIpWhite(entity);
        return Result.success(Feedback.OK, ret);
    }


    @Operation(summary = "删除 IP 白名单", description = "删除 IP 白名单")
    @PostMapping("/deleteIpWhite/{id}")
    public Result<Boolean> deleteIpWhite(@PathVariable String id) {
        Boolean ret = riskListService.delete(id,
                RiskListConstant.LIST_CLASS_WHITE,
                RiskListConstant.SUBJECT_TYPE_IP);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "IP 白名单列表", description = "IP 白名单列表")
    @PostMapping("/ipWhiteList")
    public Result<PageResponse> ipWhiteList(@RequestBody BaseQueryModel model) {
        PageResponse response = riskListService.ipWhiteList(model);
        return Result.success(Feedback.OK, response);
    }

}
