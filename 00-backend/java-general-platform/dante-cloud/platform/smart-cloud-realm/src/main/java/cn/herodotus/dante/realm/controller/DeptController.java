package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.DeptEntity;
import cn.herodotus.dante.realm.service.DeptService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/org")
@Tag(name = "组织部门管理")
public class DeptController {

    @Resource
    private DeptService deptService;

    @Operation(summary = "添加组织", description = "添加组织")
    @PostMapping("/insertOrg")
    public Result<DeptEntity> insert(@Valid @RequestBody DeptEntity entity) {
        DeptEntity ret = deptService.insert(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "添加部门", description = "添加部门")
    @PostMapping("/insertDept")
    public Result<DeptEntity> insertDept(@Valid @RequestBody DeptEntity entity) {
        DeptEntity ret = deptService.save(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除组织", description = "删除组织")
    @PostMapping("/deleteOrg/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        deptService.deleteDept(id);
        return Result.success(Feedback.OK, true);
    }

    @Operation(summary = "删除部门", description = "删除部门")
    @PostMapping("/deleteDept/{id}")
    public Result<Boolean> deleteDept(@PathVariable String id) {
        deptService.deleteDept(id);
        return Result.success(Feedback.OK, true);
    }

    @Operation(summary = "修改组织", description = "修改组织")
    @PostMapping("/updateOrg")
    public Result<DeptEntity> updateOrg(@Valid @RequestBody DeptEntity entity) {
        DeptEntity ret = deptService.update(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "修改部门", description = "修改部门")
    @PostMapping("/updateDept")
    public Result<DeptEntity> updateDept(@Valid @RequestBody DeptEntity entity) {
        DeptEntity ret = deptService.update(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询组织部门", description = "查询组织部门")
    @PostMapping("/listTree")
    public Result<List<DeptEntity>> list() {
        List<DeptEntity> ret = deptService.getTrees();
        return Result.success(Feedback.OK, ret);
    }
}
