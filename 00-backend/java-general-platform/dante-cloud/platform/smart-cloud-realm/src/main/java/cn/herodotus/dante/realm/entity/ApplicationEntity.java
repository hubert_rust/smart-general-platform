package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import cn.herodotus.dante.realm.constant.RealmConstant;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Entity
@Table(name = "sys_application")
@Data
@EqualsAndHashCode(callSuper=true)
public class ApplicationEntity extends CommonTenantEntity {
    @Column(name = "app_inner_name", length = 64)
    private String appInnerName;
    @Column(name = "logo", length = 64)
    private String logo;

    // 为了区分平台发布的sso和非平台发布的sso
    @Column(name = "tid_parent", length = 32)
    private String tidParent;

    // 是否共享到应用市场
    private Boolean share = false;
    @Column(name = "client_name", length = 64)
    private String clientName;
    @Column(name = "app_type", length = 64)
    private String appType = RealmConstant.APP_TYPE_USER;
    @Column(name = "auth_uri", length = 64)
    private String authUri;


 /*   @Column(name = "backend_id", length = 64)
    private String backendId;*/
    @Column(name = "app_tag", length = 16)
    private String appTag;

    @Column(name = "app_attr", length = 16)
    private String appAttr;
    @Column(name = "max_limit")
    private Integer maxLimit;
    private Integer expire;
    private String protocol;
    @Column(name= "client_id")
    private String clientId;
    @Column(name = "client_secret", length = 64)
    private String clientSecret;

    @Column(name = "client_secret_expires_at")
    private Date clientSecretExpiresAt;
    @Column(name = "client_id_issued_at")
    private Date clientIdIssuedAt;


    @Column(name = "logout_uris")
    private String logoutUris;
    @Column(name = "redirect_uris")
    private String redirectUris;

    private String scopes;

    private String status;    //active; deactive
    @Column(name = "remark", length = 64)
    private String remark;

    // 自建应用使用
    @Column(name= "all_show")
    private Boolean allShow = true;
    //sso server: 1是sso server
    private Integer server = 0;

    private String deleted;
    @Transient
    private String realName;

    @Deprecated
    @Transient
    private Integer fromSso = 0;   // 1表示来自sso

    // 平台sso还是用户空间sso，还是自建sso
    // APP_CLASS_PLATFORM
    @Transient
    private String from;
}
