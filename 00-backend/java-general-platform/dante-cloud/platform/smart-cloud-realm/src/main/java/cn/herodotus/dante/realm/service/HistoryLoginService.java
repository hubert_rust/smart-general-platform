package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.core.help.Browser;
import cn.herodotus.dante.realm.entity.HistoryLoginEntity;
import cn.herodotus.dante.realm.repository.HistoryLoginRepository;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HistoryLoginService extends BaseService<HistoryLoginEntity, String> {

    private HistoryLoginRepository historyLoginRepository;

    public HistoryLoginService(HistoryLoginRepository historyLoginRepository) {
        this.historyLoginRepository = historyLoginRepository;
    }

    @Override
    public BaseRepository<HistoryLoginEntity, String> getRepository() {
        return historyLoginRepository;
    }

    public boolean loginHistoryStore() {
       HistoryLoginEntity entity = new HistoryLoginEntity();

        Browser browser = Browser.resolveBrowser();
        entity.setBrowser(browser.getName());
        entity.setPlatform(browser.getPlatform());
        super.save(entity);
        return true;
    }
}
