package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

//权限空间
@Entity
@Table(name = "sys_permission_space" )
@Data
@EqualsAndHashCode(callSuper=true)
public class PermissionSpaceEntity  extends CommonTenantEntity {

    @Column(name = "tid_type")
    private String tidType;
    @Column(name = "space_code", length = 32)
    private String spaceCode;
    @Column(name = "space_name", length = 64)
    private String spaceName;
    @Column(name = "remark", length = 64)
    private String remark;
}
