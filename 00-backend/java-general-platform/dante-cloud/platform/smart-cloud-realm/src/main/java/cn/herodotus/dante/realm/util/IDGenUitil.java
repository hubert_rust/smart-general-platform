package cn.herodotus.dante.realm.util;

import java.util.UUID;

public class IDGenUitil {
    public static String getUUID(int length) {
        String[] val = UUID.randomUUID().toString().split("-");
        if (length == 8) {
            return val[0];
        } else if (length == 12) {
            return val[0] + val[1];
        } else if (length == 16) {
            return val[0] + val[1] + val[2];
        } else if (length == 20) {
            return val[0] + val[1] + val[2] + val[3];
        }
        return val[0] + val[1] + val[2] + val[3] + val[4];
    }

}
