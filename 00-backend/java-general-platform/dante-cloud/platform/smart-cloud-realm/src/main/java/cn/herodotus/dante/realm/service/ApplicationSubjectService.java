/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.entity.ApplicationSubjectEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.repository.ApplicationSubjectRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.dromara.hutool.crypto.digest.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ApplicationSubjectService extends BaseService<ApplicationSubjectEntity, String> {

    private ApplicationSubjectRepository applicationSubjectRepository;

    @Autowired
    public ApplicationSubjectService(ApplicationSubjectRepository applicationSubjectRepository) {
        this.applicationSubjectRepository = applicationSubjectRepository;
    }

    @Override
    public BaseRepository<ApplicationSubjectEntity, String> getRepository() {
        return this.applicationSubjectRepository;
    }

    public List<ApplicationSubjectEntity> findByTidAndSubjectTypeAndIdentity(String tid, String subjectType, String identity) {
        return applicationSubjectRepository.findByTidAndSubjectTypeAndIdentity(tid, subjectType, identity);
    }
    public List<ApplicationSubjectEntity> findByTidAndIdentityIn(String tid, List<String> identities) {
        return applicationSubjectRepository.findByTidAndIdentityIn(tid, identities);
    }

    // 应用授权,
    public Boolean addApplicationEmpower(List<ApplicationSubjectEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        // client都相同
        String clientId = list.get(0).getClientId();
        if (Strings.isEmpty(clientId)) {
            // clientId = CommonSecurityContextHolder.getCid();
        }
        // 增加授权的时候需要判断: 如果已经添加，就没有必要添加了
        String tid = CommonSecurityContextHolder.getTid();
        String uid = CommonSecurityContextHolder.getUid();
        Date now = new Date();
        String cid = clientId;
        list.stream().forEach(v-> {
            String data = tid + cid + v.getSubjectType() + v.getIdentity();
            String md5 = MD5.of().digestHex(data);
            v.setDigest(md5);
            v.setTid(tid);
            v.setClientId(cid);
            v.setCreateBy(uid);
            v.setCreateTime(now);
        });

        List<ApplicationSubjectEntity> appSubs
                = applicationSubjectRepository.findByTidAndClientId(tid, clientId);

        // 只保存表中不存在的
        List<ApplicationSubjectEntity> saves = Lists.newArrayList();
        for (ApplicationSubjectEntity add : list) {
            boolean finder = false;
            for (ApplicationSubjectEntity appSub : appSubs) {
                if (add.getDigest().equals(appSub.getDigest())) {
                    finder = true;
                    break;
                }
            }
            if (!finder) {
                saves.add(add);
            }
        }

        super.saveAll(saves);
        return true;
    }

    public Boolean deleteApplicationEmpower(List<ApplicationSubjectEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        super.deleteAll(list);
        return true;
    }

    public PageResponse listApplicationEmpower(BaseQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);

        Specification<ApplicationSubjectEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("subjectName"), model.getKey()));
            }
            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<ApplicationSubjectEntity> resp = new PageResponse<>();
        resp.setTotalCount((int)num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }
}

