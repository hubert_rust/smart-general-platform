package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Entity
@Table(name = "sys_tenant_admin" )
@Data
@EqualsAndHashCode(callSuper=true)
public class TenantAdminEntity extends CommonTenantEntity {
    private String uid;
    private Integer hit;
    @Column(name = "tid_type", length = 64)
    private String tidType;

    // 是否是内部管理员，也可能是其他用户空间的用户
    @Column(name = "inner_admin")
    private Boolean innerAdmin = true;
    @Transient
    private String account;
    @Transient
    private String realName;
    @Transient
    private String phone;
    @Transient
    private String email;
    @Transient
    private Date lastLoginTime;
}
