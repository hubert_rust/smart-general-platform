package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.RoleEntity;
import cn.herodotus.dante.realm.entity.RoleResourceEntity;
import cn.herodotus.dante.realm.entity.RoleSubjectEntity;
import cn.herodotus.dante.realm.model.application.ApplicationPermitModel;
import cn.herodotus.dante.realm.model.user.UserWorkspaceRoleModel;
import cn.herodotus.dante.realm.repository.RoleRepository;
import cn.herodotus.dante.realm.repository.RoleResourceRepository;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.RoleService;
import cn.herodotus.dante.realm.service.RoleSubjectService;
import cn.herodotus.dante.realm.service.permission.UserApiPermissionService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author root
 * @description 用户详情里面用户权限功能实现
 * @date 2023/9/21 9:18
 */
@Slf4j
@Service
public class UserPermitResServiceImpl {

    @Resource
    private RoleSubjectService roleSubjectService;
    @Resource
    private RoleService roleService;
    @Resource
    private ApplicationService applicationService;
    @Resource
    private UserApiPermissionService userApiPermissionService;
    @Resource
    private RoleResourceRepository roleResourceRepository;

    /*
     * @Author root
     * @Description 角色配置的应用:
     * @Date 15:52 2023/9/21
     * @Param
     * @return
     **/


    /*
     * @Author root
     * @Description //用户的所有角色，只包括当前用户空间, 【角色授予的应用】
     * @Date 9:28 2023/9/21
     * @Param [uid]
     * @return java.util.List<cn.herodotus.dante.realm.model.user.UserAllWorkspaceRoleModel>
     **/
    public List<UserWorkspaceRoleModel> userSpaceRoles(String uid) {
        String tid = CommonSecurityContextHolder.getTid();

        // 用户对应的角色主体（user，org，group）
        List<RoleSubjectEntity> list
                = userApiPermissionService.getRoleSubjectEntities(tid, uid);
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }

        // 按照roleId对角色主体分组
        Map<String, List<RoleSubjectEntity>> map
                = list.stream().collect(Collectors.groupingBy(RoleSubjectEntity::getRoleId));

        Set<String> roles = map.keySet();
        List<RoleEntity> allRole
                = ((RoleRepository) roleService.getRepository()).findAllById(roles);

        // 应用配置了菜单或者 api
        // roleId, clientId, clientFrom
        HashBasedTable<String, String, String> roleResTable = HashBasedTable.create();
        Map<String, HashBasedTable<String, String, String>> roleResMap = Maps.newHashMap();

        List<RoleResourceEntity> roleRes
                = roleResourceRepository.findRoleApplication(
                tid, roles.stream().toList());
        roleRes.stream().forEach(v -> {
                    if (roleResMap.get(v.getRoleId()) == null) {
                        roleResMap.put(v.getRoleId(), HashBasedTable.create());
                    }
                    roleResMap.get(v.getRoleId()).put(v.getClientId(), v.getClientFrom(), "");
                }
        );
        Set<String> clientSets = roleRes.stream()
                .map(m -> m.getClientId()).collect(Collectors.toSet());

        // roleId, clientId, ApplicationEntity
        // 通过roleId, clientId， 获取ApplicationEntity
        Map<String, String> clients = Maps.newHashMap();
        if (CollectionUtils.isNotEmpty(clientSets)) {
            List<ApplicationEntity> apps
                    = applicationService.getRepository().findAllById(clientSets);
            apps.stream().forEach(v -> clients.put(v.getClientId(), v.getAppTag()));

        }

        List<UserWorkspaceRoleModel> ret = Lists.newArrayList();
        for (RoleEntity roleEntity : allRole) {
            UserWorkspaceRoleModel model = new UserWorkspaceRoleModel();
            model.setRoleId(roleEntity.getId());
            model.setRoleName(roleEntity.getRoleName());
            model.setRoleSubjects(map.get(roleEntity.getId()));
            ret.add(model);

            HashBasedTable<String, String, String> row = roleResMap.get(roleEntity.getId());
            if (Objects.nonNull(row)) {
                for (Table.Cell<String, String, String> cell : row.cellSet()) {
                    ApplicationPermitModel appModel = new ApplicationPermitModel();
                    appModel.setId(IDContainer.INST.getUUID(20, false));
                    appModel.setAppTag(clients.get(cell.getRowKey()));
                    appModel.setClientId(cell.getRowKey());
                    appModel.setClientFrom(cell.getColumnKey());
                    appModel.setRoleName(roleEntity.getRoleName());
                    model.getApps().add(appModel);
                }
            }
        }

        return ret;
    }
}
