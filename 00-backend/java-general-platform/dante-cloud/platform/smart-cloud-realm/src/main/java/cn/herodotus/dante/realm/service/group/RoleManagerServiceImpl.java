package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.realm.constant.PermissionConst;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.*;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.RolePermitModel;
import cn.herodotus.dante.realm.model.UserCommonModel;
import cn.herodotus.dante.realm.model.application.ApplicationBaseModel;
import cn.herodotus.dante.realm.model.user.UserQueryModel;
import cn.herodotus.dante.realm.repository.ApplicationRepository;
import cn.herodotus.dante.realm.repository.RoleSubjectRepository;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.dto.PageResponse;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author root
 * @description TODO
 * @date 2023/8/28 18:13
 */
@Slf4j
@Service
public class RoleManagerServiceImpl {

    @Resource
    private RoleService roleService;
    @Resource
    private RoleSubjectService roleSubjectService;
    @Resource
    private ApplicationService applicationService;
    @Resource
    private ApplicationTenantSsoService applicationTenantSsoService;
    @Resource
    private ResMenuService resMenuService;
    @Resource
    private ResApiService resApiService;
    @Resource
    private RoleResourceService roleResourceService;
    @Resource
    private RoleApiDataPermissionService roleApiDataPermissionService;
    @Resource
    private UserService userService;
    @Resource
    private DeptService deptService;
    @Resource
    private GroupUserService groupUserService;
    @Resource
    private GroupService groupService;

    // 删除角色
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean deleteRole(String id) {
        String tid = roleSubjectService.getTid();
        // 检测角色
        boolean exist = ((RoleSubjectRepository) roleSubjectService.getRepository())
                .existsByTidAndRoleId(tid, id);
        if (exist) {
            throw new RealmException("realmException", RealmErrorKey.ROLE_HAS_REF, "该角色被授权主体引用");
        }
        roleService.deleteById(id);
        return true;
    }

    public boolean addRoleSubject(List<RoleSubjectEntity> list) {
        return roleSubjectService.addRoleSubject(list);
    }

    public boolean deleteRoleSubject(List<RoleSubjectEntity> list) {
        return roleSubjectService.deleteRoleSubject(list);
    }

    public PageResponse roleSubjectList(BaseQueryModel model) {
        PageResponse<RoleSubjectEntity> resp = roleSubjectService.listRoleSubject(model);
        setSubjectName(resp.getData());
        return resp;
    }

    public static void main(String[] args) {
        HashBasedTable<String, String, String> table= HashBasedTable.create();

        table.put("user", "user1", "name1");
        table.put("user", "user2", "name2");
        table.put("org", "org1", "org_name1");
        table.put("org", "org2", "org_name2");

        System.out.println();
    }

    private void setSubjectName(Collection<RoleSubjectEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        // type, identity, name
        HashBasedTable<String, String, String> table= HashBasedTable.create();
        Map<String, Map<String, String>> map = Maps.newHashMap();
        map.put(RealmConstant.SUBJECT_USER, Maps.newHashMap());
        map.put(RealmConstant.SUBJECT_ORG, Maps.newHashMap());
        map.put(RealmConstant.SUBJECT_GROUP, Maps.newHashMap());
        for (RoleSubjectEntity subjectEntity : list) {
            String identity = subjectEntity.getIdentity();
            table.put(subjectEntity.getSubjectType(), identity, "");
        }
        if (Objects.nonNull(table.rowMap().get(RealmConstant.SUBJECT_USER))) {
            List<UserEntity> users
                    = userService.listByIds(
                    table.rowMap().get(RealmConstant.SUBJECT_USER).keySet().stream().toList()
            );
            users.stream().forEach(v-> {
                table.put(RealmConstant.SUBJECT_USER, v.getId(), v.getAccount());
            });
        }
        if (Objects.nonNull(table.rowMap().get(RealmConstant.SUBJECT_GROUP))) {
            List<GroupEntity> groups = groupService.listByIds(
                    table.rowMap().get(RealmConstant.SUBJECT_GROUP).keySet().stream().toList()
            );
            groups.stream().forEach(v-> {
                table.put(RealmConstant.SUBJECT_GROUP, v.getId(), v.getGroupName());
            });
        }
        if (Objects.nonNull(table.rowMap().get(RealmConstant.SUBJECT_ORG))) {
            List<DeptEntity> orgs = deptService.listByIds(
                    table.rowMap().get(RealmConstant.SUBJECT_ORG).keySet().stream().toList()
            );
            orgs.stream().forEach(v-> {
                table.put(RealmConstant.SUBJECT_ORG, v.getId(), v.getDeptName());
            });
        }

        list.stream().forEach(v-> {
            String type = v.getSubjectType();
            String identity = v.getIdentity();
            String name = table.get(type, identity);
            v.setSubjectName(name);
        });

    }

    // 角色配置资源时，获取应用列表
    // 角色配置资源：菜单，api等，为每个应配置
    public List<ApplicationBaseModel> roleAppList(BaseQueryModel model) {
        // 应用分为SSO应用(平台应用)，自建应用
        String tid = CommonSecurityContextHolder.getTid();
        List<ApplicationBaseModel> ret = Lists.newArrayList();

        // SSO应用
        List<ApplicationTenantSsoEntity> spaceSso = applicationTenantSsoService.findByTid(tid);
        if (CollectionUtils.isNotEmpty(spaceSso)) {
            List<ApplicationEntity> ssos = applicationService.findByClientIdIn(
                    spaceSso.stream().map(m -> m.getClientId()).toList()
            );
            ssos.stream().forEach(v -> {
                ApplicationBaseModel mode = new ApplicationBaseModel();
                BeanUtils.copyProperties(v, mode);
                mode.setClientFrom(RealmConstant.APP_FROM_SSO);
                ret.add(mode);
            });
        }

        // 本用户空间应用, 前端是在当前用户空间配置
        List<ApplicationEntity> spaces =
                ((ApplicationRepository) applicationService.getRepository()).findByTidAndDeletedAndStatus(
                        tid, RealmConstant.DELETED_VALUE_DEFAULT, RealmConstant.STATUS_ACTIVE
                );
        spaces.stream().forEach(v -> {
            ApplicationBaseModel mode = new ApplicationBaseModel();
            BeanUtils.copyProperties(v, mode);
            mode.setClientFrom(RealmConstant.APP_FROM_SELF);
            ret.add(mode);
        });


        return ret;

        /*// 本用户空间应用, 前端是在当前用户空间配置
        List<ApplicationEntity> spaces =
                ((ApplicationRepository)applicationService.getRepository()).findByTidAndDeletedAndStatus(
                        tid, RealmConstant.DELETED_VALUE_DEFAULT, RealmConstant.STATUS_ACTIVE
                );
        // 表sys_tenant_sso_application中保存的是在本用户空间中可见的
        spaces.stream().forEach(v-> {
            if (!v.getShare()) {
                v.setFrom(RealmConstant.APP_SELF);
            }
            else {
                if (RealmConstant.DELETED_VALUE_DEFAULT.equals(v.getTidParent())) {
                    v.setFrom(RealmConstant.APP_PLATFORM_SSO);
                }
                else {
                    v.setFrom(RealmConstant.APP_SPACE_SSO);
                }
            }
        });*/

        // SSO应用
        /*List<ApplicationTenantSsoEntity> spaceSso = applicationTenantSsoService.findByTid(tid);
        if (CollectionUtils.isEmpty(spaceSso)) {
            return spaces;
        }
        List<ApplicationEntity> ssos = applicationService.findByClientIdIn(
                spaceSso.stream().map(m -> m.getClientId()).toList()
        );
        ssos.stream().forEach(v -> {
            v.setFromSso(1);
        });
        spaces.addAll(ssos);*/
        //return spaces;
    }

    // 前端选择应用后，加载应用对应的菜单，为角色配置权限: 资源，菜单，API等，
    // 可供选择的资源是当前用户的最大权限
    public List<Tree> appMenus(String clientId) {
        // smart-auth: pending
        List<ResMenuEntity> list = resMenuService.findByClientId(clientId);
        List<Tree> ret = DefaultTreeBuildFactory.treeBuild(
                new ArrayList<>(list), RealmConstant.PARENT_ID_DEFAULT, true
        );
        return ret;
    }


    // 同appMenus()
    public List<ResApiEntity> appApis(RolePermitModel model) {
        // key: roleId
        // value: clientId
        List<ResApiEntity> list = resApiService.findByClientId(model.getClientId(), false);

        List<RoleResourceEntity> grants
                = roleResourceService.listResources(model.getRoleId(),
                model.getClientId(), model.getClientFrom(),
                PermissionConst.ROLE_RES_OJBECT_API);

        Map<String, RoleResourceEntity> map = Maps.newHashMap();
        grants.stream().forEach(v -> map.put(v.getIdentity(), v));
        list.stream().forEach(v -> {
            RoleResourceEntity find = map.get(v.getDigest());
            if (find != null) {
                v.setGranted(true);
                v.setScope(find.getScope());
            } else {
                v.setGranted(false);
                v.setScope("N");
            }
        });
        DefaultTreeBuildFactory<ResApiEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<ResApiEntity> ret = factory1.doTreeBuild(list);
        return ret;
    }

    // 关闭数据权限
    public boolean closeApiDataPermission(List<RoleResourceEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        List<RoleResourceEntity> updates = Lists.newArrayList();
        String tid = roleResourceService.getTid();
        for (RoleResourceEntity entity : list) {
            RoleResourceEntity find = roleResourceService.findByTidAndClientIdAndClientFromAndRoleIdAndObjectTypeAndIdentity(
                    tid,
                    entity.getClientId(),
                    entity.getClientFrom(),
                    entity.getRoleId(),
                    PermissionConst.ROLE_RES_OJBECT_API,
                    entity.getIdentity()
            );
            if (Objects.nonNull(find)) {
                find.setScope("");
                updates.add(find);
            }
        }
        roleResourceService.saveAll(updates);
        return true;
    }

    // 角色数据权限增加。自定义的时候也要更新RoleResourceEntity表
    // 数据类型暂时使用RoleApiDataPermissionEntity
    public boolean addApiDataPermission(List<RoleApiDataPermissionEntity> list) {

        /*List<RoleApiDataPermissionEntity> customs = list.stream()
                .filter(p -> PermissionConst.SCOPE_CUSTOM.equals(p.getScope()))
                .toList();*/

        // smart-auth, 其实每次只是单条记录
        String tid = roleResourceService.getTid();

        // 有更新的，也有新增加的
        List<RoleResourceEntity> updates = Lists.newArrayList();
        List<RoleResourceEntity> inserts = Lists.newArrayList();
        for (RoleApiDataPermissionEntity entity : list) {
            RoleResourceEntity find
                    = roleResourceService.findByTidAndClientIdAndClientFromAndRoleIdAndObjectTypeAndIdentity(
                    tid,
                    entity.getClientId(),
                    entity.getClientFrom(),
                    entity.getRoleId(),
                    PermissionConst.ROLE_RES_OJBECT_API,
                    entity.getIdentity()
            );
            if (Objects.nonNull(find)) {
                find.setScope(entity.getScope());
                updates.add(find);
            } else {
                RoleResourceEntity ins = new RoleResourceEntity();
                ins.setTid(tid);
                ins.setRoleId(entity.getRoleId());
                ins.setClientId(entity.getClientId());
                ins.setClientFrom(entity.getClientFrom());
                ins.setIdentity(entity.getIdentity());
                ins.setObjectType(PermissionConst.ROLE_RES_OJBECT_API);
                ins.setScope(entity.getScope());

                inserts.add(ins);
            }
        }
        if (CollectionUtils.isNotEmpty(updates)) {
            roleResourceService.saveAll(updates);
        }
        if (CollectionUtils.isNotEmpty(inserts)) {
            roleResourceService.insertAll(inserts);
        }
        /*if (CollectionUtils.isNotEmpty(customs)) {

            roleApiDataPermissionService.addApiDataPermission(list);
        }*/
        return true;
    }

    // 用户最多添加25个
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean roleAddCustomUser(List<RoleApiDataPermissionEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "参数数据没有值");
        }
        List<RoleApiDataPermissionEntity> saves = Lists.newArrayList();
        String tid = roleApiDataPermissionService.getTid();
        for (RoleApiDataPermissionEntity entity : list) {
            boolean exist = roleApiDataPermissionService.existsByTidAndRoleIdAndIdentity(
                    tid, entity.getRoleId(), entity.getIdentity());
            if (!exist) {
                entity.setTid(tid);
                saves.add(entity);
            }
        }
        if (CollectionUtils.isNotEmpty(saves)) {
            roleApiDataPermissionService.insertAll(saves);
        }
        return true;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean roleDeleteCustomUser(List<RoleApiDataPermissionEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return true;
        }
        String tid = roleApiDataPermissionService.getTid();
        String roleId = list.get(0).getRoleId();
        List<String> ids = list.stream().map(m -> m.getIdentity()).toList();
        roleApiDataPermissionService.deleteByTidAndRoleIdAndIdentityIn(
                tid, roleId, ids
        );
        return true;
    }

    // 角色添加自定义组织, 每次list提交的都是全部最新的组织
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean roleUpdateCustomOrg(RolePermitModel model) {
        // 为空，就是全部删除
        /*if (CollectionUtils.isEmpty(list)) {
            return false;
        }*/

        String tid = roleApiDataPermissionService.getTid();
        String roleId = model.getRoleId();
        String digest = model.getDigest();
        model.getList().stream().forEach(v -> v.setTid(tid));
        // 删除接口对应的组织
        roleApiDataPermissionService.deleteByTidAndRoleIdAndDigestIn(
                tid, roleId, new ArrayList<String>() {{
                    add(digest);
                }});

        if (CollectionUtils.isNotEmpty(model.getList())) {
            roleApiDataPermissionService.insertAll(model.getList());
        } else {
        }
        return true;
    }

    // 数据权限: 角色自定义数据权限-组织结构列表
    // 优先级非常低，可以在暂时不考虑
    public Map<String, Object> roleCustomUserList(UserQueryModel model, String roleId) {
        String tid = deptService.getTid();

        List<RoleApiDataPermissionEntity> users
                = roleApiDataPermissionService.findByTidAndRoleIdAndCustomSubject(
                tid, roleId, PermissionConst.SCOPE_OBJECT_USER
        );
        List<DeptEntity> list
                = deptService.findByTidAndPathLike(tid, model.getDeptInnerCode() + "%");
        Set<String> set = Sets.newHashSet();
        for (DeptEntity deptEntity : list) {
            if (Strings.isNotEmpty(deptEntity.getPath())) {
                String[] item = deptEntity.getPath().split("-");
                Set<String> items = Arrays.stream(item).collect(Collectors.toSet());
                set.addAll(items);
            }
        }


        PageResponse pageResponse = userService.list(model, set);
        Map<String, Object> map = Maps.newHashMap();
        map.put("page", pageResponse);
        map.put("userSelected", users);
        return map;
    }

    // 数据权限: 角色自定义数据权限-组织结构列表
    public Map<String, Object> roleCustomOrgList(RolePermitModel model) {
        String tid = roleApiDataPermissionService.getTid();
        List<DeptEntity> tree = deptService.getTrees();

        Map<String, Object> map = Maps.newHashMap();

        List<RoleApiDataPermissionEntity> customs
                = roleApiDataPermissionService.findByTidAndRoleIdAndCustomSubject(
                tid, model.getRoleId(), model.getDigest()
        );
        map.put("tree", tree);
        map.put("treeSelected", customs);
        return map;
    }

    // api数据权限自定义表
    public List<RoleApiDataPermissionEntity> roleApiCustomList(String roleId, String digest) {
        String tid = roleApiDataPermissionService.getTid();
        List<RoleApiDataPermissionEntity> list = roleApiDataPermissionService.findByTidAndRoleId(tid, roleId);
        if (CollectionUtils.isEmpty(list)) {
            return list;
        }

        List<String> userIds = list.stream().filter(m -> PermissionConst.SCOPE_OBJECT_USER.equals(m.getCustomSubject()))
                .map(m -> m.getIdentity())
                .toList();
        List<String> deptIds = list.stream().filter(m -> PermissionConst.SCOPE_OBJECT_ORG.equals(m.getCustomSubject()))
                .map(m -> m.getIdentity())
                .toList();
        List<UserEntity> findUsers = userService.listByIds(userIds);
        List<DeptEntity> findDepts = deptService.listByIds(deptIds);
        for (RoleApiDataPermissionEntity entity : list) {
            boolean find = false;
            if (CollectionUtils.isNotEmpty(findDepts)
                    && PermissionConst.SCOPE_OBJECT_USER.equals(entity.getCustomSubject())) {
                for (UserEntity findUser : findUsers) {
                    if (entity.getIdentity().equals(findUser.getId())) {
                        entity.setAccount(findUser.getAccount());
                        entity.setUid(findUser.getId());
                        find = true;
                        break;
                    }
                }
            }
            if (find) {
                continue;
            }
            if (CollectionUtils.isNotEmpty(deptIds)
                    && PermissionConst.SCOPE_OBJECT_ORG.equals(entity.getCustomSubject())) {
                for (DeptEntity findDept : findDepts) {
                    if (entity.getIdentity().equals(findDept.getId())) {
                        entity.setDeptName(findDept.getDeptName());
                        entity.setDeptId(findDept.getId());
                        break;
                    }
                }
            }

        }

        return list;
    }

    // 撤销角色主体
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean removeRoleSubject(UserCommonModel model) {
        if (!model.getSubjectType().equals(RealmConstant.SUBJECT_USER)) {
            throw new RealmException("realmException", RealmErrorKey.REMOVE_ROLE_SUBJECT_IS_USER);
        }
        String tid = roleSubjectService.getTid();
        ((RoleSubjectRepository) roleSubjectService.getRepository()).deleteByTidAndRoleIdAndIdentityAndSubjectType(
                tid, model.getRoleId(), model.getIdentity(), model.getSubjectType()
        );

        return true;
    }


}
