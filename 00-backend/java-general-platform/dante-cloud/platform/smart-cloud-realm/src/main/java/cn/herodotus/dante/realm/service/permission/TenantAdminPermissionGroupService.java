package cn.herodotus.dante.realm.service.permission;

import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.dante.realm.entity.TenantAdminPermissionEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.AdminResPermissionModel;
import cn.herodotus.dante.realm.repository.ResMenuRepository;
import cn.herodotus.dante.realm.repository.TenantAdminPermissionRepository;
import cn.herodotus.dante.realm.service.ResMenuService;
import cn.herodotus.dante.realm.service.TenantAdminPermissionService;
import cn.herodotus.dante.realm.service.TenantAdminService;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author root
 * @description TODO
 * @date 2023/9/19 8:37
 */
@Slf4j
@Service
public class TenantAdminPermissionGroupService {
    @Resource
    private TenantAdminService tenantAdminService;
    @Resource
    private TenantAdminPermissionService tenantAdminPermissionService;
    @Resource
    private UserService userService;
    @Resource
    private ResMenuService resMenuService;

    private boolean deleteAdminRes(String tid,
                                   String uid,
                                   String clientId,
                                   boolean innerAdmin) {

        ((TenantAdminPermissionRepository)tenantAdminPermissionService.getRepository())
                .deleteByTidAndClientIdAndUidAndInnerAdmin(
                        tid, clientId, uid, innerAdmin);

        return true;
    }

    public List<Tree> queryAdminMenuRes(AdminResPermissionModel model) {
        String tid = CommonSecurityContextHolder.getTid();
        String clientId = model.getClientId();
        String uid = model.getUid();
        if (Strings.isNullOrEmpty(clientId)) {
            clientId = CommonSecurityContextHolder.getCid();
        }

        // 查询管理员菜单全集
        List<ResMenuEntity> all = resMenuService.findByClientIdAndPermisLikeAndStatus(
          clientId, "%"+RealmConstant.ACCOUNT_ATTR_MASTER+"%", RealmConstant.STATUS_ACTIVE
        );
        Map<String, String> map = Maps.newHashMap();

        List<TenantAdminPermissionEntity> list
                = ((TenantAdminPermissionRepository)tenantAdminPermissionService.getRepository())
                .findByTidAndClientIdAndUidAndObjectType(
                        tid, clientId, uid, RealmConstant.ROLE_RES_OBJECT_TYPE_MENU);
        Set<String> menus
                = list.stream().map(m->m.getIdentity()).collect(Collectors.toSet());

        List<ResMenuEntity> hasMenus = ((ResMenuRepository)resMenuService.getRepository())
                .findAllById(menus);
        hasMenus.stream().forEach(v-> map.put(v.getId(), v.getId()));
        all.stream().forEach(v-> {
            String id = v.getId();
            if (Objects.isNull(map.get(id))) {
                v.setRed(false);
            }
            else {
                v.setRed(true);
            }
        });

        List<Tree> ret = DefaultTreeBuildFactory.treeBuild(new ArrayList<>(all),
                RealmConstant.PARENT_ID_DEFAULT,
                true);
        return ret;
    }

    /*
     * @Author root
     * @Description 管理添加菜单权限, 管理员添加菜单，同时添加了api
     * 移动端的话也是添加菜单
     * @Date 8:38 2023/9/19
     * @Param
     * @return
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean addAdminRes(List<AdminResPermissionModel> list,
                               boolean innerAdmin) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }

        String tid = tenantAdminService.getTid();
        String uid = list.get(0).getUid();
        String clientId = list.get(0).getClientId();
        // 带商榷，从前端传clientId?
        if (Strings.isNullOrEmpty(clientId)) {
            clientId = CommonSecurityContextHolder.getCid();
        }
        String uidTid = CommonSecurityContextHolder.getUserTid();

        if (Strings.isNullOrEmpty(uidTid)) {
            UserEntity userEntity = userService.findById(uid);
            uidTid = userEntity.getTid();
        }

        deleteAdminRes(tid, uid, clientId, true);
        List<TenantAdminPermissionEntity> saves = Lists.newArrayList();

        for (AdminResPermissionModel model : list) {
            TenantAdminPermissionEntity entity = new TenantAdminPermissionEntity();
            entity.setTid(tid);
            entity.setClientId(clientId);
            entity.setUid(model.getUid());
            entity.setUidTid(uidTid);
            entity.setIdentity(model.getMenuId());
            entity.setObjectType(RealmConstant.ROLE_RES_OBJECT_TYPE_MENU);
            entity.setInnerAdmin(innerAdmin);
            saves.add(entity);
            if (Strings.isNullOrEmpty(model.getDigest())) {
                log.error(">>> addAdminMenuRes, menuName: {}", model.getMenuName());
            }
            else {
                TenantAdminPermissionEntity apiEntity = new TenantAdminPermissionEntity();
                apiEntity.setTid(tid);
                apiEntity.setUid(model.getUid());
                apiEntity.setUidTid(uidTid);
                apiEntity.setInnerAdmin(innerAdmin);
                apiEntity.setClientId(clientId);
                apiEntity.setIdentity(model.getDigest());
                apiEntity.setObjectType(RealmConstant.ROLE_RES_OBJECT_TYPE_API);
                saves.add(apiEntity);
            }
        }

        tenantAdminPermissionService.insertAll(saves);
        return true;
    }


}
