package cn.herodotus.dante.realm.model;

import cn.herodotus.dante.realm.entity.RoleResourceEntity;
import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/5 9:39
 */

@Data
public class RoleResourceModel implements Serializable {

    private String roleId;
    private String clientId;
    private String clientFrom;
    private List<RoleResourceEntity> list = Lists.newArrayList();
}
