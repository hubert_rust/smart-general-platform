package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.ResApiEntity;
import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.dante.realm.model.application.MenuLinkApiModel;
import cn.herodotus.dante.realm.model.application.OperationProcessType;
import cn.herodotus.dante.realm.service.ResApiService;
import cn.herodotus.dante.realm.service.ResMenuService;
import cn.herodotus.dante.realm.service.group.ApplicationServiceGroupImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/application")
@Tag(name = "自建应用管理-通用资源")
@Validated
public class ApplicationMenuController {

    @Resource
    private ResMenuService resMenuService;
    @Resource
    private ApplicationServiceGroupImpl applicationServiceGroup;
    @Resource
    private ResApiService resApiService;


    @Operation(summary = "菜单列表", description = "返回所有菜单")
    @PostMapping("/menuListOfapp/{client}")
    public Result<List<ResMenuEntity>> operationOfApp(@PathVariable String client) {
        List<ResMenuEntity> ret = resMenuService.listMenuByClient(client,  true);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "添加菜单", description = "添加菜单")
    @PostMapping("/saveMenu")
    public Result<ResMenuEntity> saveMenu(@RequestBody ResMenuEntity entity) {
        ResMenuEntity ret = resMenuService.save(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "更新菜单", description = "更新菜单")
    @PostMapping("/updateMenu")
    public Result<ResMenuEntity> updateMenu(@RequestBody ResMenuEntity entity) {
        ResMenuEntity ret = resMenuService.update(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除菜单", description = "物理删除菜单")
    @PostMapping("/deleteMenu/{id}")
    public Result<Boolean> deleteMenu(@PathVariable String id) {
        Boolean ret = resMenuService.delete(id);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "添加接口", description = "添加接口")
    @PostMapping("/saveApi")
    public Result<ResApiEntity> saveApi(@RequestBody ResApiEntity entity) {
        ResApiEntity ret = resApiService.save(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "更新接口", description = "更新接口")
    @PostMapping("/updateApi")
    public Result<ResApiEntity> updateApi(@RequestBody ResApiEntity entity) {
        ResApiEntity ret = resApiService.update(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除接口", description = "逻辑删除接口")
    @PostMapping("/deleteApi/{id}")
    public Result<Boolean> deleteApi(@PathVariable String id) {
        Boolean ret = resApiService.delete(id);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "菜单绑定接口", description = "菜单按钮绑定接口")
    @PostMapping("/menuLinkApi")
    public Result<Boolean> menuLinkApi(@RequestBody MenuLinkApiModel model) {
        Boolean ret = applicationServiceGroup.menuLinkApi(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "校验资源", description = "校验资源是否有多个菜单绑定同一个接口")
    @PostMapping("/checkRes/{client}")
    public Result<List<String>> checkRes(@PathVariable String client) {
        List<String> ret = applicationServiceGroup.checkApiRes(client);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "应用接口列表", description = "应用接口列表")
    @PostMapping("/resApiList/{client}")
    public Result<List<ResApiEntity>> resApiList(@PathVariable String client) {
        List<ResApiEntity> ret = applicationServiceGroup.listAppApiByClient(client);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "应用接口控制", description = "应用接口控制")
    @PostMapping("/processTypeOfapp")
    public Result<Boolean> operationOfApp(@RequestBody OperationProcessType model) {
        Boolean ret = applicationServiceGroup.operationProcessType(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "解除 API 绑定", description = "解除 API 绑定")
    @PostMapping("/menuUnlinkApi")
    public Result<Boolean> unlinkApi(@RequestBody List<MenuLinkApiModel> list) {
        Boolean ret = applicationServiceGroup.menuUnlinkApi(list);
        return Result.success(Feedback.OK, ret);
    }
}
