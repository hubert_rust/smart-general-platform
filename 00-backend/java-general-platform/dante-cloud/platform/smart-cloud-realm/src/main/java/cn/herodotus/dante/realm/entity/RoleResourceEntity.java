package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "sys_role_resource" )
@Data
@EqualsAndHashCode(callSuper=true)
public class RoleResourceEntity extends CommonTenantEntity {
    public RoleResourceEntity() {
    }

    public RoleResourceEntity(String roleId, String clientId) {
        this.roleId = roleId;
        this.clientId = clientId;
    }
    public RoleResourceEntity(String roleId, String clientId, String clientFrom) {
        this.roleId = roleId;
        this.clientId = clientId;
        this.clientFrom = clientFrom;
    }

    @Column(name = "role_id", length = 64)
    private String roleId;

    @Column(name = "client_id", length = 32)
    private String clientId;
    @Column(name = "object_type", length = 16)
    private String objectType;
    @Column(name = "identity", length = 64)
    private String identity;

    @Column(name = "client_from", length = 64)
    private String clientFrom;
    private String digest;

    // api 资源使用，数据权限枚举值
    private String scope = "";

    // 2023-09-25: 用户资源权限返回角色和角色主体
    @Transient
    private String clientName;

}
