package cn.herodotus.dante.realm.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/3 16:41
 */
@Data
public class UserCommonModel implements Serializable {
    private String uid;

    private String roleId;
    private String identity;
    private String subjectType;
    private List<String> list;
}
