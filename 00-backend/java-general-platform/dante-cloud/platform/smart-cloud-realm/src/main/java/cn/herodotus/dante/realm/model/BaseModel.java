package cn.herodotus.dante.realm.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/8/24 14:16
 */
@Data
public class BaseModel implements Serializable {
    private String key;
    private String value;
    private String ext;

}
