/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.repository;

import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;


public interface ResMenuRepository extends BaseRepository<ResMenuEntity, String> {

    List<ResMenuEntity> findByClientIdAndDigestIn(String clientId, List<String> digests);

    // 查询角色对应的菜单资源
    List<ResMenuEntity> findByTidAndIdIn(String tid, Set<String> ids);

    void deleteByParentId(String parentId);


    //通过parent递归查询所有子节点和自身
    @Query(value = "WITH RECURSIVE cte as (\n" +
            "  select * from sys_resource_menu where  id = ?1 \n" +
            "\tUNION all\t\n" +
            "\tselect  t.* from sys_resource_menu  t\n" +
            "\tjoin cte on cte.id = t.parent_id\n" +
            "\t\n" +
            ")\n" +
            "select * from cte", nativeQuery = true)
    List<Object[]> findByRecurise(String id);

    // 查询所有
    List<ResMenuEntity> findAllByClientId(String clientId);

    // 只查询status状态为1的
    List<ResMenuEntity> findByClientIdAndStatus(String clientId, String status);

    List<ResMenuEntity> findByClientIdAndPermisLikeAndStatus(
            String clientId, String permisLiks, String status);
}
