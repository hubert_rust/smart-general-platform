/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.TenantAdminEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.AdminAddModel;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.user.UserQueryModel;
import cn.herodotus.dante.realm.repository.TenantAdminRepository;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
public class TenantAdminService extends BaseService<TenantAdminEntity, String> {

    private TenantAdminRepository tenantAdminRepository;

    @Resource
    private UserService userService;

    @Autowired
    public TenantAdminService(TenantAdminRepository tenantAdminRepository) {
        this.tenantAdminRepository = tenantAdminRepository;
    }

    @Override
    public BaseRepository<TenantAdminEntity, String> getRepository() {
        return this.tenantAdminRepository;
    }

    //通过
    public void spaceAdminAdd(AdminAddModel adminAddModel) {
        TenantAdminEntity adminEntity = new TenantAdminEntity();
        adminEntity.setUid(super.getUid());
        adminEntity.setTid(adminAddModel.getTid());
        adminEntity.setHit(0);
        adminEntity.setTidType(adminAddModel.getTidType());
        super.save(adminEntity);
    }

    public void tenantAdminAdd(AdminAddModel adminAddModel) {
        TenantAdminEntity adminEntity = new TenantAdminEntity();
        adminEntity.setUid(super.getUid());
        adminEntity.setTid(adminAddModel.getTid());
        adminEntity.setHit(0);
        adminEntity.setTid(adminAddModel.getTidType());
        super.save(adminEntity);
    }

    public void deleteByTid(String tid) {
        tenantAdminRepository.delete(new Specification<TenantAdminEntity>() {
            @Override
            public Predicate toPredicate(Root<TenantAdminEntity> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("tid"), tid);
            }
        });
    }

    /*
     * @Author root
     * @Description 目前直接添加外部管理员，通过外部管理员id
     * @Date 11:33 2023/9/15
     * @Param
     * @return
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean addExternalAdmin(List<UserEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        // 如果已经添加就不做处理
        String tid = super.getTid();
        for (UserEntity entity : list) {
            boolean exist =
                    tenantAdminRepository.existsByTidAndUidAndTidType(
                            tid, entity.getId(), RealmConstant.TID_TYPE_REALM);
            log.info(">>> addExternalAdmin, exist: {}, userAccount: {}", exist, entity.getAccount());
            if (!exist) {
                TenantAdminEntity adminEntity = new TenantAdminEntity();
                adminEntity.setTid(tid);
                adminEntity.setInnerAdmin(false);
                adminEntity.setUid(entity.getId());
                adminEntity.setTidType(RealmConstant.TID_TYPE_REALM);
                super.insert(adminEntity);
            }
        }
        return true;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean addAdmin(List<UserEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        // 如果已经添加就不做处理
        String tid = super.getTid();
        for (UserEntity entity : list) {
            boolean exist =
                    tenantAdminRepository.existsByTidAndUidAndTidType(
                            tid, entity.getId(), RealmConstant.TID_TYPE_REALM);
            log.info(">>> addAdmin, exist: {}, userAccount: {}", exist, entity.getAccount());
            if (!exist) {
                TenantAdminEntity adminEntity = new TenantAdminEntity();
                adminEntity.setTid(tid);
                adminEntity.setUid(entity.getId());
                adminEntity.setTidType(RealmConstant.TID_TYPE_REALM);
                super.insert(adminEntity);
            }
        }
        return true;
    }

    public boolean removeAdmin(List<TenantAdminEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        tenantAdminRepository.deleteAllById(list.stream().map(m -> m.getId()).toList());
        return true;
    }

    /*
     * @Author root
     * @Description 通过uid获取外部用户
     * @Date 11:39 2023/9/15
     * @Param
     * @return
     **/
    public List<UserEntity> queryExternalUser(BaseQueryModel model) {
        if (Strings.isEmpty(model.getKey())) {
            return Lists.newArrayList();
        }
        String tid = super.getTid();
        Specification<UserEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("id"), model.getKey()));
            predicates.add(cb.notEqual(root.get("tid"), tid));
            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
            return query.getRestriction();
        };
        return userService.findAll(spec);
        /*UserEntity userEntity = userService.findById(model.getKey());
        if (Objects.isNull(userEntity)) {
            return Lists.newArrayList();
        }
        return new ArrayList<UserEntity>(){{add(userEntity);}};*/
    }

    ;

    public List<TenantAdminEntity> listAdmin(UserQueryModel model) {
        String tid = super.getTid();
        List<TenantAdminEntity> list =
                tenantAdminRepository.findByTidAndTidType(tid, RealmConstant.TID_TYPE_REALM);
        List<TenantAdminEntity> ret = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(list)) {
            List<UserEntity> userList
                    = userService.listByIds(list.stream().map(m -> m.getUid()).toList());
            Map<String, UserEntity> map = Maps.newHashMap();
            userList.stream().forEach(v->map.put(v.getId(), v));
            for (TenantAdminEntity adminEntity : list) {
                UserEntity entity = map.get(adminEntity.getUid());
                if (RealmConstant.ACCOUNT_ATTR_ROOT.equals(entity.getAccountAttr())) {
                    continue;
                }
                if (RealmConstant.ACCOUNT_ATTR_MASTER.equals(entity.getAccountAttr())) {
                    continue;
                }
                adminEntity.setEmail(entity.getEmail());
                adminEntity.setPhone(entity.getPhone());
                adminEntity.setAccount(entity.getAccount());
                adminEntity.setRealName(entity.getRealName());
                adminEntity.setLastLoginTime(entity.getLastLoginTime());
                if (adminEntity.getInnerAdmin() == null) {
                    adminEntity.setInnerAdmin(false);
                } else {
                }
                ret.add(adminEntity);

            }
            return ret;
        }
        return Lists.newArrayList();
    }


    public List<TenantAdminEntity> findByUidAndTidType(String uid, String tidType) {
        return tenantAdminRepository.findByUidAndTidType(uid, tidType);
    }

    public List<TenantAdminEntity> findByTidAndTidType(String tid, String tidType) {
        return tenantAdminRepository.findByTidAndTidType(tid, tidType);
    }

    public List<TenantAdminEntity> findByUid(String uid) {
        return tenantAdminRepository.findByUid(uid);
    }
}
