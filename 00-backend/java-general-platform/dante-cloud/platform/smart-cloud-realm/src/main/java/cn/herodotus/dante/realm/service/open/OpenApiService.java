package cn.herodotus.dante.realm.service.open;

import cn.herodotus.dante.realm.entity.DeptEntity;
import cn.herodotus.dante.realm.entity.GroupEntity;
import cn.herodotus.dante.realm.entity.GroupUserEntity;
import cn.herodotus.dante.realm.service.DeptService;
import cn.herodotus.dante.realm.service.GroupService;
import cn.herodotus.dante.realm.service.GroupUserService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Slf4j
@Service
public class OpenApiService {

    private DeptService deptService;
    private GroupUserService groupUserService;
    private GroupService groupService;

    @Autowired
    public OpenApiService(DeptService deptService,
                          GroupUserService groupUserService,
                          GroupService groupService) {
        this.deptService = deptService;
        this.groupUserService = groupUserService;
        this.groupService = groupService;
    }

    public List<GroupEntity> myGroups() {

        String tid = CommonSecurityContextHolder.getTid();
        String uid = CommonSecurityContextHolder.getUid();
        List<GroupUserEntity> list = groupUserService.findByTidAndUid(tid, uid);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return groupService.listByIds(list.stream().map(m->m.getGroupId()).toList());
    }
    public List<DeptEntity> deptTree() {

        String tid = CommonSecurityContextHolder.getTid();
        return deptService.getTrees();
    }
}
