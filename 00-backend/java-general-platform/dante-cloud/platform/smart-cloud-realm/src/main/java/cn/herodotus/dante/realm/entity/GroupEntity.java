package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "sys_group" )
@Data
@EqualsAndHashCode(callSuper=true)
public class GroupEntity extends CommonTenantEntity {

    @Column(name = "group_name", length = 64)
    private String groupName;
    @Column(name = "group_tag", length = 64)
    private String groupTag;
    @Column(name = "remark", length = 64)
    private String remark;
    @Column(name = "group_type", length = 32)
    private String groupType;

    @Transient
    private boolean red;
}
