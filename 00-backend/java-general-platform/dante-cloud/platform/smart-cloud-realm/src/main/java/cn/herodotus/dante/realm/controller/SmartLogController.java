package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.service.SmartLogService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author root
 * @description TODO
 * @date 2023/8/29 8:52
 */
@RestController
@RequestMapping("/log")
@Tag(name = "日志管理管理")
public class SmartLogController {

    @Resource
    private SmartLogService smartLogService;

    @Operation(summary = "管理员操作日志", description = "管理员操作日志")
    @PostMapping("/adminList")
    public Result<PageResponse> adminList(@Valid @RequestBody BaseQueryModel model) {
        PageResponse ret = smartLogService.adminList(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "用户行为日志", description = "用户行为日志")
    @PostMapping("/userList")
    public Result<PageResponse> userList(@Valid @RequestBody BaseQueryModel model) {
        PageResponse ret = smartLogService.userList(model);
        return Result.success(Feedback.OK, ret);
    }


}
