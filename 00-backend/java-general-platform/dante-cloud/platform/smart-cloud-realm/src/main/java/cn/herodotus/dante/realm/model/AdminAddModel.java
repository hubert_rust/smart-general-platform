package cn.herodotus.dante.realm.model;

import cn.herodotus.dante.realm.entity.ApplicationEntity;
import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

@Data
public class AdminAddModel {
    private String tid;
    private String uid;
    private String tidType;
    List<ApplicationEntity> apps = Lists.newArrayList();
}
