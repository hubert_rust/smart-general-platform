package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author root
 * @description 通用安全，全局参数，暂时不按用户空间/租户设置
 * @date 2023/9/28 10:16
 */
@Entity
@Table(name = "sys_common_security")
@Data
@EqualsAndHashCode(callSuper=true)
public class CommonSecurityEntity extends CommonTenantEntity {
    // 基础安全
    @Column(name = "verify_old_email")
    private Boolean verifyOldEmail;
    @Column(name = "verify_old_phone")
    private Boolean verifyOldPhone;

    // cookie过期时间
    @Column(name = "cookie_token_second")
    private Long cookieTokenSecond;

    // 登录安全
    // 是否账号锁定
    @Column(name = "account_lock")
    private String accountLock;

    // 周期内限定时间
    @Column(name = "cycle_time")
    private Integer cycleTime;

    // 周期内限定次数
    @Column(name = "cycle_num")
    private Integer cycleNum;

    // APP验证安全
    // 二维码有效时间
    @Column(name = "qrcode_expire")
    private Integer qrcodeExpire;

    // ticket有效时间
    @Column(name = "ticket_expire")
    private Integer ticketExpire;

    @Column(name = "browser_ticket")
    private Boolean browserTicket;

    // day hour minute second
    @Transient
    private String cookieSecondUnit = "second";
    @Transient
    private String cycleTimeUnit = "second";
    @Transient
    private String qrcodeExpireUnit = "second";
    @Transient
    private String ticketExpireUnit = "second";

}
