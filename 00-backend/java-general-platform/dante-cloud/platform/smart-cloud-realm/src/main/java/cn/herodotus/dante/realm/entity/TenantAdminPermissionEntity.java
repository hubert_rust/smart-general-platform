package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "sys_tenant_admin_permission")
@Data
@EqualsAndHashCode(callSuper=true)
public class TenantAdminPermissionEntity extends CommonTenantEntity {
    @Column(name = "uid", length = 64)
    private String uid;
    @Column(name = "uid_tid", length = 64)
    private String uidTid;

    @Column(name = "client_id", length = 64)
    private String clientId;
    // menu api
    @Column(name = "object_type", length = 16)
    private String objectType;

    // 是否共享到应用市场
    @Column(name = "identity", length = 64)
    private String identity;

    @Column(name = "inner_admin")
    private boolean innerAdmin;
}
