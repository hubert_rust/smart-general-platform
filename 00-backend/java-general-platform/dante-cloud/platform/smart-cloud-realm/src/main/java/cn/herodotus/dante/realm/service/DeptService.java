/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.DeptEntity;
import cn.herodotus.dante.realm.repository.DeptRepository;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

/**
 * <p>Description:  </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class DeptService extends BaseService<DeptEntity, String> {

    private DeptRepository deptRepository;

    @Autowired
    public DeptService(DeptRepository deptRepository) {
        this.deptRepository = deptRepository;
    }

    @Override
    public BaseRepository<DeptEntity, String> getRepository() {
        return this.deptRepository;
    }

    public boolean deleteDept(String id) {
        //smart-auth: pending
        String tid = "0";
        DeptEntity entity = super.findById(id);
        List<Object[]> list = deptRepository.findByRecurise(entity.getInnerCode(), tid);
        List<DeptEntity> dels = Lists.newArrayList();
        for (Object[] objects : list) {
            DeptEntity find = new DeptEntity();
            find.setId(objects[0].toString());
            dels.add(find);
        }
        super.deleteAll(dels);
        return true;
    }


    public DeptEntity update(DeptEntity entity) {
        return super.save(entity);
    }

    public DeptEntity insert(DeptEntity entity) {
        try {
            //smart-auth: pending
            String tid = "0";


            int find = RealmConstant.DEPT_INNER_CODE_INIT;
            List<DeptEntity> alls = deptRepository.findByTid(tid);
            alls.stream()
                    .forEach(v -> v.setIntInnerCode(Integer.valueOf(v.getInnerCode())));
            List<DeptEntity> list
                    = alls.stream()
                    .sorted(Comparator.comparing(DeptEntity::getIntInnerCode))
                    .toList();

            for (int i = 0; i < list.size(); i++) {
                int innerCode = Integer.valueOf(list.get(i).getInnerCode()).intValue();
                //最后一个
                if (i == list.size() - 1) {
                    find = innerCode + 1;
                    break;
                } else {
                    int next = Integer.valueOf(list.get(i + 1).getInnerCode()).intValue();
                    if ((next - innerCode) > 2) {
                        find = innerCode + 1;
                        break;
                    }
                }
            }


            String innerCode = Strings.padStart(find + "", 5, '0');
            //此处parentId是innerCode
            String parentId = entity.getParentId();
            entity.setInnerCode(innerCode);
            if (parentId == null || "-1".equals(parentId)) {
                entity.setPath(innerCode);
            } else {

                //添加的是部门，找到父节点
                DeptEntity findParent = deptRepository.findByInnerCodeAndTid(parentId, tid);

                String path = findParent.getPath() + "-" + innerCode;
                entity.setPath(path);
            }
            return super.insert(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("添加部门失败");
        }
    }

    public static void menuSort(List<DeptEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<DeptEntity> ret = list.stream()
                .sorted(Comparator.comparing(DeptEntity::getVoOrder)).toList();
        list.clear();
        list.addAll(ret);
        list.stream().forEach(v -> {
            List objs = v.getChildren();
            List<DeptEntity> pf = objs;
            menuSort(pf);
        });

    }

    public List<DeptEntity> getTrees() {

        String tid = super.getTid();
        Specification<DeptEntity> specification = new Specification<DeptEntity>() {
            @Override
            public Predicate toPredicate(Root<DeptEntity> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                return cb.equal(root.get("tid"), tid);
            }
        };
        List<DeptEntity> list = super.findAll(specification);


        DefaultTreeBuildFactory<DeptEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<DeptEntity> tree1 = factory1.doTreeBuild(list);
        menuSort(tree1);
        return tree1;
    }

    public List<DeptEntity> findByTidAndPathLike(String tid, String innerCode) {
        return deptRepository.findByTidAndPathLike(tid, innerCode);
    }

    public DeptEntity findByInnerCodeAndTid(String innerCode, String tid) {
        return deptRepository.findByInnerCodeAndTid(innerCode, tid);
    }

    public List<DeptEntity> listByIds(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Lists.newArrayList();
        }
        return deptRepository.findAllById(ids);
    }
}
