package cn.herodotus.dante.realm.model.user;

import cn.herodotus.dante.realm.model.BaseQueryModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class UserQueryModel extends BaseQueryModel {
    private boolean direct;
    private String deptInnerCode;
    private String path;     //like查询
    private String mainDeptId;
    private String mainDeptCode;

}
