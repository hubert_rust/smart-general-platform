package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.definition.CommonTenantEntity;
import com.google.common.collect.Lists;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Entity
@Table(name = "sys_tenant" )
@Data
@EqualsAndHashCode(callSuper=true)
public class TenantEntity extends CommonTenantEntity implements Tree {

    @Column(name = "parent_id", length = 64)
    private String parentId;
    @Column(name = "tenant_name", length = 64)
    private String tenantName;
    @Column(name = "remark", length = 64)
    private String remark;

    private String logo;
    @Column(name = "tid_type", length = 16)
    private String tidType;  //realm, tenant
    @Column(name = "status")
    private String status;

    // 获取用户空间/租户管理员使用
    @Transient
    List<TenantEntity> children = Lists.newArrayList();
    @Transient
    List<ApplicationEntity> apps = Lists.newArrayList();

    @Override
    @Transient
    public List getChildrenNode() {
        return this.children;
    }

    @Override
    @Transient
    public String getNodeId() {
        return getId();
    }

    @Override
    @Transient
    public String getNodeParentId() {
        return this.parentId == null ? null : this.parentId;
    }

    @Override
    @Transient
    public void setChildrenNode(List children) {
        this.setChildren(children);
    }

}
