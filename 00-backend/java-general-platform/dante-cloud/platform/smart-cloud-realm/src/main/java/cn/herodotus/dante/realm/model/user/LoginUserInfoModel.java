package cn.herodotus.dante.realm.model.user;

import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/8/12 21:02
 */

@Data
public class LoginUserInfoModel implements Serializable  {
    // 用户信息
    private UserEntity user;
    // 菜单单独加载
    private List<ResMenuEntity> menus;
    // 当前wks
    private TenantEntity currentWks;
    // 用户空间列表
    private List<TenantEntity>  selfs = Lists.newArrayList();

    // 代理其他的
    private List<TenantEntity>  agents = Lists.newArrayList();
}
