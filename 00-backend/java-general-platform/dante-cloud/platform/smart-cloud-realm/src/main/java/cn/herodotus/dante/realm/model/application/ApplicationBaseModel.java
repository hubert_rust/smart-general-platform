package cn.herodotus.dante.realm.model.application;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author root
 * @description TODO
 * @date 2023/9/22 10:04
 */
@Data
public class ApplicationBaseModel implements Serializable {
    private String id;
    private String appTag;
    private Boolean allShow;
    private String clientId;
    private String remark;
    private String logo;
    private String appType;
    private String appInnerName;
    private String protocol;
    private Integer server;
    private String status;
    private Date createTime;
    private Date updateTime;

    // sso self
    private String clientFrom;
}
