package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Entity
@Table(name = "sys_account_auth" )
@Data
@EqualsAndHashCode(callSuper=true)
public class UserAuthEntity extends CommonTenantEntity {


    @Column(name = "uid", length = 64)
    private String uid;

    @Column(name = "identity_type", length = 32)
    private String identityType;
    @Column(name = "identity_title", length = 32)
    private String identityTitle;
    @Column(name = "identity", length = 64)
    private String identity;

    @Column(name = "credential", length = 64)
    private String credential;
    @Column(name = "verified")
    private Integer verified;

    @Column(name = "change_pwd")
    private Date changePwd;
    @Column(name = "pwd_level")
    private Integer pwdLevel;
    private String status; //lock, active,

    @Column(name="last_login_time")
    private Date lastLoginTime;

    @Column(name="last_login_ip")
    private String lastLoginIp;

    @Column(name="last_login_application")
    private String lastLoginApplication;

}
