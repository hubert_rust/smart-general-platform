package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.ApplicationSubjectEntity;
import cn.herodotus.dante.realm.entity.ApplicationTenantSsoEntity;
import cn.herodotus.dante.realm.entity.GroupUserEntity;
import cn.herodotus.dante.realm.model.application.ApplicationPermitModel;
import cn.herodotus.dante.realm.repository.ApplicationRepository;
import cn.herodotus.dante.realm.repository.ApplicationSubjectRepository;
import cn.herodotus.dante.realm.repository.ApplicationTenantSsoRepository;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.ApplicationSubjectService;
import cn.herodotus.dante.realm.service.ApplicationTenantSsoService;
import cn.herodotus.dante.realm.service.GroupUserService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author root
 * @description 用户详情中，用户应用授权列表  *
 * @date 2023/9/20 10:52
 */
@Slf4j
@Service
public class UserApplicationPermitServiceImpl {
    @Resource
    private ApplicationService applicationService;
    @Resource
    private ApplicationSubjectService applicationSubjectService;
    @Resource
    private GroupUserService groupUserService;
    @Resource
    private ApplicationTenantSsoService applicationTenantSsoService;


    private List<ApplicationPermitModel> getFromApplicationIds(List<ApplicationEntity> allApps,
                                                               List<ApplicationSubjectEntity> subjects,
                                                               List<ApplicationEntity> selfShow,
                                                               List<ApplicationTenantSsoEntity> ssoShow) {
        Map<String, ApplicationEntity> map = Maps.newHashMap();
        List<ApplicationPermitModel> ret = Lists.newArrayList();
        // 授权对象中查询出

        allApps.stream().forEach(v -> map.put(v.getClientId(), v));

        for (ApplicationSubjectEntity subject : subjects) {
            String client = subject.getClientId();
            String subjectType = subject.getSubjectType();
            String appTag = map.get(client).getAppTag();
            ApplicationPermitModel model = new ApplicationPermitModel();
            model.setId(IDContainer.INST.getUUID(20, false));
            model.setAppTag(appTag);
            model.setClientId(client);
            model.setClientFrom(subject.getClientFrom());
            model.setSubjectType(subjectType);
            ret.add(model);
        }
        for (ApplicationEntity entity : selfShow) {
            ApplicationPermitModel model = new ApplicationPermitModel();
            model.setId(IDContainer.INST.getUUID(20, false));
            model.setAppTag(entity.getAppTag());
            model.setClientId(entity.getClientId());
            model.setClientFrom(RealmConstant.APP_FROM_SELF);
            model.setSubjectType(RealmConstant.APP_FROM_SELF_SHOW);
            ret.add(model);
        }
        for (ApplicationTenantSsoEntity entity : ssoShow) {
            String appTag = map.get(entity.getClientId()).getAppTag();
            ApplicationPermitModel model = new ApplicationPermitModel();
            model.setId(IDContainer.INST.getUUID(20, false));
            model.setAppTag(appTag);
            model.setClientId(entity.getClientId());
            model.setClientFrom(RealmConstant.APP_FROM_SSO);
            model.setSubjectType(RealmConstant.APP_FROM_SSO_SHOW);
            ret.add(model);
        }

        return ret;
    }

    // 应用授权列表，包括自建应用和SSO应用
    // 用户登录后查询应用列表也可以使用该接口
    public List<ApplicationPermitModel> appPermit(String uid) {
        String tid = CommonSecurityContextHolder.getTid();
        String deptId = CommonSecurityContextHolder.getUser().getMainDeptId();

        // 这里只包含了当前tid的应用，需要加入sso的
        List<ApplicationEntity> allApps =
                ((ApplicationRepository) applicationService.getRepository())
                        .findByTidAndDeletedAndStatus(
                                tid,
                                RealmConstant.DELETED_VALUE_DEFAULT,
                                RealmConstant.STATUS_ACTIVE);

        List<String> noIns = Lists.newArrayList();
        // 自建应用allShow
        List<ApplicationEntity> selfShow = allApps.stream()
                .filter(p -> Objects.nonNull(p.getAllShow()) && p.getAllShow()).toList();
        selfShow.stream().forEach(v -> noIns.add(v.getClientId()));
        // sso应用allShow
        List<ApplicationTenantSsoEntity> ssoShow
                = ((ApplicationTenantSsoRepository) applicationTenantSsoService.getRepository())
                .findByTidAndAllShow(tid, true);
        ssoShow.stream().forEach(v -> noIns.add(v.getClientId()));

        if (CollectionUtils.isNotEmpty(ssoShow)) {
            List<ApplicationEntity> ssoApp = applicationService.findByClientIdIn(
                    ssoShow.stream().map(m->m.getClientId()).toList()
            );
            allApps.addAll(ssoApp);
        }

        // 对应自建应用和SSO 应用中，如果allShow字段为true，那么直接就可以看到应用
        // 自建应用 和 SSO 应用
        List<GroupUserEntity> groups = groupUserService.findByTidAndUid(tid, uid);
        List<String> identitys = Lists.newArrayList();
        identitys.add(uid);
        if (Strings.isNotEmpty(deptId)) {
            identitys.add(deptId);
        }
        // 这里只是静态用户组
        groups.stream()
                .filter(p -> RealmConstant.GROUP_TYPE_STATIC.equals(p.getGroupType()))
                .forEach(v -> identitys.add(v.getGroupId()));

        // subject中已经区分了clientFrom(self sso)
        List<ApplicationSubjectEntity> subjects = null;
        if (CollectionUtils.isEmpty(noIns)) {
            subjects = applicationSubjectService.findByTidAndIdentityIn(tid, identitys);
        } else {
            subjects
                    = ((ApplicationSubjectRepository) applicationSubjectService.getRepository())
                    .findByTidAndIdentityInAndClientIdNotIn(
                            tid, identitys, noIns);
            log.info(">>> subjects");
        }

        return getFromApplicationIds(allApps, subjects, selfShow, ssoShow);
    }


}
