package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.model.application.ApplicationPermitModel;
import cn.herodotus.dante.realm.service.group.UserApplicationPermitServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
@Tag(name = "用户管理-应用授权")
@Validated
public class UserApplicationPermitController {
    @Resource
    private UserApplicationPermitServiceImpl userApplicationPermitService;

    @Operation(summary = "应用授权列表", description = "应用授权列表")
    @PostMapping("/appPermit/{uid}")
    public Result<List<ApplicationPermitModel>> appPermit(@PathVariable String uid) {
        List<ApplicationPermitModel> ret = userApplicationPermitService.appPermit(uid);
        return Result.success(Feedback.OK, ret);
    }

}
