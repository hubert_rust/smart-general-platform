/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.entity.RoleEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.repository.RoleRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class RoleService extends BaseService<RoleEntity, String> {

    private RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public BaseRepository<RoleEntity, String> getRepository() {
        return this.roleRepository;
    }

    public boolean updateExt(RoleEntity entity) {
        RoleEntity roleEntity = super.findById(entity.getId());
        roleEntity.setExt1(Strings.isEmpty(entity.getExt1()) ? "" : entity.getExt1());
        roleEntity.setExt2(Strings.isEmpty(entity.getExt2()) ? "" : entity.getExt2());
        roleEntity.setExt3(Strings.isEmpty(entity.getExt2()) ? "" : entity.getExt3());
        super.save(roleEntity);
        return true;
    }

    public RoleEntity insert(RoleEntity entity) {
        String tid = super.getTid();
        if (Strings.isEmpty(entity.getRoleName())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "角色名称不能为空值");
        }
        if (roleRepository.existsByTidAndRoleName(tid, entity.getRoleName())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "角色名称已经存在");
        }
        if (Strings.isEmpty(entity.getRoleCode())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "角色编码不能为空值");
        }
        if (roleRepository.existsByTidAndRoleCode(tid, entity.getRoleCode())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "角色编码已经存在");
        }
        return super.insert(entity);
    }
    public boolean update(RoleEntity entity) {
        String tid = super.getTid();
        if (Strings.isEmpty(entity.getRoleCode())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "角色编码不能为空值");
        }
        RoleEntity  roleEntity = super.findById(entity.getId());
        if (entity.getRoleName().equals(roleEntity.getRoleName())) {
        }
        else {
            if (roleRepository.existsByTidAndRoleName(tid, entity.getRoleName())) {
                throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "角色名称已经存在");
            }
        }
        if (entity.getRoleCode().equals(roleEntity.getRoleCode())) {
        }
        else {
            if (roleRepository.existsByTidAndRoleCode(tid, entity.getRoleCode())) {
                throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "角色编码已经存在");
            }
        }
        roleEntity.setRoleName(entity.getRoleName());
        roleEntity.setRoleCode(entity.getRoleCode());
        roleEntity.setRemark(entity.getRemark());
        super.save(roleEntity);

        return true;
    }

    public boolean activeAndDeactive(String id, String status) {
        RoleEntity entity = super.findById(id);
        entity.setStatus(status);
        super.save(entity);
        return true;
    }

    public PageResponse<RoleEntity> list(BaseQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);

        Specification<RoleEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("roleName"), model.getKey()));
            }
            predicates.add(cb.equal(root.get("tid"), super.getTid()));

            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<RoleEntity> resp = new PageResponse<>();
        resp.setTotalCount((int) num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }
}
