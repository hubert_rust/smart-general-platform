package cn.herodotus.dante.realm.model.response;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.engine.rest.protect.crypto.processor.SM4CryptoProcessor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO 根据返回结果，跳转
 * @date 2023/8/21 11:06
 */
@Builder
@Data
public class ResponseModel implements Serializable {
    private String client;
    private String uid;
    private String tid;

    private String protocol;
    private String authUrl;
    private String logoutUrl;
    private String accessToken;
    private String slt;
    private String loginType;

    // 默认管理端
    private String appType = RealmConstant.APP_TYPE_MGT;

    // 跳转地址
    private String redirect;

    //跳转到登录页面，登录成功后，跳回地址，通常是uri地址(不是相对路径)
    private String back;

    public static void main(String[] args) {
        SM4CryptoProcessor smc = new SM4CryptoProcessor();

        var name = smc.decrypt("cffd2c3c6df71e274196690ebfe858d5", "a05fe1fc50ed42a4990c6c6fc4bec398");
        System.out.println(name);
        // var pwd = smc.decrypt("00ef297efdb93560db41c00c22d28cdb", "a05fe1fc50ed42a4990c6c6fc4bec398");
        var pwd = smc.decrypt("f0afe58a63fd8dab680b906bf68f1476", "a05fe1fc50ed42a4990c6c6fc4bec398");
        System.out.println();
    }
}

