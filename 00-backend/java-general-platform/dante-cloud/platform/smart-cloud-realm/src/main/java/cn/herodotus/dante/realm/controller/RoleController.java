package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.RoleEntity;
import cn.herodotus.dante.realm.entity.RoleSubjectEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.application.ApplicationBaseModel;
import cn.herodotus.dante.realm.model.config.EnvQueryModel;
import cn.herodotus.dante.realm.service.RoleService;
import cn.herodotus.dante.realm.service.group.RoleManagerServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
@Tag(name = "角色管理-基本配置")
public class RoleController {

    @Resource
    private RoleService roleService;
    @Resource
    private RoleManagerServiceImpl roleManagerService;


    @Operation(summary = "创建角色", description = "创建角色")
    @PostMapping("/insert")
    public Result<RoleEntity> insert(@Valid @RequestBody RoleEntity entity) {
        RoleEntity ret = roleService.insert(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除角色", description = "删除角色")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        roleManagerService.deleteRole(id);
        return Result.success(Feedback.OK, true);
    }
    @Operation(summary = "修改角色", description = "修改角色")
    @PostMapping("/update")
    public Result<Boolean> update(@Valid @RequestBody RoleEntity entity) {
        Boolean ret = roleService.update(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "角色详情", description = "修改角色")
    @PostMapping("/detail/{id}")
    public Result<RoleEntity> detail(@PathVariable String id) {
        RoleEntity ret = roleService.findById(id);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "激活角色", description = "激活角色")
    @PostMapping("/active/{id}")
    public Result<Boolean> active(@PathVariable String id) {
        Boolean ret = roleService.activeAndDeactive(id, RealmConstant.STATUS_ACTIVE);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "禁用角色", description = "禁用角色")
    @PostMapping("/deactive/{id}")
    public Result<Boolean> deactive(@PathVariable String id) {
        Boolean ret = roleService.activeAndDeactive(id, RealmConstant.STATUS_DEACTIVE);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "查询角色", description = "查询角色")
    @PostMapping("/list")
    public Result<PageResponse> list(@Valid @RequestBody EnvQueryModel model) {
        PageResponse ret = roleService.list(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "角色主体列表", description = "角色主体列表")
    @PostMapping("/subjectList")
    public Result<PageResponse> subjectList(@RequestBody BaseQueryModel model) {
        PageResponse ret = roleManagerService.roleSubjectList(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "添加角色主体", description = "添加角色主体")
    @PostMapping("/addSubject")
    public Result<PageResponse> addSubject(@RequestBody List<RoleSubjectEntity> list) {
        Boolean ret = roleManagerService.addRoleSubject(list);
        return Result.success(Feedback.OK, null);
    }
    @Operation(summary = "删除角色主体", description = "删除角色主体")
    @PostMapping("/deleteSubject")
    public Result<PageResponse> deleteSubject(@RequestBody List<RoleSubjectEntity> list) {
        Boolean ret = roleManagerService.deleteRoleSubject(list);
        return Result.success(Feedback.OK, null);
    }

    @Operation(summary = "应用列表", description = "应用列表")
    @PostMapping("/apps")
    public Result<List<ApplicationBaseModel>> apps(@RequestBody BaseQueryModel model) {
        List<ApplicationBaseModel> ret = roleManagerService.roleAppList(model);
        return Result.success(Feedback.OK, ret);
    }
}
