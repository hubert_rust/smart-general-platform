package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

//用户归属哪个租户
@Entity
@Table(name = "sys_tenant_account" )
@Data
@EqualsAndHashCode(callSuper=true)
public class TenantAccountEntity extends CommonTenantEntity {
    @Column(name = "tid_type", length = 64)
    private String tidType;
    private String uid;
}
