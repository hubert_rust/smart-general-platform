package cn.herodotus.dante.realm.model.reg;

import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.entity.UserAuthEntity;
import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

//在root创建账号
@Data
public class UserModel implements Serializable {
    private String createType;  //mobile, email, account

    private String account;
    private String pwd;

    private String phone;
    private String email;
    private String realName;
    private String profile;
    private String mainDeptId;
    private String mainDeptCode;

    private String uid;
    private String id;
    private String tid;

    private String remark;
    private String gender;
    private String status;
    private Date lastLoginTime;
    private String lastLoginIp;
    private String lastLoginApplication;


    //该账户下账号
    private List<UserAuthEntity> list = Lists.newArrayList();

    //该注册账号下所有租户(空间+租户)
    private List<TenantEntity> tenants = Lists.newArrayList();

}
