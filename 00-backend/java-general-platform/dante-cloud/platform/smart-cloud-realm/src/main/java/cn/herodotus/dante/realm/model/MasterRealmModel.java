package cn.herodotus.dante.realm.model;

import lombok.Data;

//用户注册或者root创建的用户空间
@Data
public class MasterRealmModel {
    private String name;
    private String remark;
    private String account;
    private String pwd;

    private String profile;
    private String status;
}
