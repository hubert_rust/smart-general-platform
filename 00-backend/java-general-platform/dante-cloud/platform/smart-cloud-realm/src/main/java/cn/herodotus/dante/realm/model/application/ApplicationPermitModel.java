package cn.herodotus.dante.realm.model.application;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/9/20 11:41
 */
@Data
public class ApplicationPermitModel implements Serializable {
    // 非真实id
    private String id;
    private String appTag;
    private String clientFrom;
    private String clientId;
    private String subjectType;

    // roleName: 角色名称
    private String roleName;
    private boolean selected = false;
}
