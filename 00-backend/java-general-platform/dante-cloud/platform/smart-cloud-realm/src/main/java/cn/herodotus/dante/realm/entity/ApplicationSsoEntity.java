package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "sys_application_sso")
@Data
@EqualsAndHashCode(callSuper=true)
public class ApplicationSsoEntity extends CommonTenantEntity {

    // 应用id
    @Column(name= "client_id")
    private String clientId;

}
