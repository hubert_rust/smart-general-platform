package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

//环境变量
@Entity
@Table(name = "sys_config" )
@Data
@EqualsAndHashCode(callSuper=true)
public class ConfigEntity extends CommonTenantEntity {

    private String key;
    private String value;

    private Integer status;
    private String remark;
}
