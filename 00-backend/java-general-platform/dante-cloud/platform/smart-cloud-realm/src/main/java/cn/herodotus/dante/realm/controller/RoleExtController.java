package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.RoleEntity;
import cn.herodotus.dante.realm.service.RoleService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
@Tag(name = "角色管理-扩展字段")
public class RoleExtController {
    @Resource
    private RoleService roleService;



    @Operation(summary = "创建角色", description = "创建角色")
    @PostMapping("/updateExt")
    public Result<RoleEntity> updateExt(@Valid @RequestBody RoleEntity entity) {
        RoleEntity ret = roleService.save(entity);
        return Result.success(Feedback.OK, ret);
    }


}
