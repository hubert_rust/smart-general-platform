package cn.herodotus.dante.realm.service.recycle;

import cn.herodotus.dante.definition.CommonTenantEntity;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.dante.realm.util.ITenantServiceTbService;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/7 9:19
 */
@Service
@Slf4j
public class TenantServiceRecycle implements ITenantServiceTbService {

    private UserService userService;
    private UserAuthService userAuthService;
    private PostService postService;
    private DeptService deptService;
    private GroupService groupService;
    private GroupUserService groupUserService;
    private SmartLogService smartLogService;

    public List<BaseService<? extends CommonTenantEntity, String>> services = Lists.newArrayList();
    @Autowired
    public TenantServiceRecycle(UserService userService,
                                UserAuthService userAuthService,
                                PostService postService,
                                DeptService deptService,
                                GroupService groupService,
                                GroupUserService groupUserService,
                                SmartLogService smartLogService) {
        this.userService = userService;
        this.userAuthService = userAuthService;
        this.postService = postService;
        this.deptService = deptService;
        this.groupService = groupService;
        this.groupUserService = groupUserService;
        this.smartLogService = smartLogService;

        services.add(userService);
        services.add(userAuthService);
        services.add(postService);
        services.add(deptService);
        services.add(groupService);
        services.add(groupUserService);
        services.add(smartLogService);
    }



    @Transactional(rollbackFor = TransactionalRollbackException.class)
    @Override
    public Boolean removeServiceData(String tid) {
        for (BaseService<? extends CommonTenantEntity, String> service : services) {
            service.deleteByTid(tid);
        }
        return true;
    }
}
