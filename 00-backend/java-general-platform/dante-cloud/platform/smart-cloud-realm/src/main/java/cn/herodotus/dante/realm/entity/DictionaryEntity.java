package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "sys_dict" )
@Data
@EqualsAndHashCode(callSuper=true)
public class DictionaryEntity extends CommonTenantEntity {
    @Column(name = "dict_type")
    private String dictType;
    @Column(name = "data_name")
    private String dataName;
    @Column(name = "data_key")
    private String dataKey;
    @Column(name = "data_path")
    private String dataPath;
    @Column(name = "parent_id")
    private String parentId;
    @Column(name = "data_value")
    private String dataValue;
    @Column(name = "data_order")
    private Integer dataOrder;
    @Column(name = "data_ext")
    private String dataExt;
    private String deleted;
    @Column(name = "defaulted")
    private Integer defaulted;
    @Column(name = "status")
    private Integer status;
    @Column(name = "remark")
    private String remark;





}
