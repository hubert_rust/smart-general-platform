package cn.herodotus.dante.realm.model.config;

import cn.herodotus.dante.realm.model.BaseQueryModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class EnvQueryModel extends BaseQueryModel {
    private String key;
    private String value;
}
