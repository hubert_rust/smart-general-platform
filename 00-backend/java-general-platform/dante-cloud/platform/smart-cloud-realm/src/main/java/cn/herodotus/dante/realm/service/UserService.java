/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.constant.AccountConstant;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.model.user.UserQueryModel;
import cn.herodotus.dante.realm.repository.UserRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class UserService extends BaseService<UserEntity, String> {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public BaseRepository<UserEntity, String> getRepository() {
        return this.userRepository;
    }


    public boolean activeAndDeactive(List<UserEntity> ids, String status) {
        List<String> list = ids.stream().map(m->m.getId()).toList();
        List<UserEntity> users = userRepository.findByIdIsIn(list);
        users.stream().forEach(v->v.setStatus(status));
        super.saveAll(users);

        return true;
    }
    @NotNull
    public UserEntity saveUserEntity(UserModel model, String tid, String id) {

        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        userEntity.setTid(tid);
        if (AccountConstant.REG_TYPE_EMAIL.equals(model.getCreateType())) {
            userEntity.setEmail(model.getAccount());
        } else if (AccountConstant.REG_TYPE_MOBILE.equals(model.getCreateType())) {
            userEntity.setPhone(model.getAccount());
        } else if (AccountConstant.REG_TYPE_ACCOUNT.equals(model.getCreateType())) {
        }
        userEntity.setCreateType(model.getCreateType());
        userEntity.setAccount(model.getAccount());
        userEntity.setMainDeptId(model.getMainDeptId());
        userEntity.setMainDeptCode(model.getMainDeptCode());

        userEntity.setStatus(RealmConstant.REG_OPTYPE_ACTIVE);
        super.insert(userEntity);
        return userEntity;
    }
    public UserEntity updateUserEntity(UserModel model) {
        UserEntity userEntity = super.findById(model.getId());
        userEntity.setRemark(model.getRemark());
        userEntity.setGender(model.getGender());
        userEntity.setEmail(model.getEmail());
        userEntity.setPhone(model.getPhone());
        userEntity.setProfile(model.getProfile());
        userEntity.setStatus(model.getStatus());
        return super.save(userEntity);
    }

    public List<UserEntity> listByIds(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Lists.newArrayList();
        }
        return userRepository.findAllById(ids);
    }


    public PageResponse<UserEntity> list(UserQueryModel model, Collection<String> col) {
        String tid = super.getTid();
        Pageable pageable = QueryUtil.pageable(model);

        Specification<UserEntity> spec = (root, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();

            List<Predicate> ors = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                ors.add(cb.like(root.get("realName"), "%" + model.getKey() +  "%"));
                ors.add(cb.like(root.get("account"), "%" + model.getKey() +  "%"));
                ors.add(cb.like(root.get("phone"), "%" + model.getKey() + "%"));
                ors.add(cb.like(root.get("email"), "%s" + model.getKey() + "%"));
                Predicate[] orArr = new Predicate[ors.size()];
                predicates.add(cb.or(ors.toArray(orArr)));
            }

            Predicate[] predOrs = null;
            Predicate predOr = null;

            List<Predicate> ands = new ArrayList<>();
            //初始时候，没有部门innerCode
            if (Strings.isNotEmpty(model.getDeptInnerCode())) {
                //部门下直接用户
                if (model.isDirect()) {
                    ands.add(cb.equal(root.get("mainDeptId"), model.getMainDeptId()));
                } else {
                    ands.add(root.get("mainDeptCode").in(col));
                }
            }
            ands.add(cb.equal(root.get("tid"), tid));
            Predicate[] predAnds = new Predicate[ands.size()];
            // 判断如果没有or
            if (CollectionUtils.isEmpty(predicates)) {
                return query.where(cb.and(ands.toArray(predAnds))).getRestriction();
            }
            else {
                predicates.add(cb.and(ands.toArray(predAnds)));
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
                // Predicate predAnd = cb.and(ands.toArray(predAnds));
                // return query.where(predOr, predAnd).getRestriction();
            }
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<UserEntity> resp = new PageResponse<>();
        resp.setTotalCount((int) num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }
}
