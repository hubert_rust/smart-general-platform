/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.repository;


import cn.herodotus.dante.realm.entity.RoleResourceEntity;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleResourceRepository extends BaseRepository<RoleResourceEntity, String> {
    // 用户详情中查看用户角色的时候
    @Query(value ="select distinct new cn.herodotus.dante.realm.entity.RoleResourceEntity(" +
            " c.roleId, c.clientId, c.clientFrom) from RoleResourceEntity c " +
            "where c.tid = ?1 and c.roleId in ?2")
    List<RoleResourceEntity>  findRoleApplication(String tid, List<String> roleIds);

    // 登录用户api访问判断
    boolean existsByTidAndClientIdAndRoleIdInAndObjectTypeAndIdentity(
            String tid, String clientId,
            List<String> roleIds, String objectType, String identity);

    // 通过角色ID获取菜单资源
    List<RoleResourceEntity> findByTidAndClientIdAndObjectTypeAndRoleIdIn(
      String tid, String clientId, String objectType, List<String> roleIds
    );

    boolean existsByTidAndRoleIdAndClientIdAndClientFromAndObjectTypeAndIdentity(
            String tid, String roleId, String clientId, String clientFrom, String ojbectType, String identity
    );
    RoleResourceEntity findByTidAndClientIdAndClientFromAndRoleIdAndObjectTypeAndIdentity(
            String tid, String clientId, String clientFrom, String roleId, String ojbectType, String identity);
    List<RoleResourceEntity> findByRoleId(String roleId);
    void deleteByTidAndRoleIdAndObjectTypeAndIdentityIn(String tid, String roleId, String objectType, List<String> ids);

    // List<RoleResourceEntity> findByTidAndRoleIdAndObjectType(String tid, String roleId, String objectType);
    List<RoleResourceEntity> findByTidAndClientIdAndClientFromAndRoleIdAndObjectType(
            String tid, String clientid, String clientFrom, String roleId, String objectType);

    //
}
