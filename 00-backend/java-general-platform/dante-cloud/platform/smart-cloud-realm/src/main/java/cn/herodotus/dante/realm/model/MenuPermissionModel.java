package cn.herodotus.dante.realm.model;

import cn.herodotus.dante.core.tree.Tree;
import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/1 8:33
 */
@Data
public class MenuPermissionModel implements Serializable {
    private String parentId = "";
    private String rootId = "";
    private String id= "";
    private boolean catalogMerge = false; // 前端使用，表示合并第一列
    private Integer catalogIndex = -1; // 前端合并行
    private String catalog = "";  // 一级目录
    private String catalogId = "";
    private String menu = "";     // 菜单
    private String menuId= "";
    private Integer menuIndex= -1; // 前端何必行
    private boolean menuMerge = false; // 前端使用，表示是合并第二列

    private String pageType = "real";  //real  unreal
    private String page = "";     // 页面
    private String pageId = "";
    private String fun = "";      // 功能
    private String funId = "";

    private String digest = "";
    private boolean red = false;

    // 这三个参数用于前端
    private boolean catalogChecked = false;
    private boolean menuChecked = false;
    private boolean pageChecked = false;

    // menu行 引用catalogIndex
    private Integer pageIndexRef = -1;
    private Integer menuIndexRef = -1;
    private List<Tree> children = Lists.newArrayList();
}
