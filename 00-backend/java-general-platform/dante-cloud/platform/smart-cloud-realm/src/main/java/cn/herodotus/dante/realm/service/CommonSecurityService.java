/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.entity.CommonSecurityEntity;
import cn.herodotus.dante.realm.repository.CommonSecurityRepository;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>Description:  </p>
 *
 */
@Service
public class CommonSecurityService extends BaseService<CommonSecurityEntity, String> {

    private CommonSecurityRepository commonSecurityRepository;

    @Autowired
    public CommonSecurityService(CommonSecurityRepository commonSecurityRepository) {
        this.commonSecurityRepository = commonSecurityRepository;
    }

    @Override
    public BaseRepository<CommonSecurityEntity, String> getRepository() {
        return commonSecurityRepository;
    }

    public CommonSecurityEntity commonSecurityDetail(String tid) {
        CommonSecurityEntity entity = commonSecurityRepository.findByTid(tid);
        if (Objects.isNull(entity)) {
            entity = new CommonSecurityEntity();
            entity.setVerifyOldEmail(false);
            entity.setVerifyOldPhone(false);
            entity.setCookieTokenSecond(1209600L);
            entity.setCookieSecondUnit("second");
            entity.setAccountLock("open");
            entity.setCycleTime(300);
            entity.setCycleTimeUnit("second");
            entity.setCycleNum(5);
            entity.setQrcodeExpire(120);
            entity.setQrcodeExpireUnit("second");
            entity.setTicketExpire(300);
            entity.setTicketExpireUnit("second");
            entity.setBrowserTicket(true);
            super.insert(entity);
        }
        return entity;
    }
    public CommonSecurityEntity commonSecurityDetail() {
        String tid = CommonSecurityContextHolder.getTid();
        return commonSecurityDetail(tid);
    }
    private long getSecondUnit(Long data, String unit) {
        switch (unit) {
            case "day" -> {
                return 24*60*60*data;
            }
            case "hour" -> {
                return 60*60*data;
            }
            case "minute" -> {
                return 60*data;
            }
            default -> {
                return data;
            }
        }
    }
    // 基础安全
    public CommonSecurityEntity setBaseSecurity(CommonSecurityEntity model) {
        CommonSecurityEntity entity = super.findById(model.getId());
        long second = getSecondUnit(model.getCookieTokenSecond(), model.getCookieSecondUnit());
        if (Objects.isNull(entity)) {
            CommonSecurityEntity saveEntity = new CommonSecurityEntity();
            saveEntity.setVerifyOldEmail(model.getVerifyOldEmail());
            saveEntity.setVerifyOldPhone(model.getVerifyOldPhone());
            saveEntity.setCookieTokenSecond(model.getCookieTokenSecond());
            saveEntity.setCookieTokenSecond(second);
            super.insert(saveEntity);
            return saveEntity;
        }
        else {
            entity.setVerifyOldEmail(model.getVerifyOldEmail());
            entity.setVerifyOldPhone(model.getVerifyOldPhone());
            entity.setCookieTokenSecond(second);
            super.save(entity);
            return entity;
        }
    }

    // 登录安全
    public CommonSecurityEntity setLoginSecurity(CommonSecurityEntity model) {
        CommonSecurityEntity entity = super.findById(model.getId());
        if ("open".equals(model.getAccountLock())) {
            long second = getSecondUnit((long)model.getCycleTime(), model.getCycleTimeUnit());
            if (Objects.isNull(entity)) {
                entity = new CommonSecurityEntity();
                entity.setCycleTime((int)second);
                entity.setCycleNum(model.getCycleNum());
                super.insert(entity);
                return entity;
            }
            else {
                entity.setCycleTime((int)second);
                entity.setCycleNum(model.getCycleNum());
                super.save(entity);
                return entity;
            }
        }
        else {
            // 关闭不锁定
            if (Objects.isNull(entity)) {
                entity = new CommonSecurityEntity();
                entity.setAccountLock(model.getAccountLock());
                super.insert(entity);
                return entity;
            }
            else {
                entity.setAccountLock(model.getAccountLock());
                super.save(entity);
                return entity;
            }
        }

    }

    // app 验证安全
    public CommonSecurityEntity setAppValidSecurity(CommonSecurityEntity model) {
        CommonSecurityEntity entity = super.findById(model.getId());
        long qrcodeSecond = getSecondUnit((long)model.getQrcodeExpire(), model.getQrcodeExpireUnit());
        long ticketSecond = getSecondUnit((long)model.getTicketExpire(), model.getTicketExpireUnit());
        if (Objects.isNull(entity)) {
            CommonSecurityEntity saveEntity = new CommonSecurityEntity();
            saveEntity.setQrcodeExpire((int)qrcodeSecond);
            saveEntity.setTicketExpire((int)ticketSecond);
            saveEntity.setBrowserTicket(model.getBrowserTicket());
            super.insert(saveEntity);
            return saveEntity;
        }
        else {
            entity.setQrcodeExpire((int)qrcodeSecond);
            entity.setTicketExpire((int)ticketSecond);
            entity.setBrowserTicket(model.getBrowserTicket());
            super.save(entity);
            return entity;
        }
    }

}
