/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.entity.RoleSubjectEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.repository.RoleSubjectRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RoleSubjectService extends BaseService<RoleSubjectEntity, String> {

    private RoleSubjectRepository roleSubjectRepository;

    @Autowired
    public RoleSubjectService(RoleSubjectRepository roleSubjectRepository) {
        this.roleSubjectRepository = roleSubjectRepository;
    }

    @Override
    public BaseRepository<RoleSubjectEntity, String> getRepository() {
        return this.roleSubjectRepository;
    }

    // 查询角色主体
    public PageResponse listRoleSubject(BaseQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);
        String tid = super.getTid();

        Specification<RoleSubjectEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("subjectName"), model.getKey()));
            }
            predicates.add(cb.equal(root.get("tid"), tid));

            predicates.add(cb.equal(root.get("roleId"), model.getId()));
            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };

        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<RoleSubjectEntity> resp = new PageResponse<>();
        resp.setTotalCount((int)num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }




    // 添加角色主体
    public boolean addRoleSubject(List<RoleSubjectEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        String roleId = list.get(0).getRoleId();
        // 不重复添加
        List<RoleSubjectEntity> roleSubjects = roleSubjectRepository.findByRoleId(roleId);

        Map<String, RoleSubjectEntity> map = Maps.newHashMap();
        roleSubjects.stream().forEach(v-> {
           String key = v.getTid() + v.getSubjectType() + v.getIdentity();
           map.put(key, v);
        });

        String tid = CommonSecurityContextHolder.getTid();
        List<RoleSubjectEntity> saves = Lists.newArrayList();
        for (RoleSubjectEntity val : list) {
            val.setTid(tid);
            String key = val.getTid() + val.getSubjectType() + val.getIdentity();
            if (map.get(key) == null) {
                saves.add(val);
            }
        }
        super.insertAll(saves);
        return true;
    }

    // 删除角色主体
    public boolean deleteRoleSubject(List<RoleSubjectEntity> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return false;
        }
        roleSubjectRepository.deleteAllById(
                ids.stream().map(m->m.getId()).toList()
        );
        return true;
    }
}
