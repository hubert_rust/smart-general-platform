package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.definition.CommonTenantEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "sys_resource_menu" )
@Data
@EqualsAndHashCode(callSuper=true)
public class ResMenuEntity extends CommonTenantEntity implements Tree{
    @Column(name = "res_type", length = 16)
    private String resType;
    @Column(name = "digest", length = 16)
    private String digest;
    @Column(name = "client_id", length = 64)
    private String clientId;  //client_id;
    @Column(name = "parent_id", length = 64)
    private String parentId;
    @Column(name = "vo_order")
    private Integer voOrder;
    @Column(name = "vo_name", length = 64)
    private String voName;
    @Column(name = "vo_path")
    private String voPath;
    @Column(name = "vo_component")
    private String voComponent;
    @Column(name = "vo_title")
    private String voTitle;
    @Column(name = "vo_redirect")
    private String voRedirect;
    @Column(name = "vo_icon")
    private String voIcon;
    @Column(name = "vo_hide")
    private Integer voHide;
    @Column(name = "vo_badge")
    private String voBadge;
    @Column(name = "status")
    private String status;
    @Column(name = "remark")
    private String remark;

    @Column(name = "res_code")
    private String resCode;

    // root, master,
    // 平台管理员, 用户空间管理员
    // 默认权限
    private String permis;

    @JsonIgnore
    @Override
    @Transient
    public List<Tree> getChildrenNode() {
        return this.children;
    }

    @Override
    @Transient
    public String getNodeCode() {
        return null;
    }

    @Transient
    private List<Tree> children;

    @Override
    public Integer getOrder() {
       return voOrder;
    }

    @Transient
    @Override
    public String getNodeId() {
        return this.getId() == null ? null : (this.getId());
    }

    @Transient
    @Override
    public String getNodeParentId() {
        if (this.parentId == null) {
            return null;
        }
        else {
            return (this.parentId);
        }
    }

    @Transient
    @Override
    public void setChildrenNode(List children) {
        this.setChildren(children);
    }



    // 这两个参数用于前端列表合并行
    @Transient
    private int catalogIndex = -1;
    @Transient
    private int menuIndex = -1;
    @Transient
    private boolean red;



    public static void menuSort(List<ResMenuEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<ResMenuEntity> ret = list.stream()
                .sorted(Comparator.comparing(ResMenuEntity::getVoOrder)).collect(Collectors.toList());
        list.clear();
        list.addAll(ret);
        list.stream().forEach(v->{
            List objs = v.getChildren();
            List<ResMenuEntity> pf = objs;
            menuSort(pf);
        });

    }
}
