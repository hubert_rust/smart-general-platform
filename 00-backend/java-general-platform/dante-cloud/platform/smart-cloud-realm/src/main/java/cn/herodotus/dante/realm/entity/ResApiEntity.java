package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.definition.CommonTenantEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.dromara.hutool.crypto.digest.MD5;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 操作表
 * </p>
 *
 */
@Getter
@Setter
@ToString
@Table(name = "sys_resource_api")
@Entity
@EqualsAndHashCode(callSuper=true)
public class ResApiEntity extends CommonTenantEntity implements Serializable, Tree {

    private static final long serialVersionUID = 1L;

    // 表示来自哪个后台
    @Column(name = "app_inner_name")
    private String appInnerName;
    @Column(name = "client_id")
    private String clientId;
    @Column(name = "digest")
    private String digest;
    @Column(name = "operation_type")
    private String operationType;  //catalog: 目录，api
    //private String operationCode;
    @Column(name = "process_type")
    private String processType;  //open(匿名访问),login(登录可用),permit(权限控制)
    //private Integer openApi;  //匿名登录
    //private Integer loginApi; //登录后可用

    /**
     * 操作父ID
     */
    @Column(name = "parent_id")
    private String parentId;

    /**
     * 操作路径标识
     */
    @Column(name = "operation_path")
    private String operationPath;

    /**
     * 操作名称
     */
    @Column(name = "operation_name")
    private String operationName;

    private String deleted;

    /**
     * 请求 url
     */
    @Column(name = "request_url")
    private String requestUrl;

    /**
     * 请求方式
     */
    @Column(name = "request_mode")
    private String requestMode;

    private String remark;
    private Integer release;


    public static String getDigest(ResApiEntity entity) {
        String input =entity.getRequestMode()+ entity.getRequestUrl() ;
        return MD5.of().digestHex(input, StandardCharsets.UTF_8);
    }
    @Transient
    private boolean red = false;
    @Override
    @Transient
    public void setRed(boolean red) {
        this.red = red;
    }

    @Override
    @Transient
    public boolean getRed() {
        return this.red;
    }

    @Override
    @Transient
    public List<Tree> getChildrenNode() {
        return this.children;
    }

    // 角色展示对应api列表时候用到
    @Transient
    public boolean granted = false;
    @Transient
    public String scope = "";

    @Override
    @Transient
    public String getNodeCode() {
        return null;
    }

    @Transient
    private List<Tree> children;

    //访问的时候是否需要权限验证
    @Transient
    private boolean filterExclude;

    @Transient
    public boolean getFilterExclude() {
        return filterExclude;
    }

    @Transient
    public void setFilterExclude(boolean filterExclude) {
        this.filterExclude = filterExclude;
    }

    @Transient
    @Override
    public String getNodeId() {
        return this.getId() == null ? null : String.valueOf(this.getId());
    }

    @Transient
    @Override
    public String getNodeParentId() {
        if (this.parentId == null) {
           return null;
        }
        else {
            return String.valueOf(this.parentId);
        }
    }

    @JsonIgnore
    @Transient
    @Override
    public void setChildrenNode(List children) {
        this.setChildren(children);
    }

    // 前端使用是和菜单否绑定
    @Transient
    private boolean linked = false;

    public static void apiSort(List<ResApiEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<ResApiEntity> ret = list.stream()
                .sorted(Comparator.comparing(ResApiEntity::getOperationName)).collect(Collectors.toList());
        list.clear();
        list.addAll(ret);
        list.stream().forEach(v->{
            List objs = v.getChildren();
            List<ResApiEntity> pf = objs;
            apiSort(pf);
        });

    }
}
