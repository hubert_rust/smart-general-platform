package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author root
 * @description TODO
 * @date 2023/9/11 17:01
 */
@Entity
@Table(name = "sys_risk_list" )
@Data
@EqualsAndHashCode(callSuper=true)
public class RiskListEnity extends CommonTenantEntity {
    @Column(name = "list_class", length = 16)
    private String listClass;
    @Column(name = "subject_type", length = 16)
    private String subjectType;
    @Column(name = "subject_name", length = 32)
    private String subjectName;
    @Column(name = "subject_value", length = 64)
    private String subjectValue;
    @Column(name = "process_type", length = 16)
    private String processType;
    @Column(name = "add_type", length = 16)
    private String addType;
    private String remark;

}
