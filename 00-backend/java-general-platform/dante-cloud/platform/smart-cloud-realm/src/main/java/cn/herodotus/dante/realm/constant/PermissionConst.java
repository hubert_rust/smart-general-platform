package cn.herodotus.dante.realm.constant;

/**
 * @author root
 * @description TODO
 * @date 2023/8/30 16:41
 */
public interface PermissionConst {
    String SCOPE_ALL = "all";
    String SCOPE_OWN = "own";
    String SCOPE_ORG = "org";
    String SCOPE_ORG_BELOW = "org_below";
    String SCOPE_CUSTOM = "custom";

    String ROLE_RES_OBJECT_MENU = "menu";
    String ROLE_RES_OJBECT_API = "api";

    String SCOPE_OBJECT_USER = "user";
    String SCOPE_OBJECT_ORG = "org";
}
