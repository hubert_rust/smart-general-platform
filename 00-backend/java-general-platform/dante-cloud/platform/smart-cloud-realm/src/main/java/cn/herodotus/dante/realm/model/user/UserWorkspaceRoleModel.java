package cn.herodotus.dante.realm.model.user;

import cn.herodotus.dante.realm.entity.RoleSubjectEntity;
import cn.herodotus.dante.realm.model.application.ApplicationPermitModel;
import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/21 9:20
 */
@Data
public class UserWorkspaceRoleModel implements Serializable {

    private String roleId;
    private String roleName;
    private String tid;
    private String tenantName;
    // inner（内部） outer（外部）
    private String tidFrom;

    private List<RoleSubjectEntity> roleSubjects = Lists.newArrayList();
    private List<ApplicationPermitModel> apps = Lists.newArrayList();
}
