package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.realm.model.MenuPermissionModel;
import cn.herodotus.dante.realm.model.RolePermitModel;
import cn.herodotus.dante.realm.model.RoleResourceModel;
import cn.herodotus.dante.realm.service.RoleService;
import cn.herodotus.dante.realm.service.group.RoleManagerServiceImpl;
import cn.herodotus.dante.realm.service.group.RoleMenuResManagerServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
@Tag(name = "角色管理-授权资源管理")
public class RoleMenuController {
    @Resource
    private RoleService roleService;
    @Resource
    private RoleManagerServiceImpl roleManagerService;
    @Resource
    private RoleMenuResManagerServiceImpl roleMenuResManagerService;


    @Operation(summary = "菜单权限列表", description = "获取菜单权限列表")
    @PostMapping("/appMenus/{clientId}")
    public Result<List<Tree>> appMenus(@PathVariable String clientId) {
        List<Tree> ret = roleManagerService.appMenus(clientId);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "菜单权限列表", description = "获取菜单权限列表")
    @PostMapping("/menus")
    public Result<List<MenuPermissionModel>> appMenus(@RequestBody RolePermitModel model) {
        List<MenuPermissionModel> ret = roleMenuResManagerService.menus(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "保存资源权限", description = "保存资源权限")
    @PostMapping("/saveMenus")
    public Result<Boolean> saveMenus(@RequestBody RoleResourceModel model) {
        Boolean ret = roleMenuResManagerService.saveMenus(model);
        return Result.success(Feedback.OK, ret);
    }
/*
    @Operation(summary = "创建角色", description = "创建角色")
    @PostMapping("/insert")
    public Result<RoleEntity> insert(@Valid @RequestBody RoleEntity entity) {
        RoleEntity ret = roleService.save(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除角色", description = "删除角色")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        roleService.deleteById(id);
        return Result.success(Feedback.OK, true);
    }
    @Operation(summary = "修改角色", description = "修改角色")
    @PostMapping("/update")
    public Result<RoleEntity> update(@Valid @RequestBody RoleEntity entity) {
        RoleEntity ret = roleService.save(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "角色详情", description = "修改角色")
    @PostMapping("/detail/{id}")
    public Result<RoleEntity> detail(@PathVariable String id) {
        RoleEntity ret = roleService.findById(id);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询角色", description = "查询角色")
    @PostMapping("/list")
    public Result<PageResponse> list(@Valid @RequestBody EnvQueryModel model) {
        PageResponse ret = roleService.list(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "角色主体列表", description = "角色主体列表")
    @PostMapping("/subjectList")
    public Result<PageResponse> subjectList(@RequestBody BaseQueryModel model) {
        return Result.success(Feedback.OK, null);
    }

    @Operation(summary = "添加角色主体", description = "添加角色主体")
    @PostMapping("/addSubject")
    public Result<PageResponse> addSubject(@RequestBody List<RoleSubjectEntity> list) {

        return Result.success(Feedback.OK, null);
    }*/
}
