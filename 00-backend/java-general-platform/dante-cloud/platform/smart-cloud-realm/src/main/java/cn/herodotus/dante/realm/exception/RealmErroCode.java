package cn.herodotus.dante.realm.exception;

import cn.herodotus.dante.core.exception.ErrorCodeDefine;

public interface RealmErroCode {

    int BASE = ErrorCodeDefine.REALM_STAER;
    int REG_USER_EXIST = BASE + 1;
    int REG_USER_NOEXIST = BASE + 2;
    int TYPE_NOT_MATCH = BASE + 3;

    int PARAM_INVALID = BASE + 4;
    int PARENT_NOT_EXIST = BASE + 5;
    int API_HAS_ESIT = BASE + 6;
    int REQUEST_METHOD_NOT_UPDATE = BASE + 7;
    int DUP_FIELD = BASE + 8;

    int NOT_ADMIN = BASE + 9;

    int APP_NOT_ALLOW_TO_SSO = BASE + 10;

    int OTHER_USER_GROUP_USED = BASE + 11;
    int ROLE_HAS_REF = BASE + 12;
    int REMOVE_ROLE_SUBJECT_IS_USER = BASE + 13;
    int USER_REALM_REMOVE_BAN = BASE + 14;
    int OLD_PWD_NOT_MATCH = BASE + 15;
    int PLATFORM_APP_NOT_DELETE = BASE + 16;
    int API_NO_EXIST = BASE + 17;
    int APP_HAS_REF = BASE + 18;
    int BACKEND_HAS_REF = BASE + 19;
}
