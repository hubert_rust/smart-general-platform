package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.*;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.UserCommonModel;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.model.user.LoginUserInfoModel;
import cn.herodotus.dante.realm.model.user.UserDeptModel;
import cn.herodotus.dante.realm.model.user.UserQueryModel;
import cn.herodotus.dante.realm.repository.GroupUserRepository;
import cn.herodotus.dante.realm.repository.RoleRepository;
import cn.herodotus.dante.realm.repository.RoleSubjectRepository;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.dante.realm.service.permission.UserApiPermissionService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

//用户管理聚合类
@Slf4j
@Service
public class UserManagerServiceImpl {
    private UserService userService;
    private UserAuthService userAuthService;
    @Resource
    private DeptService deptService;
    @Resource
    private ResMenuService resMenuService;
    @Resource
    private TenantAdminService tenantAdminService;
    @Resource
    private TenantService tenantService;
    @Resource
    private GroupUserService groupUserService;
    @Resource
    private GroupService groupService;
    @Resource
    private RoleService roleService;
    @Resource
    private RoleSubjectService roleSubjectService;

    @Resource
    private SymmetricCryptoProcessor symmetricCryptoProcessor;

    @Resource
    private UserApiPermissionService userApiPermissionService;

    @Autowired
    public UserManagerServiceImpl(UserService userService, UserAuthService userAuthService) {
        this.userService = userService;
        this.userAuthService = userAuthService;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public Boolean addUserOnCurrentTenant(UserModel reg) {
        DeptEntity deptEntity = deptService.findById(reg.getMainDeptId());
        String id = IDContainer.INST.getUUID(20, false);
        String tid = userService.getTid();
        String secKey = ProtectConstant.SM4_SECRET;
        String account = symmetricCryptoProcessor.decrypt(reg.getAccount(), secKey);
        String pwd = symmetricCryptoProcessor.decrypt(reg.getPwd(), secKey);

        reg.setAccount(account);
        //加密pwd, pending
        //reg.setPwd(pwd);

        reg.setMainDeptCode(deptEntity.getInnerCode());

        UserEntity entity = userService.saveUserEntity(reg, tid, id);
        userAuthService.saveUserAuthEntity(reg, tid, id);
        return true;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public UserEntity updateUserOnCurrentTenant(UserModel model) {
        return userService.updateUserEntity(model);
    }

    //pending
    //删除当前租户的用户
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean deleteUserOnCurrentTenant(List<UserEntity> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return false;
        }
        userService.deleteAll(ids);
        //smart-auth: pending
        String tid = "0";
        userAuthService.deleteByIdsInAndTid(
                ids.stream().map(m -> m.getId()).toList(),
                tid
        );
        return true;
    }

    public UserEntity userDetail(String id) {
        UserEntity entity = userService.findById(id);
        List<UserAuthEntity> userAuth = userAuthService.findUserAuthByUid(id);
        entity.getUserAuths().addAll(userAuth);
        List<UserDeptModel> modes = userOfDept(id);
        entity.getDepts().addAll(modes);
        return entity;
    }

    //查询用户所属部门
    public List<UserDeptModel> userOfDept(String uid) {
        UserEntity find = userService.findById(uid);
        //目前支持一个部门
        if (Objects.isNull(find) || Objects.isNull(find.getMainDeptId())) {
            return Lists.newArrayList();
        }
        DeptEntity deptEntity = deptService.findById(find.getMainDeptId());
        String path = deptEntity.getPath();
        String[] dept = path.split(RealmConstant.PATH_SPLIT_SYMBOL);
        List<String> names = Arrays.stream(dept).toList();
        UserDeptModel model = new UserDeptModel();
        model.setDeptName(deptEntity.getDeptName());

        //smart-auth: pending
        String tid = RealmConstant.TID_VALUE_DEFAULT;
        String fullPath = RealmConstant.STR_EMPTY;
        for (String name : names) {
            DeptEntity query = deptService.findByInnerCodeAndTid(name, tid);
            if (Objects.isNull(query)) {
                log.error(">>> userOfDept, fullPath is null, uid: {}", uid);
                fullPath = "";
                break;
            }
            fullPath += query.getDeptName();
            fullPath += RealmConstant.STR_BLANK;
        }
        model.setDeptPath(fullPath);

        List<UserDeptModel> list = Lists.newArrayList();
        list.add(model);
        return list;
    }

    public PageResponse<UserEntity> list(UserQueryModel model) {
        //smart-auth: pending
        String tid = deptService.getTid();
        List<DeptEntity> list
                = deptService.findByTidAndPathLike(tid, model.getDeptInnerCode() + "%");
        Set<String> set = Sets.newHashSet();
        for (DeptEntity deptEntity : list) {
            if (Strings.isNotEmpty(deptEntity.getPath())) {
                String[] item = deptEntity.getPath().split("-");
                Set<String> items = Arrays.stream(item).collect(Collectors.toSet());
                set.addAll(items);
            }

        }

        return userService.list(model, set);
    }

    /*
     * @Author root
     * @Description // 登录后获取用户信息, 用户端
     * @Date 21:36 2023/8/12
     * @Param [agent: 来源, uid, client: 应用id]
     * @return cn.herodotus.dante.realm.model.user.LoginUserInfoModel
     **/
    public LoginUserInfoModel userInfoAfterLoginOfUser(String agent,
                                                       String uid,
                                                       String client) {
        //smart-auth: pending
        String tid = CommonSecurityContextHolder.getTid();

        LoginUserInfoModel model = new LoginUserInfoModel();
        UserEntity entity = userService.findById(uid);
        model.setUser(entity);

        // 微信小程序只获取用户信息
        if (RealmConstant.AGENT_WXMICRO.equals(agent)) {
            return model;
        }

        //如果用户是root， 返回所有菜单
        if (RealmConstant.ACCOUNT_ATTR_ROOT.equals(entity.getAccountAttr())) {
            List<ResMenuEntity> menus = resMenuService.findByClientId(client);
            model.setMenus(menus);
        } else {
        }


        return model;
    }

    /*
     * @Author root
     * @Description //TODO
     * @Date 21:16 2023/9/17
     * @Param
     * @return
     **/
    public List<ResMenuEntity> getUserMenuList() {
        String client = CommonSecurityContextHolder.getCid();
        String uid = CommonSecurityContextHolder.getUid();
        String tid = CommonSecurityContextHolder.getTid();
        UserEntity entity = userService.findById(uid);

        List<ResMenuEntity> menus = null;
        //如果用户是root， 返回所有菜单
        if (RealmConstant.ACCOUNT_ATTR_ROOT.equals(entity.getAccountAttr())) {
            menus = resMenuService.findByClientId(client);
        } else if (RealmConstant.ACCOUNT_ATTR_MASTER.equals(entity.getAccountAttr())) {
            menus = resMenuService.findByClientIdAndPermisLikeAndStatus(
                    client,
                    "%" + RealmConstant.ACCOUNT_ATTR_MASTER + "%",
                    RealmConstant.STATUS_ACTIVE
            );
        } else {
            //
            menus = userApiPermissionService.userMenuAccess(tid, client, uid, false);
        }
        DefaultTreeBuildFactory<ResMenuEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId(RealmConstant.PARENT_ID_DEFAULT);
        List<ResMenuEntity> ret = factory1.doTreeBuild(menus);
        ResMenuService.menuSort(ret);
        return ret;
    }

    /*
     * @Author root
     * @Description // 登录后获取用户信息, 管理端或者是SSO后获取用户信息
     * @Date 21:36 2023/8/12
     * @Param [agent: 来源, uid, client: 应用id]
     * @return cn.herodotus.dante.realm.model.user.LoginUserInfoModel
     **/
    public LoginUserInfoModel userInfoAfterLoginOfAdmin(String agent,
                                                        String uid,
                                                        String client) {
        //smart-auth: pending
        String tid = CommonSecurityContextHolder.getTid();

        // 非管理员，没有权限登录
        List<TenantAdminEntity> wkss
                = tenantAdminService.findByUidAndTidType(
                uid, RealmConstant.TID_TYPE_REALM
        );
        if (CollectionUtils.isEmpty(wkss)) {
            throw new RealmException("realmException", RealmErrorKey.NOT_ADMIN);
        }
       /* List<TenantAdminEntity> tenants = tenantAdminService.findByTidAndUidAndTidType(
                tid, uid, RealmConstant.TID_TYPE_TENANT
        );
        if (CollectionUtils.isEmpty(tenants)) {
            throw new RealmException("realmException", RealmErrorKey.NOT_ADMIN);
        }*/

        LoginUserInfoModel model = new LoginUserInfoModel();
        UserEntity entity = userService.findById(uid);
        model.setUser(entity);

        // 微信小程序只获取用户信息
        if (RealmConstant.AGENT_WXMICRO.equals(agent)) {
            return model;
        }

        // 用户空间或者租户列表, smart-auth: pending
        List<TenantEntity> tids
                = tenantService.findByIds(
                wkss.stream().map(m -> m.getTid()).toList()
        );

        for (TenantEntity tenantEntity : tids) {
            if (tenantEntity.getCreateBy().equals(uid)) {
                model.getSelfs().add(tenantEntity);
            } else {
                model.getAgents().add(tenantEntity);
            }
            if (tenantEntity.getId().equals(entity.getHitTid())) {
                model.setCurrentWks(tenantEntity);
            }
        }

        // 获取菜单
        model.setMenus(getUserMenuList());

        return model;
    }

    /***********用户所属*************/
    // 用户所属角色

    // 用户所属岗位

    // 用户所属组
    public List<GroupEntity> userOfGroup(String uid) {
        String tid = groupUserService.getTid();
        List<GroupUserEntity> list = groupUserService.findByTidAndUid(tid, uid);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return groupService.listByIds(list.stream().map(m -> m.getGroupId()).toList());
    }

    /*
     * @Author root
     * @Description 所有角色列表
     * @Date 8:05 2023/9/6
     * @Param
     * @return
     **/
    public List<RoleEntity> roles(String uid) {
        String tid = roleService.getTid();
        List<RoleEntity> list
                = ((RoleRepository) roleService.getRepository()).findByTid(tid);

        // 查询角色主体是user的
        List<RoleSubjectEntity> roleSubjects = ((RoleSubjectRepository) roleSubjectService.getRepository())
                .findByTidAndSubjectTypeAndIdentity(tid, RealmConstant.SUBJECT_USER, uid);
        for (RoleEntity roleEntity : list) {
            for (RoleSubjectEntity roleSubject : roleSubjects) {
                if (roleEntity.getId().equals(roleSubject.getRoleId())) {
                    roleEntity.setRed(true);
                    break;
                }
            }
        }
        return list;


    }

    /*
     * @Author root
     * @Description //当前用户看到的用户组列表
     * @Date 15:02 2023/9/3
     * @Param
     * @return
     **/
    public List<GroupEntity> groups(String uid) {
        String tid = groupService.getTid();
        List<GroupEntity> list = groupService.findByTid(tid);
        List<GroupEntity> selects = userOfGroup(uid);

        // 如果用户没有所属组，直接返回即可
        if (CollectionUtils.isEmpty(selects)) {
            return list;
        }

        for (GroupEntity groupEntity : list) {

            for (GroupEntity select : selects) {
                if (groupEntity.getId().equals(select.getId())) {
                    groupEntity.setRed(true);
                    break;
                }
            }
        }
        return list;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean updateUserBelongRole(UserCommonModel model) {
        String tid = groupUserService.getTid();
        // 先删除
        ((RoleSubjectRepository) roleSubjectService.getRepository())
                .deleteByTidAndIdentityAndAndSubjectType(
                        tid, model.getUid(), RealmConstant.SUBJECT_USER
                );
        if (CollectionUtils.isEmpty(model.getList())) {
            return true;
        }

        List<RoleSubjectEntity> saves = Lists.newArrayList();
        for (String val : model.getList()) {
            RoleSubjectEntity entity = new RoleSubjectEntity();
            entity.setSubjectType(RealmConstant.SUBJECT_USER);
            entity.setTid(tid);
            // roleId保存在mode中的list中
            entity.setRoleId(val);
            entity.setIdentity(model.getUid());
            saves.add(entity);
        }
        roleSubjectService.insertAll(saves);
        return true;
    }

    /*
     * @Author root
     * @Description 更新用户所属组
     * @Date 16:20 2023/9/3
     * @Param ids: 用户组
     * @return
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean updateUserOfStaticGroup(UserCommonModel model) {
        String tid = groupUserService.getTid();
        // 先删除
        ((GroupUserRepository) groupUserService.getRepository())
                .deleteByTidAndUidAndGroupType(tid, model.getUid(), RealmConstant.GROUP_TYPE_STATIC);
        if (CollectionUtils.isEmpty(model.getList())) {
            return true;
        }
        List<GroupEntity> finds = groupService.listByIds(model.getList());

        List<GroupUserEntity> saves = Lists.newArrayList();
        for (GroupEntity val : finds) {
            GroupUserEntity entity = new GroupUserEntity();
            entity.setTid(tid);
            entity.setUid(model.getUid());
            entity.setGroupType(RealmConstant.GROUP_TYPE_STATIC);
            entity.setGroupTag(val.getGroupTag());
            entity.setGroupId(val.getId());
            saves.add(entity);
        }
        groupUserService.insertAll(saves);
        return true;
    }

    /*
     * @Author root
     * @Description 移除用户所对应的组
     * @Date 17:23 2023/9/3
     * @Param
     * @return
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean removeBelongGroup(UserCommonModel model) {
        String tid = groupUserService.getTid();
        for (String val : model.getList()) {
            ((GroupUserRepository) groupUserService.getRepository()).deleteByTidAndUidAndGroupIdAndGroupType(
                    tid, model.getUid(), val, RealmConstant.GROUP_TYPE_STATIC
            );
        }

        return true;
    }


}
