package cn.herodotus.dante.realm.constant;

public interface AccountConstant {
    //系统初始默认一条数据
    String ACCOUNT_ATTR_ROOT = "root";

    //root分配或者注册的账号
    String ACCOUNT_ATTR_MASTER = "master";

    //
    String ACCOUNT_ATTR_NORMAL = "normal";

    String APP_ADMIN = "admin";


    String REG_TYPE_MOBILE = "mobile";
    String REG_TYPE_EMAIL = "email";
    String REG_TYPE_ACCOUNT = "account";

    String REG_TYPE_WEIXIN = "weixin";

    //account_auth表
    String ACCOUNT_TYPE_MOBILE = "手机";
    String ACCOUNT_TYPE_EMAIL = "邮箱";
    String ACCOUNT_TYPE_WEIXIN = "微信";
    String ACCOUNT_TYPE_ACCOUNT = "系统账号";

}
