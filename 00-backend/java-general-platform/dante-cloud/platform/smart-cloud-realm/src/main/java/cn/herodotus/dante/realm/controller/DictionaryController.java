/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.DictionaryEntity;
import cn.herodotus.dante.realm.model.dict.DictQueryModel;
import cn.herodotus.dante.realm.service.DictionaryService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dict")
@Tag(name = "字典管理")
@Validated
public class DictionaryController {

    @Resource
    private DictionaryService dictionaryService;

    @Operation(summary = "添加字典", description = "添加字典")
    @PostMapping("/insert")
    public Result<DictionaryEntity> insert(@Valid  @RequestBody DictionaryEntity entity) {
        DictionaryEntity ret = dictionaryService.insert(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除字典", description = "删除字典")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        dictionaryService.delete(id);
        return Result.success(Feedback.OK, true);
    }
    @Operation(summary = "修改字典", description = "修改字典")
    @PostMapping("/update")
    public Result<DictionaryEntity> update(@Valid @RequestBody DictionaryEntity entity) {
        DictionaryEntity ret = dictionaryService.update(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "字典列表", description = "字典列表")
    @PostMapping("/list")
    public Result list(@Valid @RequestBody DictQueryModel model, HttpServletResponse resp) {
        // smart-auth
        /*try {
            ShouldLoginRspModel shouldLoginRspModel = new ShouldLoginRspModel();
            shouldLoginRspModel.setRedirect("http://localhost:3003/dashboard/base");
            return Result.failure("", 302, 302, shouldLoginRspModel);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        PageResponse ret = dictionaryService.list(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "通过数据类型查询", description = "通过数据类型查询")
    @PostMapping("/listByDictType")
    public Result<List<DictionaryEntity>> listByDictType(@Valid @RequestBody DictionaryEntity model) {
        List<DictionaryEntity> ret = dictionaryService.listByDictType(model);
        return Result.success(Feedback.OK, ret);
    }

}
