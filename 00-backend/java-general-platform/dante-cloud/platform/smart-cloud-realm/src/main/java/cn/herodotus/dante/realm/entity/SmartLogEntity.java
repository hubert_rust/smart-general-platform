package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author root
 * @description TODO
 * @date 2023/8/13 17:09
 */
@Entity
@Table(name = "sys_log" )
@Data
@EqualsAndHashCode(callSuper=true)
public class SmartLogEntity extends CommonTenantEntity {
    @Column(name = "log_type")
    private String logType;
    private String apt;
    @Column(name = "real_name")
    private String realName;
    private String account;
    private String uid;
    private String tid;
    @Column(name = "device_type")
    private String deviceType;
    @Column(name = "operation_event")
    private String operationEvent;
    @Column(name = "client_ip")
    private String clientIp;
    @Column(name = "device_system")
    private String deviceSystem;
    private String browse;
    private String addr;
    @Column(name = "request_url")
    private String requestUrl;
    @Column(name = "request_method")
    private String requestMethod;
    @Column(name = "request_param")
    private String requestParam;
    @Column(name = "request_result")
    private String requestResult;
    @Column(name = "request_return")
    private String requestReturn;
    @Column(name = "client_id")
    private String clientId;

    @Column(name = "app_tag")
    private String appTag;
    // 操作结束时间
    @Column(name = "finish_time")
    private Date finishTime;

    private Integer span;


}
