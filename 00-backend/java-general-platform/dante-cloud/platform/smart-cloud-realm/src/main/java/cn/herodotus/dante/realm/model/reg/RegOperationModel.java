package cn.herodotus.dante.realm.model.reg;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RegOperationModel implements Serializable {
    private List<String> ids;
    private String tid;
    private String uid;
    private String opType;    //active, deactive, recycle
    private String opObject;  //user, tenant


}
