package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.ResApiEntity;
import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.dante.realm.model.BaseModel;
import cn.herodotus.dante.realm.service.ResMenuService;
import cn.herodotus.dante.realm.service.group.ApplicationSsoServiceGroupImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/appsso")
@Tag(name = "SSO应用管理-菜单资源")
@Validated
public class ApplicationSsoMenuController {

    @Resource
    private ResMenuService resMenuService;
    @Resource
    private ApplicationSsoServiceGroupImpl applicationSsoServiceGroup;


    @Operation(summary = "SSO应用菜单列表", description = "返回SSO应用菜单")
    @PostMapping("/menuListOfapp/{client}")
    public Result<List<ResMenuEntity>> operationOfApp(@PathVariable String client) {
        List<ResMenuEntity> ret = resMenuService.listMenuByClient(client,  true);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "SSO应用菜单列表", description = "返回SSO应用菜单")
    @PostMapping("/menuApi")
    public Result<ResApiEntity> operationOfApp(@RequestBody BaseModel baseModel) {
        ResApiEntity ret = applicationSsoServiceGroup.menuApiDetail(
                baseModel.getKey(), baseModel.getValue());
        return Result.success(Feedback.OK, ret);
    }

}
