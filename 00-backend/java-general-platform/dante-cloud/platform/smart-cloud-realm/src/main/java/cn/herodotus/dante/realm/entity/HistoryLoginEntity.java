/*
 * Copyright [2020] [MaxKey of copyright http://www.maxkey.top]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Entity
@Table(name = "sys_history_login" )
@Data
@EqualsAndHashCode(callSuper=true)
public class HistoryLoginEntity extends CommonTenantEntity {
	private static final long serialVersionUID = -1321470643357719383L;
	@Column(name = "session_id")
	String sessionId;
	//是否是管理员
	String admin;
	@Column(name = "uid")
	String uid;
	@Column(name = "username")
	String username;
	@Column(name = "display_name")
	String displayName;
	@Column(name = "login_type")
	String loginType;
	@Column
	String message;
	@Column
	String code;
	@Column
	String provider;
	@Column(name = "source_ip")
	String sourceIp;
	@Column(name = "ip_region")
	String ipRegion;
	@Column(name = "ip_location")
	String ipLocation;
	@Column
	String browser;
	@Column
	String platform;
	@Column
	String application;
	@Column(name = "login_url")
	String loginUrl;
	@Column(name = "login_time")
	LocalDateTime loginTime;
	@Column(name = "logout_time")
	LocalDateTime logoutTime;
	@Column(name = "session_status")
	String sessionStatus;
}
