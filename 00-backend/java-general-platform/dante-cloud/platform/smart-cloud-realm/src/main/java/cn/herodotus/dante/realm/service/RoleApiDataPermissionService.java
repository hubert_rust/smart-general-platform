/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.constant.PermissionConst;
import cn.herodotus.dante.realm.entity.RoleApiDataPermissionEntity;
import cn.herodotus.dante.realm.repository.RoleApiDataPermissionRepository;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>Description:  </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class RoleApiDataPermissionService extends BaseService<RoleApiDataPermissionEntity, String> {

    private RoleApiDataPermissionRepository roleApiDataPermissionRepository;

    @Autowired
    public RoleApiDataPermissionService(RoleApiDataPermissionRepository roleApiDataPermissionRepository) {
        this.roleApiDataPermissionRepository = roleApiDataPermissionRepository;
    }

    @Override
    public BaseRepository<RoleApiDataPermissionEntity, String> getRepository() {
        return roleApiDataPermissionRepository;
    }

    // 角色对应的api添加数据权限

    public boolean addApiDataPermission(List<RoleApiDataPermissionEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        String tid = CommonSecurityContextHolder.getTid();
        List<RoleApiDataPermissionEntity> customs = Lists.newArrayList();
        for (RoleApiDataPermissionEntity entity : list) {
            entity.setTid(tid);
            if (PermissionConst.SCOPE_CUSTOM.equals(entity.getScope())) {
                // 在前端按list传递
            }
        }
        String roleId = list.get(0).getRoleId();
        // 先删除之前的数据
        roleApiDataPermissionRepository.deleteByTidAndRoleIdAndDigestIn(
                tid, roleId, list.stream().map(m->m.getDigest()).toList()
                );
        super.insertAll(list);
        return true;
    }

    public List<RoleApiDataPermissionEntity> findByTidAndRoleId(
            String tid, String roleId
    ) {
        return roleApiDataPermissionRepository.findByTidAndRoleId( tid, roleId );
    }

    public boolean deleteByTidAndRoleIdAndDigestIn(String tid, String roleId, List<String> digests) {
        roleApiDataPermissionRepository.deleteByTidAndRoleIdAndDigestIn(
                tid, roleId, digests
        );
        return true;
    }
    // 特别是第一个api的数据权限添加组织的时候有用
    public boolean deleteByTidAndRoleIdAndIdentityIn(String tid, String roleId, List<String> identitys) {
        roleApiDataPermissionRepository.deleteByTidAndRoleIdAndIdentityIn(
                tid, roleId, identitys
        );
        return true;
    }
    public boolean existsByTidAndRoleIdAndIdentity(String tid, String roleId, String identity) {
        return roleApiDataPermissionRepository.existsByTidAndRoleIdAndIdentity(tid, roleId, identity);
    }
    public List<RoleApiDataPermissionEntity> findByTidAndRoleIdAndCustomSubject(String tid,
                                                                                String roleId,
                                                                                String digest) {
        return roleApiDataPermissionRepository.findByTidAndRoleIdAndDigest(
                tid, roleId, digest
        );
    }
}
