package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.ResApiEntity;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.ResApiService;
import cn.herodotus.dante.realm.service.group.ApplicationServiceGroupImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/appsso")
@Tag(name = "SSO应用管理-接口资源")
@Validated
public class ApplicationSsoApiController {

    @Resource
    private ApplicationService applicationService;
    @Resource
    private ApplicationServiceGroupImpl applicationServiceGroup;
    @Resource
    private ResApiService resApiService;




    @Operation(summary = "应用接口列表", description = "应用接口列表")
    @PostMapping("/apiListOfApp/{client}")
    public Result<List<ResApiEntity>> apiListOfApp(@PathVariable String client) {
        List<ResApiEntity> ret = applicationServiceGroup.listAppApiByClient(client);
        return Result.success(Feedback.OK, ret);
    }

}
