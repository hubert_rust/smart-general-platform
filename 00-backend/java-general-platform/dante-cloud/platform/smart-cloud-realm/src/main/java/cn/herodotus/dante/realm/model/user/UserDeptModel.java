package cn.herodotus.dante.realm.model.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDeptModel implements Serializable {
    private String deptName;
    private String parentDeptName;
    private String deptPath;
}
