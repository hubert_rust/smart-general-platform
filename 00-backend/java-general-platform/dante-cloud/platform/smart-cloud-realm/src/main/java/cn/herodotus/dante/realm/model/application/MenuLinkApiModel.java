package cn.herodotus.dante.realm.model.application;

import lombok.Data;

import java.io.Serializable;

@Data
public class MenuLinkApiModel implements Serializable {
    private String clientId;
    private String menuId;
    private String resApiId;
    private String digest;
}
