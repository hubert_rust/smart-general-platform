package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.model.MasterRealmModel;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//用户空间
@Slf4j
@Service
public class RealmServiceImpl {

    private TenantService tenantService;
    private TenantAdminService tenantAdminService;
    private UserService userService;

    @Autowired
    public RealmServiceImpl(TenantService tenantService,
                            TenantAdminService tenantAdminService,
                            UserService userService) {
        this.tenantService = tenantService;
        this.tenantAdminService = tenantAdminService;
        this.userService = userService;
    }

    //切换用户空间,targetId切换到目标用户空间
    //pending
    /*public TenantEntity switchRealm(String fromId, String targetTid) {
        RequestBaseUser baseUser = UserContextHolder.getBaseUser();
        //需要判断是否是管理员
        List<TenantAdminEntity> list = tenantAdminService.findByUid(baseUser.getUid());
        Optional<TenantAdminEntity> find = list.stream()
                .filter(p -> p.getTid().equals(targetTid))
                .findFirst();
        if (!find.isPresent() || find.get() == null) {
            throw new RuntimeException("没有权限切换");
        }

        return tenantService.findByTid(targetTid);
    }*/



    //用户注册，或者root创建的租户(同时创建用户账号)
    //pending
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public TenantEntity createMasterTenant(MasterRealmModel model) {
        TenantEntity entity = new TenantEntity();
        String tid = IDContainer.INST.getUUID(16, false);
        entity.setTid(tid);
        entity.setParentId(RealmConstant.PARENT_ID_DEFAULT);
        entity.setTidType(RealmConstant.TID_TYPE_REALM);
        entity.setTenantName(model.getName());
        entity.setRemark(model.getRemark());
        entity.setStatus(model.getStatus());
        //添加用户

        //角色
        //return createTenant(entity);
        return null;
    }
}
