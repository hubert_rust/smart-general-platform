package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.UserAuthEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.event.TestEvent;
import cn.herodotus.dante.realm.model.reg.RegOperationModel;
import cn.herodotus.dante.realm.model.reg.RegQueryModel;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.service.group.RegServicerImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.rest.core.annotation.Crypto;
import cn.herodotus.engine.rest.core.context.ServiceContext;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reg")
@Tag( name = "注册系统账号")
public class RegController {

    @Resource
    private RegServicerImpl regServicer;

    @Operation(summary = "注册账号", description = "后台管理端手动开户")
    @PostMapping("/create")
    public Result<UserAuthEntity> createReg(@RequestBody UserModel model) {
        UserAuthEntity entity = regServicer.createRegUser(model);
        return Result.success(Feedback.OK, entity);
    }
    @Operation(summary = "注册账号重置密码", description = "后台管理端手动重置密码")
    @Crypto(responseEncrypt = false)
    @PostMapping("/resetPwd")
    public Result<Boolean> changePassword(@RequestParam(name = "userId") String userId,
                                          @RequestParam(name = "password") String password) {
    //public Result<Boolean> resetPwd(@RequestBody RegModel model) {
        UserModel model = new UserModel();
        model.setId(userId);
        model.setPwd(password);
        Boolean ret = regServicer.resetRegUserPassword(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "修改基本信息", description = "更新注册账户基本信息")
    @PostMapping("/updateBaseInfo")
    public Result<UserEntity> updateBaseInfo(@RequestBody UserEntity model) {
        UserEntity ret = regServicer.updateBaseInfo(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "激活账号", description = "后台管理端手动激活账号")
    @PostMapping("/active")
    public Result<Boolean> active(@RequestBody RegOperationModel model) {
        Boolean ret = regServicer.regOperation(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "禁用账号", description = "后台管理端手动禁用账号")
    @PostMapping("/deactive")
    public Result<Boolean> deactive(@RequestBody RegOperationModel model) {
        Boolean ret = regServicer.regOperation(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "禁用用户空间", description = "后台管理端手动禁用子空间")
    @PostMapping("/deactiveTenant")
    public Result<Boolean> deactiveTenant(@RequestBody RegOperationModel model) {
        Boolean ret = regServicer.regOperation(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "激活用户空间", description = "后台管理端手动激活子空间")
    @PostMapping("/activeTenant")
    public Result<Boolean> activeTenant(@RequestBody RegOperationModel model) {
        Boolean ret = regServicer.regOperation(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "回收子系统", description = "后台管理端手动回收子系统")
    @PostMapping("/recycle")
    public Result<Boolean> recycle(@RequestBody RegOperationModel model) {
        Boolean ret = regServicer.recycle(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询账号", description = "查询账号")
    @PostMapping("/list")
    public Result<PageResponse> list(@RequestBody RegQueryModel model) {
        ServiceContext.getInstance().publishEvent(new TestEvent("test Event"));
        PageResponse ret = regServicer.list(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "注册账号详情", description = "注册账号详情")
    @PostMapping("/detail/{uid}")
    public Result<UserModel> detail(@PathVariable String uid) {
        UserModel ret = regServicer.regDetail(uid);
        return Result.success(Feedback.OK, ret);
    }
}
