/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.constant.PermissionConst;
import cn.herodotus.dante.realm.entity.RoleResourceEntity;
import cn.herodotus.dante.realm.repository.RoleResourceRepository;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>Description:  </p>
 *
 */
@Service
public class RoleResourceService extends BaseService<RoleResourceEntity, String> {

    private RoleResourceRepository roleResourceRepository;

    @Autowired
    public RoleResourceService(RoleResourceRepository roleResourceRepository) {
        this.roleResourceRepository = roleResourceRepository;
    }

    @Override
    public BaseRepository<RoleResourceEntity, String> getRepository() {
        return roleResourceRepository;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public Boolean  roleAddResource(List<RoleResourceEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        // 如果存在不添加, 配置是按照应用配置
        List<RoleResourceEntity> saves = Lists.newArrayList();
        String tid = getTid();
        String roleId = list.get(0).getRoleId();
        String objectType = list.get(0).getObjectType();
        String clientId = list.get(0).getClientId();
        String clientFrom = list.get(0).getClientFrom();
        for (RoleResourceEntity entity : list) {
            entity.setTid(tid);
            boolean ret = roleResourceRepository.existsByTidAndRoleIdAndClientIdAndClientFromAndObjectTypeAndIdentity(
              tid, roleId, clientId, clientFrom, objectType, entity.getIdentity()
            );
            if (!ret) {
                saves.add(entity);
            }
        }

        if (CollectionUtils.isEmpty(saves)) {
            return true;
        }
        super.insertAll(saves);
        return true;
    }
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean roleDeleteApiResource(List<RoleResourceEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        String tid = getTid();
        String roleId = list.get(0).getRoleId();
        roleResourceRepository.deleteByTidAndRoleIdAndObjectTypeAndIdentityIn(
                tid, roleId, PermissionConst.ROLE_RES_OJBECT_API,
                list.stream().map(m->m.getIdentity()).toList()
        );
        return true;
    }
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean roleDeleteMenuResource(List<RoleResourceEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }

        // 获取menu对应的api
        List<String> digests = list.stream().map(m->m.getDigest()).toList();
        String tid = getTid();
        String roleId = list.get(0).getRoleId();
        // 先删除菜单
        roleResourceRepository.deleteByTidAndRoleIdAndObjectTypeAndIdentityIn(
                tid, roleId, PermissionConst.ROLE_RES_OBJECT_MENU,
                list.stream().map(m->m.getIdentity()).toList()
        );
        roleResourceRepository.deleteByTidAndRoleIdAndObjectTypeAndIdentityIn(
                tid, roleId, PermissionConst.ROLE_RES_OJBECT_API,
                digests
        );
        return true;
    }

    /*
     * @Author root
     * @Description //TODO
     * @Date 8:23 2023/8/31
     * @Param objectType: menu  api
     * @return
     **/
    public List<RoleResourceEntity> listResources(String roleId,
                                                  String clientId,
                                                  String clientFrom,
                                                  String objectType) {
        return roleResourceRepository.findByTidAndClientIdAndClientFromAndRoleIdAndObjectType(
                getTid(), clientId, clientFrom, roleId, objectType
        );
    }

    public RoleResourceEntity findByTidAndClientIdAndClientFromAndRoleIdAndObjectTypeAndIdentity(String tid,
                                                                         String clientId,
                                                                         String clientFrom,
                                                                         String roleId,
                                                                         String objectType,
                                                                         String identity) {
        return roleResourceRepository.findByTidAndClientIdAndClientFromAndRoleIdAndObjectTypeAndIdentity(
                tid, clientId, clientFrom, roleId, objectType, identity
        );
    }
}
