package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.model.OperationModel;
import cn.herodotus.dante.model.RemoteBaseModel;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.*;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.application.MenuLinkApiModel;
import cn.herodotus.dante.realm.model.application.OperationProcessType;
import cn.herodotus.dante.realm.remote.RemoteBackendService;
import cn.herodotus.dante.realm.repository.BackendRepository;
import cn.herodotus.dante.realm.repository.ResMenuRepository;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.dante.scan.entity.OperationEntity;
import cn.herodotus.dante.scan.repository.OperationRepository;
import cn.herodotus.dante.scan.service.OperationService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import com.google.common.collect.*;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ApplicationServiceGroupImpl {

    @Resource
    private BackendService backendService;
    @Resource
    private OperationService operationService;
    @Resource
    private ApplicationService applicationService;
    @Resource
    private UserService userService;
    @Resource
    private ResApiService resApiService;

    @Resource
    private ResMenuService resMenuService;
    @Resource
    private ApplicationSubjectService applicationSubjectService;
    @Resource
    private GroupUserService groupUserService;

    @Resource
    private ApplicationSsoService applicationSsoService;
    @Resource
    private RemoteBackendService remoteBackendService;


    //发布接口，取消发布
    public boolean releaseProcess(String id, int release) {
        //发布接口就是从operation表中发布到resource_api表中
        ResApiEntity entity = resApiService.findById(id);
        entity.setRelease(release);
        resApiService.update(entity);
        return true;
    }

    public boolean operationProcessType(OperationProcessType model) {
        ResApiEntity entity = resApiService.findById(model.getId());
        entity.setProcessType(model.getProcessType());
        resApiService.update(entity);
        return true;
    }

    public void treeSort(List<OperationEntity> list) {
        if (list == null || list.size() <= 0) {
            return;
        }

        List<OperationEntity> ret = list.stream()
                .sorted(Comparator.comparing(OperationEntity::getOperationName)).collect(Collectors.toList());
        list.clear();
        list.addAll(ret);
        list.stream().forEach(v -> {
            List objs = v.getChildrenNode();
            List<OperationEntity> pf = objs;
            treeSort(pf);
        });
    }

    //应用后台接口列表
    public List<OperationEntity> listBackendApi(String client) {
        ApplicationEntity find = applicationService.findById(client);
        List<OperationEntity> list
                = operationService.findByAppInnerName(find.getAppInnerName());

        Map<String, String> map = Maps.newHashMap();
        List<ResApiEntity> ress = resApiService.findByClientId(client, false);
        ress.stream().forEach(v -> map.put(v.getDigest(), v.getDigest()));
        for (OperationEntity entity : list) {
            entity.setRelease(map.get(entity.getDigest()) == null ? 0 : 1);
        }
        //List<Tree> ret = DefaultTreeBuildFactory.treeBuild(new ArrayList<Tree>(list), "-1", true);
        DefaultTreeBuildFactory<OperationEntity> factory = new DefaultTreeBuildFactory<>();
        factory.setRootParentId("-1");
        List<OperationEntity> tree = factory.doTreeBuild(list);
        treeSort(tree);

        return tree;
    }

    //应用接口列表, pending
    public List<ResApiEntity> listAppApiByClient(String client) {
        List<ResApiEntity> list = resApiService.findByClientId(client, false);
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        List<String> digests = list.stream().map(m -> m.getDigest()).toList();
        if (CollectionUtils.isEmpty(digests)) {
            return Lists.newArrayList();
        }

        Map<String, ResMenuEntity> map = Maps.newHashMap();
        List<ResMenuEntity> ress = ((ResMenuRepository) resMenuService.getRepository())
                .findByClientIdAndDigestIn(client, digests);
        ress.stream().forEach(v -> map.put(v.getDigest(), v));
        list.stream().forEach(v -> {
            if (Objects.nonNull(map.get(v.getDigest()))) {
                v.setLinked(true);
            }
        });

        DefaultTreeBuildFactory<ResApiEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<ResApiEntity> ret = factory1.doTreeBuild(list);
        return ret;
    }

    //后台接口列表
/*    public List<OperationEntity> apiListOfBackendByByClient(String client) {
        ApplicationEntity entity = applicationService.findById(client);
        List<OperationEntity> list = operationService.findByTag(entity.getAppTag(), true);
        return list;
    }*/

    public List<ApplicationEntity> applicationList() {
        List<ApplicationEntity> list = applicationService.list();
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        List<String> userIds = list.stream().map(m -> m.getId()).toList();
        List<UserEntity> users = userService.listByIds(userIds);
        for (ApplicationEntity entity : list) {
            for (UserEntity user : users) {
                if (entity.getCreateBy().equals(user.getId())) {
                    entity.setRealName(user.getRealName());
                    break;
                }
            }
        }
        return list;
    }

    //校验接口是否有多个菜单绑定同一个接口
    public List<String> checkApiRes(String client) {
        List<String> ret = Lists.newArrayList();
        //ResApiEntity entity = applicationService.findById(client);
        List<ResMenuEntity> list = resMenuService.findByClientId(client);
        Map<String, List<ResMenuEntity>> groups = list.stream()
                .filter(p -> Strings.isNotEmpty(p.getDigest()))
                .collect(Collectors.groupingBy(ResMenuEntity::getDigest));
        List<String> apis = Lists.newArrayList();

        for (String key : groups.keySet()) {
            if (groups.get(key).size() > 1) {
                apis.add(key);
                List<ResMenuEntity> ress = groups.get(key);
                String err = "";
                for (ResMenuEntity entity : ress) {
                    String title = entity.getVoTitle();
                    err += ("[" + title + "] ");
                }
                ret.add(err + "配置了相同的API资源, 请检查!");
            }
        }

        return ret;
    }

    // 远程加载应用接口到operation表
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public List<OperationEntity> remoteLoad(String client) {
        ApplicationEntity find = applicationService.findById(client);
        RemoteBaseModel model = new RemoteBaseModel();
        BackendEntity backend = ((BackendRepository)backendService.getRepository())
                .findByTidAndAppInnerNameAndStatus(backendService.getTid(), find.getAppInnerName(), "1");
        model.setKey(find.getAppInnerName());
        try {
            Result<List<OperationModel>> resp
                    = remoteBackendService.getBackendOperations(new URI(backend.getBackendAddr()), model);
            if (resp.getCode() == 20000) {
                List<OperationEntity> saves = Lists.newArrayList();
                for (OperationModel datum : resp.getData()) {
                    OperationEntity entity = new OperationEntity();
                    BeanUtils.copyProperties(datum, entity);
                    saves.add(entity);
                }
                ((OperationRepository) operationService.getRepository())
                        .deleteOperation(find.getAppInnerName());
                operationService.insertAll(saves);
                return saves;
            }
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RealmException("realmException", e.getMessage());
        }
    }
    //同步到apiRes, 同步目录或者api

    /*
     * @Author root
     * @Description 同步operation表中的api到应用的sys_resource_api中
     * @Date 8:38 2023/9/14
     * @Param force = true 表示强制同步, 强制同步需要更新sys_resource_api已经有的，但不改变processType
     * @return
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public int sync2apiResOfApp(List<OperationEntity> selects, String clientId, Boolean force) {
        if (CollectionUtils.isEmpty(selects)) {
            return 0;
        }
        ApplicationEntity find = applicationService.findById(clientId);
        // 父节点也需要同步
        Set<String> sets = Sets.newHashSet();
        for (OperationEntity entity : selects) {
            String path = entity.getOperationPath();
            if (Strings.isEmpty(path)) {
                log.error(">>> sync2ApiResOfApp, path is null, operationName: {}",
                        entity.getOperationName());
                continue;
            }
            String[] ids = path.split("-");
            for (String id : ids) {
                sets.add(id);
            }
        }
        // 1、先查找在operation中找到父节点，
        List<OperationEntity> list = ((OperationRepository) operationService.getRepository())
                .findAllById(sets.stream().toList());

        // 如果同步过的就不需要同步了，通过digest比较
        Map<String, ResApiEntity> map = Maps.newHashMap();
        Table<String, String, ResApiEntity> table = HashBasedTable.create();
        List<ResApiEntity> ress
                = resApiService.findByClientIdAndDigestInAndAppInnerName(
                clientId,
                list.stream().map(m -> m.getDigest()).toList(),
                find.getAppInnerName());
        // 如果强制同步，删除ResApi中

        ress.stream().forEach(v -> map.put(v.getDigest(), v));

        List<ResApiEntity> saves = Lists.newArrayList();
        // 需要更新的
        List<ResApiEntity> updates = Lists.newArrayList();

        // 2、这里是使用list而不是selects
        for (OperationEntity entity : list) {
            if (map.get(entity.getDigest()) == null) {
                ResApiEntity resApiEntity = new ResApiEntity();
                BeanUtils.copyProperties(entity, resApiEntity);
                resApiEntity.setId(entity.getId());
                resApiEntity.setClientId(clientId);
                resApiEntity.setParentId(entity.getParentId());
                resApiEntity.setAppInnerName(find.getAppInnerName());
                resApiEntity.setClientId(clientId);
                saves.add(resApiEntity);
            } else {
                ResApiEntity apiEntity = map.get(entity.getDigest());
                apiEntity.setRemark(entity.getRemark());
                updates.add(apiEntity);
            }
        }
        if (CollectionUtils.isNotEmpty(saves)) {
            resApiService.insertAll(saves);
        }
        if (CollectionUtils.isNotEmpty(updates)) {
            resApiService.saveAll(updates);
        }


        return saves.size();
    }


    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean menuUnlinkApi(List<MenuLinkApiModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        List<ResMenuEntity> updates = ((ResMenuRepository) resMenuService.getRepository())
                .findAllById(list.stream().map(m -> m.getMenuId()).toList());

        updates.stream().forEach(v -> v.setDigest(RealmConstant.STR_EMPTY));
        resMenuService.saveAll(updates);
        return true;
    }


    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean menuLinkApi(MenuLinkApiModel model) {
        ResMenuEntity menuEntity = resMenuService.findById(model.getMenuId());
        if (Objects.isNull(menuEntity)) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "菜单不存在");
        }
        ResApiEntity apiEntity = resApiService.findById(model.getResApiId());
        if (Objects.isNull(apiEntity)) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "接口资源不存在");
        }
        menuEntity.setDigest(apiEntity.getDigest());
        resMenuService.save(menuEntity);
        return true;
    }

    // 登录用户获取应用列表，用户访问接口时，只校验tid状态，应用状态
    public List<ApplicationEntity> loginUserGetApplictions() {
        String tid = CommonSecurityContextHolder.getTid();
        String uid = CommonSecurityContextHolder.getUid();

        Map<String, ApplicationEntity> maps = Maps.newHashMap();
        // 用户主体
        List<ApplicationSubjectEntity> users
                = applicationSubjectService
                .findByTidAndSubjectTypeAndIdentity(tid, RealmConstant.SUBJECT_USER, uid);


        // 用户组主体
        List<GroupUserEntity> guers = groupUserService.findByTidAndUid(tid, uid);
        if (CollectionUtils.isNotEmpty(guers)) {
            List<ApplicationSubjectEntity> groups = applicationSubjectService.findByTidAndIdentityIn(
                    tid, guers.stream().map(m -> m.getId()).toList()
            );
            if (CollectionUtils.isNotEmpty(guers)) {
                users.addAll(groups);
            }
        }

        // 组织主体
        String deptId = CommonSecurityContextHolder.getUser().getMainDeptId();
        List<ApplicationSubjectEntity> orgs = applicationSubjectService.findByTidAndIdentityIn(
                tid,
                new ArrayList<String>() {{
                    add(deptId);
                }}
        );
        if (CollectionUtils.isNotEmpty(orgs)) {
            users.addAll(orgs);
        }

        if (CollectionUtils.isEmpty(users)) {
            applicationService.findByClientIdIn(
                    users.stream().map(m -> m.getClientId()).toList()
            ).stream().forEach(v -> maps.put(v.getId(), v));
        }

        return maps.values().stream().toList();
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean applicationAddtoSso(String clientId) {
        // 平台应用不能加入到sso, smart-auth: pending
        if ("67601992f3574c75809a".equals(clientId)) {
            throw new RealmException("realmException", RealmErrorKey.APP_NOT_ALLOW_TO_SSO);
        }
        String tid = applicationService.getTid();
        ApplicationEntity application = applicationService.findById(clientId);
        application.setShare(true);
        applicationService.save(application);
        ApplicationSsoEntity applicationSsoEntity = new ApplicationSsoEntity();
        applicationSsoEntity.setClientId(clientId);
        applicationSsoEntity.setTid(tid);
        applicationSsoService.insert(applicationSsoEntity);
        return true;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean applicationDeleteFromSso(String clientId) {
        ApplicationEntity application = applicationService.findById(clientId);
        application.setShare(false);
        applicationService.save(application);
        return applicationSsoService.deleteByTidAndAndClientId(CommonSecurityContextHolder.getTid(), clientId);
    }

}
