package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.constant.RiskListConstant;
import cn.herodotus.dante.realm.entity.RiskListEnity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.service.RiskListService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/risk")
@Tag(name = "风险名单-IP 黑名单")
public class RiskListIPBlackController {

    @Resource
    private RiskListService riskListService;

    @Operation(summary = "添加 IP 黑名单", description = "添加 IP 黑名单")
    @PostMapping("/insertIpBlack")
    public Result<RiskListEnity> insertIpBlack(@Valid @RequestBody RiskListEnity entity) {
        RiskListEnity ret = riskListService.addIpBlack(entity);
        return Result.success(Feedback.OK, ret);
    }


    @Operation(summary = "删除 IP 黑名单", description = "删除 IP 黑名单")
    @PostMapping("/deleteIpBlack/{id}")
    public Result<Boolean> deleteIpBlack(@PathVariable String id) {
        Boolean ret = riskListService.delete(id,
                RiskListConstant.LIST_CLASS_BLACK,
                RiskListConstant.SUBJECT_TYPE_IP);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "IP 黑名单列表", description = "IP 黑名单列表")
    @PostMapping("/ipBlackList")
    public Result<PageResponse> ipBlackList(@RequestBody BaseQueryModel model) {
        PageResponse response = riskListService.ipBlackList(model);
        return Result.success(Feedback.OK, response);
    }

}
