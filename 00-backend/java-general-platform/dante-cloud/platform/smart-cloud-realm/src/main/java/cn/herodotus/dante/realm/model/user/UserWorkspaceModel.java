package cn.herodotus.dante.realm.model.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/8/12 18:15
 */
@Data
public class UserWorkspaceModel implements Serializable {
    private String id;
    private String name;

    // 当前用户空间
    private String currentId;
    private String domain;
    private String status;
    private String logo;
    private String remark;

}
