package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.realm.constant.AccountConstant;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.TenantAdminEntity;
import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.entity.UserAuthEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.reg.RegOperationModel;
import cn.herodotus.dante.realm.model.reg.RegQueryModel;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.service.TenantAdminService;
import cn.herodotus.dante.realm.service.TenantService;
import cn.herodotus.dante.realm.service.UserAuthService;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.dante.realm.service.recycle.TenantServiceRecycle;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.dto.PageResponse;
import com.google.common.collect.Lists;
import jakarta.annotation.Resource;
import jakarta.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.dromara.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class RegServicerImpl {

    private TenantService tenantService;
    private UserService userService;
    private UserAuthService userAuthService;
    private TenantAdminService tenantAdminService;

    @Resource
    private TenantServiceRecycle tenantServiceRecycle;

    @Autowired
    public RegServicerImpl(TenantService tenantService,
                           UserService userService,
                           TenantAdminService tenantAdminService,
                           UserAuthService userAuthService) {
        this.tenantService = tenantService;
        this.userService = userService;
        this.userAuthService = userAuthService;
        this.tenantAdminService = tenantAdminService;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean recycle(RegOperationModel model) {
        if (CollectionUtils.isEmpty(model.getIds())) {
            return true;
        }

        List<TenantEntity> list = Lists.newArrayList();
        for (String id : model.getIds()) {
            UserEntity userEntity = userService.findById(id);
            if (Objects.isNull(userEntity)) {
                continue;
            }
            // system不能删除
            if (userEntity.getTid().equals(RealmConstant.DELETED_VALUE_DEFAULT)) {
                continue;
            }

            // 删除userAuth
            userAuthService.deleteByUidAndTid(userEntity.getId(), userEntity.getTid());
            List<TenantEntity> tenants = tenantService.findSubTenentById(userEntity.getTid());
            list.addAll(tenants);
            TenantEntity entity = new TenantEntity();
            entity.setId(userEntity.getId());
            entity.setTid(userEntity.getTid());
            list.add(entity);
        }
        for (TenantEntity entity : list) {
            tenantServiceRecycle.removeServiceData(entity.getTid());
            // 删除tid
            tenantService.deleteByTid(entity.getTid());
        }
        return true;
    }

    //更新注册用户基本信息
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public UserEntity updateBaseInfo(UserEntity entity) {
        UserEntity userEntity = userService.findById(entity.getId());
        userEntity.setPhone(entity.getPhone());
        userEntity.setEmail(entity.getEmail());
        userEntity.setRealName(entity.getRealName());
        userEntity.setGender(entity.getGender());
        userEntity.setRemark(entity.getRemark());
        return userService.save(userEntity);
    }

    //1:激活租户 2:激活账号: 账号使用UserAuthEntity
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean regOperation(RegOperationModel model) {
        if (RealmConstant.REG_OPOBJECT_USER.equals(model.getOpObject())) {
            //账号
            List<UserAuthEntity> list = userAuthService.findByIds(model.getIds());
            list.stream().forEach(v->v.setStatus(model.getOpType()));
            userAuthService.saveAll(list);
            return true;
        } else if (RealmConstant.REG_OPOBJECT_TENANT.equals(model.getOpObject())) {
            //租户
            List<TenantEntity> list = tenantService.findByIds(model.getIds());
            list.stream().forEach(v->v.setStatus(model.getOpType()));
            tenantService.saveAll(list);
            return true;
        }
        throw new RealmException("realmException", RealmErrorKey.TYPE_NOT_MATCH);
    }

    //禁用子系统(账号)


    //重置密码, user_auth表所有密码都更新
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean resetRegUserPassword(UserModel model) {
        UserAuthEntity entity = userAuthService.findById(model.getId());
        if (Objects.isNull(entity)) {
            throw new RealmException("realmException", RealmErrorKey.REG_USER_NOEXIST);
        }
        List<UserAuthEntity> list = userAuthService.findUserAuthByUid(entity.getUid());
        list.stream().forEach(v -> v.setCredential(model.getPwd()));
        userAuthService.saveAll(list);
        return true;
    }

    /*
     * @Author root
     * @Description 人工注册租户
     * @Date 9:56 2023/9/11
     * @Param 
     * @return 
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public UserAuthEntity createRegUser(UserModel model) {


        String tid = IDContainer.INST.getTid();
        String id = IDContainer.INST.getUUID(0, true);
        // UserEntity userEntity = userService.saveUserEntity(model, tid, id);

        UserAuthEntity userAuthEntity
                = userAuthService.saveUserAuthEntity(model, tid, id);

        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        userEntity.setTid(tid);
        if (AccountConstant.REG_TYPE_EMAIL.equals(model.getCreateType())) {
            userEntity.setEmail(model.getAccount());
        } else if (AccountConstant.REG_TYPE_MOBILE.equals(model.getCreateType())) {
            userEntity.setPhone(model.getAccount());
        } else if (AccountConstant.REG_TYPE_ACCOUNT.equals(model.getCreateType())) {
        }
        userEntity.setCreateType(model.getCreateType());
        userEntity.setAccount(model.getAccount());
        userEntity.setMainDeptId(model.getMainDeptId());
        userEntity.setMainDeptCode(model.getMainDeptCode());

        // 注册后用户属性为: master
        userEntity.setHitTid(tid);
        userEntity.setAccountAttr(RealmConstant.ACCOUNT_ATTR_MASTER);
        userEntity.setStatus(RealmConstant.REG_OPTYPE_ACTIVE);
        userService.insert(userEntity);

        //用户默认权限: 菜单和接口等权限从原始表中获取，分配管理员的时候选择菜单等权限
        //用户空间创建:
        TenantEntity entity = new TenantEntity();
        entity.setId(tid);
        entity.setTid(tid);
        // master parentId = -1, 后续创建的用户空间parentId是该id(通过用户uid可以获取到)
        entity.setParentId(RealmConstant.PARENT_ID_DEFAULT);
        entity.setTidType(RealmConstant.TID_TYPE_REALM);
        entity.setTenantName(model.getAccount() + "-默认用户空间");
        entity.setRemark("注册用户默认创建用户空间");
        entity.setStatus(RealmConstant.USER_STATUS_ACTIVE);
        tenantService.insert(entity);

        TenantAdminEntity adminEntity = new TenantAdminEntity();
        adminEntity.setUid(id);
        adminEntity.setTidType(RealmConstant.TID_TYPE_REALM);
        adminEntity.setTid(tid);
        tenantAdminService.insert(adminEntity);

        return userAuthEntity;
    }
    /*public PageResponse<UserAuthEntity> list(RegQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);

        Specification<UserAuthEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getIdentify())) {
                predicates.add(cb.equal(root.get("identify"), model.getIdentify()));
            }
            predicates.add(cb.equal(root.get("createBy"), RealmConstant.UID_VALUE_DEFAULT));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
            return query.getRestriction();
        };
        long num = userAuthService.count(spec);

        Page page = userAuthService.findByPage(spec, pageable);
        PageResponse<UserAuthEntity> resp = new PageResponse<>();
        resp.setTotalCount((int)num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }*/

    /*
     * @Author root
     * @Description 查询的是注册的用户（用户attr是master
     * @Date 8:47 2023/9/15
     * @Param 
     * @return 
     **/
    public PageResponse<UserEntity> list(RegQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);

        Specification<UserEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getIdentify())) {
                predicates.add(cb.equal(root.get("account"), model.getIdentify()));
            }
            predicates.add(cb.equal(root.get("accountAttr"), RealmConstant.ACCOUNT_ATTR_MASTER));
            predicates.add(cb.equal(root.get("createBy"), RealmConstant.UID_VALUE_DEFAULT));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
            return query.getRestriction();
        };
        long num = userService.count(spec);

        Page page = userService.findByPage(spec, pageable);
        PageResponse<UserEntity> resp = new PageResponse<>();
        resp.setTotalCount((int)num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }

    public UserModel regDetail(String uid) {
        UserModel userModel = new UserModel();
        //查询子账户
        List<UserAuthEntity> userAuth = userAuthService.findUserAuthByUid(uid);
        userModel.getList().addAll(userAuth);

        UserEntity userEntity = userService.findById(uid);
        BeanUtil.copyProperties(userEntity, userModel);
        userModel.setUid(uid);
        userModel.setId(uid);


        TenantEntity tenantEntity
                = tenantService.findByTid(userEntity.getTid());
        List<TenantEntity> tenants
                = tenantService.findSubTenentById(tenantEntity.getId());
        userModel.getTenants().add(tenantEntity);
        userModel.getTenants().addAll(tenants);
        DefaultTreeBuildFactory<TenantEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<TenantEntity> tree = factory1.doTreeBuild(userModel.getTenants());
        userModel.setTenants(tree);

        return userModel;
    }
}
