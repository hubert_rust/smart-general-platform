package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.GroupEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.config.EnvQueryModel;
import cn.herodotus.dante.realm.model.user.GroupUserModel;
import cn.herodotus.dante.realm.service.GroupService;
import cn.herodotus.dante.realm.service.GroupUserService;
import cn.herodotus.dante.realm.service.group.GroupManagerServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/group")
@Tag(name = "用户组管理")
public class GroupController {

    @Resource
    private GroupService groupService;
    @Resource
    private GroupUserService groupUserService;
    @Resource
    private GroupManagerServiceImpl groupManagerService;

    @Operation(summary = "创建静态用户组", description = "创建静态用户组")
    @PostMapping("/insert")
    public Result<GroupEntity> insert(@Valid @RequestBody GroupEntity entity) {
        GroupEntity ret = groupService.insert(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除静态用户组", description = "删除静态用户组")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        groupManagerService.deleteGroup(id);
        return Result.success(Feedback.OK, true);
    }
    @Operation(summary = "修改静态用户组", description = "修改静态用户组")
    @PostMapping("/update")
    public Result<GroupEntity> update(@Valid @RequestBody GroupEntity entity) {
        GroupEntity ret = groupService.save(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "用户组详情", description = "用户组角色")
    @PostMapping("/detail/{id}")
    public Result<GroupEntity> detail(@PathVariable String id) {
        GroupEntity ret = groupService.findById(id);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询用户组", description = "查询用户组")
    @PostMapping("/list")
    public Result<PageResponse> list(@Valid @RequestBody EnvQueryModel model) {
        PageResponse ret = groupService.list(model);
        return Result.success(Feedback.OK, ret);
    }


    @Operation(summary = "用户组添加用户", description = "用户组添加用户")
    @PostMapping("/insertUser")
    public Result<Boolean> insertUser(@Valid @RequestBody GroupUserModel model) {
        Boolean ret = groupManagerService.groupInsertUser(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除用户组用户", description = "删除用户组用户")
    @PostMapping("/deleteUser")
    public Result<Boolean> deleteUser(@RequestBody GroupUserModel model) {
        groupManagerService.groupDeleteUser(model);
        return Result.success(Feedback.OK, true);
    }

    @Operation(summary = "查询用户组成员", description = "查询用户组中用户")
    @PostMapping("/listUser")
    public Result<PageResponse> listUser(@Valid @RequestBody BaseQueryModel model) {
        PageResponse ret = groupManagerService.listUser(model);
        return Result.success(Feedback.OK, ret);
    }
}
