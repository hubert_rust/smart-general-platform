package cn.herodotus.dante.realm.model;

import cn.herodotus.dante.realm.constant.RealmConstant;
import lombok.Data;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

@Data
public  class BaseQueryModel implements Serializable {
    private String dir = RealmConstant.DIR_ASC;
    private String key; //搜索关键字
    private String id;

    private String[] sortItem;

    //从零开始
    private Integer current;
    private Integer pageSize;

    private String startTime;
    private String endTime;

    public Sort.Direction getDirection() {
        if (RealmConstant.DIR_ASC.equals(getDir())) {
            return Sort.Direction.ASC;
        }
        else if (RealmConstant.DIR_DESC.equals(getDir())) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }
}
