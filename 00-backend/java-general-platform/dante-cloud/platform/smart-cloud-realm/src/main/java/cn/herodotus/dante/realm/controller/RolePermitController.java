package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.ResApiEntity;
import cn.herodotus.dante.realm.entity.RoleApiDataPermissionEntity;
import cn.herodotus.dante.realm.entity.RoleResourceEntity;
import cn.herodotus.dante.realm.model.RolePermitModel;
import cn.herodotus.dante.realm.model.user.UserQueryModel;
import cn.herodotus.dante.realm.service.RoleApiDataPermissionService;
import cn.herodotus.dante.realm.service.RoleResourceService;
import cn.herodotus.dante.realm.service.RoleService;
import cn.herodotus.dante.realm.service.group.ApplicationServiceGroupImpl;
import cn.herodotus.dante.realm.service.group.RoleManagerServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
@Tag(name = "角色管理-接口权限管理")
public class RolePermitController {
    @Resource
    private RoleService roleService;
    @Resource
    private RoleManagerServiceImpl roleManagerService;
    @Resource
    private ApplicationServiceGroupImpl applicationServiceGroup;
    @Resource
    private RoleApiDataPermissionService roleApiDataPermissionService;
    @Resource
    private RoleResourceService roleResourceService;


    @Operation(summary = "接口权限列表", description = "接口权限列表")
    @PostMapping("/appApis")
    public Result<List<ResApiEntity>> appApis(@RequestBody RolePermitModel model) {
        List<ResApiEntity> ret = roleManagerService.appApis(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "数据权限配置", description = "数据权限配置")
    @PostMapping("/dataScope")
    public Result<Boolean> dataScope(@RequestBody List<RoleApiDataPermissionEntity> list) {
        Boolean ret = roleManagerService.addApiDataPermission(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "关闭数据权限", description = "关闭数据权限")
    @PostMapping("/closeDataScope")
    public Result<Boolean> closeApiDataPermisson(@RequestBody List<RoleResourceEntity> list) {
        Boolean ret = roleManagerService.closeApiDataPermission(list);
        return Result.success(Feedback.OK, ret);
    }

    /*@Operation(summary = "添加菜单资源", description = "角色添加菜单资源")
    @PostMapping("/addMenus")
    public Result<Boolean> addMenus(@RequestBody List<RoleResourceEntity> list) {
        Boolean ret = roleResourceService.roleAddResource(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除菜单资源", description = "角色删除菜单资源")
    @PostMapping("/deleteMenus")
    public Result<Boolean> deleteMenus(@RequestBody List<RoleResourceEntity> list) {
        Boolean ret = roleResourceService.roleDeleteApiResource(list);
        return Result.success(Feedback.OK, ret);
    }*/
    @Operation(summary = "添加接口资源", description = "角色添加接口资源")
    @PostMapping("/addApis")
    public Result<Boolean> addApis(@RequestBody List<RoleResourceEntity> list) {
        Boolean ret = roleResourceService.roleAddResource(list);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除接口资源", description = "角色删除接口资源")
    @PostMapping("/deleteApis")
    public Result<Boolean> deleteApis(@RequestBody List<RoleResourceEntity> list) {
        Boolean ret = roleResourceService.roleDeleteApiResource(list);
        return Result.success(Feedback.OK, ret);
    }

  /*  @Operation(summary = "已授权菜单资源", description = "已授权菜单资源")
    @PostMapping("/grantApis/{roleId}")
    public Result<List<RoleResourceEntity>> grantApis(@PathVariable String roleId) {
        List<RoleResourceEntity> ret = roleResourceService.listResources(roleId, PermissionConst.ROLE_RES_OJBECT_API);
        return Result.success(Feedback.OK, ret);
    }*/
    /*@Operation(summary = "已授权接口资源", description = "已授权接口资源")
    @PostMapping("/grantMenus/{roleId}")
    public Result<List<RoleResourceEntity>> grantMenus(@PathVariable String roleId) {
        List<RoleResourceEntity> ret = roleResourceService.listResources(roleId, PermissionConst.ROLE_RES_OBJECT_MENU);
        return Result.success(Feedback.OK, ret);
    }*/

    @Operation(summary = "自定义数据权限更新加组织", description = "自定义数据权限更新加组织")
    @PostMapping("/customUpdateOrg")
    public Result<Boolean> customUpdateOrg(@RequestBody RolePermitModel model) {
        Boolean ret = roleManagerService.roleUpdateCustomOrg(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "自定义数据权限添加用户", description = "自定义数据权限添加用户")
    @PostMapping("/customAddUser")
    public Result<Boolean> customAddUser(@RequestBody List<RoleApiDataPermissionEntity> list) {
        Boolean ret = roleManagerService.roleAddCustomUser(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "自定义数据权限删除用户", description = "自定义数据权限删除用户")
    @PostMapping("/customDeleteUser")
    public Result<Boolean> customDeleteUser(@RequestBody List<RoleApiDataPermissionEntity> list) {
        Boolean ret = roleManagerService.roleDeleteCustomUser(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "自定义数据权限组织列表", description = "自定义数据权限组织列表")
    @PostMapping("/customOrgList")
    public Result<Map<String, Object>> customOrgList(@RequestBody RolePermitModel model) {
        Map<String, Object> ret = roleManagerService.roleCustomOrgList(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "自定义数据权限用户列表", description = "自定义数据权限用户列表")
    @PostMapping("/customUserList/{roleId}")
    public Result<Map<String, Object>> customUserList(@RequestBody UserQueryModel model,
                                                      @PathVariable String roleId) {
        Map<String, Object> ret = roleManagerService.roleCustomUserList(model, roleId);
        return Result.success(Feedback.OK, ret);
    }
}
