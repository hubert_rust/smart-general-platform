/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.SmartLogEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.repository.SmartLogRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SmartLogService extends BaseService<SmartLogEntity, String> {

    private SmartLogRepository smartLogRepository;

    @Autowired
    public SmartLogService(SmartLogRepository smartLogRepository) {
        this.smartLogRepository = smartLogRepository;
    }

    @Override
    public BaseRepository<SmartLogEntity, String> getRepository() {
        return this.smartLogRepository;
    }

    public PageResponse<SmartLogEntity> userList(BaseQueryModel model) {
        return list(model, RealmConstant.APP_TYPE_USER);
    }
    public PageResponse<SmartLogEntity> adminList(BaseQueryModel model) {
        return list(model, RealmConstant.APP_TYPE_MGT);
    }

    public static void main(String[] args) throws Exception {


        String format = "yyyy-MM-dd HH:mm:ss";
        Date str = DateUtils.parseDate("2023-08-29 00:00:01",format );
        System.out.println(str);
    }

    /*
     * @Author root
     * @Description 用户访问日志
     * @Date 8:57 2023/9/8
     * @Param 
     * @return 
     **/
    public PageResponse<SmartLogEntity> accessList(BaseQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);
        Specification<SmartLogEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            List<Predicate> ors = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                ors.add(cb.equal(root.get("logType"), RealmConstant.LOG_TYPE_LOGIN));
                ors.add(cb.equal(root.get("logType"), RealmConstant.LOG_TYPE_LOGOUT));
                Predicate[] orArr = new Predicate[ors.size()];
                predicates.add(cb.or(ors.toArray(orArr)));
            }

            List<Predicate> apts = new ArrayList<>();
            apts.add(cb.equal(root.get("account"), model.getKey()));


            Predicate[]  aptArr = new Predicate[apts.size()];
            predicates.add(cb.and(apts.toArray(aptArr)));
            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<SmartLogEntity> resp = new PageResponse<>();
        resp.setTotalCount((int) num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }


    // 管理员日志查询
    // select * from sys_log where (key = realName  or key = account) and apt =
    public PageResponse<SmartLogEntity> list(BaseQueryModel model, String apt) {
        Pageable pageable = QueryUtil.pageable(model);
        String format = "yyyy-MM-dd HH:mm:ss";
        Specification<SmartLogEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            List<Predicate> ors = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                ors.add(cb.like(root.get("realName"), "%" +model.getKey() + "%"));
                ors.add(cb.like(root.get("account"), "%" + model.getKey() + "%"));
                Predicate[] orArr = new Predicate[ors.size()];
                predicates.add(cb.or(ors.toArray(orArr)));
            }

            List<Predicate> apts = new ArrayList<>();
            apts.add(cb.equal(root.get("apt"), apt));
            apts.add(cb.equal(root.get("tid"), super.getTid()));

            if (Strings.isNotEmpty(model.getStartTime())) {
                try {
                    Date str = DateUtils.parseDate(model.getStartTime() + " 00:00:00", format);
                    apts.add(cb.greaterThanOrEqualTo(root.get("createTime"), str));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (Strings.isNotEmpty(model.getEndTime())) {
                try {
                    Date str = DateUtils.parseDate(model.getEndTime() + " 23:59:59", format);
                    apts.add(cb.lessThanOrEqualTo(root.get("createTime"), str));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Predicate[]  aptArr = new Predicate[apts.size()];
            predicates.add(cb.and(apts.toArray(aptArr)));
            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<SmartLogEntity> resp = new PageResponse<>();
        resp.setTotalCount((int) num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }
}
