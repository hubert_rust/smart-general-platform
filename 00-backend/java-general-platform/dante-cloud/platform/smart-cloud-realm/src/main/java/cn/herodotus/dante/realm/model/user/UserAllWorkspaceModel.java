package cn.herodotus.dante.realm.model.user;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/8/17 12:04
 */
@Data
public class UserAllWorkspaceModel {
    // 当前打开的用户空间
    private UserWorkspaceModel model;

    // 自建创建的用户空间
    private List<UserWorkspaceModel> workspaces = Lists.newArrayList();

    // 代理的用户空间
    private List<UserWorkspaceModel> agents = Lists.newArrayList();
}
