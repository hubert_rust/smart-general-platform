package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import cn.herodotus.dante.realm.constant.RealmConstant;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "sys_backend")
@Data
@EqualsAndHashCode(callSuper=true)
public class BackendEntity extends CommonTenantEntity {
    @Column(name = "backend_name", length = 64)
    @NotBlank(message = "名称不能为空")
    @Length(message = "名称长度不能超过32", max = 32)
    private String backendName;
    @Column(name = "app_inner_name", length = 64)
    @Length(message = "内部名称长度不能超过64" , max = 64)
    private String appInnerName;
    @Column(name = "backend_addr", length = 64)
    @Length(message = "后台地址长度不能超过64", max = 64)
    private String backendAddr;
    @Column(name = "context_path", length = 64)
    @Length(max = 64)
    private String contextPath;
    @Column(name = "remark", length = 64)
    @Length(message = "备注长度不能超过64", max = 64)
    private String remark;

    private String status = RealmConstant.STATUS_ACTIVE;
}
