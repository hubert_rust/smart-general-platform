package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.ConfigEntity;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.group.ApplicationServiceGroupImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/application")
@Tag(name = "自建应用管理-基础功能")
@Validated
public class ApplicationController {

    @Resource
    private ApplicationService applicationService;
    @Resource
    private ApplicationServiceGroupImpl applicationServiceGroup;

    @Operation(summary = "应用详情", description = "应用详情")
    @PostMapping("/detail/{id}")
    public Result<ApplicationEntity> detail(@PathVariable String id) {
        ApplicationEntity ret = applicationService.findById(id);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "添加应用", description = "添加应用")
    @PostMapping("/insert")
    public Result<ApplicationEntity> save(@Valid @RequestBody ApplicationEntity entity) {
        ApplicationEntity ret = applicationService.insert(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "修改应用基础信息", description = "修改应用基础信息")
    @PostMapping("/update")
    public Result<ApplicationEntity> update(@Valid @RequestBody ApplicationEntity entity) {
        ApplicationEntity ret = applicationService.updateBase(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "修改应用认证信息", description = "修改应用认证信息")
    @PostMapping("/updateAuth")
    public Result<ApplicationEntity> updateAuth(@Valid @RequestBody ApplicationEntity entity) {
        ApplicationEntity ret = applicationService.updateAuth(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除应用", description = "删除应用")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@Valid @PathVariable String id) {
        boolean ret = applicationService.delete(id);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "应用列表", description = "应用列表")
    @PostMapping("/list")
    public Result<List<ApplicationEntity>> list(@Valid @RequestBody ConfigEntity entity) {
        List<ApplicationEntity> ret = applicationServiceGroup.applicationList();
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "激活应用", description = "激活应用")
    @PostMapping("/activeApp/{id}")
    public Result<Boolean> activeApp(@PathVariable String id) {
        boolean ret = applicationService.activeAndDeactiveApp(id, RealmConstant.REG_OPTYPE_ACTIVE);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "禁用应用", description = "应用下线")
    @PostMapping("/deactiveApp/{id}")
    public Result<Boolean> deactiveApp(@PathVariable String id) {
        boolean ret = applicationService.activeAndDeactiveApp(id, RealmConstant.REG_OPTYPE_DEACTIVE);
        return Result.success(Feedback.OK, ret);
    }


}
