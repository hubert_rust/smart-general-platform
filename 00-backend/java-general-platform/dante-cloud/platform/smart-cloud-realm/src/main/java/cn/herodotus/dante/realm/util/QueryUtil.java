package cn.herodotus.dante.realm.util;

import cn.herodotus.dante.realm.model.BaseQueryModel;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class QueryUtil {
    public static Pageable pageable(BaseQueryModel pageRequest) {
        int pageSize = pageRequest.getPageSize() < 0 ? 10 : pageRequest.getPageSize();
        int current = pageRequest.getCurrent()-1 < 0 ? 0 : (pageRequest.getCurrent()-1);
        Pageable page = PageRequest
                .of(current, pageSize)
                .withSort(pageRequest.getDirection(), pageRequest.getSortItem());
        return page;
    }


}
