package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ResApiEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.repository.ResApiRepository;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class ResApiService extends BaseService<ResApiEntity, String> {
    @Resource
    private ResApiRepository resApiRepository;

    @Autowired
    public ResApiService(ResApiRepository resApiRepository) {
        this.resApiRepository = resApiRepository;
    }

    @Override
    public BaseRepository<ResApiEntity, String> getRepository() {
        return this.resApiRepository;
    }

    public ResApiEntity getOne(String id) {
        return super.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String id) {
        ResApiEntity find = super.findById(id);
        find.setDeleted(IDContainer.INST.getUUID(20, false));
        super.save(find);
        return true;
    }
    @Transactional(rollbackFor = Exception.class)
    public ResApiEntity update(ResApiEntity entity) {
        ResApiEntity find = super.findById(entity.getId());
        if (!find.getOperationType().equals(entity.getOperationType())) {
            throw new RealmException("realmException",  RealmErrorKey.REQUEST_METHOD_NOT_UPDATE);
        }
        //更新了接口类型（processType)

        return super.save(entity);
    }
    @Transactional(rollbackFor = Exception.class)
    public ResApiEntity save(ResApiEntity entity) {

        if (!entity.getParentId().equals(RealmConstant.PARENT_ID_DEFAULT)) {
            ResApiEntity find = super.findById(entity.getParentId());
            if (Objects.isNull(find)) {
                throw new RealmException("realmException", RealmErrorKey.PARENT_NOT_EXIST);
            }
        }

        //如果digest一直，则重复
        if (RealmConstant.OPERATION_TYPE_API.equals(entity.getOperationType())) {
            String digest = ResApiEntity.getDigest(entity);
            List<ResApiEntity> list = resApiRepository.findByAppInnerNameAndDigest(entity.getAppInnerName(), digest);
            if (CollectionUtils.isNotEmpty(list)) {
                throw new RealmException("realmException", RealmErrorKey.API_HAS_EXIST);
            }
            entity.setDigest(digest);
        }
        return super.save(entity);
    }
    public List<ResApiEntity> findByTag(String tag, boolean tree) {
        List<ResApiEntity> list
                = resApiRepository.findByAppInnerNameAndDeleted(tag, RealmConstant.DELETED_VALUE_DEFAULT);
        if (!tree) {
            return list;
        }
        DefaultTreeBuildFactory<ResApiEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<ResApiEntity> ret = factory1.doTreeBuild(list);
        return ret;
    }

    //增加或者修改接口的时候用到
    public List<ResApiEntity> findCatalog(String tag) {
        List<ResApiEntity> list
                = resApiRepository.findByAppInnerNameAndDeletedAndOperationType(tag, RealmConstant.DELETED_VALUE_DEFAULT, RealmConstant.OPERATION_TYPE_CATALOG);
        DefaultTreeBuildFactory<ResApiEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<ResApiEntity> ret = factory1.doTreeBuild(list);
        return ret;
    }


    public List<ResApiEntity> getListInDigest(String contextPath,
                                                 Collection<String> collection) {
        if (collection.isEmpty()) {
            return Lists.newArrayList();
        }
        Specification<ResApiEntity> spec = new Specification<ResApiEntity>() {
            @Override
            public Predicate toPredicate(Root<ResApiEntity> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(cb.in(root.get("digest").in(collection)));
                predicates.add(cb.equal(root.get("context_path"), contextPath));

                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
        };
        return super.findAll(spec);
    }

    // tree 默认未true
    public List<ResApiEntity> findByClientId(String clientId, Boolean tree) {
        List<ResApiEntity> list =  resApiRepository.findByClientId(clientId);
        if (!tree) {
            return list;
        }
        DefaultTreeBuildFactory<ResApiEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<ResApiEntity> ret = factory1.doTreeBuild(list);
        return  ret;
    }
    public List<ResApiEntity> findByClientId(String clientId, boolean tree) {
        List<ResApiEntity> list =  resApiRepository.findByClientId(clientId);
        if (tree) {
            DefaultTreeBuildFactory<ResApiEntity> factory1 = new DefaultTreeBuildFactory<>();
            factory1.setRootParentId("-1");
            List<ResApiEntity> ret = factory1.doTreeBuild(list);
            return ret;
        }
        return list;
    }


    public List<ResApiEntity> findByClientIdAndDigestInAndAppInnerName(
            String clientId, List<String> digests, String appInnerName) {
        return resApiRepository.findByClientIdAndDigestInAndAppInnerName(
                clientId, digests, appInnerName);
    }

    public ResApiEntity findByDigestInAndClientId(List<String> digests, String clientId) {
        return resApiRepository.findByDigestInAndClientId(digests, clientId);
    }
}
