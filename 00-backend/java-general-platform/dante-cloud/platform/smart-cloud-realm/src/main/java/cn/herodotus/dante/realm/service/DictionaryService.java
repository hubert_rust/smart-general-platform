/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.DictionaryEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.dict.DictQueryModel;
import cn.herodotus.dante.realm.repository.DictionaryRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Description:  </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Slf4j
@Service
public class DictionaryService extends BaseService<DictionaryEntity, String> {

    private DictionaryRepository dictionaryRepository;

    @Autowired
    public DictionaryService(DictionaryRepository dictionaryRepository) {
        this.dictionaryRepository = dictionaryRepository;
    }

    @Override
    public BaseRepository<DictionaryEntity, String> getRepository() {
        return this.dictionaryRepository;
    }

    //pending
    public boolean delete(String id)  {
        DictionaryEntity entity = super.findById(id);
        if (Strings.isNullOrEmpty(entity.getParentId())) {
            Specification<DictionaryEntity> spec = (root, query, cb) -> {
                return cb.equal(root.get("parentId"), entity.getId());
            };
            List<DictionaryEntity> list = super.findAll(spec);
            super.deleteAll(list);
        } else {
            super.deleteById(id);
        }

        return true;
    }

    private boolean insertValdator(String parentId, String fdName, String fdValue) {
        Specification<DictionaryEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("parentId"), parentId));
            predicates.add(cb.equal(root.get(fdName), fdValue));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
            return query.getRestriction();
        };
        return CollectionUtils.isEmpty(super.findAll(spec)) ? true : false;
    }

    public DictionaryEntity update(DictionaryEntity entity) {
        //parentId不能修改
        DictionaryEntity find = findById(entity.getId());
        if (!entity.getParentId().equals(find.getParentId())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "[parentId]不能修改");
        }

        //pengding, 校验
        return super.save(entity);
    }

    public DictionaryEntity insert(DictionaryEntity entity) {
        //同一个类型下，dataName和dataKey都不能重复
        if (!entity.getParentId().equals(RealmConstant.PARENT_ID_DEFAULT)) {
            DictionaryEntity parent = super.findById(entity.getParentId());

            boolean valid = insertValdator(entity.getParentId(),
                    "dataName",
                    entity.getDataName());
            if (!valid) {
                throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "[dataName]已经存在");
            }
            valid = insertValdator(entity.getParentId(),
                    "dataKey",
                    entity.getDataKey());
            if (!valid) {
                throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "[dataKey]已经存在");
            }
        }

        return super.save(entity);
    }

    public List<DictionaryEntity> listByDictType(DictionaryEntity entity) {
        Specification<DictionaryEntity> spec =  (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("dictType"), entity.getDictType()));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            List<Order> orders=new ArrayList<>();
            orders.add(cb.asc(root.get("dataOrder")));
            query.where(cb.or(predicates.toArray(predicateArray))).orderBy(orders);
            return query.getRestriction();
        };


        return super.findAll(spec);
    }
    //pending
    public PageResponse<DictionaryEntity> list(DictQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);
        Specification<DictionaryEntity> spec = null;
        spec = (root, query, cb) -> {
            List<Predicate> all = Lists.newArrayList();
            List<Predicate> predicates = new ArrayList<>();
            if (!Strings.isNullOrEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("dataName"), model.getKey()));
                predicates.add(cb.equal(root.get("dictType"), model.getKey()));
                predicates.add(cb.equal(root.get("dataKey"), model.getKey()));
                predicates.add(cb.equal(root.get("dataValue"), model.getKey()));

                Predicate[] predicateArray = new Predicate[predicates.size()];
                all.add(cb.or(predicates.toArray(predicateArray)));
            }
            all.add(cb.equal(root.get("tid"), super.getTid()));
            Predicate[] allArray = new Predicate[all.size()];
            query.where(cb.and(all.toArray(allArray)));
            return query.getRestriction();
        };

        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<DictionaryEntity> resp = new PageResponse<>();
        resp.setTotalCount((int) num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }

}
