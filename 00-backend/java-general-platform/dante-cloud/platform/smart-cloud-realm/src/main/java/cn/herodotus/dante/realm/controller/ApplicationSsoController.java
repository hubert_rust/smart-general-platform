package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.application.ApplicationSsoModel;
import cn.herodotus.dante.realm.service.ApplicationTenantSsoService;
import cn.herodotus.dante.realm.service.group.ApplicationSsoServiceGroupImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/8/24 8:29
 */
@RestController
@RequestMapping("/appsso")
@Tag(name = "应用管理")
@Validated
public class ApplicationSsoController {

    @Resource
    private ApplicationSsoServiceGroupImpl applicationSsoServiceGroup;
    @Resource
    private ApplicationTenantSsoService applicationTenantSsoService;

    /*@Operation(summary = "应用详情", description = "应用详情")
    @PostMapping("/detail/{id}")
    public Result<ApplicationEntity> detail(@PathVariable String id) {
        ApplicationEntity ret = applicationService.findById(id);
        return Result.success(Feedback.OK, ret);
    }*/
    /*@Operation(summary = "添加应用", description = "添加应用")
    @PostMapping("/insert")
    public Result<ApplicationEntity> save(@Valid @RequestBody ApplicationEntity entity) {
        ApplicationEntity ret = applicationService.save(entity);
        return Result.success(Feedback.OK, ret);
    }*/




    /*@Operation(summary = "删除应用", description = "删除应用")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@Valid @PathVariable String id) {
        boolean ret = applicationService.delete(id);
        return Result.success(Feedback.OK, ret);
    }*/

    @Operation(summary = "应用列表", description = "应用列表")
    @PostMapping("/list")
    public Result<List<ApplicationSsoModel>> list(@RequestBody BaseQueryModel model) {
        List<ApplicationSsoModel> ret = applicationSsoServiceGroup.listSsoApplication();
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "应用市场列表", description = "应用市场列表")
    @PostMapping("/listMarket")
    public Result<List<ApplicationEntity>> marketList(@RequestBody BaseQueryModel model) {
        List<ApplicationEntity> ret = applicationSsoServiceGroup.listSsoMarketplace();
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "从应用市场中添加应用", description = "从应市场添加应用")
    @PostMapping("/addsso/{clientId}")
    public Result<Boolean> addsso(@PathVariable String clientId) {
        Boolean ret = applicationSsoServiceGroup.addSsoMarketApp2AppList(clientId);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除SSO应用", description = "删除SSO应用")
    @PostMapping("/deletesso/{id}")
    public Result<Boolean> deletesso(@PathVariable String id) {
        Boolean ret = applicationSsoServiceGroup.deleteAppFromSso(id);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "SSO应用详情", description = "SSO应用详情")
    @PostMapping("/ssoDetail/{clientId}")
    public Result<ApplicationSsoModel> ssoDetail(@PathVariable String clientId) {
        ApplicationSsoModel ret = applicationSsoServiceGroup.ssoDetail(clientId);
        return Result.success(Feedback.OK, ret);
    }
}
