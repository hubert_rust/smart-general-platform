/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.constant.AccountConstant;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.UserAuthEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.repository.UserAuthRepository;
import cn.herodotus.dante.realm.util.PassayUtil;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;
import jakarta.annotation.Resource;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class UserAuthService extends BaseService<UserAuthEntity, String> {

    private UserAuthRepository userAuthRepository;

    @Resource
    private SymmetricCryptoProcessor symmetricCryptoProcessor;

    @Autowired
    public UserAuthService(UserAuthRepository userAuthRepository) {
        this.userAuthRepository = userAuthRepository;
    }

    @Override
    public BaseRepository<UserAuthEntity, String> getRepository() {
        return this.userAuthRepository;
    }

    public List<UserAuthEntity> findByIds(List<String> ids) {
        return userAuthRepository.findAllById(ids);
    }
    public boolean deleteByUidAndTid(String uid, String tid) {
        userAuthRepository.deleteByUidAndTid(uid, tid);
        return true;
    }

    public boolean deleteByIdsInAndTid(List<String> ids, String tid) {
        userAuthRepository.deleteByIdInAndTid(ids, tid);
        return true;
    }
    public List<UserAuthEntity> findUserAuthByUid(String uid) {
        Specification<UserAuthEntity> spec = new Specification<UserAuthEntity>() {
            @Override
            public Predicate toPredicate(Root<UserAuthEntity> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {

                return cb.equal(root.get("uid"), uid);
            }
        };
        return super.findAll(spec);
    }
    public List<UserAuthEntity> findUserAccountByTypeAndIdentity(String identityType, String identity) {
        Specification<UserAuthEntity> spec = new Specification<UserAuthEntity>() {
            @Override
            public Predicate toPredicate(Root<UserAuthEntity> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(cb.equal(root.get("identityType"), identityType));
                predicates.add(cb.equal(root.get("identity"), identity));

                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
        };

        List<UserAuthEntity> list = super.findAll(spec);
        return list;
    }


    //创建用户的时候，同时创建登录账号
    @NotNull
    public UserAuthEntity saveUserAuthEntity(UserModel model, String tid, String id) {
        //先判断用户是否重复
        List<UserAuthEntity> list
                = this.findUserAccountByTypeAndIdentity(model.getCreateType(), model.getAccount());
        if (!CollectionUtils.isEmpty(list)) {
            throw new RealmException("realmException", RealmErrorKey.REG_USER_EXIST);
        }

        UserAuthEntity userAuthEntity = new UserAuthEntity();
        userAuthEntity.setUid(id);
        userAuthEntity.setTid(tid);

        userAuthEntity.setStatus(RealmConstant.REG_OPTYPE_ACTIVE);
        userAuthEntity.setCredential(model.getPwd());
        userAuthEntity.setIdentity(model.getAccount());
        userAuthEntity.setCreateBy(RealmConstant.TID_VALUE_DEFAULT);
        userAuthEntity.setUpdateBy(RealmConstant.TID_VALUE_DEFAULT);
        userAuthEntity.setVerified(0);

        String password = symmetricCryptoProcessor.decrypt(model.getPwd(), ProtectConstant.SM4_SECRET);
        Integer pwdLevel = PassayUtil.passwordLevel(password, null);
        userAuthEntity.setPwdLevel(pwdLevel);
        if (AccountConstant.REG_TYPE_EMAIL.equals(model.getCreateType())) {
            userAuthEntity.setIdentityTitle(AccountConstant.ACCOUNT_TYPE_EMAIL);
            userAuthEntity.setIdentityType(model.getCreateType());
        } else if (AccountConstant.REG_TYPE_MOBILE.equals(model.getCreateType())) {
            userAuthEntity.setIdentityTitle(AccountConstant.ACCOUNT_TYPE_MOBILE);
            userAuthEntity.setIdentityType(model.getCreateType());
        } else if (AccountConstant.REG_TYPE_ACCOUNT.equals(model.getCreateType())) {
            userAuthEntity.setIdentityTitle(AccountConstant.ACCOUNT_TYPE_ACCOUNT);
            userAuthEntity.setIdentityType(model.getCreateType());
        } else {
            throw new RealmException("realmException", RealmErrorKey.TYPE_NOT_MATCH);
        }
        super.save(userAuthEntity);
        return userAuthEntity;
    }

    public List<UserAuthEntity> findByCreateBy(String createBy) {
        return userAuthRepository.findByCreateBy(createBy);
    }




    public UserAuthEntity findByIdentity(String identity) {
        return userAuthRepository.findByIdentity(identity);
    }

}
