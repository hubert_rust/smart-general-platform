package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import cn.herodotus.dante.realm.constant.RealmConstant;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "sys_token_salt")
@Data
@EqualsAndHashCode(callSuper=true)
public class TokenSaltEntity extends CommonTenantEntity {
    @Column(name = "uid", length = 64)
    private String uid;
    @Column(name = "nav", length = 64)
    private String nav;
    @Column(name = "salt", length = 64)
    private String salt;
    @Column(name = "rft", length = 64)
    private String rft;
    @Column(name = "dev", length = 64)
    private String dev;

    private String state = RealmConstant.STATUS_ACTIVE;
}
