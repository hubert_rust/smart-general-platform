package cn.herodotus.dante.realm.model;

import lombok.Data;

@Data
public class AddUserModel {
    private String account;
    private String mobile;
    private String email;
    private String type; //account, mobile, emial
    private String tid;
    private String tidType;
}
