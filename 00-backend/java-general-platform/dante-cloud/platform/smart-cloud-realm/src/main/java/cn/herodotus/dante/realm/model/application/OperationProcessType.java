package cn.herodotus.dante.realm.model.application;

import lombok.Data;

import java.io.Serializable;

@Data
public class OperationProcessType implements Serializable {
    private String id;
    private String processType;
}
