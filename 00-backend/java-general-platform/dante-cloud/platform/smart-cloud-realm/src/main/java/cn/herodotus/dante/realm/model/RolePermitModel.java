package cn.herodotus.dante.realm.model;

import cn.herodotus.dante.realm.entity.RoleApiDataPermissionEntity;
import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author root
 * @description 数据权限
 * @date 2023/8/30 15:49
 */
@Data
public class RolePermitModel {

    private String uid;
    private String roleId;
    private String digest;
    private String clientId;
    private String clientFrom;
    private List<RoleApiDataPermissionEntity> list = Lists.newArrayList();
}
