/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.repository.ResMenuRepository;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class ResMenuService extends BaseService<ResMenuEntity, String> {

    private ResMenuRepository resMenuRepository;

    @Autowired
    public ResMenuService(ResMenuRepository resMenuRepository) {
        this.resMenuRepository = resMenuRepository;
    }

    @Override
    public BaseRepository<ResMenuEntity, String> getRepository() {
        return this.resMenuRepository;
    }


    public boolean checkParam(ResMenuEntity entity) {
        if (Strings.isEmpty(entity.getParentId())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "parentId没有值");
        }
        if (Strings.isEmpty(entity.getClientId())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID, "clientId没有值");
        }
        return true;
    }
    @Transactional(rollbackFor = Exception.class)
    public ResMenuEntity save(ResMenuEntity entity) throws RealmException {
        checkParam(entity);
        return super.insert(entity);
    }
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String id) {
        List<Object[]> list = resMenuRepository.findByRecurise(id);
        if (CollectionUtils.isEmpty(list)) {
            return true;
        }
        List<ResMenuEntity> removes = Lists.newArrayList();
        for (Object[] objects : list) {
            ResMenuEntity entity = new ResMenuEntity();
            Object obj = objects[0];
            entity.setId(obj.toString());
            removes.add(entity);
        }
        super.deleteAll(removes);
        return true;
    }
    @Transactional(rollbackFor = Exception.class)
    public ResMenuEntity update(ResMenuEntity entity) throws RealmException {
        checkParam(entity);
        return super.save(entity);
    }


    //菜单排序
    public static void menuSort(List<ResMenuEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<ResMenuEntity> ret = list.stream()
                .sorted(Comparator.comparing(ResMenuEntity::getVoOrder)).collect(Collectors.toList());
        list.clear();
        list.addAll(ret);
        list.stream().forEach(v->{
            List objs = v.getChildren();
            List<ResMenuEntity> pf = objs;
            menuSort(pf);
        });

    }
    public List<ResMenuEntity> listMenuByClient(String client, boolean tree) {
        List<ResMenuEntity> list = resMenuRepository.findAllByClientId(client);
        if (!tree) {
            return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
        }

        //需要排序
        DefaultTreeBuildFactory<ResMenuEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<ResMenuEntity> ret = factory1.doTreeBuild(list);
        menuSort(ret);
        return ret;
    }

    public List<ResMenuEntity> findByClientId(String client) {
        return resMenuRepository.findByClientIdAndStatus(client, RealmConstant.STATUS_ACTIVE);
    }

    public List<ResMenuEntity> findByClientIdAndPermisLikeAndStatus(
            String clientId, String permisLike, String status
    ){
        return resMenuRepository.findByClientIdAndPermisLikeAndStatus(
                clientId, permisLike, status
        );
    }

}
