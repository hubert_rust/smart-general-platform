/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.repository;

import cn.herodotus.dante.realm.entity.DeptEntity;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface DeptRepository extends BaseRepository<DeptEntity, String> {
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    @Modifying
    @Query("delete from DeptEntity c where c.tid = :tid and c.path like CONCAT(':basePath','%')")
    void deleteDept(@Param("tid") String tid, @Param("basePath") String basePath);


    DeptEntity findByInnerCodeAndTid(String innerCode, String tid);

    List<DeptEntity> findByTid(String tid);

    List<DeptEntity> findByTidAndPathLike(String tid, String path);

    @Query(value = "WITH RECURSIVE cte as ( \n" +
            " select * from sys_dept where  inner_code = ?1 and tid = ?2 \n" +
            " UNION all\n" +
            " select  t.* from sys_dept  t\n" +
            " join cte on cte.inner_code = t.parent_id\n" +
            " \n" +
            " )\n" +
            "  select * from cte", nativeQuery = true)
    List<Object[]> findByRecurise(String innerCode, String tid);

}
