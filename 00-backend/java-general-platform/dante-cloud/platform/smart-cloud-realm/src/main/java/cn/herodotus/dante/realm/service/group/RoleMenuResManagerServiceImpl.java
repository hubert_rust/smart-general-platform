package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.*;
import cn.herodotus.dante.realm.model.MenuPermissionModel;
import cn.herodotus.dante.realm.model.RolePermitModel;
import cn.herodotus.dante.realm.model.RoleResourceModel;
import cn.herodotus.dante.realm.repository.*;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.dromara.hutool.core.map.multi.RowKeyTable;
import org.dromara.hutool.core.map.multi.Table;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author root
 * @description TODO
 * @date 2023/9/5 11:02
 */
@Slf4j
@Service
public class RoleMenuResManagerServiceImpl {
    @Resource
    private RoleSubjectService roleSubjectService;
    @Resource
    private GroupUserService groupUserService;
    @Resource
    private UserService userService;
    @Resource
    private RoleService roleService;
    @Resource
    private RoleResourceService roleResourceService;
    @Resource
    private ResMenuService resMenuService;
    @Resource
    private ResApiService resApiService;

    // 用户角色: 通过角色主体(用户，用户组，组织)获取:
    // 登录用户也可以使用此接口
    // multi: true表示按角色返回（角色不重复），false表示角色主体返回（角色重复）
    public List<RoleEntity> userOfRoleList(String uid, boolean role) {
        String tid = roleSubjectService.getTid();
        // 1角色ID 2 subjectType 3 identity
        Table<String, String, RoleSubjectEntity> table = new RowKeyTable<>();
        // key: roleId, value: identity  通过角色id找角色主体ID
        Map<String, String> map = Maps.newHashMap();

        List<String> query = Lists.newArrayList();
        // 1
        getUserOfRoleSubject(uid, tid, table, query);
        if (CollectionUtils.isEmpty(query)) {
            return null;
        }

        List<RoleEntity> finds = ((RoleRepository) roleService.getRepository()).findByTidAndIdIn(tid, query);
        if (role) {
            return finds;
        } else {
            // 按照角色主体返回
            List<RoleEntity> subjects = Lists.newArrayList();
            finds.stream().forEach(v -> {
                if (table.get(v.getId(), RealmConstant.SUBJECT_USER) != null) {
                    RoleEntity roleEntity = new RoleEntity();
                    BeanUtils.copyProperties(v, roleEntity);
                    roleEntity.setRoleSubject(RealmConstant.SUBJECT_USER);
                    roleEntity.setSubjectName(table.get(v.getId(), RealmConstant.SUBJECT_USER).getSubjectName());
                    roleEntity.setIdentity(table.get(v.getId(), RealmConstant.SUBJECT_USER).getIdentity());
                    subjects.add(roleEntity);
                }
                if (table.get(v.getId(), RealmConstant.SUBJECT_ORG) != null) {
                    RoleEntity roleEntity = new RoleEntity();
                    BeanUtils.copyProperties(v, roleEntity);
                    roleEntity.setRoleSubject(RealmConstant.SUBJECT_ORG);
                    roleEntity.setSubjectName(table.get(v.getId(), RealmConstant.SUBJECT_ORG).getSubjectName());
                    roleEntity.setIdentity(table.get(v.getId(), RealmConstant.SUBJECT_ORG).getIdentity());
                    subjects.add(roleEntity);
                }
                if (table.get(v.getId(), RealmConstant.SUBJECT_GROUP) != null) {
                    RoleEntity roleEntity = new RoleEntity();
                    BeanUtils.copyProperties(v, roleEntity);
                    roleEntity.setRoleSubject(RealmConstant.SUBJECT_GROUP);
                    roleEntity.setSubjectName(table.get(v.getId(), RealmConstant.SUBJECT_GROUP).getSubjectName());
                    roleEntity.setIdentity(table.get(v.getId(), RealmConstant.SUBJECT_GROUP).getIdentity());
                    subjects.add(roleEntity);
                }
            });
            return subjects;
        }
    }

    // 获取用户所有角色ID，查询角色主体: 用户，组织，用户组
    private void getUserOfRoleSubject(String uid,
                                      String tid,
                                      // Table<String, String, String> table,
                                      Table<String, String, RoleSubjectEntity> table,
                                      List<String> query) {

        List<RoleSubjectEntity> list = ((RoleSubjectRepository) roleSubjectService.getRepository())
                .findByTidAndSubjectTypeAndIdentity(tid, RealmConstant.SUBJECT_USER, uid);
        list.stream().forEach(v -> {
            query.add(v.getRoleId());
            //table.put(v.getRoleId(), RealmConstant.SUBJECT_USER, v.getIdentity());
            table.put(v.getRoleId(), RealmConstant.SUBJECT_USER, v);
        });


        // 2 角色主体是用户组，先查找uid所在用户组
        List<GroupUserEntity> group = ((GroupUserRepository) groupUserService.getRepository()).findByTidAndUidAndGroupType(
                tid, uid, RealmConstant.GROUP_TYPE_STATIC
        );
        if (CollectionUtils.isNotEmpty(group)) {
            List<String> ids = group.stream().map(m -> m.getGroupId()).toList();
            List<RoleSubjectEntity> groupSubject = ((RoleSubjectRepository) roleSubjectService.getRepository())
                    .findByTidAndSubjectTypeAndIdentityIn(tid, RealmConstant.SUBJECT_GROUP, ids);
            groupSubject.stream().forEach(v -> {
                query.add(v.getRoleId());
                table.put(v.getRoleId(), RealmConstant.SUBJECT_GROUP, v);
            });
        }

        // 3 角色主体是组织，先查找uid所在组织
        UserEntity userEntity = userService.findById(uid);
        List<RoleSubjectEntity> orgSubject = ((RoleSubjectRepository) roleSubjectService.getRepository())
                .findByTidAndSubjectTypeAndIdentity(tid, RealmConstant.SUBJECT_ORG, userEntity.getMainDeptId());
        orgSubject.stream().forEach(v -> {
            query.add(v.getRoleId());
            // table.put(v.getRoleId(), RealmConstant.SUBJECT_ORG, v.getIdentity());
            table.put(v.getRoleId(), RealmConstant.SUBJECT_ORG, v);
        });
    }

    private void setSelectedMenu(MenuPermissionModel model, Map<String, ResMenuEntity> map) {
        Object obj = map.get(model.getCatalogId());
        if (Objects.nonNull(obj)) {
            model.setCatalogChecked(true);
        }
        obj = map.get(model.getMenuId());
        if (Objects.nonNull(obj)) {
            model.setMenuChecked(true);
        }
        obj = map.get(model.getPageId());
        if (Objects.nonNull(obj)) {
            model.setPageChecked(true);
        }
        if (CollectionUtils.isNotEmpty(model.getChildren())) {
            for (Tree child : model.getChildren()) {
                obj = map.get(child.getNodeId());
                if (Objects.nonNull(obj)) {
                    child.setRed(true);
                }
            }
        }
    }
    // 菜单输出到前端:
    //一级目录  菜单   页面    按钮
    //一级目录和菜单需要合并行，如果没有页面(采集对接按钮), 增加一个虚拟页面，只是为了前端好显示
    private List<MenuPermissionModel> generatorModel(String uid, List<ResMenuEntity> list) {
        //需要排序
        DefaultTreeBuildFactory<ResMenuEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<ResMenuEntity> ret = factory1.doTreeBuild(list);
        ResMenuEntity.menuSort(ret);


        // 排序后list中节点有了子节点
        Map<String, ResMenuEntity> map = Maps.newHashMap();
        list.stream().forEach(v -> map.put(v.getId(), v));

        List<ResMenuEntity> last = Lists.newArrayList();
        //getTailNode(new ArrayList<Tree>(ret), last);

        List<ResMenuEntity> funList = Lists.newArrayList();
        List<MenuPermissionModel> rowMenus = Lists.newArrayList();
        for (ResMenuEntity entity : ret) {
            if (CollectionUtils.isEmpty(entity.getChildren())) {
                MenuPermissionModel permissionModel = new MenuPermissionModel();
                permissionModel.setId(IDContainer.INST.getUUID(20, true));
                permissionModel.setCatalog(entity.getVoTitle());
                permissionModel.setCatalogId(entity.getId());
                permissionModel.setParentId(entity.getParentId());
                permissionModel.setRootId(entity.getId());

                rowMenus.add(permissionModel);
                continue;
            }
            for (Tree child : entity.getChildren()) {

                ResMenuEntity item = (ResMenuEntity) child;
                if (CollectionUtils.isEmpty(item.getChildren())) {
                    MenuPermissionModel itemModel = new MenuPermissionModel();
                    itemModel.setId(IDContainer.INST.getUUID(20, true));
                    itemModel.setCatalog(entity.getVoTitle());
                    itemModel.setCatalogId(entity.getId());
                    itemModel.setMenu(item.getVoTitle());
                    itemModel.setMenuId(item.getId());
                    itemModel.setParentId(item.getParentId());
                    itemModel.setRootId(entity.getId());
                    rowMenus.add(itemModel);
                    continue;
                }

                // page层
                for (Tree itemChild : item.getChildren()) {
                    ResMenuEntity tailItem = (ResMenuEntity) itemChild;
                    MenuPermissionModel tailModel = new MenuPermissionModel();
                    tailModel.setCatalog(entity.getVoTitle());
                    tailModel.setCatalogId(entity.getId());
                    tailModel.setMenu(item.getVoTitle());
                    tailModel.setMenuId(item.getId());
                    tailModel.setId(IDContainer.INST.getUUID(20, true));
                    tailModel.setPage(tailItem.getVoTitle());
                    tailModel.setPageId(tailItem.getId());
                    tailModel.setParentId(tailItem.getParentId());
                    tailModel.setRootId(entity.getId());
                    rowMenus.add(tailModel);
                    if (CollectionUtils.isNotEmpty(tailItem.getChildren())) {
                        tailModel.getChildren().addAll(tailItem.getChildren());
                    }
                }
            }
        }


        // 前端合并行参数
        Map<String, List<MenuPermissionModel>> groups
                = rowMenus
                .stream()
                .collect(Collectors.groupingBy(MenuPermissionModel::getCatalogId));
        String token = "";
        for (MenuPermissionModel rowMenu : rowMenus) {
            // setSelectedMenu(rowMenu, menuMap);
            if (!token.equals(rowMenu.getCatalogId())) {
                token = rowMenu.getCatalogId();
                if (CollectionUtils.isNotEmpty(groups.get(token))) {
                    rowMenu.setCatalogMerge(true);
                    rowMenu.setCatalogIndex(groups.get(token).size());
                    rowMenu.setMenuIndexRef(groups.get(token).size());
                }
            } else {
                if (CollectionUtils.isNotEmpty(groups.get(token))) {
                    rowMenu.setMenuIndexRef(groups.get(token).size());
                }
            }
        }
        token = "";
        for (MenuPermissionModel rowMenu : rowMenus) {
            if (!token.equals(rowMenu.getMenuId())) {
                token = rowMenu.getMenuId();
                ResMenuEntity entity = map.get(token);
                if (CollectionUtils.isNotEmpty(entity.getChildren())) {
                    rowMenu.setMenuMerge(true);
                    rowMenu.setMenuIndex(entity.getChildren().size());
                    rowMenu.setPageIndexRef(entity.getChildren().size());
                }
            } else {
                ResMenuEntity entity = map.get(token);
                if (CollectionUtils.isNotEmpty(entity.getChildren())) {
                    rowMenu.setPageIndexRef(entity.getChildren().size());
                }
            }
        }

        return rowMenus;
    }

    // 单个角色所拥有的菜单资源，前端使用


   /* // 用户所拥有的菜单资源
    public Map<String, ResMenuEntity> getUserOfMenu(String uid) {
        Map<String, ResMenuEntity> map = Maps.newHashMap();
        String tid = roleResourceService.getTid();
        // 只返回角色列表即可
        Table<String, String, RoleSubjectEntity> table = new RowKeyTable<>();
        // key: roleId, value: identity  通过角色id找角色主体ID
        List<String> query = Lists.newArrayList();
        getUserOfRoleSubject(uid, tid, table, query);

        if (CollectionUtils.isNotEmpty(query)) {
            // 通过角色返回菜单资源
            List<RoleResourceEntity> roleRes = ((RoleResourceRepository) roleResourceService.getRepository())
                    .findByTidAndObjectTypeAndRoleIdIn(tid, RealmConstant.ROLE_RES_OBJECT_TYPE_MENU, query);
            if (CollectionUtils.isEmpty(roleRes)) {
                return map;
            }
            List<ResMenuEntity> menus
                    = ((ResMenuRepository) resMenuService.getRepository()).findByTidAndIdIn(tid,
                    roleRes.stream().map(m -> m.getIdentity()).collect(Collectors.toSet()));
            menus.stream().forEach(v->map.put(v.getId(), v));
        }

        return map;
    }
*/
    /*
     * @Author root
     * @Description 单个角色资源: 当前角色/管理员能看到的资源, 同时包含已经勾选的
     * @Date 10:56 2023/9/5
     * @Param
     * @return
     **/
    public List<MenuPermissionModel> menus(RolePermitModel model) {
        String tid = resMenuService.getTid();
        // smart-auth: pending
        // list是应用所有菜单
        List<ResMenuEntity> list = resMenuService.findByClientId(model.getClientId());
        List<MenuPermissionModel> models =  generatorModel(model.getUid(), list);

        List<RoleResourceEntity> roleRes
                = roleResourceService.listResources(model.getRoleId(),
                model.getClientId(),
                model.getClientFrom(),
                RealmConstant.ROLE_RES_OBJECT_TYPE_MENU);
        if (CollectionUtils.isEmpty(roleRes)) {
            return models;
        }

        Map<String, ResMenuEntity> map = Maps.newHashMap();
        List<ResMenuEntity> menus
                = ((ResMenuRepository)resMenuService.getRepository())
                .findByTidAndIdIn(tid, roleRes.stream().map(m->m.getIdentity()).collect(Collectors.toSet()));
        menus.stream().forEach(v-> map.put(v.getId(), v));
        for (MenuPermissionModel permissionModel : models) {
            setSelectedMenu(permissionModel, map);
        }
        return models;
    }
    /*
     * @Author root
     * @Description //用户详情中-用户资源权限，用户角色菜单, 只返回用户
     * @Date 16:24 2023/9/25
     * @Param
     * @return
     **/
    public List<ResApiEntity> userApis(RolePermitModel model) {
        String tid = resMenuService.getTid();


        // smart-auth: pending
        // list是应用所有菜单
        List<ResApiEntity> list = resApiService.findByClientId(model.getClientId(), false);

        List<RoleResourceEntity> roleRes
                = roleResourceService.listResources(model.getRoleId(),
                model.getClientId(),
                model.getClientFrom(),
                RealmConstant.ROLE_RES_OBJECT_TYPE_API);
        if (CollectionUtils.isEmpty(roleRes)) {
            return list;
        }

        Map<String, String> map = Maps.newHashMap();
        roleRes.stream().forEach(v-> map.put(v.getIdentity(), v.getIdentity()));
        for (ResApiEntity entity : list) {
            if (map.get(entity.getDigest()) != null) {
                entity.setGranted(true);
            }
            else {
                entity.setGranted(false);
            }
        }
        DefaultTreeBuildFactory<ResApiEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId(RealmConstant.PARENT_ID_DEFAULT);
        List<ResApiEntity> ret = factory1.doTreeBuild(list);
        ResApiEntity.apiSort(ret);
        return ret;
    }
    /*
     * @Author root
     * @Description //用户详情中-用户资源权限，用户角色菜单, 只返回用户
     * @Date 16:24 2023/9/25
     * @Param
     * @return
     **/
    public List<ResMenuEntity> userMenus(RolePermitModel model) {
        String tid = resMenuService.getTid();


        // smart-auth: pending
        // list是应用所有菜单
        List<ResMenuEntity> list = resMenuService.findByClientId(model.getClientId());

        List<RoleResourceEntity> roleRes
                = roleResourceService.listResources(model.getRoleId(),
                model.getClientId(),
                model.getClientFrom(),
                RealmConstant.ROLE_RES_OBJECT_TYPE_MENU);
        if (CollectionUtils.isEmpty(roleRes)) {
            DefaultTreeBuildFactory<ResMenuEntity> factory1 = new DefaultTreeBuildFactory<>();
            factory1.setRootParentId(RealmConstant.PARENT_ID_DEFAULT);
            List<ResMenuEntity> ret = factory1.doTreeBuild(list);
            ResMenuEntity.menuSort(ret);
            return ret;
        }

        Map<String, String> map = Maps.newHashMap();
        roleRes.stream().forEach(v-> map.put(v.getIdentity(), v.getIdentity()));
        for (ResMenuEntity entity : list) {
            if (map.get(entity.getId()) != null) {
                entity.setRed(true);
            }
            else {
                entity.setRed(false);
            }
        }
        DefaultTreeBuildFactory<ResMenuEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId(RealmConstant.PARENT_ID_DEFAULT);
        List<ResMenuEntity> ret = factory1.doTreeBuild(list);
        ResMenuEntity.menuSort(ret);
        return ret;
    }

    // 保存角色的菜单资源
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean saveMenus(RoleResourceModel model) {
        String tid = roleResourceService.getTid();
        // 先删除菜单和菜单对应的接口
        List<RoleResourceEntity> list
                = roleResourceService.listResources(
                model.getRoleId(),
                model.getClientId(),
                model.getClientFrom(),
                RealmConstant.ROLE_RES_OBJECT_TYPE_MENU);
        if (CollectionUtils.isNotEmpty(list)) {
            List<String> digests = list.stream()
                    .filter(m -> !Strings.isEmpty(m.getDigest()))
                    .map(v -> v.getDigest()).toList();
            roleResourceService.deleteByIds(list.stream().map(m -> m.getId()).toList());

            if (CollectionUtils.isNotEmpty(digests)) {
                ((RoleResourceRepository) roleResourceService.getRepository())
                        .deleteByTidAndRoleIdAndObjectTypeAndIdentityIn(
                                tid, model.getRoleId(), RealmConstant.ROLE_RES_OBJECT_TYPE_API, digests);
            }
        }
        if (CollectionUtils.isEmpty(model.getList())) {
            return true;
        }

        // 保存菜单对应的api
        List<RoleResourceEntity> apis = Lists.newArrayList();
        model.getList().stream()
                .filter(p -> Strings.isNotEmpty(p.getDigest()))
                .forEach(v -> {
                    RoleResourceEntity entity = new RoleResourceEntity();
                    entity.setRoleId(model.getRoleId());
                    entity.setTid(tid);
                    entity.setClientId(model.getClientId());
                    entity.setClientFrom(model.getClientFrom());
                    entity.setIdentity(v.getDigest());
                    entity.setObjectType(RealmConstant.ROLE_RES_OBJECT_TYPE_API);
                    apis.add(entity);
                });

        if (CollectionUtils.isNotEmpty(apis)) {
            model.getList().addAll(apis);
        }
        model.getList().stream().forEach(v-> {
            v.setClientFrom(model.getClientFrom());
            v.setClientId(model.getClientId());
        });
        roleResourceService.insertAll(model.getList());
        return true;
    }



}
