package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/*
 * @Author root
 * @Description // 应用访问授权
 * @Date 15:02 2023/8/21
 * @Param
 * @return
 **/
@Entity
@Table(name = "sys_application_subject" )
@Data
@EqualsAndHashCode(callSuper=true)
public class ApplicationSubjectEntity extends CommonTenantEntity {
    @Column(name = "client_Id", length = 64)
    private String clientId;

    // 授权主体名称
    @Column(name = "subject_name", length = 64)
    private String subjectName;

    // self sso
    @Column(name = "client_from", length = 16)
    private String clientFrom;

    // 为了方便查找相同授权
    private String digest;
    // 授权主体类型: 用户，组织，用户组，不包括角色
    // SUBJECT_
    @Column(name = "subject_type", length = 64)
    private String subjectType;
    // id
    @Column(name = "identity", length = 64)
    private String identity;

    // 授权作用， 暂时没有用
    private String scope;
    private Date expire;


}
