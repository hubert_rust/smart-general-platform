package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.ResApiService;
import cn.herodotus.dante.realm.service.group.ApplicationServiceGroupImpl;
import cn.herodotus.dante.scan.entity.OperationEntity;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/application")
@Tag(name = "自建应用管理-后台资源")
@Validated
public class ApplicationApiController {

    @Resource
    private ApplicationService applicationService;
    @Resource
    private ApplicationServiceGroupImpl applicationServiceGroup;
    @Resource
    private ResApiService resApiService;

    @Operation(summary = "刷新后台接口", description = "刷新后台接口")
    @PostMapping("/remoteLoad/{client}")
    public Result<List<OperationEntity>> remoteLoad(@PathVariable String client) {
        List<OperationEntity> ret = applicationServiceGroup.remoteLoad(client);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "批量同步到应用接口", description = "批量同步到应用接口资源")
    @PostMapping("/sync/{client}")
    public Result<Integer> sync(@RequestBody List<OperationEntity> list,
                                @PathVariable String client) {
        Integer ret = applicationServiceGroup.sync2apiResOfApp(list, client, false);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "同步到应用接口", description = "同步到应用接口资源")
    @PostMapping("/syncApi/{client}")
    public Result<Integer> syncApi(@RequestBody OperationEntity entity,
                                @PathVariable String client) {
        List<OperationEntity> list = new ArrayList<>(){{add(entity);}};
        Integer ret = applicationServiceGroup.sync2apiResOfApp(list, client, false);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "强制同步到应用接口", description = "强制同步到应用接口资源")
    @PostMapping("/forceSyncApi/{client}")
    public Result<Integer> forceSyncApi(@RequestBody OperationEntity entity,
                                   @PathVariable String client) {
        List<OperationEntity> list = new ArrayList<>(){{add(entity);}};
        Integer ret = applicationServiceGroup.sync2apiResOfApp(list, client, true);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "刷新后台接口列表", description = "刷新后台接口列表")
    @PostMapping("/backendListOfapp/{client}")
    public Result<List<OperationEntity>> apiListOfBackend(@PathVariable String client) {
        List<OperationEntity> ret = applicationServiceGroup.listBackendApi(client);
        return Result.success(Feedback.OK, ret);
    }

  /*  @Operation(summary = "后台接口列表", description = "后台接口列表")
    @PostMapping("/opListOfapp/{client}")
    public Result<List<ResApiEntity>> operationOfApp(@PathVariable String client) {
        List<ResApiEntity> ret = applicationServiceGroup.listOperationByClient(client);
        return Result.success(Feedback.OK, ret);
    }*/


    @Operation(summary = "发布接口", description = "正式发布接口")
    @PostMapping("/releaseApi/{id}")
    public Result<Boolean> releaseApi(@PathVariable String id) {
        Boolean ret = applicationServiceGroup.releaseProcess(id, 1);
        return Result.success(Feedback.OK, ret);
    }



}
