package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Entity
@Table(name = "sys_group_role" )
@Data
@EqualsAndHashCode(callSuper=true)
public class GroupRoleEntity extends CommonTenantEntity {

    @Column(name = "group_id", length = 64)
    private String groupId;
    @Column(name = "group_tag", length = 64)
    private String groupTag;
    @Column(name = "role_id", length = 64)
    private String roleId;
    @Column(name = "group_type", length = 32)
    private String groupType;

    @Transient
    private List<UserEntity> users;
}
