package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

// 弃用
@Deprecated
@Entity
@Table(name = "sys_tenant_application" )
@Data
@EqualsAndHashCode(callSuper=true)
public class TenantApplicationEntity extends CommonTenantEntity {
    @Column(name = "tid_type", length = 64)
    private String tidType;
    @Column(name = "app_id", length = 64)
    private String appId;
}
