/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.constant.RiskListConstant;
import cn.herodotus.dante.realm.entity.RiskListEnity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.repository.RiskListRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>Description:  </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class RiskListService extends BaseService<RiskListEnity, String> {

    private RiskListRepository riskListRepository;

    public RiskListService(RiskListRepository riskListRepository) {
        this.riskListRepository = riskListRepository;
    }

    @Override
    public BaseRepository<RiskListEnity, String> getRepository() {
        return this.riskListRepository;
    }
    public RiskListEnity addIpBlack(RiskListEnity enity) {
        enity.setListClass(RiskListConstant.LIST_CLASS_BLACK);
        enity.setSubjectType(RiskListConstant.SUBJECT_TYPE_IP);
        super.insert(enity);
        return enity;
    }
    public RiskListEnity addIpWhite(RiskListEnity enity) {
        enity.setListClass(RiskListConstant.LIST_CLASS_WHITE);
        enity.setSubjectType(RiskListConstant.SUBJECT_TYPE_IP);
        super.insert(enity);
        return enity;
    }
    public boolean addUserWhite(List<RiskListEnity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return true;
        }
        List<String> subjectValues
                = list.stream().map(m->m.getSubjectValue()).toList();
        String tid = super.getTid();

        // 已经存在的
        List<RiskListEnity> finds = riskListRepository.findByTidAndSubjectTypeAndSubjectValueIn(
                tid, RiskListConstant.SUBJECT_TYPE_USER, subjectValues);
        Map<String, String> map = Maps.newHashMap();
        finds.stream().forEach(v->map.put(v.getSubjectValue(), v.getSubjectValue()));

        List<RiskListEnity> saves = Lists.newArrayList();
        for (RiskListEnity enity : list) {
            if (Strings.isEmpty(map.get(enity.getSubjectValue()))) {
                enity.setListClass(RiskListConstant.LIST_CLASS_WHITE);
                enity.setSubjectType(RiskListConstant.SUBJECT_TYPE_USER);
                enity.setAddType(RiskListConstant.ADD_TYPE_HAND);
                enity.setProcessType(RiskListConstant.PROCESS_TYPE_SKIP_AUTH);
                saves.add(enity);
            }
        }
        if (CollectionUtils.isNotEmpty(saves)) {
            super.insertAll(saves);
        }
        return true;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean delete(String id, String listClass, String subjectType) {
        riskListRepository.deleteByIdAndListClassAndSubjectType(id, listClass, subjectType);
        return true;
    }

    public PageResponse<RiskListEnity> ipBlackList(BaseQueryModel model) {

        return riskList(model, RiskListConstant.LIST_CLASS_BLACK, RiskListConstant.SUBJECT_TYPE_IP);
    }
    public PageResponse<RiskListEnity> ipWhiteList(BaseQueryModel model) {
        return riskList(model, RiskListConstant.LIST_CLASS_WHITE, RiskListConstant.SUBJECT_TYPE_IP);
    }
    public PageResponse<RiskListEnity> userWhiteList(BaseQueryModel model) {
        return riskList(model, RiskListConstant.LIST_CLASS_WHITE, RiskListConstant.SUBJECT_TYPE_USER);
    }
    public PageResponse<RiskListEnity> riskList(BaseQueryModel model, String listClass, String subjectType) {
        Pageable pageable = QueryUtil.pageable(model);

        Specification<RiskListEnity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("subjectName"), model.getKey()));
            }
            predicates.add(cb.equal(root.get("listClass"), listClass));
            predicates.add(cb.equal(root.get("subjectType"), subjectType));
            predicates.add(cb.equal(root.get("tid"), super.getTid()));

            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<RiskListEnity> resp = new PageResponse<>();
        resp.setTotalCount((int)num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }
}
