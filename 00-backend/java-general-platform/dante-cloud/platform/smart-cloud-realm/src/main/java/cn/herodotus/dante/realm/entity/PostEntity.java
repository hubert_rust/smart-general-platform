package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import cn.herodotus.dante.realm.constant.RealmConstant;
import com.google.common.collect.Lists;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Entity
@Table(name = "sys_post" )
@Data
@EqualsAndHashCode(callSuper=true)
public class PostEntity  extends CommonTenantEntity {
    @Column(name = "parent_id")
    private String parentId = RealmConstant.PARENT_ID_DEFAULT;
    @Column(name = "post_name")
    private String postName;
    @Column(name = "post_code")
    private String postCode;
    @Column(name = "remark")
    private String remark;

    @Column(name = "link_dept")
    private String linkDept = "-1";
    private String status;

    @Transient
    private List<String> deptIds = Lists.newArrayList();
}
