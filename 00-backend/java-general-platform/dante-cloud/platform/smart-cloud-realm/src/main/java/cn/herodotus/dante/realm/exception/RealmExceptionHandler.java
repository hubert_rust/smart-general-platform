/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.exception;

import cn.herodotus.dante.core.exception.CommonExceptionInterface;
import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.assistant.core.exception.GlobalExceptionHandler;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Description: 统一异常处理器 </p>
 *
 * @author : gengwei.zheng
 * @date : 2019/11/18 8:12
 */
@Slf4j
@Component("realmException")
public class RealmExceptionHandler implements CommonExceptionInterface {


    private static final Map<String, Result<String>> EXCEPTION_DICTIONARY = new HashMap<>();

    static {
        EXCEPTION_DICTIONARY.put(RealmErrorKey.REG_USER_EXIST, getResult(RealmResultErrorCode.REG_USER_EXIST, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.PARENT_NOT_EXIST, getResult(RealmResultErrorCode.PARENT_NOT_EXIST, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.API_HAS_EXIST, getResult(RealmResultErrorCode.API_HAS_EXIST, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.REQUEST_METHOD_NOT_UPDATE, getResult(RealmResultErrorCode.REQUEST_METHOD_NOT_UPDATE, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.PARAM_INVALID, getResult(RealmResultErrorCode.PARAM_INVALID, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.DUP_FIELD, getResult(RealmResultErrorCode.DUP_FIELD, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.NOT_ADMIN, getResult(RealmResultErrorCode.NOT_ADMIN, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.APP_NOT_ALLOW_TO_SSO, getResult(RealmResultErrorCode.APP_NOT_ALLOW_TO_SSO, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.ROLE_HAS_REF, getResult(RealmResultErrorCode.ROLE_HAS_REF, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.OTHER_USER_GROUP_USED, getResult(RealmResultErrorCode.OTHER_USER_GROUP_USED, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.REMOVE_ROLE_SUBJECT_IS_USER, getResult(RealmResultErrorCode.REMOVE_ROLE_SUBJECT_IS_USER, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.USER_REALM_REMOVE_BAN, getResult(RealmResultErrorCode.USER_REALM_REMOVE_BAN, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.OLD_PWD_NOT_MATCH, getResult(RealmResultErrorCode.OLD_PWD_NOT_MATCH, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.PLATFORM_APP_NOT_DELETE, getResult(RealmResultErrorCode.PLATFORM_APP_NOT_DELETE, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.API_NO_EXIST, getResult(RealmResultErrorCode.API_NO_EXIST, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.APP_HAS_REF, getResult(RealmResultErrorCode.APP_HAS_REF, 500));
        EXCEPTION_DICTIONARY.put(RealmErrorKey.BACKEND_HAS_REF, getResult(RealmResultErrorCode.BACKEND_HAS_REF, 500));

    }

    /**
     * Rest Template 错误处理
     *

     * @return Result 对象
     * @see <a href="https://www.baeldung.com/spring-rest-template-error-handling">baeldung</a>
     */

    public static Result<String> getResult(RealmResultErrorCode resultErrorCodes, int httpStatus) {
        return Result.failure(resultErrorCodes.getMessage(), resultErrorCodes.getCode(), httpStatus, null);
    }
    //ex中的message是RealmErrorKey
    @Override
    public Result<String> resolveException(Exception ex, HttpServletRequest request) {
        String path = request.getRequestURI();
        Exception reason = new Exception();
        if (ex instanceof RealmException realmException) {
            RealmException realmEx = (RealmException) ex;
            String message = realmEx.getMessage();
            Result<String> result = null;
            if (Strings.isNotEmpty(realmEx.getKey())) {
                Result<String> staticResult = EXCEPTION_DICTIONARY.get(realmEx.getKey());
                result = new Result<>();
                result.message(staticResult.getMessage() + (Strings.isEmpty(message) ? "" : message));
                result.path(path);
                result.stackTrace(ex.getStackTrace());
                result.detail(ex.getMessage());
                result.code(staticResult.getCode());
                result.status(staticResult.getStatus());
                return result;
            }
            else {
                result = EXCEPTION_DICTIONARY.get(realmEx.getKey());
                result.message(result.getMessage() + (Strings.isEmpty(message) ? "" : message));
                result.path(path);
                result.stackTrace(ex.getStackTrace());
                result.detail(ex.getMessage());
                return result;
            }

        }  else {
            log.error("[REALM]", ex.getMessage());
        }

        return GlobalExceptionHandler.resolveException(ex, path);
    }


}
