package cn.herodotus.dante.realm.model.user;

import cn.herodotus.dante.realm.model.reg.UserModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GroupUserModel implements Serializable {
    private String groupTag;
    private String groupId;
    private String groupType;

    List<UserModel> users;
}
