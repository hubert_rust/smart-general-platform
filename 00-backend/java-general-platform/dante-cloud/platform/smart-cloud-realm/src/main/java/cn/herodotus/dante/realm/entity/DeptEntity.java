package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/*
 * @param: 
 * @return: 组织结构
 * @Author: admin 
 * @Date:  2021/5/7
 * @Description: 
 **/
@Entity
@Table(name = "sys_dept" )
@Data
@EqualsAndHashCode(callSuper=true)
public class DeptEntity extends CommonTenantEntity implements Tree {
    @Column(name = "dept_name")
    private String deptName;

    @Column(name = "dept_full_name")
    private String deptFullName;
    @Column(name = "inner_code")
    private String innerCode;
    @Column(name = "dept_code")
    private String deptCode;
    private String path;
    @Column(name = "vo_order")
    private Integer voOrder;
    @Column(name = "parent_id")
    private String parentId;
    // 部门类型(集团/学院)
    @Column(name = "dept_type")
    private String deptType;
    // 部门类别（党群机构、分党委、教学单位、教辅单位、行政部门、学术/科研机构、直属附属单位）
    @Column(name = "dept_class")
    private String deptClass;
    // 部门级别（院级/处级/科级）
    @Column(name = "dept_level")
    private String deptLevel;
    @Column(name = "header_name")
    private String headerName;
    @Column(name = "header_uid")
    private String headerUid;
    private Integer status;
    private Long deleted;
    private String tel;
    private String remark;

    @Transient
    private List<DeptEntity> children;


    @Override
    @Transient
    public List getChildrenNode() {
        return this.children;
    }

    @Override
    @Transient
    public String getNodeId() {
        return this.innerCode == null ? null : this.innerCode;
    }

    @Override
    @Transient
    public String getNodeParentId() {
        return this.parentId == null ? null : this.parentId;
    }

    @Override
    @Transient
    public void setChildrenNode(List children) {
        this.setChildren(children);
    }

    @Transient
    private Integer intInnerCode;

    @Override
    @Transient
    public String toString() {
        return "DeptEntity{" +
                "deptName='" + deptName + '\'' +
                ", deptFullName='" + deptFullName + '\'' +
                ", deptCode='" + deptCode + '\'' +
                ", path='" + path + '\'' +
                ", parentCode='" + innerCode + '\'' +
                ", deptType='" + deptType + '\'' +
                ", deptClass='" + deptClass + '\'' +
                ", deptLevel='" + deptLevel + '\'' +
                ", headerName='" + headerName + '\'' +
                ", headerUid=" + headerUid +
                ", tel='" + tel + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
