package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.ResMenuEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.UserPermitModel;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.model.user.LoginUserInfoModel;
import cn.herodotus.dante.realm.model.user.UserQueryModel;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.dante.realm.service.group.UserManagerServiceImpl;
import cn.herodotus.dante.realm.service.group.UserMenuResManageServiceImpl;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Tag(name = "用户管理-基本信息")
@Validated
public class UserController {
    @Resource
    private UserManagerServiceImpl userManagerService;
    @Resource
    private UserMenuResManageServiceImpl userMenuResManageService;
    @Resource
    private UserService userService;

    @Operation(summary = "添加用户", description = "添加用户")
    @PostMapping("/insert")
    public Result<Boolean> insert(@RequestBody UserModel model) {
        Boolean entity = userManagerService.addUserOnCurrentTenant(model);
        return Result.success(Feedback.OK, entity);
    }
    @Operation(summary = "删除用户", description = "删除用户")
    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody List<UserEntity> ids) {
        userManagerService.deleteUserOnCurrentTenant(ids);
        return Result.success(Feedback.OK, true);
    }
    @Operation(summary = "用户详情", description = "用户详情")
    @PostMapping("/detail/{id}")
    public Result<UserEntity> detail(@PathVariable String id) {
        UserEntity ret = userManagerService.userDetail(id);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "编辑用户", description = "编辑用户")
    @PostMapping("/update")
    public Result<UserEntity> update(@RequestBody UserModel model) {
        UserEntity ret = userManagerService.updateUserOnCurrentTenant(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询用户", description = "查询用户")
    @PostMapping("/list")
    public Result<PageResponse> list(@RequestBody UserQueryModel model) {
        PageResponse resp = userManagerService.list(model);
        return Result.success(Feedback.OK, resp);
    }

    @Operation(summary = "激活用户", description = "激活用户")
    @PostMapping("/active")
    public Result<Boolean> active(@RequestBody List<UserEntity> ids) {
        userService.activeAndDeactive(ids, RealmConstant.REG_OPTYPE_ACTIVE);
        return Result.success(Feedback.OK, true);
    }
    @Operation(summary = "禁用用户", description = "禁用用户")
    @PostMapping("/deactive")
    public Result<Boolean> deactive(@RequestBody List<UserEntity> ids) {
        userService.activeAndDeactive(ids, RealmConstant.REG_OPTYPE_DEACTIVE);
        return Result.success(Feedback.OK, true);
    }

    @Operation(summary = "用户应用列表", description = "用户应用列表")
    @PostMapping("/apps")
    public Result<List<ApplicationEntity>> apps(@RequestBody UserPermitModel model) {
        List<ApplicationEntity> ret = userMenuResManageService.userApps(model);
        return Result.success(Feedback.OK, ret);
    }

   /* @Operation(summary = "用户所属部门", description = "用户所属部门")
    @PostMapping("/userDept/{uid}")
    public Result<List<UserDeptModel>> userDept(@PathVariable String uid) {
        List<UserDeptModel> ret = userManagerService.userOfDept(uid);
        return Result.success(Feedback.OK, ret);
    }*/
   @Operation(summary = "获取用户信息", description = "获取用户信息")
   @PostMapping("/userInfo")
   public Result<LoginUserInfoModel> loginUserInfo() {
       LoginUserInfoModel model = userManagerService.userInfoAfterLoginOfAdmin(
               CommonSecurityContextHolder.getAgent(),
               CommonSecurityContextHolder.getUid(),
               CommonSecurityContextHolder.getCid()
       );
       return Result.success(Feedback.OK, model);
   }
    @Operation(summary = "获取用户菜单", description = "获取用户菜单")
    @PostMapping("/userMenu")
    public Result<List<ResMenuEntity>> loginUserMenu() {
        List<ResMenuEntity> model = userManagerService.getUserMenuList();
        return Result.success(Feedback.OK, model);
    }
}
