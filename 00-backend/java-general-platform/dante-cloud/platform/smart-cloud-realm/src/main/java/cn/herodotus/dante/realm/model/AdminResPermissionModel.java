package cn.herodotus.dante.realm.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/9/19 8:42
 */
@Data
public class AdminResPermissionModel implements Serializable {
    private String menuName;
    private String clientId;
    private String menuId;
    private String uid;
    private String digest;
}
