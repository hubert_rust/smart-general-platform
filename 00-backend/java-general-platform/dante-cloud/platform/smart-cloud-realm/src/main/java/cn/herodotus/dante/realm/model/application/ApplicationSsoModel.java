package cn.herodotus.dante.realm.model.application;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/9/20 16:29
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class ApplicationSsoModel extends ApplicationBaseModel implements Serializable {

}
