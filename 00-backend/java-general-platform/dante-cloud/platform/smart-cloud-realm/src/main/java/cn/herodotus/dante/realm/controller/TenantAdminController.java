package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.realm.entity.TenantAdminEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.AdminResPermissionModel;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.user.UserQueryModel;
import cn.herodotus.dante.realm.service.TenantAdminService;
import cn.herodotus.dante.realm.service.permission.TenantAdminPermissionGroupService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin")
@Tag(name = "用户空间管理员")
@Validated
public class TenantAdminController {
    @Resource
    private TenantAdminService tenantAdminService;
    @Resource
    private TenantAdminPermissionGroupService tenantAdminPermissionGroupService;

    @Operation(summary = "管理员列表", description = "管理员列表")
    @PostMapping("/listAdmin")
    public Result<List<TenantAdminEntity>> listAdmin(@RequestBody UserQueryModel model) {
        List<TenantAdminEntity> ret = tenantAdminService.listAdmin(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "添加管理员", description = "添加管理员")
    @PostMapping("/addAdmin")
    public Result<Boolean> addAdmin(@RequestBody List<UserEntity> list) {
        boolean ret = tenantAdminService.addAdmin(list);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询外部管理员", description = "查询外部管理员")
    @PostMapping("/queryExternalAdmin")
    public Result<List<UserEntity>> queryExternalAdmin(@RequestBody BaseQueryModel model) {
        List<UserEntity> ret = tenantAdminService.queryExternalUser(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "添加外部管理员", description = "添加外部管理员")
    @PostMapping("/addExternalAdmin")
    public Result<Boolean> addExternalAdmin(@RequestBody List<UserEntity> list) {
        boolean ret = tenantAdminService.addExternalAdmin(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除管理员", description = "删除管理员")
    @PostMapping("/removeAdmin")
    public Result<Boolean> removeAdmin(@RequestBody List<TenantAdminEntity> list) {
        Boolean ret = tenantAdminService.removeAdmin(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "设置内部管理员权限", description = "设置内部管理员权限")
    @PostMapping("/setInnerAdminPermit")
    public Result<Boolean> setInnerAdminPermit(@RequestBody List<AdminResPermissionModel> list) {
        Boolean ret = tenantAdminPermissionGroupService.addAdminRes(list, true);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "设置外部管理员权限", description = "设置外部管理员权限")
    @PostMapping("/setExternalAdminPermit")
    public Result<Boolean> setExternalAdminPermit(@RequestBody List<AdminResPermissionModel> list) {
        Boolean ret = tenantAdminPermissionGroupService.addAdminRes(list, false);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询管理员权限", description = "查询管理员权限")
    @PostMapping("/queryAdminMenu")
    public Result<List<Tree>> queryAdminMenu(@RequestBody AdminResPermissionModel model) {
        List<Tree> ret = tenantAdminPermissionGroupService.queryAdminMenuRes(model);
        return Result.success(Feedback.OK, ret);
    }
}
