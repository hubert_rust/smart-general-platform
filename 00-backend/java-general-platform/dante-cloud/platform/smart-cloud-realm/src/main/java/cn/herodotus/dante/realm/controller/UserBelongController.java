package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.GroupEntity;
import cn.herodotus.dante.realm.entity.RoleEntity;
import cn.herodotus.dante.realm.model.UserCommonModel;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.dante.realm.service.group.RoleManagerServiceImpl;
import cn.herodotus.dante.realm.service.group.RoleMenuResManagerServiceImpl;
import cn.herodotus.dante.realm.service.group.UserManagerServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Tag(name = "用户管理-用户归属")
@Validated
public class UserBelongController {
    @Resource
    private UserManagerServiceImpl userManagerService;
    @Resource
    private UserService userService;
    @Resource
    private RoleManagerServiceImpl roleManagerService;
    @Resource
    private RoleMenuResManagerServiceImpl roleMenuResManagerService;

    @Operation(summary = "更新用户所属组", description = "更新用户所属组")
    @PostMapping("/updateStaticGroup")
    public Result<Boolean> updateStaticGroup(@RequestBody UserCommonModel model) {
        Boolean ret = userManagerService.updateUserOfStaticGroup(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "更新用户所角色", description = "更新用户所角色")
    @PostMapping("/updateBelongRole")
    public Result<Boolean> updateBelongRole(@RequestBody UserCommonModel model) {
        Boolean ret = userManagerService.updateUserBelongRole(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "移除用户所属组", description = "移除用户所属组")
    @PostMapping("/removeBelongGroup")
    public Result<Boolean> removeGroup(@RequestBody UserCommonModel model) {
        Boolean ret = userManagerService.removeBelongGroup(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "用户所属组列表", description = "用户所属组列表")
    @PostMapping("/belongGroup/{uid}")
    public Result<List<GroupEntity>> belongGroup(@PathVariable String uid) {
        List<GroupEntity> ret = userManagerService.userOfGroup(uid);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "用户组列表", description = "用户组列表")
    @PostMapping("/groups/{uid}")
    public Result<List<GroupEntity>> groups(@PathVariable String uid) {
        List<GroupEntity> ret = userManagerService.groups(uid);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "角色列表", description = "角色列表")
    @PostMapping("/roles/{uid}")
    public Result<List<RoleEntity>> roles(@PathVariable String uid) {
        List<RoleEntity> ret = userManagerService.roles(uid);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "用户拥有角色", description = "用户拥有角色")
    @PostMapping("/belongRole/{uid}")
    public Result<List<RoleEntity>> belongRole(@PathVariable String uid) {
        List<RoleEntity> ret = roleMenuResManagerService.userOfRoleList(uid, false);
        return Result.success(Feedback.OK, ret);
    }

    // 只能移除角色主体是用户的
    @Operation(summary = "移除用户所属角色主体", description = "移除用户所属角色主体")
    @PostMapping("/removeRoleSubject")
    public Result<Boolean> removeRoleSubject(@RequestBody UserCommonModel model) {
        Boolean ret = roleManagerService.removeRoleSubject(model);
        return Result.success(Feedback.OK, ret);
    }
}
