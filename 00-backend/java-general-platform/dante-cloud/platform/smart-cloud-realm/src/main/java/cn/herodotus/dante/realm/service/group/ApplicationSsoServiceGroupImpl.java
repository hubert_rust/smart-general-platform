package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.entity.*;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.application.ApplicationSsoModel;
import cn.herodotus.dante.realm.repository.ApplicationTenantSsoRepository;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import jakarta.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ApplicationSsoServiceGroupImpl {

    @Resource
    private ApplicationService applicationService;
    @Resource
    private UserService userService;
    @Resource
    private ResApiService resApiService;

    @Resource
    private RoleResourceService roleResourceService;

    @Resource
    private ApplicationSsoService applicationSsoService;
    @Resource
    private ApplicationTenantSsoService applicationTenantSsoService;

    public ApplicationSsoModel ssoDetail(String client) {
        String tid = CommonSecurityContextHolder.getTid();
        ApplicationTenantSsoEntity entity
                = ((ApplicationTenantSsoRepository)applicationTenantSsoService.getRepository())
                .findByTidAndClientId(tid, client);
        ApplicationEntity application = applicationService.findById(client);

        ApplicationSsoModel model = new ApplicationSsoModel();
        BeanUtils.copyProperties(application, model);
        model.setAllShow(entity.getAllShow());
        return model;
    }

    // 当前用户空间或者租户拥有的应用列表
    public List<ApplicationSsoModel> listSsoApplication() {
        String tid = CommonSecurityContextHolder.getTid();
        List<ApplicationTenantSsoEntity> list = applicationTenantSsoService.findByTid(tid);
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        Map<String, Boolean> map = Maps.newHashMap();
        list.stream().forEach(v-> map.put(v.getClientId(), v.getAllShow()));
        List<ApplicationEntity> apps = applicationService.findByClientIdIn(
                        list.stream().map(m->m.getClientId()).toList()
                );
        List<ApplicationSsoModel> ret = Lists.newArrayList();
        apps.stream().forEach(v-> {
            ApplicationSsoModel model = new ApplicationSsoModel();
            BeanUtils.copyProperties(v, model);
            Boolean allShow = map.get(v.getClientId());
            // 设置allShow
            model.setAllShow(allShow);
            ret.add(model);
        });
        return ret;
    }

    // 应用市场
    public List<ApplicationEntity> listSsoMarketplace() {
        List<ApplicationSsoEntity> list = applicationSsoService.findAll();
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        return applicationService.findByClientIdIn(
                list.stream().map(m->m.getClientId()).toList()
        );
    }

    /*
     * @Author root
     * @Description  将应用市场中应用添加到应用列表
     * @Date 9:55 2023/8/24
     * @Param
     * @return
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean addSsoMarketApp2AppList(String clientId) {
        ApplicationTenantSsoEntity entity = new ApplicationTenantSsoEntity();
        entity.setClientId(clientId);
        entity.setTid(CommonSecurityContextHolder.getTid());
        applicationTenantSsoService.insert(entity);
        return true;
    }

    // 从sso应用列表中删除应用, id是应用的clientId
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean deleteAppFromSso(String id) {
        // 如果角色资源(role_res)中已经配置，无法删除
        String tid = CommonSecurityContextHolder.getTid();
        Specification<RoleResourceEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("tid"), tid));
            predicates.add(cb.equal(root.get("clientId"), id));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
            return query.getRestriction();
        };
        boolean exist = roleResourceService.getRepository().exists(spec);
        if (exist) {
            throw new RealmException("realmException", RealmErrorKey.APP_HAS_REF, "角色资源");
        }

        applicationTenantSsoService.deleteByTidAndAndClientId(CommonSecurityContextHolder.getTid(), id);
        return true;
    }

    /*
     * @Author root
     * @Description SSO应用中，菜单对应的API信息
     * @Date 14:08 2023/8/24
     * @Param
     * @return
     **/
    public ResApiEntity menuApiDetail(String clientId, String digest) {
        return resApiService.findByDigestInAndClientId(
                new ArrayList<String>(){{add(digest);}}, clientId
        );
    }

}
