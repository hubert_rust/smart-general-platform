package cn.herodotus.dante.realm.constant;

public interface RealmConstant {

    String STATUS_ACTIVE = "active";
    String STATUS_DEACTIVE = "deactive";
    String MESSAGE_TYPE_LOG = "LOG";
    String LOG_TYPE_LOGIN = "login";         //登录日志
    String LOG_TYPE_LOGOUT = "logout";       //登出日志
    String LOG_TYPE_OPERATION = "operation"; //操作日志
    String SUCCESS = "success";
    String FAIL = "fail";
    String MESSAGE_TOPIC_REDIS = "topic.redis";

    // 应用属性
    String APP_ATTR_SSO  = "sso";   //sso应用
    String APP_ATTR_SELF = "self";  //自建应用

    // application type
    // 管理端
    String APP_TYPE_MGT = "mgt";
    // 用户端
    String APP_TYPE_USER = "user";
    String APP_TYPE_APP = "app";

    // 应用扩展属性
    //String APP_EXT_

    // account attr
    String ACCOUNT_ATTR_ROOT = "root";  // 只有一个账号属于root
    String ACCOUNT_ATTR_ROOT_ADMIN = "root_admin";
    String ACCOUNT_ATTR_MASTER = "master";
    String ACCOUNT_ATTR_MASTER_ADMIN = "master_admin";

    String PATH_SPLIT_SYMBOL = "-";
    String STR_EMPTY = "";
    String STR_BLANK = " ";
    String authentication_mode = "authentication_mode";

    // 租户类型
    String TID_TYPE_REALM = "space";
    String TID_TYPE_TENANT = "tenant";

    int DEPT_INNER_CODE_INIT = 1;


    String UID_VALUE_DEFAULT = "0";
    String TID_VALUE_DEFAULT = "0";
    String DELETED_VALUE_DEFAULT = "0";

    String PARENT_ID_DEFAULT = "-1";


    //注册用户
    String REG_OPTYPE_RECYCLE = "recycle";
    String REG_OPTYPE_ACTIVE =  "active";
    String REG_OPTYPE_DEACTIVE =  "deactive";
    String REG_OPOBJECT_USER = "user";
    String REG_OPOBJECT_TENANT = "tenant";


    String DIR_ASC = "asc";
    String DIR_DESC = "desc";

    //接口控制类型
    String API_PROCESS_LOGIN = "login";
    String API_PROCESS_ANONYMOUS = "anonymous";
    String API_PROCESS_CONTROL = "control";

    //接口
    String OPERATION_TYPE_CATALOG = "catalog";
    String OPERATION_TYPE_API = "api";

    String APP_TYPE_BACKEND = "backend";
    String APP_TYPE_MICRO = "micro";
    String APP_TYPE_SINGLE = "single";
    String APP_TYPE_STANDARD = "standard";
    String APP_TYPE_CLIENT = "client";


    String ALLOW = "allow";
    String REJECT = "reject";
    String SUBJECT_USER = "user";
    String SUBJECT_GROUP = "group";
    String SUBJECT_ORG = "org";

    String GROUP_TYPE_STATIC = "static";
    String GROUP_TYPE_VARY = "vary";

    String USER_STATUS_ACTIVE = "active";
    String USER_STATUS_DEACTIVE = "deactive";


    // agent: wxmicro, pc, mobile,
    // 微信小程序
    String AGENT_WXMICRO = "wxmicro";
    String AGENT_PC = "pc";
    String AGENT_MOBILE = "mobile";


    // 菜单
    String MENU_RES_TYPE_CATALOG = "M";
    String MENU_RES_TYPE_MENU = "C";
    String MENU_RES_TYPE_PAGE = "P";
    String MENU_RES_TYPE_FUN = "F";


    // 角色资源对象类型
    String ROLE_RES_OBJECT_TYPE_MENU = "menu";
    String ROLE_RES_OBJECT_TYPE_API = "api";

    // 应用分类
    String APP_PLATFORM_SSO = "platform_sso"; // 平台发布的应用（root以及平台管理员发布到sso）
    String APP_SPACE_SSO = "space_sso";       // master创建后发布的应用（master及管理员发布到sso）
    String APP_SELF = "self";         // 自建应用

    // 应用授权主体中用到
    String APP_FROM_SELF = "self";
    String APP_FROM_SSO = "sso";
    // tid中所有可见
    String APP_FROM_SSO_SHOW = "all_sso";
    // 自建应用中所有可见
    String APP_FROM_SELF_SHOW = "all_self";

}
