package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/*
 * @Author root
 * @Description 角色主体
 * @Date 15:35 2023/8/24
 * @Param 
 * @return 
 **/
@Entity
@Table(name = "sys_role_subject" )
@Data
@EqualsAndHashCode(callSuper=true)
public class RoleSubjectEntity extends CommonTenantEntity {

    @Column(name = "role_id", length = 64)
    private String roleId;
    @Column(name = "subject_name", length = 32)
    private String subjectName;
    //授权主体类型
    @Column(name = "subject_type", length = 64)
    private String subjectType;
    @Column(name = "identity", length = 64)
    private String identity;

    private Date expire;

    @Transient
    private boolean selected = true;
}
