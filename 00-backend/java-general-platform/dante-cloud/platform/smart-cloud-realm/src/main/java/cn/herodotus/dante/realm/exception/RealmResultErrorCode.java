package cn.herodotus.dante.realm.exception;


import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "响应结果状态", description = "自定义错误码以及对应的、友好的错误信息")
public enum RealmResultErrorCode {
    REG_USER_EXIST(RealmErroCode.REG_USER_EXIST, "注册用户已经存在"),
    REG_USER_NOEXIST(RealmErroCode.REG_USER_NOEXIST, "注册用户不存在"),
    PARAM_INVALID(RealmErroCode.PARAM_INVALID, "参数非法: "),
    TYPE_NOT_MATCH(RealmErroCode.TYPE_NOT_MATCH, "类型不匹配"),
    PARENT_NOT_EXIST(RealmErroCode.PARENT_NOT_EXIST, "父节点不存在"),
    API_HAS_EXIST(RealmErroCode.API_HAS_ESIT, "应用中接口已经存在"),
    DUP_FIELD(RealmErroCode.DUP_FIELD, "重复字段: "),
    NOT_ADMIN(RealmErroCode.NOT_ADMIN, "不是管理员，没有权限"),
    APP_NOT_ALLOW_TO_SSO(RealmErroCode.APP_NOT_ALLOW_TO_SSO, "该应用不允许添加到SSO"),
    OTHER_USER_GROUP_USED(RealmErroCode.OTHER_USER_GROUP_USED,
            "该用户组被引用:"),
    ROLE_HAS_REF(RealmErroCode.ROLE_HAS_REF, "该角色被引用:"),
    REMOVE_ROLE_SUBJECT_IS_USER(RealmErroCode.REMOVE_ROLE_SUBJECT_IS_USER, "只能移除角色主体是用户的记录"),
    USER_REALM_REMOVE_BAN(RealmErroCode.REMOVE_ROLE_SUBJECT_IS_USER, "禁止删除该用户空间"),
    OLD_PWD_NOT_MATCH(RealmErroCode.OLD_PWD_NOT_MATCH, "原始密码不匹配"),
    PLATFORM_APP_NOT_DELETE(RealmErroCode.PLATFORM_APP_NOT_DELETE, "平台内置应用不允许删除"),
    API_NO_EXIST(RealmErroCode.API_NO_EXIST, "接口不存在"),
    APP_HAS_REF(RealmErroCode.APP_HAS_REF, "应用被引用: "),
    BACKEND_HAS_REF(RealmErroCode.BACKEND_HAS_REF, "后台被引用: "),
    REQUEST_METHOD_NOT_UPDATE(RealmErroCode.REQUEST_METHOD_NOT_UPDATE, "类型不能修改");


    @Schema(title = "结果代码")
    private final int code;
    @Schema(title = "结果信息")
    private final String message;


    RealmResultErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
