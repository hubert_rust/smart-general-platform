package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


@Entity
@Table(name = "sys_account_exception")
@Data
@EqualsAndHashCode(callSuper=true)
public class AccountExceptionEntity extends CommonTenantEntity {
    private String uid;

    @Column(name = "exception_time", length = 64)
    private Date exceptionTime;

    @Column(name = "exception_num", length = 64)
    private Integer exceptionNum;

    @Column(name = "release_time", length = 64)
    private Date releaseTime;

    private String reason;
}
