package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.PostEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.service.PostService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/post")
@Tag(name = "岗位管理")
public class PostController {

    @Resource
    private PostService postService;

    @Operation(summary = "添加岗位", description = "添加岗位")
    @PostMapping("/insert")
    public Result<PostEntity> insert(@Valid @RequestBody PostEntity entity) {
        PostEntity ret = postService.insert(entity);
        return Result.success(Feedback.OK, ret);
    }


    @Operation(summary = "删除岗位", description = "删除岗位")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        postService.deleteById(id);
        return Result.success(Feedback.OK, true);
    }

    @Operation(summary = "更新岗位基本信息", description = "更新岗位基本信息")
    @PostMapping("/update")
    public Result<Boolean> update(@Valid @RequestBody PostEntity entity) {
        Boolean ret = postService.update(entity);
        return Result.success(Feedback.OK, ret);
    }
   /* @Operation(summary = "关联部门", description = "关联部门")
    @PostMapping("/linkDept")
    public Result<Boolean> linkDept(@Valid @RequestBody PostLinkDeptModel model) {
        Boolean ret = postService.linkDept(model);
        return Result.success(Feedback.OK, ret);
    }*/

    @Operation(summary = "查询岗位", description = "查询岗位")
    @PostMapping("/list")
    public Result<PageResponse> list(@RequestBody BaseQueryModel model) {
        PageResponse ret = postService.list(model);
        return Result.success(Feedback.OK, ret);
    }
}
