/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.AdminAddModel;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.user.UserAllWorkspaceModel;
import cn.herodotus.dante.realm.model.user.UserWorkspaceModel;
import cn.herodotus.dante.realm.service.TenantService;
import cn.herodotus.dante.realm.service.group.UserWorkspaceService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>Description: SysDepartmentController </p>
 *
 * @author : gengwei.zheng
 * @date : 2021/9/23 23:13
 */
@RestController
@RequestMapping("/wks")
@Tag(name = "用户空间管理")
public class UserWorkspaceController {

    @Resource
    private UserWorkspaceService workspaceService;
    @Resource
    private TenantService tenantService;

   @Operation(summary = "用户空间详情", description = "用户空间详情")
   @PostMapping("/detail/{id}")
   public Result<TenantEntity> detail(@PathVariable String id) {
       TenantEntity ret = tenantService.findById(id);
       return Result.success(Feedback.OK, ret);
   }
    @Operation(summary = "创建用户空间", description = "创建用户空间")
    @PostMapping("/insert")
    public Result<UserAllWorkspaceModel> createSpace(@Valid  @RequestBody UserWorkspaceModel model) {
        UserAllWorkspaceModel ret = workspaceService.createNormalTenant(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "更新用户空间", description = "更新用户空间")
    @PostMapping("/update")
    public Result<TenantEntity> updateSpace(@Valid  @RequestBody UserWorkspaceModel model) {
        TenantEntity ret = workspaceService.updateNormalTenant(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除用户空间", description = "删除用户空间")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        Boolean ret = workspaceService.deleteNormalTenant(id);
        return Result.success(Feedback.OK, ret);
    }
    /*@Operation(summary = "添加用户", description = "添加用户")
    @PostMapping("addUser")
    public Result<UserEntity> addUser(@Valid  @RequestBody AddUserModel addUserModel) {
        addUserModel.setType("space");
        UserEntity ret = tenantService.addUser(addUserModel);
        return Result.success(Feedback.OK, ret);
    }*/

    // 用户空间
    @Operation(summary = "用户空间管理员列表", description = "用户空间管理员列表")
    @PostMapping("/adminList")
    public Result<List<UserEntity>> adminList(@RequestBody BaseQueryModel model) {
        // 一类是自己创建的
        List<UserEntity> ret = workspaceService.workspaceAdmins();
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "用户空间添加管理员", description = "用户空间添加管理员")
    @PostMapping("/addSpaceAdmin")
    public Result<Boolean> spaceAdminAdd(@RequestBody AdminAddModel adminAddModel) {
        Boolean ret = workspaceService.spaceAdminAdd(adminAddModel);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "切换用户空间", description = "切换用户空间")
    @PostMapping("switch/{id}")
    public Result<Boolean> switchWorkspace(@PathVariable String id) {
        Boolean ret = workspaceService.switchWorkspace(id);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "登录后用户空间", description = "登录后用户空间")
    @PostMapping("/listRealm")
    public Result<UserAllWorkspaceModel> list(@RequestBody BaseQueryModel queryModel) {
        UserAllWorkspaceModel ret = workspaceService.listWorkspaceAfterLogin();
        return Result.success(Feedback.OK, ret);
    }
}
