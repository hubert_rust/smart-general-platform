/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.GroupUserEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.repository.GroupUserRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupUserService extends BaseService<GroupUserEntity, String> {

    private GroupUserRepository groupUserRepository;

    @Autowired
    public GroupUserService(GroupUserRepository groupUserRepository) {
        this.groupUserRepository = groupUserRepository;
    }

    @Override
    public BaseRepository<GroupUserEntity, String> getRepository() {
        return this.groupUserRepository;
    }



    public PageResponse<GroupUserEntity> list(BaseQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);
        //smart-auth: pending
        String tid = "0";
        Specification<GroupUserEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("groupId"), model.getId()));
            predicates.add(cb.equal(root.get("tid"), tid));
            if (Strings.isNotEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("account"), model.getKey()));
            }
            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<GroupUserEntity> resp = new PageResponse<>();
        resp.setTotalCount((int) num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }

    public boolean deleteByTidAndGroupIdAndUidIn(String groupId, List<String> uids) {
        //smart-auth
        String tid = RealmConstant.TID_VALUE_DEFAULT;
        groupUserRepository.deleteByTidAndGroupIdAndUidIn(tid, groupId, uids);
        return true;
    }
    public int countByTidAndGroupId(String tid, String groupId) {
        return groupUserRepository.countByTidAndGroupId(tid, groupId);
    }

    public List<GroupUserEntity> findByTidAndUid(String tid, String uid) {
        return groupUserRepository.findByTidAndUid(tid, uid);
    }
}
