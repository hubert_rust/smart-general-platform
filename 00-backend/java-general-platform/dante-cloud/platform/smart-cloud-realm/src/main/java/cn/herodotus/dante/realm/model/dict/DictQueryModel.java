package cn.herodotus.dante.realm.model.dict;

import cn.herodotus.dante.realm.model.BaseQueryModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class DictQueryModel extends BaseQueryModel {
    private String key;
    private String value;
}
