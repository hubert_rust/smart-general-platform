package cn.herodotus.dante.realm.exception;

public interface RealmErrorKey {
    String REG_USER_EXIST = "regUserExist";
    String REG_USER_NOEXIST = "regUserNoExit";

    String TYPE_NOT_MATCH = "typeNotMatch";

    String PARAM_INVALID = "paramInvalid";
    String PARENT_NOT_EXIST = "panentNotExit";
    String API_HAS_EXIST = "apiHasExist";
    String REQUEST_METHOD_NOT_UPDATE = "requestMethodNotUpdate";

    String DUP_FIELD = "dupField";

    String NOT_ADMIN = "notAdmin";

    String APP_NOT_ALLOW_TO_SSO = "appNotAllowToSso";
    String OTHER_USER_GROUP_USED = "otherUserGroupUsed";
    String ROLE_HAS_REF = "roleHasRef";
    String REMOVE_ROLE_SUBJECT_IS_USER = "removeRoleSubjectIsUser";
    String USER_REALM_REMOVE_BAN = "userRealmRemoveBan";
    String OLD_PWD_NOT_MATCH = "oldPwdNotMatch";
    String PLATFORM_APP_NOT_DELETE = "platformAppNotDelete";
    String API_NO_EXIST = "apiNoExist";
    String APP_HAS_REF = "appHasRef";
    String BACKEND_HAS_REF = "backendHasRef";
}
