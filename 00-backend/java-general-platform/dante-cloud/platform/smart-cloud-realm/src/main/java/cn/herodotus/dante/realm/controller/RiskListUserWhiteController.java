package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.constant.RiskListConstant;
import cn.herodotus.dante.realm.entity.RiskListEnity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.service.RiskListService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/risk")
@Tag(name = "风险名单-用户白名单")
public class RiskListUserWhiteController {
    @Resource
    private RiskListService riskListService;

    @Operation(summary = "添加用户白名单", description = "添加用户白名单")
    @PostMapping("/insertUserWhite")
    public Result<Boolean> insert(@Valid @RequestBody List<RiskListEnity> list) {
        Boolean ret = riskListService.addUserWhite(list);
        return Result.success(Feedback.OK, ret);
    }


    @Operation(summary = "删除用户白名单", description = "删除用户白名单")
    @PostMapping("/deleteUserWhite/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        Boolean ret = riskListService.delete(id,
                RiskListConstant.LIST_CLASS_WHITE,
                RiskListConstant.SUBJECT_TYPE_USER);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "用户白名单列表", description = "用户白名单列表")
    @PostMapping("/userWhiteList")
    public Result<PageResponse> userWhiteList(@RequestBody BaseQueryModel model) {
        PageResponse response = riskListService.userWhiteList(model);
        return Result.success(Feedback.OK, response);
    }

}
