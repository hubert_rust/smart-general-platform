/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.CommonSecurityEntity;
import cn.herodotus.dante.realm.service.CommonSecurityService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/commonSecurity")
@Tag(name = "通用安全")
@Validated
public class CommonSecuirtyController {

    @Resource
    private CommonSecurityService commonSecurityService;


    @Operation(summary = "安全详情", description = "安全详情")
    @PostMapping("/detail")
    public Result<CommonSecurityEntity> securityDetail() {
        CommonSecurityEntity ret = commonSecurityService.commonSecurityDetail();
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "设置基础安全", description = "设置基础安全")
    @PostMapping("/baseSet")
    public Result<CommonSecurityEntity> baseSet(@Valid @RequestBody CommonSecurityEntity entity) {
        CommonSecurityEntity ret = commonSecurityService.setBaseSecurity(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "设置登录安全", description = "设置登录安全")
    @PostMapping("/loginSet")
    public Result<CommonSecurityEntity> loginSet(@RequestBody CommonSecurityEntity entity) {
        CommonSecurityEntity ret = commonSecurityService.setLoginSecurity(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "APP 验证安全", description = "APP 验证安全")
    @PostMapping("/appValidSet")
    public Result<CommonSecurityEntity> appValidSet(@RequestBody CommonSecurityEntity entity) {
        CommonSecurityEntity ret = commonSecurityService.setAppValidSecurity(entity);
        return Result.success(Feedback.OK, ret);
    }

}
