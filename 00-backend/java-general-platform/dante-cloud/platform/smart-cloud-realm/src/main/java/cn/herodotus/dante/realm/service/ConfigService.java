/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.entity.ConfigEntity;
import cn.herodotus.dante.realm.model.config.EnvQueryModel;
import cn.herodotus.dante.realm.repository.ConfigRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import jakarta.persistence.criteria.Predicate;
import lombok.val;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Description:  </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class ConfigService extends BaseService<ConfigEntity, String> {

    private ConfigRepository configRepository;

    @Autowired
    public ConfigService(ConfigRepository configRepository) {
        this.configRepository = configRepository;
    }

    @Override
    public BaseRepository<ConfigEntity, String> getRepository() {
        return this.configRepository;
    }

    public ConfigEntity getDefaultAuthMode() {
        val configEntity = new ConfigEntity();
        //configEntity.setKey(RealmConstant.authentication_mode);
        //configEntity.setValue(AuthorizationGrantType.AUTHORIZATION_CODE.getValue());
        configEntity.setStatus(1);
        return configEntity;
    }

    // 平台认证方式
    public ConfigEntity authorizationGrantType() {
        Specification<ConfigEntity> specification = (root, criteriaQuery, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            //predicates.add(cb.equal(root.get("key"), cn.herodotus.dante.module.realm.constant.RealmConstant.authentication_mode));
            predicates.add(cb.equal(root.get("status"), 1));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            criteriaQuery.where(cb.and(predicates.toArray(predicateArray)));
            return criteriaQuery.getRestriction();
        };
        List<ConfigEntity> list = super.findAll(specification);

        return CollectionUtils.isNotEmpty(list) ? list.get(0) : getDefaultAuthMode();
    }

    public PageResponse<ConfigEntity> list(EnvQueryModel model) {
        Pageable pageable = QueryUtil.pageable(model);

        Specification<ConfigEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("key"), model.getKey()));
            }
            if (Strings.isNotEmpty(model.getValue())) {
                predicates.add(cb.equal(root.get("value"), model.getValue()));
            }
            predicates.add(cb.equal(root.get("tid"), super.getTid()));
            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<ConfigEntity> resp = new PageResponse<>();
        resp.setTotalCount((int)num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }
}
