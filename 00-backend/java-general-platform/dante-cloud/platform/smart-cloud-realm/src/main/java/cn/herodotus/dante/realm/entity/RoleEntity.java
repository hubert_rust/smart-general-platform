package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Entity
@Table(name = "sys_role" )
@Data
@EqualsAndHashCode(callSuper=true)
public class RoleEntity extends CommonTenantEntity {

    @Column(name = "role_name", length = 32)
    private String roleName;
    @Column(name = "role_code", length = 64)
    private String roleCode;
    @Column(name = "parent_id", length = 64)
    private String parentId;
    @Column(name = "remark", length = 64)
    private String remark;
    //角色属性: json, key: value
    @Column(name = "ext1", length = 32)
    private String ext1;
    private String ext2;
    private String ext3;

    private String status;

    private Date expire;

    // 授权主体，本角色是否在user，group，org授权主体中
    @Transient
    private String roleSubject = "";
    @Transient
    private String subjectName = ""; // 角色主体名称
    @Transient
    private String identity = "";

    @Transient
    private boolean red = false;

}
