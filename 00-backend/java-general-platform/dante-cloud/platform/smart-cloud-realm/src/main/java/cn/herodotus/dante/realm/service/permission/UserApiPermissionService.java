package cn.herodotus.dante.realm.service.permission;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.constant.RiskListConstant;
import cn.herodotus.dante.realm.entity.*;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.repository.ResMenuRepository;
import cn.herodotus.dante.realm.repository.RiskListRepository;
import cn.herodotus.dante.realm.repository.RoleResourceRepository;
import cn.herodotus.dante.realm.repository.RoleSubjectRepository;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author root
 * @description TODO
 * @date 2023/9/17 21:16
 */
@Slf4j
@Service
public class UserApiPermissionService {

    @Resource
    private RiskListService riskListService;
    @Resource
    private RoleSubjectService roleSubjectService;
    @Resource
    private GroupUserService groupUserService;
    @Resource
    private RoleResourceService roleResourceService;
    @Resource
    private ResMenuService resMenuService;
    @Resource
    private ResApiService resApiService;
    @Resource
    private UserService userService;
    @Resource
    private DeptService deptService;
    @Resource
    private GroupService groupService;



    /*
     * @Author root
     * @Description 用户获取角色ID列表
     * @Date 21:39 2023/9/17
     * @Param
     * @return
     **/
    public List<String> userRoleAccess(String tid, String uid) {
        List<RoleSubjectEntity> users = getRoleSubjectEntities(tid, uid);

        if (CollectionUtils.isEmpty(users)) {
            return Lists.newArrayList();
        }
        return users.stream().map(m->m.getRoleId()).toList();
    }

    public List<RoleSubjectEntity> getRoleSubjectEntities(String tid, String uid) {
        UserEntity entity = userService.findById(uid);

        List<String> identitys = Lists.newArrayList();
        Map<String, String> identityMap = Maps.newHashMap();
        identitys.add(uid);
        identityMap.put(uid, entity.getAccount());

        // 部门主体
        String dept = CommonSecurityContextHolder.getUser().getMainDeptId();
        if (Strings.isNotEmpty(dept)){
            DeptEntity deptEntity = deptService.findById(dept);
            identitys.add(dept);
            identityMap.put(dept, deptEntity.getDeptName());
        }
        // 用户组主体
        List<GroupUserEntity> groups = groupUserService.findByTidAndUid(tid, uid);
        if (CollectionUtils.isNotEmpty(groups)) {
            List<String> groupIds = Lists.newArrayList();
            groups.stream().forEach(v-> { groupIds.add(v.getGroupId()); });

            identitys.addAll(groupIds);
            List<GroupEntity> findGroup = groupService.listByIds(groupIds);
            findGroup.stream().forEach(v->identityMap.put(v.getId(), v.getGroupName()));
        }

        List<RoleSubjectEntity> ret = ((RoleSubjectRepository)roleSubjectService.getRepository())
                .findByTidAndIdentityIn(tid, identitys);
        // 更新授权主体名称，表中保存的不同步
        for (RoleSubjectEntity subjectEntity : ret) {
            String name = identityMap.get(subjectEntity.getIdentity());
            subjectEntity.setSubjectName(name);
        }

        return ret;
    }
    /*
     * @Author root
     * @Description 登录用户菜单获取
     * @Date 21:21 2023/9/17
     * @Param
     * @return
     **/

    public List<ResMenuEntity> userMenuAccess(String tid, String clientId, String uid, boolean tree) {
        // 通过角色主体：用户，用户组，组织获取用户角色
        List<String> roles = userRoleAccess(tid, uid);

        List<RoleResourceEntity> roleRes = ((RoleResourceRepository)roleResourceService.getRepository())
                .findByTidAndClientIdAndObjectTypeAndRoleIdIn(
                        tid, clientId, RealmConstant.ROLE_RES_OBJECT_TYPE_MENU, roles);
        Set<String> sets
                = roleRes.stream().map(m->m.getIdentity()).collect(Collectors.toSet());
        List<ResMenuEntity> menus
                = ((ResMenuRepository)resMenuService.getRepository()).findAllById(sets);
        if (tree) {
            DefaultTreeBuildFactory.treeBuild(new ArrayList<>(menus), "-1", true);
        }
        return menus;
    }

    /*
     * @Author root
     * @Description 用户接口访问权限判断
     * @Date 21:18 2023/9/17
     * @Param
     * @return
     **/
    public boolean userApiAccess(String client,
                                 String tid,
                                 String uid,
                                 String digest) {
        // 用户白名单
        boolean existWhiteUser = ((RiskListRepository)riskListService.getRepository())
                .existsByTidAndListClassAndSubjectTypeAndSubjectValue(
                        tid,
                        RiskListConstant.LIST_CLASS_WHITE,
                        RiskListConstant.SUBJECT_TYPE_USER,
                        uid);
        if (existWhiteUser) {
            return true;
        }
        log.info(">>> userApiAccess: tid: {}, uid: {}, digest: {}", tid, uid, digest);
        // 查看api信息: 如果是登录或者匿名访问，直接返回
        ResApiEntity resApiEntity = resApiService.findByDigestInAndClientId(new ArrayList<>(){{add(digest);}}, client);
        if (Objects.isNull(resApiEntity)) {
            throw new RealmException("realmException", RealmErrorKey.API_NO_EXIST);
        }

        // 登录和匿名需要区分清楚, pending
        String processType = resApiEntity.getProcessType();
        if (RealmConstant.API_PROCESS_ANONYMOUS.equals(processType)
                || RealmConstant.API_PROCESS_LOGIN.equals(processType) ) {
            return true;
        }

        // 通过角色主体：用户，用户组，组织获取用户角色
        List<String> roles = userRoleAccess(tid, uid);
        boolean exist =
                ((RoleResourceRepository)roleResourceService.getRepository())
                .existsByTidAndClientIdAndRoleIdInAndObjectTypeAndIdentity(
                        tid,
                        client,
                        roles,
                        RealmConstant.ROLE_RES_OBJECT_TYPE_API,
                        digest);
        log.info(">>> userApiAccess, result: {}", exist);
        return exist;
    }
}
