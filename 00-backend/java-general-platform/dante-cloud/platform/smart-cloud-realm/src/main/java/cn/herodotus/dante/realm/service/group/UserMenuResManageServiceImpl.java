package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.*;
import cn.herodotus.dante.realm.model.UserPermitModel;
import cn.herodotus.dante.realm.repository.ApplicationSubjectRepository;
import cn.herodotus.dante.realm.repository.GroupUserRepository;
import cn.herodotus.dante.realm.service.*;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author root
 * @description TODO
 * @date 2023/9/6 9:29
 */
@Service
@Slf4j
public class UserMenuResManageServiceImpl {

    @Resource
    private ApplicationSubjectService applicationSubjectService;
    @Resource
    private ApplicationService applicationService;
    @Resource
    private GroupUserService groupUserService;
    @Resource
    private UserService userService;
    @Resource
    private DeptService deptService;
    @Resource
    private ApplicationTenantSsoService applicationTenantSsoService;


    public List<ApplicationEntity> userApps(UserPermitModel model) {
        String tid = applicationSubjectService.getTid();
        UserEntity userEntity = userService.findById(model.getUid());
        if (RealmConstant.ACCOUNT_ATTR_ROOT.equals(userEntity.getAccountAttr())) {

        }

        // 应用ID
        Set<String> appIds = Sets.newHashSet();
        Map<String, String> sso = Maps.newHashMap();
        // 1 查询用户主体
        List<ApplicationSubjectEntity> userSubject = ((ApplicationSubjectRepository)applicationSubjectService.getRepository())
                .findByTidAndIdentityIn(tid, new ArrayList<String>() {{add(model.getUid());}});
        userSubject.stream().forEach(v-> {
            appIds.add(v.getClientId());
            if (RealmConstant.APP_FROM_SSO.equals(v.getClientFrom())) {
                sso.put(v.getClientId(), v.getClientId());
            }
        });

        // 2 查询用户组主体
        List<GroupUserEntity> group = ((GroupUserRepository)groupUserService.getRepository())
                .findByTidAndUidAndGroupType(tid, model.getUid(), RealmConstant.GROUP_TYPE_STATIC);
        if (CollectionUtils.isNotEmpty(group)) {
            List<ApplicationSubjectEntity> groupSubject = ((ApplicationSubjectRepository) applicationSubjectService.getRepository())
                    .findByTidAndIdentityIn(tid, group.stream().map(m->m.getGroupId()).toList());

            groupSubject.stream().forEach(v-> {
                appIds.add(v.getClientId());
                sso.put(v.getClientId(), v.getClientId());
            });
        }

        // 3 查询组织主体
        if (!Strings.isNullOrEmpty(userEntity.getMainDeptId())) {
            List<DeptEntity> depts
                    = deptService.listByIds(new ArrayList<String>() {{add(userEntity.getMainDeptId());}});
            List<ApplicationSubjectEntity> orgSubject = ((ApplicationSubjectRepository) applicationSubjectService.getRepository())
                    .findByTidAndIdentityIn(tid, depts.stream().map(m->m.getId()).toList());

            orgSubject.stream().forEach(v-> {
                appIds.add(v.getClientId());
                sso.put(v.getClientId(), v.getClientId());
            });
        }

        if (CollectionUtils.isEmpty(appIds)) {
            return Lists.newArrayList();
        }
        List<ApplicationEntity> applist
                = applicationService.findByClientIdIn(appIds.stream().toList());
        applist.stream().forEach(v-> {
            if (sso.get(v.getId()) != null) {
                v.setFromSso(1);
            }
        });
        return applist;
    }
}
