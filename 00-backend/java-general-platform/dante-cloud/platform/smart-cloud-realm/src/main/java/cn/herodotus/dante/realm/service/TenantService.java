/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.AddUserModel;
import cn.herodotus.dante.realm.model.AdminAddModel;
import cn.herodotus.dante.realm.repository.TenantRepository;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
@Slf4j
public class TenantService extends BaseService<TenantEntity, String> {

    private TenantRepository tenantRepository;
    private TenantAdminService tenantAdminService;
    private UserService userService;
    private TenantApplicationService tenantApplicationService;

    @Autowired
    public TenantService(TenantRepository tenantRepository,
                         UserService userService,
                         TenantApplicationService tenantApplicationService,
                         TenantAdminService tenantAdminService) {
        this.tenantRepository = tenantRepository;
        this.userService = userService;
        this.tenantApplicationService = tenantApplicationService;
        this.tenantAdminService = tenantAdminService;
    }

    @Override
    public BaseRepository<TenantEntity, String> getRepository() {
        return this.tenantRepository;
    }


    //
    public TenantEntity findByTid(String tid) {
        return tenantRepository.findByTid(tid);
    }
    public List<TenantEntity> findByIds(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Lists.newArrayList();
        }
        return tenantRepository.findAllById(ids);
    }

    //
    public List<TenantEntity> findTenantByParentIds(List<String> parentIds) {
        Specification<TenantEntity> spec = (root, query, cb)->{
            Path<Object> bizId = root.get("parentId");
            List<Predicate> list = new ArrayList<>();
            CriteriaBuilder.In<Object> in = cb.in(bizId);
            in.value(parentIds);
            list.add(in);
            Predicate[] array = new Predicate[list.size()];
            return cb.and(list.toArray(array));
        };
        return super.findAll(spec);
    }

    //查找子租户
    public List<TenantEntity> findSubTenentById(String id) {
        TenantEntity entity = super.findById(id);
        if (Objects.isNull(entity)) {
            return Lists.newArrayList();
        }
        if (RealmConstant.PARENT_ID_DEFAULT.equals(entity.getParentId())) {
            //第一级
            List<TenantEntity> list = tenantRepository.findByParentId(id);
            List<TenantEntity> subs = findTenantByParentIds(
                    list.stream().map(v->v.getId())
                            .collect(Collectors.toList())
            );
            list.addAll(subs);
            return list;
        }
        else if (RealmConstant.TID_TYPE_REALM.equals(entity.getTidType())) {
            //第二级
            return Lists.newArrayList();
        }
        else {
            //第三级没有子级了
            return Lists.newArrayList();
        }
    }

    //停用租户
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean active(String id) {
        TenantEntity entity = super.findById(id);
        entity.setStatus(RealmConstant.REG_OPTYPE_ACTIVE);
        super.save(entity);
        return true;
    }
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean deactive(String id) {
        TenantEntity entity = super.findById(id);
        entity.setStatus(RealmConstant.REG_OPTYPE_DEACTIVE);
        super.save(entity);
        return true;

    }


    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean deleteTenant(String id) {
        TenantEntity entity = super.findById(id);
        if (Objects.isNull(entity)) {
            return false;
        }
        //删除租户相关数据
        //删除应用
        tenantApplicationService.deleteByTid(entity.getTid());
        //删除管理员
        tenantApplicationService.deleteByTid(entity.getTid());
        super.deleteById(id);
        return true;
    }

/*
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public TenantEntity updateTenant(TenantEntity entity) {
        tenantApplicationService.deleteByTid(entity.getTid());
        tenantApplicationService.saveApplication(entity.getApps(),
                entity.getTid(), "tenant");
        super.save(entity);
        return entity;
    }
*/

    public static String ID() {
        return IDContainer.INST.getUUID(20, false);
    }
    // 创建租户而非用户空间
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public TenantEntity createTenant(TenantEntity entity) {
        String tid = IDContainer.INST.getUUID(20, false);
        //添加应用
        tenantApplicationService.saveApplication(entity.getApps(),
                entity.getTid(), entity.getTidType());

        //添加管理员, 也需要给super用户添加,
        AdminAddModel adminAddModel = new AdminAddModel();
        adminAddModel.setUid(super.getUid());
        tenantAdminService.spaceAdminAdd(adminAddModel);

        entity.setTid(tid);
        return super.save(entity);
    }

    //在租户，用户空间增加用户
    public UserEntity addUser(AddUserModel addUserModel) {
        UserEntity userEntity = new UserEntity();
        userEntity.setTid(addUserModel.getTid());
        //userEntity.setTidType(addUserModel.getTidType());
        return userService.save(userEntity);
    }


}
