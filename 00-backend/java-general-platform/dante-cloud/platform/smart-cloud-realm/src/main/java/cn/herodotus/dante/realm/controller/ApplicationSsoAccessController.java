package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.ApplicationSubjectEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.service.ApplicationSubjectService;
import cn.herodotus.dante.realm.service.ApplicationTenantSsoService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/appsso")
@Tag(name = "SSO应用管理-访问授权")
@Validated
public class ApplicationSsoAccessController {

    @Resource
    private ApplicationSubjectService applicationSubjectService;
    @Resource
    private ApplicationTenantSsoService applicationTenantSsoService;


    @Operation(summary = "添加应用授权", description = "添加应用授权")
    @PostMapping("/addEmpower")
    public Result<Boolean> addApplicationSubject(@RequestBody List<ApplicationSubjectEntity> list) {
        boolean ret = applicationSubjectService.addApplicationEmpower(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除应用授权", description = "删除应用授权")
    @PostMapping("/deleteEmpower")
    public Result<Boolean> deleteAppEmpower(@RequestBody List<ApplicationSubjectEntity> list) {
        boolean ret = applicationSubjectService.deleteApplicationEmpower(list);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询应用授权", description = "查询应用授权")
    @PostMapping("/listEmpower")
    public Result<PageResponse> listAppEmpower(@RequestBody BaseQueryModel model) {
        PageResponse ret = applicationSubjectService.listApplicationEmpower(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "设置所有用户可见", description = "设置所有用户可见")
    @PostMapping("/allUserEnable/{clientId}")
    public Result<Boolean> allUserEnable(@PathVariable String clientId) {
        Boolean ret = applicationTenantSsoService.allUserAvailable(clientId, true);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "禁止所有用户可见", description = "禁止所有用户可见")
    @PostMapping("/allUserDisable/{clientId}")
    public Result<Boolean> allUserDisable(@PathVariable String clientId) {
        Boolean ret = applicationTenantSsoService.allUserAvailable(clientId, false);
        return Result.success(Feedback.OK, ret);
    }
}
