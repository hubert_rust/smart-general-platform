/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.TenantApplicationEntity;
import cn.herodotus.dante.realm.repository.TenantApplicationRepository;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class TenantApplicationService extends BaseService<TenantApplicationEntity, String> {

    private TenantApplicationRepository tenantApplicationRepository;

    @Autowired
    public TenantApplicationService(TenantApplicationRepository tenantApplicationRepository) {
        this.tenantApplicationRepository = tenantApplicationRepository;
    }

    @Override
    public BaseRepository<TenantApplicationEntity, String> getRepository() {
        return this.tenantApplicationRepository;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public void deleteByTid(String tid) {

       tenantApplicationRepository.delete(new Specification<TenantApplicationEntity>() {
           @Override
           public Predicate toPredicate(Root<TenantApplicationEntity> root,
                                        CriteriaQuery<?> query,
                                        CriteriaBuilder criteriaBuilder) {
               return criteriaBuilder.equal(root.get("tid"), tid);
           }
       });
    }
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public void saveApplication(List<ApplicationEntity> list, String tid, String tidType) {
        if (CollectionUtils.isNotEmpty(list)) {
            List<TenantApplicationEntity> apps = Lists.newArrayList();
            for (ApplicationEntity app : list) {
                TenantApplicationEntity tenantApplicationEntity
                        = new TenantApplicationEntity();
                tenantApplicationEntity.setTid(tid);
                tenantApplicationEntity.setAppId(app.getId());
                tenantApplicationEntity.setTidType(tidType);
            }
            super.saveAllAndFlush(apps);
        }
    }
}
