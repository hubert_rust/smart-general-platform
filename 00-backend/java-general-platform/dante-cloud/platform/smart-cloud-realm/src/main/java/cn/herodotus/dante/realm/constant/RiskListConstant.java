package cn.herodotus.dante.realm.constant;

/**
 * @author root
 * @description TODO
 * @date 2023/9/11 17:11
 */
public interface RiskListConstant {
    String LIST_CLASS_WHITE = "white";
    String LIST_CLASS_BLACK = "black";

    String SUBJECT_TYPE_IP = "ip";
    String SUBJECT_TYPE_USER = "user";

    // 禁止登录
    String PROCESS_TYPE_BAN_LOGIN = "ban_login";
    // 跳过认证，登录即可访问
    String PROCESS_TYPE_SKIP_AUTH = "skip_auth";

    // 手动添加
    String ADD_TYPE_HAND = "hand";
}
