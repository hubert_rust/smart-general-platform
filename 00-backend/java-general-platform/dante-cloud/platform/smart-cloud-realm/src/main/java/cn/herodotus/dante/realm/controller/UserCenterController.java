package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.model.user.ChangePasswordModel;
import cn.herodotus.dante.realm.service.SmartLogService;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.dante.realm.service.group.UserCenterServiceImpl;
import cn.herodotus.dante.realm.service.group.UserManagerServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author root
 * @description 用户中心
 * @date 2023/9/8 15:13
 */
@RestController
@RequestMapping("/ucenter")
@Tag(name = "个人中心")
public class UserCenterController {

    @Resource
    private SmartLogService smartLogService;
    @Resource
    private UserManagerServiceImpl userManagerService;
    @Resource
    private UserService userService;
    @Resource
    private UserCenterServiceImpl userCenterService;

    @Operation(summary = "用户访问日志", description = "用户访问日志")
    @PostMapping("/userAccessList")
    public Result<PageResponse> userAccessList(@Valid @RequestBody BaseQueryModel model) {
        PageResponse ret = smartLogService.accessList(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "编辑用户", description = "编辑用户")
    @PostMapping("/userEdit")
    public Result<UserEntity> userEdit(@RequestBody UserModel model) {
        String uid = userService.getUid();
        UserEntity entity = userService.findById(uid);
        model.setUid(uid);
        model.setStatus(entity.getStatus());
        UserEntity ret = userManagerService.updateUserOnCurrentTenant(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "修改密码", description = "修改密码")
    @PostMapping("/change")
    public Result<Boolean> change(@RequestBody ChangePasswordModel model) {
        Boolean ret = userCenterService.changePassword(model);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "密码规则校验", description = "密码规则校验")
    @PostMapping("/check")
    public Result<ChangePasswordModel> check(@RequestBody ChangePasswordModel model) {
        ChangePasswordModel ret = userCenterService.check(model);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "账号信息", description = "账号信息")
    @PostMapping("/userAuth")
    public Result<ChangePasswordModel> userAuth() {
        ChangePasswordModel ret = userCenterService.userAuthDetail();
        return Result.success(Feedback.OK, ret);
    }
}
