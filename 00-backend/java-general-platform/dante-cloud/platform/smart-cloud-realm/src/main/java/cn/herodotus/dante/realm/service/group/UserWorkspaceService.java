/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.TenantAdminEntity;
import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.AdminAddModel;
import cn.herodotus.dante.realm.model.user.UserAllWorkspaceModel;
import cn.herodotus.dante.realm.model.user.UserWorkspaceModel;
import cn.herodotus.dante.realm.service.TenantAdminService;
import cn.herodotus.dante.realm.service.TenantService;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.dante.realm.service.recycle.TenantServiceRecycle;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * <p>Description: 用户空间管理， </p>
 */
@Service
public class UserWorkspaceService  {


    private TenantAdminService tenantAdminService;
    private TenantService tenantService;
    private UserService userService;
    private TenantServiceRecycle tenantServiceRecycle;

    @Autowired
    public UserWorkspaceService(TenantAdminService tenantAdminService,
                                TenantService tenantService,
                                TenantServiceRecycle tenantServiceRecycle,
                                UserService userService) {
        this.tenantAdminService = tenantAdminService;
        this.tenantService = tenantService;
        this.userService = userService;
        this.tenantServiceRecycle = tenantServiceRecycle;
    }



    /*
     * @Author root
     * @Description 注册用户的时候创建默认空间, 用空间名，默认是账号名称
     * @Date 18:09 2023/8/12
     * @Param []
     * @return cn.herodotus.dante.realm.entity.TenantEntity
     **/
    public TenantEntity createWorkspaceOnRegUser(UserWorkspaceModel model) {
        TenantEntity entity = new TenantEntity();
        entity.setTenantName(model.getName());
        entity.setStatus(model.getStatus());
        entity.setId(TenantService.ID());

        return entity;
    }

    // 登录后加载
    public UserAllWorkspaceModel listWorkspaceAfterLogin() {
        UserAllWorkspaceModel ret = list();

        String uid = CommonSecurityContextHolder.getUid();
        UserEntity userEntity = userService.findById(uid);
        TenantEntity entity = tenantService.findById(userEntity.getHitTid());
        if (Objects.nonNull(entity)) {
            UserWorkspaceModel wks = new UserWorkspaceModel();
            BeanUtils.copyProperties(entity, wks);
            wks.setName(entity.getTenantName());
            ret.setModel(wks);
            ret.getWorkspaces().add(wks);
        }
        return ret;
    }

    // 切换用户空间
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean switchWorkspace(String id) {
        String uid = CommonSecurityContextHolder.getUid();
        UserEntity entity = userService.findById(uid);
        entity.setHitTid(id);
        userService.save(entity);
        /*UserAllWorkspaceModel ret = list();
        TenantEntity entity = tenantService.findById(id);

        UserWorkspaceModel wks = new UserWorkspaceModel();
        BeanUtils.copyProperties(entity, wks);
        wks.setName(entity.getTenantName());
        ret.setModel(wks);
        ret.getWorkspaces().add(wks);*/

        return true;
    }

    // 登录后获取用户空间列表
    public UserAllWorkspaceModel list() {

        UserAllWorkspaceModel userAllWorkspaceModel = new UserAllWorkspaceModel();
        // 一类是自己创建的用户列表
        // 通过管理员表查询，
        String uid = CommonSecurityContextHolder.getUid();
        List<TenantAdminEntity> list = tenantAdminService.findByUid(uid);
        if (CollectionUtils.isEmpty(list)) {
            return userAllWorkspaceModel;
        }
        List<TenantEntity> tenants = tenantService.findByIds(list.stream()
                .map(m->m.getTid())
                .toList());

        //自己创建的
        List<TenantEntity> selfs
                = tenants.stream().filter(p-> uid.equals(p.getCreateBy())).toList();
        List<TenantEntity> agents
                = tenants.stream().filter(p->!uid.equals(p.getCreateBy())).toList();
        List<UserWorkspaceModel> list1 = Lists.newArrayList();
        selfs.stream().forEach(v-> {
            UserWorkspaceModel userWorkspaceModel = new UserWorkspaceModel();
            BeanUtils.copyProperties(v, userWorkspaceModel);
            userWorkspaceModel.setName(v.getTenantName());
            list1.add(userWorkspaceModel);
        });
        List<UserWorkspaceModel> list2 = Lists.newArrayList();
        agents.stream().forEach(v-> {
            UserWorkspaceModel userWorkspaceModel = new UserWorkspaceModel();
            BeanUtils.copyProperties(v, userWorkspaceModel);
            userWorkspaceModel.setName(v.getTenantName());
            list2.add(userWorkspaceModel);
        });
        userAllWorkspaceModel.setWorkspaces(list1);
        userAllWorkspaceModel.setAgents(list2);

        return userAllWorkspaceModel;
        // 一类是托管的
    }

    /*
     * @Author root
     * @Description //TODO
     * @Date 9:55 2023/9/7
     * @Param
     * @return
     **/
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean deleteNormalTenant(String id) {
        // 平台用户空间不能被删除
        if (RealmConstant.DELETED_VALUE_DEFAULT.equals(id)) {
            throw new RealmException("realmException", RealmErrorKey.USER_REALM_REMOVE_BAN);
        }

        String uid = CommonSecurityContextHolder.getUid();
        UserEntity entity = userService.findById(uid);
        entity.setHitTid(RealmConstant.PARENT_ID_DEFAULT);
        userService.save(entity);

        // 每个用户空间管理员 删除的时候，需要更新
        String tid = CommonSecurityContextHolder.getTid();
        userService.findById(uid);
        //


        // 删除其他agent管理员
        // 删除相关数据
        tenantServiceRecycle.removeServiceData(tid);
        // 删除用户空间下的租户数据，先找到用户空间下租户



        tenantService.deleteTenant(id);
        return true;
    }
    // 更新用户空间
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public TenantEntity updateNormalTenant(UserWorkspaceModel model) {
        TenantEntity entity = tenantService.findById(model.getCurrentId());
        entity.setTenantName(model.getName());
        entity.setRemark(model.getRemark());
        entity.setLogo(model.getLogo());
        return tenantService.save(entity);
    }

    /*
     * @Author root
     * @Description  新建用户空间，需要再租户管理员表中添加
     * @Date 18:01 2023/8/24
     * @Param
     * @return
     **/
    //pending
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public UserAllWorkspaceModel createNormalTenant(UserWorkspaceModel model) {
        // 只有管理员才能创建用户空间, agent(代管空间管理员)不能创建用户空间(可以创建的场景暂不考虑)
        String uid = CommonSecurityContextHolder.getUid();
        String tid = CommonSecurityContextHolder.getTid();
        UserEntity user = userService.findById(uid);
        if (user.getAccountAttr().equals(RealmConstant.ACCOUNT_ATTR_ROOT)
                || RealmConstant.ACCOUNT_ATTR_MASTER.equals(user.getAccountAttr())) {
        }
        else {
            throw new RealmException("realmException", RealmErrorKey.NOT_ADMIN);
        }

        String id = IDContainer.INST.getTid();

        UserEntity userEntity = userService.findById(uid);
        userEntity.setHitTid(id);
        userService.save(userEntity);

        UserAllWorkspaceModel ret = list();
        TenantEntity entity = new TenantEntity();
        entity.setId(id);
        entity.setTid(id);

        // parent设置
        // 注意:
        // master中用户空间的parent都是这个master对应的tid
        // root中用户空间的parent都是这个root对应的tid
        // master用户默认一个用户空间，parentId= -1
        // 在master这个默认空间创建一个新用户空间
        TenantEntity tenantEntity = tenantService.findById(tid);
        if (RealmConstant.PARENT_ID_DEFAULT.equals(tenantEntity.getParentId())) {
            entity.setParentId(tid);
        }
        else {
            TenantEntity parent = tenantService.findById(tenantEntity.getParentId());
            if (RealmConstant.PARENT_ID_DEFAULT.equals(parent.getParentId())) {
                entity.setParentId(parent.getTid());
            }

        }
        entity.setTidType(RealmConstant.TID_TYPE_REALM);
        entity.setTenantName(model.getName());
        entity.setRemark(model.getRemark());
        entity.setStatus(model.getStatus());
        tenantService.insert(entity);

        TenantAdminEntity adminEntity = new TenantAdminEntity();
        adminEntity.setCreateBy(uid);
        adminEntity.setUid(uid);
        adminEntity.setTidType(RealmConstant.TID_TYPE_REALM);
        adminEntity.setTid(id);
        tenantAdminService.insert(adminEntity);

        /*UserWorkspaceModel wks = new UserWorkspaceModel();
        BeanUtils.copyProperties(entity, wks);
        wks.setName(entity.getTenantName());
        ret.setModel(wks);

        ret.getWorkspaces().add(wks);*/

        //直接返回切换后列表
        return ret;
    }

    // 管理员工作空间，第一个注册的用户，拥有所有权限
    // 用户空间管理员列表
    public List<UserEntity> workspaceAdmins() {
        String tid = CommonSecurityContextHolder.getUserTid();
        List<TenantAdminEntity> list
                = tenantAdminService.findByTidAndTidType(tid, RealmConstant.TID_TYPE_REALM);
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }

        List<UserEntity> users
                = userService.listByIds(list.stream().map(m->m.getUid()).toList());
        users.stream().forEach(v-> {
            if (v.getTid().equals(tid)) {
                v.setAgent(false);
            }
            else {
                v.setAgent(true);
            }
        });


        return users;
    }

    public boolean spaceAdminAdd(AdminAddModel adminAddModel) {
        return true;
    }
}
