/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.entity.PostEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.PostLinkDeptModel;
import cn.herodotus.dante.realm.repository.PostRepository;
import cn.herodotus.dante.realm.util.QueryUtil;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostService extends BaseService<PostEntity, String> {

    private PostRepository postRepository;

    @Autowired
    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public BaseRepository<PostEntity, String> getRepository() {
        return this.postRepository;
    }

    public PostEntity insert(PostEntity entity) {
        PostEntity postEntity = super.insert(entity);
        if (CollectionUtils.isEmpty(entity.getDeptIds())) {
            return postEntity;
        }
        List<PostEntity> list = Lists.newArrayList();
        for (String deptId : entity.getDeptIds()) {
            PostEntity child = new PostEntity();
            child.setParentId(postEntity.getId());
            child.setLinkDept(deptId);
            list.add(child);
        }
        super.insertAll(list);
        return postEntity;
    }

    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean update(PostEntity entity) {
        PostEntity find = super.findById(entity.getId());
        find.setPostName(entity.getPostName());
        find.setPostCode(entity.getPostCode());
        find.setRemark(entity.getRemark());

        postRepository.deleteByParentId(find.getId());
        if (CollectionUtils.isEmpty(entity.getDeptIds())) {
            super.save(find);
            return true;
        }
        List<PostEntity> list = Lists.newArrayList();
        for (String deptId : entity.getDeptIds()) {
            PostEntity child = new PostEntity();
            child.setParentId(find.getId());
            child.setLinkDept(deptId);
            list.add(child);
        }
        super.save(find);
        super.saveAll(list);
        return true;
    }

    public boolean linkDept(PostLinkDeptModel model) {
        PostEntity find = super.findById(model.getPostId());
        if (CollectionUtils.isEmpty(model.getDeptIds())) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID);
        }
        List<PostEntity> list = Lists.newArrayList();
        for (String deptId : model.getDeptIds()) {
            PostEntity postEntity = new PostEntity();
            postEntity.setParentId(model.getPostId());
            postEntity.setLinkDept(deptId);
            list.add(postEntity);
        }
        super.saveAll(list);
        return true;
    }

    public PageResponse<PostEntity> list(BaseQueryModel model) {
        String tid = super.getTid();
        Pageable pageable = QueryUtil.pageable(model);

        Specification<PostEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Strings.isNotEmpty(model.getKey())) {
                predicates.add(cb.equal(root.get("postName"), model.getKey()));
            }
            predicates.add(cb.equal(root.get("tid"), tid));

            if (CollectionUtils.isNotEmpty(predicates)) {
                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
            return null;
        };
        long num = super.count(spec);

        Page page = super.findByPage(spec, pageable);
        PageResponse<PostEntity> resp = new PageResponse<>();
        resp.setTotalCount((int) num);
        resp.setPageIndex(page.getNumber());
        resp.setPageSize(model.getPageSize());
        resp.setSuccess(true);
        resp.setData(page.getContent());
        return resp;
    }
}
