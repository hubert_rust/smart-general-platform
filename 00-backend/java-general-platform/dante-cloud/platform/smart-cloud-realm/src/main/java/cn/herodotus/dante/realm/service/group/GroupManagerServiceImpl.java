package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.entity.GroupEntity;
import cn.herodotus.dante.realm.entity.GroupRoleEntity;
import cn.herodotus.dante.realm.entity.GroupUserEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.model.reg.UserModel;
import cn.herodotus.dante.realm.model.user.GroupUserModel;
import cn.herodotus.dante.realm.repository.GroupUserRepository;
import cn.herodotus.dante.realm.repository.RoleSubjectRepository;
import cn.herodotus.dante.realm.service.*;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.data.core.dto.PageResponse;
import com.google.common.collect.Lists;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class GroupManagerServiceImpl {

    @Resource
    private GroupUserService groupUserService;
    @Resource
    private GroupService groupService;
    @Resource
    private UserService userService;
    @Resource
    private GroupRoleService groupRoleService;
    @Resource
    private RoleSubjectService roleSubjectService;

    // 查询用户组中用户
    public PageResponse listUser(BaseQueryModel model) {
        PageResponse<GroupUserEntity> resp = groupUserService.list(model);
        if (resp == null || CollectionUtils.isEmpty(resp.getData())) {
            return resp;
        }
        List<String> list = resp.getData().stream().map(m->m.getUid()).toList();
        List<UserEntity> users = userService.listByIds(list);

        PageResponse<UserEntity> pageResponse = new PageResponse<>();
        pageResponse.setData(users);
        pageResponse.setTotalCount(resp.getTotalCount());
        pageResponse.setPageSize(resp.getPageSize());
        pageResponse.setPageIndex(resp.getPageIndex());
        pageResponse.setSuccess(true);


        return pageResponse;
    }



    // group 移除用户
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean groupDeleteUser(GroupUserModel model) {
        if (CollectionUtils.isEmpty(model.getUsers())) {
            return false;
        }
        groupUserService.deleteByTidAndGroupIdAndUidIn(
                model.getGroupId(),
                model.getUsers().stream().map(m->m.getUid()).toList()
        );
        return true;
    }
    // group中添加用户
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public boolean groupInsertUser(GroupUserModel model) {
        GroupEntity entity = groupService.findById(model.getGroupId());
        if(CollectionUtils.isEmpty(model.getUsers())) {
            return false;
        }

        // 需要过滤已经添加的
        List<GroupUserEntity> list = Lists.newArrayList();
        for (UserModel user : model.getUsers()) {
            GroupUserEntity entity1 = new GroupUserEntity();
            entity1.setGroupId(model.getGroupId());
            entity1.setGroupTag(entity.getGroupTag());
            entity1.setGroupType(entity.getGroupType());
            entity1.setUid(user.getUid());
            entity1.setAccount(user.getAccount());
            list.add(entity1);

        }
        groupUserService.saveAll(list);
        return true;
    }
    // 删除用户
    public boolean deleteGroup(String id) {
        String tid = groupUserService.getTid();
        boolean exist
                = ((GroupUserRepository)groupUserService.getRepository())
                .existsByTidAndGroupId(tid, id);
        if (exist) {
            throw new RealmException("realmException", RealmErrorKey.OTHER_USER_GROUP_USED, "用户-用户组关系");
        }
        exist = ((RoleSubjectRepository)roleSubjectService.getRepository())
                .existsByTidAndIdentity(tid, id);
        if (exist) {
            throw new RealmException("realmException", RealmErrorKey.OTHER_USER_GROUP_USED, "角色-授权主体关系");
        }
        groupService.deleteById(id);
        return true;
    }

    // 用户组添加授权
    public boolean groupInsertRole(List<GroupRoleEntity> list) {
        return groupRoleService.groupAddRole(list);
    }
    // 用户组删除授权
    public boolean groupDeleteRole(List<GroupRoleEntity> list) {
        return groupRoleService.groupRemoveRole(list);
    }

}
