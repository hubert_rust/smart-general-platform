package cn.herodotus.dante.realm.event;

import cn.herodotus.engine.message.core.definition.LocalApplicationEvent;

public class TestEvent extends LocalApplicationEvent<String> {

    public TestEvent(String data) {
        super(data);
    }
}
