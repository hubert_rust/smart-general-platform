package cn.herodotus.dante.realm.model;

import cn.herodotus.dante.realm.entity.RoleSubjectEntity;
import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author root
 * @description  用户详情中用户资源，接口权限， 应用授权
 * @date 2023/8/30 15:49
 */
@Data
public class UserPermitModel {

    private String uid;
    private String roleId;
    private String digest;
    private String clientId;
    private List<RoleSubjectEntity> list = Lists.newArrayList();
}
