package cn.herodotus.dante.realm.service.group;

import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.constant.AccountConstant;
import cn.herodotus.dante.realm.entity.UserAuthEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.user.ChangePasswordModel;
import cn.herodotus.dante.realm.repository.UserAuthRepository;
import cn.herodotus.dante.realm.repository.UserRepository;
import cn.herodotus.dante.realm.service.UserAuthService;
import cn.herodotus.dante.realm.service.UserService;
import cn.herodotus.dante.realm.util.PassayUtil;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.exception.transaction.TransactionalRollbackException;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;
import com.google.common.collect.Lists;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/10 10:19
 */
@Slf4j
@Service
public class UserCenterServiceImpl {

    @Resource
    private UserAuthService userAuthService;
    @Resource
    private UserService userService;
    @Resource
    private SymmetricCryptoProcessor symmetricCryptoProcessor;

    public ChangePasswordModel userAuthDetail() {
        String uid = CommonSecurityContextHolder.getUid();
        String tid = CommonSecurityContextHolder.getTid();
        UserAuthEntity authEntity = ((UserAuthRepository)userAuthService.getRepository())
                .findByTidAndIdentityTypeAndUid(tid, AccountConstant.REG_TYPE_ACCOUNT, uid);

        ChangePasswordModel changePasswordModel = new ChangePasswordModel();
        changePasswordModel.setPwdLevel(authEntity.getPwdLevel());
        changePasswordModel.setChangePwd(authEntity.getChangePwd());
        changePasswordModel.setAccount(authEntity.getIdentity());
        return changePasswordModel;
    }
    @Transactional(rollbackFor = TransactionalRollbackException.class)
    public Boolean changePassword(ChangePasswordModel model) {
        UserEntity user = ((UserRepository)userService.getRepository())
                .findByAccount(model.getAccount());
        String uid = CommonSecurityContextHolder.getUid();
        String tid = CommonSecurityContextHolder.getTid();
        if (!user.getId().equals(uid)) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID);
        }

        UserAuthEntity authEntity = ((UserAuthRepository)userAuthService.getRepository())
                .findByTidAndIdentityTypeAndUid(tid, AccountConstant.REG_TYPE_ACCOUNT, uid);
        String cred = authEntity.getCredential();
        cred = symmetricCryptoProcessor.decrypt(cred, ProtectConstant.SM4_SECRET);
        String secKey = ProtectConstant.SM4_SECRET;
        String old = symmetricCryptoProcessor.decrypt(model.getOldPassword(), secKey);
        if (!cred.equals(old)) {
            throw new RealmException("realmException", RealmErrorKey.OLD_PWD_NOT_MATCH);
        }

        authEntity.setPwdLevel(model.getPwdLevel());
        authEntity.setChangePwd(new Date());
        authEntity.setCredential(model.getNewPassword());
        userAuthService.save(authEntity);
        return true;
    }
    public ChangePasswordModel check(ChangePasswordModel model) {
        ChangePasswordModel changePasswordModel = new ChangePasswordModel();
        String secKey = ProtectConstant.SM4_SECRET;
        String pass = symmetricCryptoProcessor.decrypt(model.getNewPassword(), secKey);

        List<String> errMessage = Lists.newArrayList();
        Integer ret = PassayUtil.passwordLevel(pass, errMessage);
        changePasswordModel.setPwdLevel(ret);
        return changePasswordModel;
    }

}
