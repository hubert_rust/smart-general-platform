/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.service;

import cn.herodotus.dante.common.util.NanoIdUtils;
import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.constant.ConstsProtocols;
import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.repository.ApplicationRepository;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Service
public class ApplicationService extends BaseService<ApplicationEntity, String> {

    private ApplicationRepository applicationRepository;

    @Autowired
    public ApplicationService(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public BaseRepository<ApplicationEntity, String> getRepository() {
        return this.applicationRepository;
    }

    public List<ApplicationEntity> findByClientIdIn(Collection<String> clientIds) {
        return applicationRepository.findByClientIdIn(clientIds);
    }
    public ApplicationEntity findByAppId(String appId) {
       return this.applicationRepository.findByClientId(appId);
    }

    //
    public ApplicationEntity findByServer(String appType, Integer server) {
        return this.applicationRepository.findByAppTypeAndServer(appType, server);
    }

    //创建SSO应用，创建自建应用
    public ApplicationEntity insert(ApplicationEntity entity) {
        String tid = super.getTid();
        String id = IDContainer.INST.getUUID(20, false);
        entity.setId(id);
        entity.setTid(tid);

        entity.setClientId(id);
        if (entity.getStatus() == null) {
            entity.setStatus(RealmConstant.REG_OPTYPE_ACTIVE);
        }
        if (entity.getProtocol() == null) {
            entity.setProtocol(ConstsProtocols.DEFAULT);
        }
        entity.setClientSecret(NanoIdUtils.randomNanoId());
        entity.setDeleted(RealmConstant.DELETED_VALUE_DEFAULT);
        entity.setClientIdIssuedAt(LocalDate.now().toDate());
        return super.insert(entity);
    }
    public ApplicationEntity updateBase(ApplicationEntity entity) {
        ApplicationEntity find = super.findById(entity.getId());
        find.setRemark(entity.getRemark());
        find.setAppInnerName(entity.getAppInnerName());
        find.setAppTag(entity.getAppTag());
        find.setLogo(entity.getLogo());
        return super.save(find);
    }

    public ApplicationEntity updateAuth(ApplicationEntity entity) {
        ApplicationEntity find = super.findById(entity.getId());
        find.setAuthUri(entity.getAuthUri());
        find.setRedirectUris(entity.getRedirectUris());
        find.setLogoutUris(entity.getLogoutUris());
        return super.save(find);
    }
    public boolean activeAndDeactiveApp(String id, String status) {
        ApplicationEntity find = super.findById(id);
        find.setStatus(status);
        super.save(find);
        return true;
    }
    public boolean delete(String id) {
        ApplicationEntity find = super.findById(id);
        if (find.getId().equals(RealmConstant.DELETED_VALUE_DEFAULT)) {
            throw new RealmException("realmException", RealmErrorKey.PLATFORM_APP_NOT_DELETE);
        }
        if (Objects.nonNull(find.getServer()) && find.getServer().intValue() == 1) {
            throw new RealmException("realmException", RealmErrorKey.PLATFORM_APP_NOT_DELETE);
        }

        String del = IDContainer.INST.getUUID(20, false);
        super.deleteById(id);
        // find.setDeleted(del);
        // super.save(find);
        return true;
    }
    //
    public List<ApplicationEntity> list() {
        //smart-auth
        //获取该用户空间下的所有创建者创建的应用，
        String tid = super.getTid();
        List<ApplicationEntity> list
                = applicationRepository.findByTidAndDeleted(tid, RealmConstant.DELETED_VALUE_DEFAULT);
        return list;
    }

    public List<ApplicationEntity> findByTid(String tid) {
        return applicationRepository.findByTidAndDeleted(tid, RealmConstant.DELETED_VALUE_DEFAULT);
    }

    public boolean selfApplicationAllShow(String clientId, boolean allShow) {
        String tid = CommonSecurityContextHolder.getTid();
        ApplicationEntity entity = super.findById(clientId);
        entity.setAllShow(allShow);
        super.save(entity);
        return true;

    }
}
