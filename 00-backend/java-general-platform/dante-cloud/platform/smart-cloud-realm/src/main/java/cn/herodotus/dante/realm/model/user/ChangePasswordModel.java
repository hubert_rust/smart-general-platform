package cn.herodotus.dante.realm.model.user;

import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/10 9:26
 */
@Data
public class ChangePasswordModel implements Serializable {
    private String uid;
    private String account;
    private String oldPassword;
    private String newPassword;

    private Date changePwd;

    private Integer pwdLevel;  //0: 弱， 1: 中， 2: 强
    private List<String> errMessage = Lists.newArrayList();
}
