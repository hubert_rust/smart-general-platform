package cn.herodotus.dante.realm.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author root
 * @description TODO
 * @date 2023/8/30 16:07
 */
@Entity
@Table(name = "sys_role_api_data_permission" )
@Data
@EqualsAndHashCode(callSuper=true)
public class RoleApiDataPermissionEntity extends CommonTenantEntity {
    @Column(name = "client_id", length = 32)
    private String clientId;
    @Column(name = "client_from", length = 16)
    private String clientFrom;
    @Column(name = "role_id", length = 16)
    private String roleId;
    private String digest;
    private String scope;
    @Column(name = "custom_subject", length = 16)
    private String customSubject;
    private String identity;
    @Column(name = "identity_name", length = 16)
    private String identityName;

    @Transient
    public String account;
    @Transient
    public String uid;
    @Transient
    public String deptName;
    @Transient
    public String deptId;

}
