/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.realm.entity.TenantEntity;
import cn.herodotus.dante.realm.entity.UserEntity;
import cn.herodotus.dante.realm.model.AddUserModel;
import cn.herodotus.dante.realm.model.AdminAddModel;
import cn.herodotus.dante.realm.service.TenantAdminService;
import cn.herodotus.dante.realm.service.TenantService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tenant")
@Tags({@Tag( name = "租户管理")})
@Validated
public class TenantController {

    @Resource
    private TenantService tenantService;
    @Resource
    private TenantAdminService tenantAdminService;

    @Operation(summary = "停用租户", description = "停用租户")
    @PostMapping("/stop/{id}")
    public Result<Boolean> stop(@PathVariable String id) {
        boolean ret = tenantService.deactive(id);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "启用租户", description = "启用租户")
    @PostMapping("/active/{id}")
    public Result<Boolean> active(@PathVariable String id) {
        boolean ret = tenantService.active(id);
        return Result.success(Feedback.OK, ret);
    }


    @Operation(summary = "创建租户", description = "创建租户")
    @PostMapping("/create")
    public Result<TenantEntity> create(@Valid  @RequestBody TenantEntity entity) {
        TenantEntity ret = tenantService.createTenant(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除租户", description = "删除租户")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        boolean ret = tenantService.deleteTenant(id);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "租户添加用户", description = "租户添加用户")
    @PostMapping("/addUser")
    public Result<UserEntity> addUser(@Valid  @RequestBody AddUserModel model) {
        UserEntity ret = tenantService.addUser(model);
        return Result.success(Feedback.OK, ret);
    }

    //添加租户管理员
    @Operation(summary = "添加租户管理员", description = "添加租户管理员")
    @PostMapping("/addAdmin")
    public Result<AdminAddModel> addAdmin(@Valid  @RequestBody AdminAddModel adminAddModel) {
        tenantAdminService.tenantAdminAdd(adminAddModel);
        return Result.success(Feedback.OK, adminAddModel);
    }

    //租户管理员
    @Operation(summary = "删除租户管理员", description = "删除租户管理员")
    @PostMapping("/removeAdmin")
    public Result<AdminAddModel> removeAdmin(@Valid  @RequestBody AdminAddModel adminAddModel) {
        tenantAdminService.tenantAdminAdd(adminAddModel);
        return Result.success(Feedback.OK, adminAddModel);
    }

    //查询管理员
    @Operation(summary = "添加租户管理员", description = "添加租户管理员")
    @PostMapping("/listAdmin")
    public Result<AdminAddModel> listAdmin(@Valid  @RequestBody AdminAddModel adminAddModel) {
        tenantAdminService.tenantAdminAdd(adminAddModel);
        return Result.success(Feedback.OK, adminAddModel);
    }

}
