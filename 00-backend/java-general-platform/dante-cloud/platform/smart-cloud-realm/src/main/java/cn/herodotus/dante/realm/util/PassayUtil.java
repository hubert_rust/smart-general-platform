package cn.herodotus.dante.realm.util;

import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.passay.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author root
 * @description TODO
 * @date 2023/9/11 10:41
 */
public class PassayUtil {
    public static Integer passwordLevel(String password, List<String> out) {
        List<String> errMessage = out;
        if (Objects.isNull(errMessage))  {
            errMessage = Lists.newArrayList();
        }
        Integer ret = 0;
        List<Rule> rules = new ArrayList<>();
        rules.add(new LengthRule(8, 16));
        CharacterCharacteristicsRule characteristicsRule = new CharacterCharacteristicsRule(3,
                new CharacterRule(EnglishCharacterData.Alphabetical, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                new CharacterRule(EnglishCharacterData.Special, 1));
        rules.add(characteristicsRule);
        PasswordValidator validator = new PasswordValidator(rules);

        RuleResult ruleResult = validator.validate(new PasswordData(password));
        if (ruleResult.isValid()) {
            return 2;
        } else {
            for (RuleResultDetail detail : ruleResult.getDetails()) {
                System.out.println(detail.getErrorCode());
                switch (detail.getErrorCode()) {
                    case LengthRule.ERROR_CODE_MIN -> errMessage.add("密码长度小于8");
                    case LengthRule.ERROR_CODE_MAX -> errMessage.add("密码长度大于16");
                    case "INSUFFICIENT_ALPHABETICAL" -> errMessage.add("没有英文字符");
                    case "INSUFFICIENT_SPECIAL" -> errMessage.add("没有特殊字符");
                    case "INSUFFICIENT_DIGIT" -> errMessage.add("没有数字");
                    default -> {
                        break;
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(errMessage)) {
                ret = errMessage.size() > 2 ? 0 : 1;
            }
        }
        return ret;
    }
}
