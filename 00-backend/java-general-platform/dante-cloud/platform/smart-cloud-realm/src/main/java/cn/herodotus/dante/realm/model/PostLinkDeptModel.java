package cn.herodotus.dante.realm.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PostLinkDeptModel implements Serializable {
    private String postId;
    private List<String> deptIds;
}
