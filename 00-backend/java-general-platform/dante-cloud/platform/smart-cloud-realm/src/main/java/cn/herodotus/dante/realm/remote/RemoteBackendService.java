package cn.herodotus.dante.realm.remote;

import cn.herodotus.dante.model.OperationModel;
import cn.herodotus.dante.model.RemoteBaseModel;
import cn.herodotus.engine.assistant.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URI;
import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/13 8:30
 */
@FeignClient(name = "smart-auth")
public interface RemoteBackendService {
    @RequestMapping(value = "/open/apis", method = RequestMethod.POST)
    Result<List<OperationModel>> getBackendOperations(
            URI uri, @RequestBody RemoteBaseModel model);
}
