/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.realm.controller;

import cn.herodotus.dante.core.constant.ExceptionConst;
import cn.herodotus.dante.core.exception.RealmException;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.entity.BackendEntity;
import cn.herodotus.dante.realm.exception.RealmErrorKey;
import cn.herodotus.dante.realm.model.BaseQueryModel;
import cn.herodotus.dante.realm.repository.BackendRepository;
import cn.herodotus.dante.realm.service.ApplicationService;
import cn.herodotus.dante.realm.service.BackendService;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.data.core.dto.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.persistence.criteria.Predicate;
import jakarta.validation.Valid;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/backend")
@Tag(name = "后台管理")
@Validated
public class BackendController {

    @Resource
    private BackendService backendService;
    @Resource
    private ApplicationService applicationService;

    @Operation(summary = "添加后台", description = "添加后台")
    @PostMapping("/insert")
    public Result<BackendEntity> insert(@Validated @RequestBody BackendEntity entity) {
        // 同一个tid下，名称不能重复，地址不能重复
        Boolean exist = ((BackendRepository)backendService.getRepository())
                .existsByTidAndBackendName(applicationService.getTid(), entity.getBackendName());
        if (exist) {
            throw new RealmException(ExceptionConst.EXCEPTION_REALM, RealmErrorKey.PARAM_INVALID, "名称重复");
        }

        BackendEntity ret = backendService.insert(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "删除后台", description = "删除后台")
    @PostMapping("/delete/{id}")
    public Result<Boolean> delete(@PathVariable String id) {
        String tid = CommonSecurityContextHolder.getTid();
        BackendEntity entity = backendService.findById(id);
        if (Objects.isNull(entity)) {
            throw new RealmException("realmException", RealmErrorKey.PARAM_INVALID);
        }
        Specification<ApplicationEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("tid"), tid));
            predicates.add(cb.equal(root.get("appInnerName"), entity.getAppInnerName()));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
            return query.getRestriction();
        };
        boolean exist = applicationService.getRepository().exists(spec);
        if (exist) {
            throw new RealmException("realmException", RealmErrorKey.BACKEND_HAS_REF);
        }

        backendService.deleteById(id);
        return Result.success(Feedback.OK, true);
    }

    @Operation(summary = "修改后台", description = "修改后台")
    @PostMapping("/update")
    public Result<BackendEntity> update(@Valid @RequestBody BackendEntity entity) {
        String tid = CommonSecurityContextHolder.getTid();
        BackendEntity backendEntity = backendService.findById(entity.getId());

        Specification<ApplicationEntity> spec = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("tid"), tid));
            predicates.add(cb.equal(root.get("appInnerName"), backendEntity.getAppInnerName()));

            Predicate[] predicateArray = new Predicate[predicates.size()];
            query.where(cb.and(predicates.toArray(predicateArray)));
            return query.getRestriction();
        };
        if (backendEntity.getAppInnerName().equals(entity.getAppInnerName())) {
        }
        else {
            boolean exist = applicationService.getRepository().exists(spec);
            if (exist)  {
                throw new RealmException("realmException", RealmErrorKey.BACKEND_HAS_REF, "不允许修改内部名称");
            }
        }

        BackendEntity ret = backendService.save(entity);
        return Result.success(Feedback.OK, ret);
    }

    @Operation(summary = "查询后台", description = "后台列表")
    @PostMapping("/list")
    public Result<PageResponse> list(@Valid @RequestBody BaseQueryModel model) {
        PageResponse ret = backendService.list(model);
        return Result.success(Feedback.OK, ret);
    }


}
