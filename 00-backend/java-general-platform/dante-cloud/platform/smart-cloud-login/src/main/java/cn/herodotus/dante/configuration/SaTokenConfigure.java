package cn.herodotus.dante.configuration;

import cn.herodotus.dante.starter.interceptor.SaInterceptor;
import cn.herodotus.engine.cache.redisson.annotation.EnableHerodotusRedisson;
import com.google.common.collect.Lists;
import org.redisson.api.RedissonClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.List;


/**
 * [Sa-Token 权限认证] 配置类 
 * @author click33
 *
 */
@EnableHerodotusRedisson
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

	@Resource
	private RedissonClient redissonClient;
	/**
	 * 注册 Sa-Token 拦截器打开注解鉴权功能  
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 注册 Sa-Token 拦截器打开注解鉴权功能
		List<String> excludes = Lists.newArrayList();
		excludes.add("/sso/sa-res/**");
		excludes.add("/sso/login.js");
		excludes.add(" sso/jquery.min.js");

		registry.addInterceptor(new SaInterceptor()).order(1).addPathPatterns("/**").excludePathPatterns(excludes);
	}


}
