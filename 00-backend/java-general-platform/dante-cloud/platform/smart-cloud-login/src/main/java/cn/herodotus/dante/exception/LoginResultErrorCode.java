package cn.herodotus.dante.exception;


import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "响应结果状态", description = "自定义错误码以及对应的、友好的错误信息")
public enum LoginResultErrorCode {
    LOGIN_PARAM_INVALID(LoginErroCode.LOGIN_PARAM_INVALID, "参数错误: ");



    @Schema(title = "结果代码")
    private final int code;
    @Schema(title = "结果信息")
    private final String message;


    LoginResultErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
