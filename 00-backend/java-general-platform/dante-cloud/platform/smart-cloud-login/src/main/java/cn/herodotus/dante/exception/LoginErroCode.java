package cn.herodotus.dante.exception;

import cn.herodotus.dante.core.exception.ErrorCodeDefine;

public interface LoginErroCode {

    int BASE = ErrorCodeDefine.LOGIN_STAET;
    int LOGIN_PARAM_INVALID = BASE + 1;

}
