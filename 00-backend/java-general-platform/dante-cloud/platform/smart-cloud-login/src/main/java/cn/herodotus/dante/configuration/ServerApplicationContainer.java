package cn.herodotus.dante.configuration;

import cn.herodotus.dante.realm.constant.RealmConstant;
import cn.herodotus.dante.realm.entity.ApplicationEntity;
import cn.herodotus.dante.realm.service.ApplicationService;
import com.google.common.base.Strings;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Slf4j
@Component
public class ServerApplicationContainer implements InitializingBean, DisposableBean {
    public static ServerApplicationContainer INST;

    // 管理端和用户端默认clientId
    private String defaultMgtClient;
    private String defaultUserClient;

    // 管理端和用户端默认redirect
    private String defaultMgtRedirect;
    private String defaultUserRedirect;

    private String defaultMgtQueryString;
    private String defaultUserQueryString;




    @Resource
    private ApplicationService applicationService;

    public String getDefaultMgtClient() {
        return defaultMgtClient;
    }

    public String getDefaultUserClient() {
        return defaultUserClient;
    }

    public String getDefaultMgtRedirect() {
        return defaultMgtRedirect;
    }

    public String getDefaultUserRedirect() {
        return defaultUserRedirect;
    }

    public String getDefaultMgtQueryString() {
        return defaultMgtQueryString;
    }

    public String getDefaultUserQueryString() {
        return defaultUserQueryString;
    }

    public String getDefaultClient(String appType) {
        return RealmConstant.APP_TYPE_MGT.equals(appType) ? defaultMgtClient: defaultUserClient;
    }
    public String getDefaultRedirect(String appType) {
        if (RealmConstant.APP_TYPE_MGT.equals(appType)) {
            return defaultMgtRedirect;
        }
        else if (RealmConstant.APP_TYPE_APP.equals(appType)) {
            return defaultUserRedirect;
        }
        log.error(">>> invalid appType: {}", appType);
        return "";
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        ServerApplicationContainer.INST = this;
        mgtAppExtracted();
        userAppExtracted();
    }

    private void userAppExtracted() {
        ApplicationEntity userApp = applicationService.findByServer(RealmConstant.APP_TYPE_APP, 1);
        if (Objects.isNull(userApp)) {
            log.error("[ServerApplicationContainer], 请先配置SSO Server USER 应用");
        }
        else {
            defaultUserClient = userApp.getClientId();
            if (Strings.isNullOrEmpty(userApp.getRedirectUris())) {
                log.error("[ServerApplicationContainer], 请先配置App Server Uris");
            }
            else {
                defaultUserRedirect = userApp.getRedirectUris().split(";")[0];
                defaultUserQueryString
                        = "?client=" + defaultUserClient + "&redirect=" + defaultUserRedirect;
            }
        }
    }

    private void mgtAppExtracted() {
        ApplicationEntity mgtApp = applicationService.findByServer(RealmConstant.APP_TYPE_MGT, 1);

        if (Objects.isNull(mgtApp)) {
            log.error("[ServerApplicationContainer], 请先配置SSO Server MGT 应用");
        } else {
            defaultMgtClient = mgtApp.getClientId();
            if (Strings.isNullOrEmpty(mgtApp.getRedirectUris())) {
                log.error("[ServerApplicationContainer], 请先配置App Server Uris");
            } else {
                defaultMgtRedirect = mgtApp.getRedirectUris().split(";")[0];
                defaultMgtQueryString
                        = "?client=" + defaultMgtClient + "&redirect=" + defaultMgtRedirect;
            }
        }
    }

    @Override
    public void destroy() throws Exception {

    }

}
