/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Cloud Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Cloud 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://gitee.com/dromara/dante-cloud
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/dromara/dante-cloud
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.sdk.configuration;

import cn.herodotus.dante.sdk.configuration.AppClientAuthCheckService;
import cn.herodotus.dante.sdk.configuration.AppClientAuthInterceptors;
import cn.herodotus.dante.sdk.service.IAuthInterspect;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration(proxyBeanMethods = false)
@EntityScan( basePackages = {
})
@EnableJpaRepositories(
        basePackages = "cn.herodotus.dante.sdk.repository"
)
@ComponentScan(basePackages = {
        "cn.herodotus.dante.sdk.service",
})
//@EnableHerodotusRedisson
@AutoConfiguration
@EnableJpaAuditing
public class AppClientConfiguration {

    private static final Logger log = LoggerFactory.getLogger(AppClientAuthInterceptors.class);

    @Resource
    private IAuthInterspect authInterspect;
    @Resource
    private SymmetricCryptoProcessor symmetricCryptoProcessor;

    @PostConstruct
    public void postConstruct() {
        log.info("[Smart] |- Service [AppClient SDK Ability] Auto Configure.");
    }

    @Bean
    public AppClientAuthCheckService appClientAuthCheckService() {
        return new AppClientAuthCheckService(authInterspect, symmetricCryptoProcessor);
    }

    @Bean
    @ConditionalOnMissingBean
    public AppClientAuthInterceptors appClientAuthInterceptors() {
        return new AppClientAuthInterceptors(appClientAuthCheckService());
    }


}
