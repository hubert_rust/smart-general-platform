package cn.herodotus.dante.sdk.service;

import cn.herodotus.dante.core.auth.CommonBaseUser;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.domain.Result;
import com.dtflys.forest.exceptions.ForestRuntimeException;
import com.dtflys.forest.http.ForestRequest;
import com.dtflys.forest.http.ForestResponse;
import com.dtflys.forest.interceptor.Interceptor;
import com.google.common.base.Strings;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author root
 * @description TODO
 * @date 2023/10/18 8:54
 */

public class AccessForestInterceptor implements Interceptor<Result<CommonBaseUser>> {
    @Override
    public boolean beforeExecute(ForestRequest request) {
        HttpServletRequest srcRequest = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        String cookie = srcRequest.getHeader(HttpHeaders.COOKIE);
        String smartToken = srcRequest.getHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_SMART_TOKEN);

        String cid = srcRequest.getHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_CID);
        String protocol = srcRequest.getHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_PROTOCOL);
        if ("jwt".equals(protocol)) {
            String token = srcRequest.getHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_JWT_TOKEN);
            request.addHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_JWT_TOKEN, token);
            request.addHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_PROTOCOL, protocol);
            request.addHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_CID, cid);
        }
        if (!Strings.isNullOrEmpty(smartToken)) {
            request.addHeader(cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders.HTTP_SMART_TOKEN, smartToken);
        }
        if (!Strings.isNullOrEmpty(cookie)) {
            request.addHeader(HttpHeaders.COOKIE, cookie);
        }
        return Interceptor.super.beforeExecute(request);
    }

    @Override
    public void onSuccess(Result<CommonBaseUser> data, ForestRequest request, ForestResponse response) {
        Interceptor.super.onSuccess(data, request, response);
    }

    @Override
    public void onError(ForestRuntimeException ex, ForestRequest request, ForestResponse response) {
        Interceptor.super.onError(ex, request, response);
    }
}
