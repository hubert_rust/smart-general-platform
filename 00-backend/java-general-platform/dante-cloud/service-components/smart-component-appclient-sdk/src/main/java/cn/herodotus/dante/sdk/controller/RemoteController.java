package cn.herodotus.dante.sdk.controller;

import cn.herodotus.dante.core.auth.CommonBaseUser;
import cn.herodotus.dante.sdk.service.IUserInfoService;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.dtflys.forest.http.ForestResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Tag(name = "开放接口")
@Validated
public class RemoteController {
    @Resource
    private IUserInfoService userInfoService;

    @Operation(summary = "平台认证模式", description = "平台认证模式")
    @PostMapping("/userInfo")
    public Object userInfo() {
        ForestResponse<Result<CommonBaseUser>> obj = userInfoService.userInfo();
        Result<CommonUserDetails> userResult
                = JSON.parseObject(obj.getContent(),
                new TypeReference<Result<CommonBaseUser>>() {}.getType());

        return obj.getContent();
        //return Result.success(Feedback.OK, obj);
    }
}
