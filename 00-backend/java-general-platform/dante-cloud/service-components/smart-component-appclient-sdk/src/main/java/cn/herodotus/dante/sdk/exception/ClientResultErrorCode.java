package cn.herodotus.dante.sdk.exception;


import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "响应结果状态", description = "自定义错误码以及对应的、友好的错误信息")
public enum ClientResultErrorCode {
    AUTH_SERVER_INFO(ClientErroCode.AUTH_SERVER_INFO, "鉴权服务器: ");



    @Schema(title = "结果代码")
    private final int code;
    @Schema(title = "结果信息")
    private final String message;


    ClientResultErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
