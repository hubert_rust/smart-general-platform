package cn.herodotus.dante.sdk.service;

import cn.herodotus.dante.core.auth.CommonBaseUser;
import cn.herodotus.dante.core.domain.IntrospectModel;
import cn.herodotus.dante.core.domain.IntrospectRespModel;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Result;
import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dtflys.forest.annotation.Var;
import com.dtflys.forest.http.ForestResponse;
import org.springframework.stereotype.Service;

/**
 * @author root
 * @description TODO
 * @date 2023/10/18 9:32
 */
// 也可以在配置文件中配置拦截器
@Service
@BaseRequest(baseURL = "${thirdApi.baseUrl}", interceptor = AccessForestInterceptor.class)
public interface IAuthInterspect {
    @Post(value = "/sso/introspect/${nonce}")
    ForestResponse<Result<CommonBaseUser>> interspect(@Var("nonce") String nonce, @JSONBody IntrospectModel model);
}
