package cn.herodotus.dante.sdk.controller;

import cn.herodotus.dante.model.OperationModel;
import cn.herodotus.dante.model.RemoteBaseModel;
import cn.herodotus.dante.scan.entity.OperationEntity;
import cn.herodotus.dante.scan.service.OpenServiceImpl;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springdoc.core.service.OperationService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*
@RestController
@RequestMapping("/open")
@Tag(name = "开放接口")
@Validated
public class OpenController {
    @Resource
    private OpenServiceImpl openService;

    @Operation(summary = "获取应用基础API数据", description = "获取应用基础API数据")
    @PostMapping("/apis")
    public Object apis(@RequestBody RemoteBaseModel model) {

        List<OperationModel> list = openService.operations(model.getKey());
        return Result.success(Feedback.OK, list);
    }
}
*/
