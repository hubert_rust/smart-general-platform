package cn.herodotus.dante.sdk.configuration;


import cn.herodotus.dante.core.exception.AuthException;
import cn.herodotus.dante.core.exception.ClientException;
import cn.herodotus.dante.sdk.exception.ClientErrorKey;
import cn.herodotus.dante.service.IAuthCheck;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Result;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;


//HandlerInterceptorAdapter需要继承，HandlerInterceptor需要实现
//建议使用HandlerInterceptorAdapter，因为可以按需进行方法的覆盖。

@Slf4j
public class AppClientAuthInterceptors implements HandlerInterceptor {//HandlerInterceptorAdapter {
    private IAuthCheck authCheck;

    public AppClientAuthInterceptors(IAuthCheck authCheck) {
        this.authCheck = authCheck;
    }


    /*
     * 预处理回调方法,实现处理器的预处理。
     * 第三个参数为响应的处理器,自定义Controller,返回值为true表示继续流程（如调用下一个拦截器或处理器）
     * 或者接着执行postHandle()和afterCompletion()；false表示流程中断，不会继续调用其他的拦截器或处理器，中断执行
     * */
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object object) throws Exception {
        if (object instanceof ResourceHttpRequestHandler) {
            return true;
        }

        /*try {*/
        Result<CommonUserDetails> result = authCheck.introspect(request);
        if (result.getCode() == 0) {
            log.info(">>> AppClientAuthInterceptors, result: {}", request.toString());
            CommonSecurityContextHolder.setUser(result.getData());
            return true;
        } else {
            throw new ClientException("clientException", ClientErrorKey.AUTH_SERVER_INFO,
                    result.getMessage());

        }
       /* } catch (Exception e) {
            e.printStackTrace();
            throw new AuthException("authException", e.getMessage());
        }*/
    }


    /*
     * 后处理回调方法，实现处理器的后处理（DispatcherServlet进行视图返回渲染之前进行调用）
     * 此时我们可以通过modelAndView（模型和视图对象）对模型数据进行处理或对视图进行处理，
     * modelAndView也可能为null。
     * */
    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object o, ModelAndView modelAndView) throws Exception {
    	/*if (response.getStatus() == 500) {
			modelAndView.setViewName("/errorpage/500");
            request.getRequestDispatcher("500.html").forward(request, response);
		} else if (response.getStatus() == 404) {
			modelAndView.setViewName("/errorpage/404");
            request.getRequestDispatcher("404.html").forward(request, response);
		}*/
    }

    /*
     * 整个请求处理完毕回调方法,该方法也是需要当前对应的Interceptor的preHandle()的返回值为true时才会执行.
     * 也就是在DispatcherServlet渲染了对应的视图之后执行。
     * 用于进行资源清理。整个请求处理完毕回调方法。
     * 如性能监控中我们可以在此记录结束时间并输出消耗时间，还可以进行一些资源清理，
     * 类似于try-catch-finally中的finally，但仅调用处理器执行链中
     * */
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {

        CommonSecurityContextHolder.clearUserDetail();
    }
}
