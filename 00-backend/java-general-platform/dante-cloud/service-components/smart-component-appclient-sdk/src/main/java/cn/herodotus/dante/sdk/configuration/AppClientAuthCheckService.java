package cn.herodotus.dante.sdk.configuration;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.auth.CommonBaseUser;
import cn.herodotus.dante.core.constant.CoreConst;
import cn.herodotus.dante.core.domain.IntrospectModel;
import cn.herodotus.dante.sdk.service.IAuthInterspect;
import cn.herodotus.dante.service.IAuthCheck;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import com.dtflys.forest.http.ForestResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.util.Strings;

/**
 * @author root
 * @description TODO
 * @date 2023/10/18 11:43
 */
// 各应用到鉴权中心鉴权
public class AppClientAuthCheckService implements IAuthCheck {

    private IAuthInterspect authInterspect;
    private SymmetricCryptoProcessor symmetricCryptoProcessor;

    public AppClientAuthCheckService(IAuthInterspect authInterspect,
                                     SymmetricCryptoProcessor symmetricCryptoProcessor) {
        this.authInterspect = authInterspect;
        this.symmetricCryptoProcessor = symmetricCryptoProcessor;
    }



    @Override
    public Result<CommonUserDetails> introspect(HttpServletRequest request) {
        try {
            String requestUrl = String.valueOf(request.getAttribute("org.springframework.web.servlet.HandlerMapping.bestMatchingPattern"));
            String requestMethod = request.getMethod();
            String digest = IDContainer.INST.getMD5(requestMethod + requestUrl);
            String cid = CommonSecurityContextHolder.getCid();
            String nonce = IDContainer.INST.getSnowFlakeString();
            IntrospectModel model = new IntrospectModel();
            model.setClientId(cid);
            model.setRequestUrl(requestUrl);
            model.setRequestMethod(requestMethod);
            model.setDigest(digest);
            String val = symmetricCryptoProcessor.encrypt(nonce,
                    ProtectConstant.SM4_SECRET);
            model.setNonce(val);
            ForestResponse<Result<CommonBaseUser>> result = authInterspect.interspect(nonce, model);
            //

            Result<CommonUserDetails> userResult
                    = JSON.parseObject(result.getContent(),
                    new TypeReference<Result<CommonBaseUser>>() {}.getType());

            return userResult;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
