package cn.herodotus.dante.sdk.idaas.controller;


import cn.herodotus.dante.core.auth.CommonBaseUser;
import cn.herodotus.dante.sdk.idaas.service.IDaasGetDataService;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.dtflys.forest.http.ForestResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/open")
@Tag(name = "开放接口")
@Validated
public class RemoteIDaasOpenController {

    @Resource
    private IDaasGetDataService iDaasGetDataService;


    @Operation(summary = "用户所在群组列表", description = "用户所在群组列表")
    @PostMapping("/myGroups")
    public Result<List<Object>> myGroups() {
        ForestResponse<Result<List<Object>>> resp = iDaasGetDataService.myGroups();
        Result<List<Object>> result
                = JSON.parseObject(resp.getContent(),
                new TypeReference<Result<List<Object>>>() {}.getType());
        return result;
    }
    @Operation(summary = "用户所在群组列表", description = "用户所在群组列表")
    @PostMapping("/deptTree")
    public Result<List<Object>> deptTree() {
        ForestResponse<Result<List<Object>>> resp = iDaasGetDataService.deptTree();
        Result<List<Object>> result
                = JSON.parseObject(resp.getContent(),
                new TypeReference<Result<List<Object>>>() {}.getType());
        return result;
    }
}
