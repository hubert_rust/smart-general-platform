package cn.herodotus.dante.scan.service;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.constant.CoreConst;
import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.scan.constant.OperationConstant;
import cn.herodotus.dante.scan.entity.OperationEntity;
import cn.herodotus.dante.scan.repository.OperationRepository;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.collect.Lists;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class OperationService extends BaseService<OperationEntity, String> {
    private OperationRepository operationRepository;

    @Autowired
    public OperationService(OperationRepository operationRepository) {
        this.operationRepository = operationRepository;
    }

    @Override
    public BaseRepository<OperationEntity, String> getRepository() {
        return this.operationRepository;
    }

    public OperationEntity getOne(String id) {
        return super.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String id) {
        OperationEntity find = super.findById(id);
        find.setDeleted(IDContainer.INST.getUUID(20, false));
        super.save(find);
        return true;
    }
   /* @Transactional(rollbackFor = Exception.class)
    public OperationEntity update(OperationEntity entity) {
        OperationEntity find = super.findById(entity.getId());
        if (!find.getOperationType().equals(entity.getOperationType())) {
            throw new RealmException(RealmErrorKey.REQUEST_METHOD_NOT_UPDATE);
        }
        //更新了接口类型（processType)

        return super.save(entity);
    }
    @Transactional(rollbackFor = Exception.class)
    public OperationEntity save(OperationEntity entity) {

        if (!entity.getParentId().equals(RealmConstant.PARENT_ID_DEFAULT)) {
            OperationEntity find = super.findById(entity.getParentId());
            if (Objects.isNull(find)) {
                throw new RealmException(RealmErrorKey.PARENT_NOT_EXIST);
            }
        }

        //如果digest一直，则重复
        if (OperationConstant.OPERATION_TYPE_API.equals(entity.getOperationType())) {
            String digest = OperationEntity.getDigest(entity);
            List<OperationEntity> list = operationRepository.findByAppInnerNameAndDigest(entity.getAppInnerName(), digest);
            if (CollectionUtils.isNotEmpty(list)) {
                throw new RealmException(RealmErrorKey.API_HAS_EXIST);
            }
            entity.setDigest(digest);
        }
        return super.save(entity);
    }*/
    public List<OperationEntity> findByTag(String tag, boolean tree) {
        List<OperationEntity> list
                = operationRepository.findByAppInnerNameAndDeleted(tag,
                CoreConst.DELETED_VALUE_DEFAULT);
        if (!tree) {
            return list;
        }
        DefaultTreeBuildFactory<OperationEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<OperationEntity> ret = factory1.doTreeBuild(list);
        return ret;
    }

    //增加或者修改接口的时候用到
    public List<OperationEntity> findCatalog(String tag) {
        List<OperationEntity> list
                = operationRepository.findByAppInnerNameAndDeletedAndOperationType(tag,
                CoreConst.TID_VALUE_DEFAULT,
                OperationConstant.OPERATION_TYPE_CATALOG);
        DefaultTreeBuildFactory<OperationEntity> factory1 = new DefaultTreeBuildFactory<>();
        factory1.setRootParentId("-1");
        List<OperationEntity> ret = factory1.doTreeBuild(list);
        return ret;
    }

    //启动后自动扫描接口使用
    public boolean deleteOperation(String contextPath) {
        this.operationRepository.deleteOperation(contextPath);
        return true;
    }

    public List<OperationEntity> getListInDigest(String contextPath,
                                                 Collection<String> collection) {
        if (collection.isEmpty()) {
            return Lists.newArrayList();
        }
        Specification<OperationEntity> spec = new Specification<OperationEntity>() {
            @Override
            public Predicate toPredicate(Root<OperationEntity> root,
                                         CriteriaQuery<?> query,
                                         CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(cb.in(root.get("digest").in(collection)));
                predicates.add(cb.equal(root.get("context_path"), contextPath));

                Predicate[] predicateArray = new Predicate[predicates.size()];
                query.where(cb.and(predicates.toArray(predicateArray)));
                return query.getRestriction();
            }
        };
        return super.findAll(spec);
    }



    public List<OperationEntity> findByAppInnerName(String appInnerName) {
        return operationRepository.findByAppInnerNameAndDeleted(appInnerName, "0");
    }
    public List<OperationEntity> findByAppInnerNameAndDigest(String appInnerName, List<String> digests) {
        return operationRepository.findByDigestInAndAppInnerName(digests, appInnerName);
    }

}
