package cn.herodotus.dante.scan.service;

import cn.herodotus.dante.model.OperationModel;
import cn.herodotus.dante.scan.entity.OperationEntity;
import cn.herodotus.dante.service.IBackendOperationService;
import com.google.common.collect.Lists;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/12 18:10
 */
@Slf4j
@Service
public class OpenServiceImpl implements IBackendOperationService {

    @Resource
    private OperationService operationService;

    @Override
    public List<OperationModel> operations(String appInnerName) {
        List<OperationEntity> list = operationService.findByAppInnerName(appInnerName);
        List<OperationModel> ret = Lists.newArrayList();
        for (OperationEntity entity : list) {
            OperationModel model = new OperationModel();
            BeanUtils.copyProperties(entity, model);
            ret.add(model);
        }
        return ret;
    }
}
