package cn.herodotus.dante.scan.constant;

/**
 * @author root
 * @description TODO
 * @date 2023/10/17 16:07
 */
public interface OperationConstant {
    //接口
    String OPERATION_TYPE_CATALOG = "catalog";
    String OPERATION_TYPE_API = "api";

    //接口控制类型
    String API_PROCESS_LOGIN = "login";
    String API_PROCESS_ANONYMOUS = "anonymous";
    String API_PROCESS_CONTROL = "control";
}
