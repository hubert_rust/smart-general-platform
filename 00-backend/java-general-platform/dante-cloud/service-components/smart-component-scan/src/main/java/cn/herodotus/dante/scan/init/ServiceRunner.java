package cn.herodotus.dante.scan.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/*
 * @Author root
 * @Description //TODO 
 * @Date 16:22 2023/10/17
 * @Param 
 * @return 
 **/
@Component
@Slf4j
public class ServiceRunner implements CommandLineRunner {
    @Autowired
    OperationLoaderService operaionLoaderService;

    @Async
    @Override
    public void run(String... args) {
        operaionLoaderService.loadOperation();
    }


}
