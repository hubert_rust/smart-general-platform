package cn.herodotus.dante.scan.init;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.core.constant.ConfigConstant;
import cn.herodotus.dante.core.constant.CoreConst;
import cn.herodotus.dante.scan.constant.OperationConstant;
import cn.herodotus.dante.scan.entity.OperationEntity;
import cn.herodotus.dante.scan.service.OperationService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

@Slf4j
@Service
public class OperationLoaderService {
    private final ApplicationContext applicationContext;
    private final OperationService operationService;

    private static final String DEFALUT_REQUEST_MODE = "REQUEST";
    private static final String DEFALUT_OPERATION_TYPE = "operation";
    private static final String OPERATION_PATH_SPLIT = "-";
    private static final String DEFAULT_OPERATION_PATH = "-1";

    //private static String appInnerName = "";

    private static List<String> ignorRequest = Lists.newArrayList();

    static {
        ignorRequest.add("/swagger-resources");
        ignorRequest.add("/swagger-resources/configuration/security");
        ignorRequest.add("/swagger-resources/configuration/ui");
        ignorRequest.add("/sso");
        ignorRequest.add("/${server.error.path:${error.path:error}}");
    }

    @Autowired
    public OperationLoaderService(ApplicationContext applicationContext,
                                  OperationService operationService) {
        this.applicationContext = applicationContext;
        this.operationService = operationService;
    }

    public Integer loadOperation() {
        return getOperation();
    }

    public Integer loadRemoteOperation() {
        List<Class<?>> list = remoteRequestScan();
        for (Class<?> aClass : list) {

        }
        return null;
    }
    public List<Class<?>> remoteRequestScan() {
        List<Class<?>> classList = new ArrayList<>();

        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        final String BASE_PACKAGE = "com.cloud";
        final String RESOURCE_PATTERN = "/**/*.class";
        try {
            String pattern = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                    ClassUtils.convertClassNameToResourcePath(BASE_PACKAGE) + RESOURCE_PATTERN;
            Resource[] resources = resourcePatternResolver.getResources(pattern);
            //MetadataReader 的工厂类
            MetadataReaderFactory readerfactory = new CachingMetadataReaderFactory(resourcePatternResolver);
            for (Resource resource : resources) {
                //用于读取类信息
                MetadataReader reader = readerfactory.getMetadataReader(resource);
                //扫描到的class
                String classname = reader.getClassMetadata().getClassName();
                //log.info(">>> classname: {}", classname);
                Class<?> clazz = Class.forName(classname);
                //判断是否有指定主解
                if (clazz.isAnnotationPresent(FeignClient.class)) {
                    classList.add(clazz);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
        }
        return classList;
    }


    public Integer getOperation() {
        String contentUrl = getContentUrl();
        String contextPath = contentUrl.replaceAll("/", "");
        String appName = getSpringApplicationName();

        Map<String, OperationEntity> curMap = Maps.newConcurrentMap();

        //一个后台中可能包含多个应用
        Map<String, Object> controllerBeanMap = applicationContext.getBeansWithAnnotation(Controller.class);
        operationService.deleteOperation(appName);

        List<OperationEntity> loadList = Lists.newArrayList();
        for (String beanName : controllerBeanMap.keySet()) {
            //2023-01-06: 多应用打包到一起，应用分开后
            Class<?> controllerClass = applicationContext.getBean(beanName).getClass();

            String classUrl = getClassUrl(contextPath, contentUrl, controllerClass);

            RequestMapping requestMapping = AnnotationUtils.findAnnotation(controllerClass, RequestMapping.class);
            if (Objects.isNull(requestMapping)) {
                log.error("[INIT] not find RequestMapping in: {}", controllerClass.getName());
                continue;
            }
            String name = "";
            Tags annotation = AnnotationUtils.findAnnotation(controllerClass, Tags.class);
            if (annotation != null) {
                Tag[] tag = annotation.value();
                name = "[" + tag[0].name() + "]";
            }
            else {
                Tag tag = AnnotationUtils.findAnnotation(controllerClass, Tag.class);
                if (tag != null) {
                    name = "["+ tag.name()+"]";
                }
                else {
                    log.error("[INIT] not find Tag or Tags in: {}", controllerClass.getName());
                }
            }
            OperationEntity operationEntity = new OperationEntity();
            String parentId = IDContainer.INST.getUUID(16, false);
            operationEntity.setId(parentId);
            operationEntity.setTid(CoreConst.TID_VALUE_DEFAULT);
            operationEntity.setOperationName(name);
            operationEntity.setOperationType(OperationConstant.OPERATION_TYPE_CATALOG);
            operationEntity.setRequestUrl(classUrl);
            operationEntity.setParentId("-1");
            operationEntity.setDeleted(CoreConst.DELETED_VALUE_DEFAULT);
            operationEntity.setProcessType(OperationConstant.API_PROCESS_CONTROL);
            operationEntity.setOperationPath(parentId+"");
            operationEntity.setRequestMode("");
            //operationEntity.setPass(false);
            operationEntity.setAppInnerName(appName);
            operationEntity.setDigest(getDigest(operationEntity));

            Optional<String> opt = ignorRequest.stream()
                    .filter(p->operationEntity.getRequestUrl().contains(p))
                    .findFirst();
            if (!opt.isPresent()) {
                loadList.add(operationEntity);
            }
            // 遍历 methodUrl
            Method[] methods = controllerClass.getMethods();
            for (Method method : methods) {
                OperationEntity methodOperationEntity
                        = getMethodOperationEntity(parentId, classUrl, method, appName);

                Optional<String> op = ignorRequest.stream()
                        .filter(p->operationEntity.getRequestUrl().contains(p))
                        .findFirst();
                if (methodOperationEntity != null && !op.isPresent()) {
                    loadList.add(methodOperationEntity);
                }
            }
        }

        //保持现有的配置
        /*for (OperationEntity operationEntity : loadList) {
            String digest = operationEntity.getDigest();
            OperationEntity entity = curMap.get(digest);
            if (Objects.nonNull(entity)) {
                if (Strings.isNullOrEmpty(operationEntity.getOperationName())) {
                    operationEntity.setOperationName(entity.getOperationName());
                    operationEntity.setRelease(entity.getRelease());
                }
            }
        }*/
        // 全部重新保存即可
        operationService.saveAll(loadList);
        return loadList.size();
    }

    private String getDigest(OperationEntity operationEntity) {
        // String operationName = Strings.isNullOrEmpty(operationEntity.getOperationName()) ? " " : operationEntity.getOperationName();
        // digest = requestMethod + requestUrl;
        String requestMode = Strings.isNullOrEmpty(operationEntity.getRequestMode()) ? " " : operationEntity.getRequestMode();
        return IDContainer.INST.getMD5(requestMode + operationEntity.getRequestUrl());
    }



    private String getSpringApplicationName() {
        Environment env = applicationContext.getEnvironment();
        String name = env.getProperty(ConfigConstant.SPRING_APP_NAME);
        if (Strings.isNullOrEmpty(name)) {
            log.error("[INIT] spring.application.name must has a value");
            System.exit(0);
        }
        return name;
    }
    private String getContentUrl() {
        Environment env = applicationContext.getEnvironment();
        String contentUrl = env.getProperty(ConfigConstant.CONTEXT_PATH);
        if (StringUtils.isBlank(contentUrl) || Objects.equals(contentUrl, "/")) {
            return "/";
        }
        if (!contentUrl.startsWith("/")) {
            contentUrl = "/" + contentUrl;
        }
        if (contentUrl.endsWith("/")) {
            contentUrl = contentUrl.substring(0, contentUrl.length() - 1);
        }
        return contentUrl;
    }

    //realAppInnerName: 真正应用
    private String getClassUrl(String realAppInnerName,
                               String contentUrl,
                               Class<?> controllerClass) {
        RequestMapping requestMapping = AnnotationUtils.findAnnotation(controllerClass, RequestMapping.class);
        if (requestMapping == null) {
            return "";
        }

        String classUrl = requestMapping.value()[0];
        if (classUrl == null /*|| Objects.equals(contentUrl, "/")*/) {
            throw new NullPointerException("{} 类未配置请求路径");
        }
        if (classUrl.indexOf("/")>=0) {
            classUrl = classUrl.replaceAll("/", "");
        }
        return "/" + classUrl;

    }


    private OperationEntity getMethodOperationEntity(String contentPathId,
                                                     String classUrl,
                                                     Method method,
                                                     String realAppInnerName) {
        PostMapping postMapping = AnnotationUtils.findAnnotation(method, PostMapping.class);
        GetMapping getMapping = AnnotationUtils.findAnnotation(method, GetMapping.class);
        PutMapping putMapping = AnnotationUtils.findAnnotation(method, PutMapping.class);
        DeleteMapping deleteMapping = AnnotationUtils.findAnnotation(method, DeleteMapping.class);
        RequestMapping mapping = AnnotationUtils.findAnnotation(method, RequestMapping.class);
        String methodUrl = null;
        String simpleName = null;
        if (postMapping != null) {
            methodUrl = Arrays.stream(postMapping.value()).findFirst().orElse("");
            simpleName = RequestMethod.POST.name();
        } else if (getMapping != null) {
            methodUrl = Arrays.stream(getMapping.value()).findFirst().orElse("");
            simpleName = RequestMethod.GET.name();
        } else if (putMapping != null) {
            methodUrl = Arrays.stream(putMapping.value()).findFirst().orElse("");
            simpleName = RequestMethod.PUT.name();
        } else if (deleteMapping != null) {
            methodUrl = Arrays.stream(deleteMapping.value()).findFirst().orElse("");
            simpleName = RequestMethod.DELETE.name();
        } else if (mapping != null) {  // mapping 顺序一定要在后面
            methodUrl = Arrays.stream(mapping.value()).findFirst().orElse("");
            simpleName = DEFALUT_REQUEST_MODE;
        }
        if (StringUtils.isEmpty(methodUrl)) {
            return null;
        }
        OperationEntity operationEntity = new OperationEntity();
        String id = IDContainer.INST.getUUID(16, false);
        operationEntity.setId(id);
        operationEntity.setParentId(contentPathId);
        operationEntity.setOperationPath(contentPathId + OPERATION_PATH_SPLIT + id);
        Operation apiOperation = AnnotationUtils.findAnnotation(method, Operation.class);
        String methodOperationName = "";
        if (apiOperation != null) {
            String value = apiOperation.summary();
            String remark = apiOperation.description();
            operationEntity.setRemark(remark);
            operationEntity.setOperationName(value);
            if (StringUtils.isBlank(value)) {
                log.error("[INIT], method: {} not find summary", methodOperationName);
                //methodOperationName = apiOperation.notes();
            }
        }

        String requestUrl = classUrl + "/" + methodUrl;
        requestUrl = requestUrl.replaceAll("//", "/");
        operationEntity.setRequestUrl(requestUrl);
        operationEntity.setRequestMode(simpleName);
        operationEntity.setTid(CoreConst.TID_VALUE_DEFAULT);
        operationEntity.setProcessType(OperationConstant.API_PROCESS_CONTROL);
        operationEntity.setDeleted(CoreConst.DELETED_VALUE_DEFAULT);
        //operationEntity.setPass(false);
        operationEntity.setAppInnerName(realAppInnerName);
        String digest = getDigest(operationEntity);
        operationEntity.setDigest(digest);
        operationEntity.setRelease(1);
        operationEntity.setOperationType(OperationConstant.OPERATION_TYPE_API);
        return operationEntity;
    }

}
