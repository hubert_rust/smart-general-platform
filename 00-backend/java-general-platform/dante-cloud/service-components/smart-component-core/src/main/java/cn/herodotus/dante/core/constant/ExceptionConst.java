package cn.herodotus.dante.core.constant;

/**
 * @author root
 * @description TODO
 * @date 2023/11/17 11:10
 */
public interface ExceptionConst {
    String EXCEPTION_AUTH = "authException";
    String EXCEPTION_REALM = "realmException";
    String EXCEPTION_LOGIN = "loginException";
}
