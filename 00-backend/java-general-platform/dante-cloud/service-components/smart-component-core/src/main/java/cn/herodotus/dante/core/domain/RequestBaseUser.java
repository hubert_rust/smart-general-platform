package cn.herodotus.dante.core.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/*
 * @param:
 * @return:
 * @Author: admin
 * @Date:  2021/8/9
 * @Description:
 **/
@Deprecated
@Data
@NoArgsConstructor
public class RequestBaseUser implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String uid;        //用户标识
    protected String nav;
    protected String tid;        //租户/组织
    protected Long cid;      //client id
    protected String appName;  //app name
    protected List<Long> rids;
    protected String domain;   //为了支持一个平台对接多个域名
    protected String permitCode;

    private String realName;
    private String deptCode;
    private String deptPath;

    private String account;
    private String userClass;
    private String type;
    private String subType;
    private String token;
    private String tsalt;
    private String rftoken;

}
