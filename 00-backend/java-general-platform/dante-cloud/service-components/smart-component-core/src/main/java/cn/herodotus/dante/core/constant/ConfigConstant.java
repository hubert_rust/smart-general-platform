package cn.herodotus.dante.core.constant;

public interface ConfigConstant {
    String SPRING_APP_NAME = "spring.application.name";
    String CONTEXT_PATH = "server.servlet.context-path";

    String HEADER_SMART_TOKEN = "Smart-Token";
    String HEADER_CLIENT_ID = "Cid";

    String HEADER_DEVICE_TYPE = "Device-Type";
    String HEADER_DEVICE_VAL_DESKTOP = "Desktop";
}
