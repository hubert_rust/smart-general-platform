/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.herodotus.dante.core.tree;

import lombok.EqualsAndHashCode;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 树构建的抽象类，定义构建tree的基本步骤
 *
 */
@EqualsAndHashCode(callSuper=false)
public abstract class AbstractTreeBuildFactory<T> {


    /**
     * 树节点构建整体过程
     *
     */
    public List<T> doTreeBuild(List<T> nodes) {

        //构建之前的节点处理工作
        List<T> readyToBuild = beforeBuild(nodes);

        //具体构建的过程
        List<T> builded = executeBuilding(readyToBuild);

        //构建之后的处理工作
        return afterBuild(builded);
    }

    public static void treeSort(List<Tree> list) {
        if (list == null || list.size()<=0) {
            return;
        }

        List<Tree> ret = list.stream()
                .sorted(Comparator.comparing(Tree::getOrder)).collect(Collectors.toList());
        list.clear();
        list.addAll(ret);
        list.stream().forEach(v->{
            List objs = v.getChildrenNode();
            List<Tree> pf = objs;
            treeSort(pf);
        });
    }
    /**
     * 构建之前的处理工作
     *
     */
    protected abstract List<T> beforeBuild(List<T> nodes);

    /**
     * 具体的构建过程
     *
     */
    protected abstract List<T> executeBuilding(List<T> nodes);

    /**
     * 构建之后的处理工作
     *
     */
    protected abstract List<T> afterBuild(List<T> nodes);
}