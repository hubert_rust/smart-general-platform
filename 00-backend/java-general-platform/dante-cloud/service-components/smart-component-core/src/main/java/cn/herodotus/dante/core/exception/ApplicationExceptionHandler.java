/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.core.exception;

import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.assistant.core.exception.GlobalExceptionHandler;
import cn.herodotus.engine.assistant.core.exception.PlatformException;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.util.Strings;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>Description: 统一异常处理器 </p>
 *
 * @author : gengwei.zheng
 * @date : 2019/11/18 8:12
 */
@Slf4j
@RestControllerAdvice
public class ApplicationExceptionHandler {

    private Map<String, CommonExceptionInterface> map = Maps.newConcurrentMap();
    private RealmExceptionInterface realmExceptionInterface;
    private LoginExceptionInterface loginExceptionInterface;
    private AuthExceptionInterface authExceptionInterface;

    private static final Map<String, Result<String>> EXCEPTION_DICTIONARY = new HashMap<>();

    static {
        /*EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.ACCESS_DENIED, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.ACCESS_DENIED));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.INSUFFICIENT_SCOPE, FeedbackFactory.getForbiddenResult(ResultErrorCodes.INSUFFICIENT_SCOPE));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.INVALID_CLIENT, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.INVALID_CLIENT));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.INVALID_GRANT, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.INVALID_GRANT));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.INVALID_REDIRECT_URI, FeedbackFactory.getPreconditionFailedResult(ResultErrorCodes.INVALID_REDIRECT_URI));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.INVALID_REQUEST, FeedbackFactory.getPreconditionFailedResult(ResultErrorCodes.INVALID_REQUEST));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.INVALID_SCOPE, FeedbackFactory.getPreconditionFailedResult(ResultErrorCodes.INVALID_SCOPE));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.INVALID_TOKEN, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.INVALID_TOKEN));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.SERVER_ERROR, FeedbackFactory.getInternalServerErrorResult(ResultErrorCodes.SERVER_ERROR));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.TEMPORARILY_UNAVAILABLE, FeedbackFactory.getServiceUnavailableResult(ResultErrorCodes.TEMPORARILY_UNAVAILABLE));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.UNAUTHORIZED_CLIENT, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.UNAUTHORIZED_CLIENT));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.UNSUPPORTED_GRANT_TYPE, FeedbackFactory.getNotAcceptableResult(ResultErrorCodes.UNSUPPORTED_GRANT_TYPE));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.UNSUPPORTED_RESPONSE_TYPE, FeedbackFactory.getNotAcceptableResult(ResultErrorCodes.UNSUPPORTED_RESPONSE_TYPE));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.UNSUPPORTED_TOKEN_TYPE, FeedbackFactory.getNotAcceptableResult(ResultErrorCodes.UNSUPPORTED_TOKEN_TYPE));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.ACCOUNT_EXPIRED, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.ACCOUNT_EXPIRED));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.BAD_CREDENTIALS, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.BAD_CREDENTIALS));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.CREDENTIALS_EXPIRED, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.CREDENTIALS_EXPIRED));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.ACCOUNT_DISABLED, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.ACCOUNT_DISABLED));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.ACCOUNT_LOCKED, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.ACCOUNT_LOCKED));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.ACCOUNT_ENDPOINT_LIMITED, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.ACCOUNT_ENDPOINT_LIMITED));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.USERNAME_NOT_FOUND, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.USERNAME_NOT_FOUND));
        EXCEPTION_DICTIONARY.put(OAuth2ErrorKeys.SESSION_EXPIRED, FeedbackFactory.getUnauthorizedResult(ResultErrorCodes.SESSION_EXPIRED));*/
    }

    @Autowired
    public ApplicationExceptionHandler(Map<String, CommonExceptionInterface> map) {
        map.forEach(this.map::put);
    }

    /**
     * Rest Template 错误处理
     *
     * @param ex       错误
     * @param request  请求
     * @param response 响应
     * @return Result 对象
     * @see <a href="https://www.baeldung.com/spring-rest-template-error-handling">baeldung</a>
     */
    @ExceptionHandler({HttpClientErrorException.class, HttpServerErrorException.class})
    public static Result<String> restTemplateException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        Result<String> result = resolveException(ex, request.getRequestURI());
        response.setStatus(result.getStatus());
        return result;
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public static Result<String> validationMethodArgumentException(MethodArgumentNotValidException ex, HttpServletRequest request, HttpServletResponse response) {
        return validationBindException(ex, request, response);
    }

    @ExceptionHandler({BindException.class})
    public static Result<String> validationBindException(BindException ex, HttpServletRequest request, HttpServletResponse response) {
        Result<String> result = resolveException(ex, request.getRequestURI());

        BindingResult bindingResult = ex.getBindingResult();
        FieldError fieldError = bindingResult.getFieldError();
        //返回第一个错误的信息
        if (ObjectUtils.isNotEmpty(fieldError)) {
            result.setMessage(fieldError.getDefaultMessage());
            result.validation(fieldError.getDefaultMessage(), fieldError.getCode(), fieldError.getField());
        }

        response.setStatus(result.getStatus());
        return result;
    }

    @ExceptionHandler({HibernateException.class})
    public Result<String> hibernateException(HibernateException ex,
                                             HttpServletRequest request,
                                             HttpServletResponse resp) {
        return Result.failure(ex.getMessage());

    }

    /*@Deprecated
    @ExceptionHandler({RealmException.class})
    public Result<String> exception(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        Result<String> result = realmExceptionInterface.resolveException(ex, request);
        response.setStatus(result.getStatus());
        return result;
    }
    @Deprecated
    @ExceptionHandler({CommonLoginException.class})
    public Result<String> loginException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        Result<String> result = loginExceptionInterface.resolveException(ex, request);
        response.setStatus(result.getStatus());
        return result;
    }*/
    /*@Deprecated
    @ExceptionHandler({AuthException.class})
    public Result<String> authException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        Result<String> result = authExceptionInterface.resolveException(ex, request);
        response.setStatus(result.getStatus());
        return result;
    }*/


    public static Result<String> resolveException(Exception ex, String path) {
        return GlobalExceptionHandler.resolveException(ex, path);
    }


    /*
     * @Author root
     * @Description //TODO 统一使用此接口
     * @Date 17:52 2023/10/17
     * @Param
     * @return
     **/
    @ExceptionHandler({PlatformException.class})
    public Result<String> platformException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        PlatformException platformException = (PlatformException) ex;
        String exName = platformException.getName();
        if (Strings.isNotEmpty(exName)) {
            if (Objects.nonNull(map.get(exName))) {
                Result<String> result = map.get(exName).resolveException(ex, request);
                response.setStatus(result.getStatus());
                return result;
            } else {

            }
        }

        return GlobalExceptionHandler.resolveException(ex, request.getRequestURL().toString());
    }

}
