package cn.herodotus.dante.core.constant;

/**
 * @author root
 * @description TODO
 * @date 2023/10/17 16:08
 */
public interface CoreConst {
    String TID_VALUE_DEFAULT = "0";
    String DELETED_VALUE_DEFAULT = "0";


    int SUCCESS_CODE = 0;
    // introspect 返回码
    // 非法请求
    int INTROSPECT_INVALID_REQ = 500;
    // 未登录
    int INTROSPECT_NOT_LOGIN = 501;
    // 用户无权限
    int INPROSPECT_NOT_PERMIT = 401;
}
