package cn.herodotus.dante.core.web;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public class ContextUtil {

    public static HttpServletRequest getCurrRequest() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            if (requestAttributes != null) {
                HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
                return request;
            }
        } catch (IllegalStateException e) {
        }
        return null;
    }

    public static HttpServletResponse getCurrResponse() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            if (requestAttributes != null) {
                HttpServletResponse response = ((ServletRequestAttributes) requestAttributes).getResponse();
                return response;
            }
        } catch (IllegalStateException e) {
        }
        return null;
    }
    
    public static String getParam(HttpServletRequest request, String name) {
		String v = request.getHeader(name);//SID是带了则取，否则不取
        if(v == null || v.length() == 0) {
        	v = request.getParameter(name);   
        	if(v == null || v.length() == 0) {
        		return null;
        	}
        }
        return v;
	}
    public static String getParam(String name) {
        HttpServletRequest request = getCurrRequest();
        String v = request.getHeader(name);//SID是带了则取，否则不取
        if(v == null || v.length() == 0) {
            v = request.getParameter(name);
            if(v == null || v.length() == 0) {
                return null;
            }
        }
        return v;
    }
}
