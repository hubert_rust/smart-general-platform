/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.herodotus.dante.core.tree;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 默认递归工具类，用于遍历有父子关系的节点，例如菜单树，字典树等等
 *
 * @author fengshuonan
 * @Date 2018/7/25 下午5:59
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class DefaultTreeBuildFactory<T extends Tree> extends AbstractTreeBuildFactory<T> {

    /**
     * 顶级节点的父节点id(默认-1)
     */
    private String rootParentId = "-1";
    private List<T> trees;
    // key对应的父节点
    private Map<String, T> maps = Maps.newHashMap();

    public T getParent(String key) {
        return maps.get(key);
    }

    /*
     * @Author root
     * @Description 外部接口
     * @Date 10:18 2023/8/30
     * @Param: sort=true 表示排序
     * @return
     **/
    public static List<Tree> treeBuild(List<Tree> list, String rootParentId, Boolean sort) {
        DefaultTreeBuildFactory<Tree> factory = new DefaultTreeBuildFactory<>();
        factory.setRootParentId("-1");
        List<Tree> ret = factory.doTreeBuild(list);
        if (sort) {
            treeSort(ret);
        }
        return ret;
    }

    /**
     * 查询子节点的集合
     *
     * @param totalNodes 所有节点的集合
     * @param node       被查询节点的id
     */
    private void buildChildNodes(List<T> totalNodes, T node) {
        if (totalNodes == null || node == null) {
            return;
        }

        List<T> nodeSubLists = getChildren(totalNodes, node);
        if (nodeSubLists.size() == 0) {
        } else {
            List<T> children = Lists.newArrayList();
            children.addAll(nodeSubLists);
            node.setChildrenNode(children);
            children.stream().forEach(v -> maps.put(v.getNodeId(), node));

            for (T nodeSub : nodeSubLists) {
                buildChildNodes(totalNodes, nodeSub);
            }
        }

    }

    /**
     * 获取子一级节点的集合
     *
     * @param list 所有节点的集合
     * @param node 被查询节点的model
     */
    private List<T> getChildren(List<T> list, T node) {
        return list.stream()
                .filter(p -> p.getNodeParentId().equals(node.getNodeId()))
                .collect(Collectors.toList());
    }

    @Override
    protected List<T> beforeBuild(List<T> nodes) {
        //默认不进行前置处理,直接返回
        return nodes;
    }

    @Override
    protected List<T> executeBuilding(List<T> nodes) {
/*        for (T treeNode : nodes) {
            this.buildChildNodes(nodes, treeNode);
        }*/
        nodes.forEach(v -> buildChildNodes(nodes, v));
        return nodes;
    }

    @Override
    protected List<T> afterBuild(List<T> nodes) {
        //去掉所有的二级节点
        List<T> ret = nodes.stream().filter(p -> {
            return p.getNodeParentId() == null || p.getNodeParentId().equals(rootParentId);
        }).collect(Collectors.toList());
        this.trees = ret;
        return ret;
    }

    //获取树的最后一个节点
    public void getLastNodes(List<Tree> nodes, List<Tree> retList) {
        nodes.stream().forEach(v -> {
            if (CollectionUtils.isEmpty(v.getChildrenNode())) {
                retList.add(v);
            } else {
                getLastNodes(v.getChildrenNode(), retList);
            }
        });
    }

    //知道树的叶子节点，从全列表中构建树
    public List<T> genSubTreeByLeaf(List<T> allList, List<T> leafs) {
        if (CollectionUtils.isEmpty(leafs)
                || CollectionUtils.isEmpty(allList)) {
            return null;
        }
        Map<String, T> subTreeMap = Maps.newHashMap();
        leafs.forEach(f -> {
            subTreeMap.put(f.getNodeId(), f);
            T parent = this.getMaps().get(f.getNodeId());
            while (parent != null) {
                subTreeMap.put(parent.getNodeId(), parent);
                parent = this.getMaps().get(parent.getNodeId());
            }
        });

        List<T> retList = subTreeMap.values().stream().collect(Collectors.toList());
        DefaultTreeBuildFactory<T> factory = new DefaultTreeBuildFactory<>();
        factory.setRootParentId("-1");
        List<T> ret = factory.doTreeBuild(retList);
        return ret;
    }

    public static Set<String> getNodeByPath(String path, String split) {
        Set<String> retStr = Sets.newHashSet();
        String[] pad = path.split(split);
        for (String s : pad) {
            retStr.add(s);
        }
        return retStr;
    }

    public static void main(String[] args) {
        List<String> list = Lists.newArrayList();
        list.add("10-32");
        list.add("10-11-12-14");
        list.add("22-23-24");
        Set<String> set = getNodeByPaths(list, "-");
        System.out.println();
    }

    public static Set<String> getNodeByPaths(Collection<String> paths, String split) {
        Set<String> retStr = Sets.newHashSet();
        paths.stream().forEach(v -> {
            Set<String> ret = getNodeByPath(v, split);
            retStr.addAll(ret);
        });

        return retStr;
    }

    public static Map<Long, Long> getNodeMapByPaths(Collection<String> paths, String split) {
        Map<Long, Long> map = Maps.newHashMap();
        paths.stream().forEach(v -> {
            Set<String> ret = getNodeByPath(v, split);
            ret.stream().forEach(ch -> {
                map.put(Long.valueOf(ch), Long.valueOf(ch));
            });
        });

        return map;
    }

    //list列表中，通过parentId，生成treeList
    public List<T> doListToTree(List<T> list, String parentId) {
        return list.stream()
                .filter(parent -> parent.getNodeParentId().equals(parentId))
                .collect(Collectors.toSet())
                .stream()
                .map(child -> {
                    child.setChildrenNode(doListToTree(list, child.getNodeId()));
                    return child;
                }).collect(Collectors.toList());
    }

    private void setDeleteNode2Red(Tree node, Set<String> sets) {
        if (Objects.isNull(node)
                || CollectionUtils.isEmpty(node.getChildrenNode())) {
            return;
        }

        if (sets.contains(node.getNodeId())) {
            node.setRed(true);
        }

        node.getChildrenNode().stream().forEach(v -> {
            if (sets.contains(v.getNodeId())) {
                v.setRed(true);
            }
            setDeleteNode2Red(v, sets);
        });

    }

    //删除数中节点获取数
    //sets:需要删除的节点，map：节点的父亲节点，T：根节点
    public Tree getSubTreeByDeleteNode(Tree node, Set<String> sets, Map<String, T> map) {
        if (Objects.isNull(node)) {
            return null;
        }
        // set中保存的是叶子节点
        if (sets.contains(node.getNodeId())) {
            node.setRed(true);
        }
        if (CollectionUtils.isEmpty(node.getChildrenNode())) {
            return node;
        }
        int i = node.getChildrenNode().size() - 1;
        for (int m = i; m >= 0; m--) {
            getSubTreeByDeleteNode(node.getChildrenNode().get(m), sets, map);
        }
        for (int m = i; m >= 0; m--) {
            if (node.getChildrenNode().get(m).getRed() == true) {
                node.getChildrenNode().remove(m);
            }
        }
        if (node.getChildrenNode().size() <= 0) {
            node.setRed(true);
        }

        //返回根节点
        return (node.getRed() == true) ? null : node;
    }
}