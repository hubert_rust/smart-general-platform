package cn.herodotus.dante.core.exception;

import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.exception.PlatformException;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class RealmException extends PlatformException {
    private String key;

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    public RealmException(String name, String key, String message) {
        super(message);
        this.key = key;
        setName(name);
    }
    public RealmException() {
        super();
    }

    public RealmException(String name, String key) {
        // super(key);
        this.key = key;
        setName(name);
    }

    public RealmException(String message, Throwable cause) {
        super(message, cause);
    }

    public RealmException(Throwable cause) {
        super(cause);
    }

    protected RealmException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public Feedback getFeedback() {
        return super.getFeedback();
    }
}
