package cn.herodotus.dante.core.access;

import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders;
import cn.herodotus.engine.rest.core.definition.crypto.SymmetricCryptoProcessor;
import cn.herodotus.engine.rest.protect.constant.ProtectConstant;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hutool.core.data.id.NanoId;
import org.dromara.hutool.crypto.digest.MD5;
import org.springframework.stereotype.Component;

/**
 * @author root
 * @description TODO
 * @date 2023/9/13 8:11
 */
@Slf4j
@Component
public class ServerFeignInterceptor implements RequestInterceptor {
    @Resource
    private SymmetricCryptoProcessor symmetricCryptoProcessor;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        log.info(">>> ServerFeignInterceptor");
        String rand = NanoId.randomNanoId();
        String enRand = symmetricCryptoProcessor.encrypt(rand, ProtectConstant.SM4_SECRET);
        String md5 = MD5.of().digestHex(enRand);
        String cid = CommonSecurityContextHolder.getCid();

        requestTemplate.header(HttpHeaders.HTTP_SRC, "inner");
        requestTemplate.header(HttpHeaders.HTTP_RAND, enRand);
        requestTemplate.header(HttpHeaders.HTTP_CID, cid);
        requestTemplate.header(HttpHeaders.HTTP_DIGEST, md5);
        requestTemplate.header("Accept", "application/json");
    }
}
