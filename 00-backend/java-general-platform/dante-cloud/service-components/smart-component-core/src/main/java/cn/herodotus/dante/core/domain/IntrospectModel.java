package cn.herodotus.dante.core.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/10/18 12:22
 */

@Data
public class IntrospectModel implements Serializable {
    // 应用id
    private String clientId;

    // md5加密
    private String digest;

    // 应用名称
    private String appTag;

    // 请求方法
    private String requestMethod;

    // 请求Url
    private String requestUrl;

    // 在请求地址上增加nonce
    private String nonce;
}
