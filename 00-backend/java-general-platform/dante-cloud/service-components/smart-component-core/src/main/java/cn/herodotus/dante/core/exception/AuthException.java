package cn.herodotus.dante.core.exception;

import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.exception.PlatformException;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class AuthException extends PlatformException {
    private String key;



    public AuthException(String name, String key, String message) {
        super(message);
        this.key = key;
        super.setName(name);
    }
    public AuthException() {
        super();
    }

    public AuthException(String name, String key) {
        // super(message);
        this.key = key;
        super.setName(name);
    }

    public AuthException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthException(Throwable cause) {
        super(cause);
    }

    protected AuthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public Feedback getFeedback() {
        return super.getFeedback();
    }
}
