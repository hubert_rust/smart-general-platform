package cn.herodotus.dante.core.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/10/18 14:07
 */
@Data
public class IntrospectRespModel implements Serializable {

    private int code;
    private String message;
}
