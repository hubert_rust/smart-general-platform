package cn.herodotus.dante.core.exception;

import cn.herodotus.engine.assistant.core.domain.Result;
import jakarta.servlet.http.HttpServletRequest;

public interface LoginExceptionInterface {
    Result<String> resolveException(Exception ex, HttpServletRequest request);
}
