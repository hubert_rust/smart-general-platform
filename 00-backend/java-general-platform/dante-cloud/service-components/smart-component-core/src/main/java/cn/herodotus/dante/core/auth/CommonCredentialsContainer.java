package cn.herodotus.dante.core.auth;

public interface CommonCredentialsContainer {
    void eraseCredentials();
}
