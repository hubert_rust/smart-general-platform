package cn.herodotus.dante.core.domain;

@Deprecated
public class UserContextHolder {
    public static ThreadLocal<RequestBaseUser> userThreadLocal;

    static {
        userThreadLocal =  ThreadLocal.withInitial(RequestBaseUser::new);
    }

    public static RequestBaseUser getBaseUser() {
        return userThreadLocal.get();
    }
    public static void setBaseUser(RequestBaseUser user) {
        userThreadLocal.set(user);
    }
    public static void clearBaseUser() {
        userThreadLocal.remove();
    }
}
