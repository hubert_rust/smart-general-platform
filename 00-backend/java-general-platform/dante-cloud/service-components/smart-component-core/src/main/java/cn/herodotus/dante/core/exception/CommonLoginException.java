package cn.herodotus.dante.core.exception;

import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.exception.PlatformException;

public class CommonLoginException extends PlatformException {
    private String key ;
    public CommonLoginException() {
        super();
    }
    public CommonLoginException(String key, String message) {
        super(message);
        this.key = key;
    }

    public CommonLoginException(String message) {
        super(message);
    }

    public CommonLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommonLoginException(Throwable cause) {
        super(cause);
    }

    protected CommonLoginException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public Feedback getFeedback() {
        return super.getFeedback();
    }
}
