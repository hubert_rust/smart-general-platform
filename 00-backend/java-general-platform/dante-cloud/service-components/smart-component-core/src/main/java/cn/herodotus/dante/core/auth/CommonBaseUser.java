package cn.herodotus.dante.core.auth;

import cn.herodotus.engine.assistant.core.domain.CommonGrantedAuthority;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import lombok.Data;

import java.util.Set;

@Data
public class CommonBaseUser implements CommonUserDetails, CommonCredentialsContainer {

    private Object object;

    private String uid;

    // @Deprecated
    private String clientId;

    private String tid;
    private String mainDeptId;

    private String password;

    private String username;

    private String realName;
    private Set<CommonGrantedAuthority> authorities;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private boolean enabled;

    private Set<String> roles;

    private String employeeId;

    private String avatar;

    @Override
    public void eraseCredentials() {
        password = null;
    }

}
