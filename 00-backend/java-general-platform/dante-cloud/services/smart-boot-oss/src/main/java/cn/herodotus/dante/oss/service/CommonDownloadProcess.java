package cn.herodotus.dante.oss.service;

import cn.herodotus.dante.oss.constant.BaseConfigConstant;
import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.dante.oss.model.FileExtInfo;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.Objects;

//只是直接下载使用
@Component
public class CommonDownloadProcess {
    @Resource
    private FileService fileService;
    @Resource
    private BaseConfigService baseConfigService;


    private FileExtInfo preProcess(String id) {
        FileEntity fileEntity = fileService.findById(id);
        if (fileEntity == null) {
            return null;
        }
        String uploadPath = baseConfigService.getValByConfigType(BaseConfigConstant.CONFIG_TYPE_UPLOAD_PATH);
        String filePath = uploadPath + fileEntity.getFileUrl();

        File file = new File(filePath);
        if (!file.exists()) {
            return null;
        }
        FileExtInfo fileExtInfo = new FileExtInfo();
        fileExtInfo.setSrcFile(fileEntity.getSrcName());
        fileExtInfo.setFileName(filePath);

        return fileExtInfo;
    }

    public void downloadProcess(String id, HttpServletResponse response) {
        FileExtInfo fileExtInfo = preProcess(id);
        if (Objects.isNull(fileExtInfo)) {
            throw new RuntimeException("文件不存在");
        }
        try {
            downloadFile(id, fileExtInfo, response);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void downloadFile(String id,
                              FileExtInfo fileExtInfo,
                              HttpServletResponse response) throws Exception {

        response.setHeader("Content-Disposition",
                "attachment;filename=" + URLEncoder.encode(fileExtInfo.getSrcFile(), "UTF-8"));

        try (FileInputStream fileInputStream = new FileInputStream(fileExtInfo.getFileName());
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(response.getOutputStream())) {
            FileCopyUtils.copy(bufferedInputStream, bufferedOutputStream);
        } catch (Exception e) {

        } finally {

        }
    }
}
