package cn.herodotus.dante.oss.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

// 收藏夹
@Entity
@Table(name = "sys_file_fav")
@Data
@EqualsAndHashCode(callSuper=true)
public class FileFavEntity extends CommonTenantEntity {
    @Column(name = "uid", length = 64)
    private String uid;


    // 为了区分平台发布的sso和非平台发布的sso
    @Column(name = "file_id", length = 64)
    private String libId;















}
