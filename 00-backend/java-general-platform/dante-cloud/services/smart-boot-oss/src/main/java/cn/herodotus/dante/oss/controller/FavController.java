package cn.herodotus.dante.oss.controller;

import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.dante.oss.service.FileService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/fav")
@Tag(name = "收藏夹管理")
public class FavController {
    @Resource
    private FileService fileService;


    @Operation(summary = "我的收藏夹", description = "我的收藏夹")
    @PostMapping("/myFav")
    public Result<List<FileEntity>> myFav(){
        List<FileEntity> list = fileService.myFav();
        return Result.success(Feedback.OK, list);
    }

    @Operation(summary = "取消收藏", description = "取消收藏")
    @PostMapping("/cancelStar/{id}")
    public Result cancelStar(@PathVariable String id){
        Boolean ret = fileService.cancelStar(id);
        return Result.success(Feedback.OK, ret);
    }
}
