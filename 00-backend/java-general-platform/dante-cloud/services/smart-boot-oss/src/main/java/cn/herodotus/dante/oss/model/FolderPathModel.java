package cn.herodotus.dante.oss.model;

import cn.herodotus.dante.oss.entity.FileEntity;
import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FolderPathModel implements Serializable {
    // 当前点击查看的目录
    private FileEntity current;
    private List<FileEntity> path = Lists.newArrayList();
    private List<FileEntity> child = Lists.newArrayList();
}
