/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.oss.service;

import cn.herodotus.dante.core.tree.DefaultTreeBuildFactory;
import cn.herodotus.dante.oss.constant.OssConstant;
import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.dante.oss.exception.OssErrorKey;
import cn.herodotus.dante.oss.exception.OssException;
import cn.herodotus.dante.oss.model.FolderPathModel;
import cn.herodotus.dante.oss.repository.FileRepository;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * <p>Description: 单位管理服务 </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/1/20 11:39
 */
@Slf4j
@Service
public class FileService extends BaseService<FileEntity, String> {

    private FileRepository fileRepository;

    @Autowired
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    @Override
    public BaseRepository<FileEntity, String> getRepository() {
        return this.fileRepository;
    }

    public boolean removeLib(FileEntity entity) {
        return true;
    }

    public boolean removeFile(List<FileEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }

        List<String> ids = list.stream().map(m->m.getId()).toList();
        List<FileEntity> finds = fileRepository.findAllById(ids);
        finds.stream().forEach(v->v.setState(0));
        fileRepository.saveAll(finds);
        return true;
    }
    // 新建文件夹
    public FileEntity createFolder(FileEntity entity) {
        if (Strings.isNullOrEmpty(entity.getFileName())) {
            throw new OssException("ossException", OssErrorKey.PARAM_INVALID, "名称不能为空");
        }
        String tid = super.getTid();
        boolean exist = fileRepository.existsByTidAndParentIdAndUidAndFileName(
                tid, entity.getParentId(), super.getUid(), entity.getFileName()
        );
        if(exist) {
            throw new OssException("ossException", OssErrorKey.PARAM_INVALID,
                    "名称【" + entity.getFileName() +"】已经被占用重复");
        }
        entity.setStar(0);
        entity.setUid(super.getUid());
        super.insert(entity);
        return entity;
    }
    public FileEntity createLib(FileEntity entity) {
        if (Strings.isNullOrEmpty(entity.getLibName())) {
            throw new OssException("ossException", OssErrorKey.PARAM_INVALID, "名称不能为空");
        }

        String tid = super.getTid();
        boolean exist = fileRepository.existsByTidAndParentIdAndUidAndLibName(
                tid, OssConstant.DEFAULT_PARENTID, super.getUid(), entity.getLibName()
        );
        if(exist) {
            throw new OssException("ossException", OssErrorKey.PARAM_INVALID, "名称重复");
        }
        entity.setStar(0);
        entity.setFileClass(OssConstant.FILE_CLASS_LIB);
        entity.setFileName(entity.getLibName());
        entity.setParentId("-1");
        entity.setUid(super.getUid());
        super.insert(entity);
        return entity;
    }
    public boolean renameLib(FileEntity entity) {
        String tid = super.getTid();
        boolean exist = fileRepository.existsByTidAndParentIdAndUidAndLibName(
                tid, OssConstant.DEFAULT_PARENTID, super.getUid(), entity.getLibName()
        );
        if(exist) {

            throw new OssException("ossException", OssErrorKey.PARAM_INVALID, "名称重复");
        }
        FileEntity finder = super.findById(entity.getId());
        finder.setLibName(entity.getLibName());
        super.save(finder);
        return true;
    }
    // 查看某个目录
    public FolderPathModel folderBrowser(String id) {
        FolderPathModel model = new FolderPathModel();
        List<Object[]> listPath = fileRepository.findParentById(id);
        for (Object[] objects : listPath) {
            FileEntity entity = new FileEntity();
            entity.setId(Objects.nonNull(objects[0]) ? objects[0].toString() : null);
            entity.setParentId(Objects.nonNull(objects[1]) ? objects[1].toString() : null);
            entity.setFileName(Objects.nonNull(objects[2]) ? objects[2].toString() : null);
            entity.setFileClass(Objects.nonNull(objects[3]) ? objects[3].toString() : null);
            entity.setShowStyle(Objects.nonNull(objects[4]) ? objects[4].toString() : null);
            entity.setLevel(Objects.nonNull(objects[5]) ? Integer.valueOf(objects[5].toString()) : 0);
            model.getPath().add(entity);
        }

        List<FileEntity> list
                = fileRepository.findByTidAndUidAndParentIdAndState(
                super.getTid(), super.getUid(), id, 1
        );

        if (!CollectionUtils.isEmpty(list)) {
            model.getChild().addAll(list);
        }

        FileEntity entity = super.findById(id);
        if (Objects.isNull(entity)) {
        }
        else {
            model.setCurrent(entity);
        }
        return model;
    }

    public List<FileEntity> myFav() {
        try {
            String tid = super.getTid();
            List<FileEntity> list
                    = fileRepository.findByTidAndUidAndStarAndState(
                    tid, super.getUid(), 1, 1
            );
           return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        }
    }
    public List<FileEntity> mylib(String parentId) {
        try {
            String tid = super.getTid();
            List<FileEntity> list
                    = fileRepository.findByTidAndUidAndParentIdAndState(
                            tid, super.getUid(), parentId, 1
            );
            if (OssConstant.DEFAULT_PARENTID.equals(parentId)) {
                return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
            }
            DefaultTreeBuildFactory<FileEntity> factory = new DefaultTreeBuildFactory<>();
            factory.setRootParentId("-1");
            List<FileEntity> tree = factory.doTreeBuild(list);
            return tree;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        }
    }


    public boolean addStar(String id) {
        FileEntity entity = super.findById(id);
        entity.setStar(1);
        super.save(entity);
        return true;
    }
    public boolean cancelStar(String id) {
        FileEntity entity = super.findById(id);
        entity.setStar(0);
        super.save(entity);
        return true;
    }
    public boolean deletLib(String id) {
        super.deleteById(id);
        return true;
    }
}
