package cn.herodotus.dante.oss.exception;

public interface OssErrorKey {


    String PARAM_INVALID = "paramInvalid";
    String NAME_INVALID = "nameInvalid";


    String AUTH_SERVER_INFO = "authServerInfo";
}
