package cn.herodotus.dante.oss.model;

import cn.herodotus.engine.data.core.dto.Response;
import lombok.Data;

@Data
public class UploadResult extends Response {
    private String fileId;  //文件ID
    private String fileUrl;
    private String serverFilePath; //上传后文件名称：绝对路径
    private String uploadPath;     //上传后文件路径: 绝对路径
    private String srcName;
    private Long fileByte;
    private String taskName;
    private String taskId;

    public static UploadResult success(String fileId, String fileUrl, String srcName) {
        UploadResult uploadResult = new UploadResult();
        uploadResult.setFileId(fileId);
        uploadResult.setFileUrl(fileUrl);
        uploadResult.setSrcName(srcName);
        uploadResult.setSuccess(true);
        return uploadResult;
    }
    public static UploadResult failure(String error, String fileName) {
        UploadResult uploadResult = new UploadResult();
        uploadResult.setSuccess(false);
        uploadResult.setErrMessage(error);
        return uploadResult;
    }

}
