package cn.herodotus.dante.oss.model;

import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

//资源目录模型定义
@Data
public class CatalogModel implements Serializable {
    private String srcName; // 当前目录原名称
    private Long taskId;    // task 到id
    //private String resName; // 服务器生成到名字
    //private String resPath; // 新生成的资源路径
    private String uploadFile;     // 上传后文件
    private String resType;        // "catalog, res"
    private String resId;          // 如果是文件,资源标识,
    private String relativeUrl;            // 访问地址

    private List<CatalogModel>  children = Lists.newArrayList();
}
