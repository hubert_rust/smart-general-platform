package cn.herodotus.dante.oss.controller;

import cn.herodotus.dante.oss.constant.OssConstant;
import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.dante.oss.model.FolderPathModel;
import cn.herodotus.dante.oss.service.FileService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lib")
@Tag(name = "资料库管理")
public class LibController {


    @Resource
    private FileService fileService;

    @Operation(summary = "删除资源库", description = "删除资源库")
    @PostMapping("/removeLib")
    public Result<Boolean> removeLib(@RequestBody List<FileEntity> entity){
        Boolean ret = fileService.removeFile(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "删除文件目录", description = "删除文件目录")
    @PostMapping("/removeFile")
    public Result<Boolean> removeFile(@RequestBody List<FileEntity> entity){
        Boolean ret = fileService.removeFile(entity);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "我的收藏夹", description = "我的收藏夹")
    @PostMapping("/myFav")
    public Result<List<FileEntity>> myFav(){
        List<FileEntity> list = fileService.myFav();
        return Result.success(Feedback.OK, list);
    }
    @Operation(summary = "添加资源库", description = "添加资源库")
    @PostMapping("/addLib")
    public Result<FileEntity> createLib(@RequestBody FileEntity entity){
        FileEntity fileEntity = fileService.createLib(entity);
        return Result.success(Feedback.OK, fileEntity);
    }
    @Operation(summary = "新建文件夹", description = "新建文件夹")
    @PostMapping("/addFolder")
    public Result<FileEntity> createFolder(@RequestBody FileEntity entity){
        FileEntity fileEntity = fileService.createFolder(entity);
        return Result.success(Feedback.OK, fileEntity);
    }
    @Operation(summary = "修改名称", description = "修改名称")
    @PostMapping("/rename")
    public Result<FileEntity> rename(@RequestBody FileEntity entity){
        FileEntity fileEntity = fileService.createLib(entity);
        return Result.success(Feedback.OK, fileEntity);
    }
    @Operation(summary = "我的资料库", description = "我的资料库")
    @PostMapping("/mylib")
    public Result<List<FileEntity>> mylib(){
        List<FileEntity> list = fileService.mylib(OssConstant.DEFAULT_PARENTID);
        return Result.success(Feedback.OK, list);
    }

    @Operation(summary = "浏览文件夹", description = "浏览文件夹")
    @PostMapping("/libBrowse/{id}")
    public Result<FolderPathModel> libBrowse(@PathVariable(value = "id") String id){
        FolderPathModel list = fileService.folderBrowser(id);
        return Result.success(Feedback.OK, list);
    }
    @Operation(summary = "添加星标", description = "添加星标")
    @PostMapping("/addStar/{id}")
    public Result addStar(@PathVariable String id){
        Boolean ret = fileService.addStar(id);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "取消星标", description = "取消星标")
    @PostMapping("/cancelStar/{id}")
    public Result cancelStar(@PathVariable String id){
        Boolean ret = fileService.cancelStar(id);
        return Result.success(Feedback.OK, ret);
    }
}
