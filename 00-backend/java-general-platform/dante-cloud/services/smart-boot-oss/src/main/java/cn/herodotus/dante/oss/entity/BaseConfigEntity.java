package cn.herodotus.dante.oss.entity;

import cn.herodotus.dante.definition.CommonTenantEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "sys_base_config")
public class BaseConfigEntity extends CommonTenantEntity {
    @Column(name = "client_id", length = 64)
    private String clientId;
    @Column(name = "config_type", length = 64)
    private String configType;
    @Column(name = "param")
    private String param;
    @Column(name = "state")
    private Integer state;
    private String remark;
}
