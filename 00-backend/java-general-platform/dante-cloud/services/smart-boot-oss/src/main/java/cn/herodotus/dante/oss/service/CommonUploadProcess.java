package cn.herodotus.dante.oss.service;

import cn.herodotus.dante.oss.constant.BaseConfigConstant;
import cn.herodotus.dante.oss.constant.OssConstant;
import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.dante.oss.model.UploadResult;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Component
public class CommonUploadProcess {
    @Resource
    private FileService fileService;
    @Resource
    private BaseConfigService baseConfigService;

    public static String pathFormat(String url) {
        if (Strings.isNullOrEmpty(url)) {
            return url;
        }

        String last = url.substring(url.length() - 1);
        if (!last.equals(File.separator)) {
            return url + File.separator;
        }
        return url;

    }

    public List<UploadResult> uploadProcess(HttpServletRequest request,
                                            String parent,
                                            MultipartFile[] fs) {

        List<UploadResult> list = commonUpload(request, parent, fs);

        list.stream().forEach(v -> {
            v.setUploadPath(null);
            v.setServerFilePath(null);
        });
        return list;
    }

    private List<UploadResult> commonUpload(HttpServletRequest request,
                                            String parent,
                                            MultipartFile[] fs) {

        String serverUrl = baseConfigService.getValByConfigType(BaseConfigConstant.CONFIG_TYPE_SERVER_URL);
        String uploadPath = baseConfigService.getValByConfigType(BaseConfigConstant.CONFIG_TYPE_UPLOAD_PATH);
        if (Strings.isNullOrEmpty(uploadPath)) {
            throw new RuntimeException("请配置上传路径");
        }
        if (Strings.isNullOrEmpty(serverUrl)) {
            throw new RuntimeException("请配置域名访问URL");
        }
        //serverUrl = pathFormat(serverUrl);
        uploadPath = pathFormat(uploadPath);

        LocalDateTime time = LocalDateTime.now();
        String s = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        String path = uploadPath + s;
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }

        List<UploadResult> list = Lists.newArrayList();
        for (MultipartFile f : fs) {
            String srcName = new String(f.getOriginalFilename().getBytes(StandardCharsets.ISO_8859_1),
                    StandardCharsets.UTF_8);
            UploadResult result = upFile(f,
                    path,
                    s,
                    srcName,
                    serverUrl, parent);
            list.add(result);
        }


        return list;
    }


    public UploadResult upFile(MultipartFile file,
                               String path,
                               String fileRelativePath,
                               String srcFileName,
                               String serverUrl,
                               String parent) {

        try {
            String tid = CommonSecurityContextHolder.getTid();
            String uid = CommonSecurityContextHolder.getUid();
            String fileName = file.getOriginalFilename(); // 文件名
            String suffix = "";  //文件后缀。
            if (StringUtils.isNotEmpty(fileName)) {
                suffix = fileName.substring(fileName.lastIndexOf("."));
            }
            InputStream inputStream = file.getInputStream();
            String fileNo = UUID.randomUUID().toString().replaceAll("-", "");
            String newFileNameTemp = fileNo + suffix;
            String filePath = path + File.separator + newFileNameTemp;
            File f = new File(filePath);
            FileOutputStream fos = new FileOutputStream(f);
            FileCopyUtils.copy(inputStream, fos);

            FileEntity entity = new FileEntity();
            entity.setSrcName(srcFileName);
            entity.setFileName(srcFileName);
            entity.setFileExt(suffix);
            entity.setStar(0);
            entity.setTid(tid);
            entity.setUid(uid);
            entity.setParentId(parent);
            entity.setFileClass(OssConstant.FILE_CLASS_FILE);
            entity.setFileSize(file.getSize());
            entity.setFileUrl(File.separator + fileRelativePath + File.separator + newFileNameTemp);
            fileService.insert(entity);

            UploadResult uploadResult = new UploadResult();
            uploadResult.setFileId(entity.getId());
            uploadResult.setSrcName(srcFileName);
            uploadResult.setServerFilePath(filePath);
            uploadResult.setUploadPath(path);
            uploadResult.setFileByte(file.getSize());
            uploadResult.setFileUrl(serverUrl + fileRelativePath + File.separator + newFileNameTemp);
            return uploadResult;

        } catch (Exception e) {
            return UploadResult.failure(e.getMessage(), srcFileName);
        }

    }

}
