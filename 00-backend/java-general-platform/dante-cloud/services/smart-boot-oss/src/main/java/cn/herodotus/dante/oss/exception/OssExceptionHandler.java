/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.oss.exception;

import cn.herodotus.dante.core.exception.CommonExceptionInterface;
import cn.herodotus.engine.assistant.core.domain.Result;
import cn.herodotus.engine.assistant.core.exception.GlobalExceptionHandler;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


/*
 * @Author root
 * @Description //TODO
 * @Date 8:35 2023/10/17
 * @Param
 * @return
 **/
@Slf4j
@Component("ossException")
public class OssExceptionHandler implements CommonExceptionInterface {


    private static final Map<String, Result<String>> EXCEPTION_DICTIONARY = new HashMap<>();

    static {
        EXCEPTION_DICTIONARY.put(OssErrorKey.PARAM_INVALID, getResult(OssResultErrorCode.PARAM_INVALID, 500));
        EXCEPTION_DICTIONARY.put(OssErrorKey.AUTH_SERVER_INFO, getResult(OssResultErrorCode.AUTH_SERVER_INFO, 500));
    }

    /**
     * Rest Template 错误处理
     *

     * @return Result 对象
     * @see <a href="https://www.baeldung.com/spring-rest-template-error-handling">baeldung</a>
     */

    public static Result<String> getResult(OssResultErrorCode resultErrorCodes, int httpStatus) {
        return Result.failure(resultErrorCodes.getMessage(), resultErrorCodes.getCode(), httpStatus, null);
    }
    //ex中的message是RealmErrorKey
    @Override
    public Result<String> resolveException(Exception ex, HttpServletRequest request) {
        String path = request.getRequestURI();
        Exception reason = new Exception();
        if (ex instanceof OssException ossException) {
            OssException ossEx = (OssException) ex;
            String message = ossEx.getMessage();
            Result<String> result = null;
            if (Strings.isNotEmpty(ossEx.getKey())) {
                Result<String> staticResult = EXCEPTION_DICTIONARY.get(ossEx.getKey());
                result = new Result<>();
                result.message(staticResult.getMessage() + (Strings.isEmpty(message) ? "" : message));
                result.path(path);
                result.stackTrace(ex.getStackTrace());
                result.detail(ex.getMessage());
                result.code(staticResult.getCode());
                result.status(staticResult.getStatus());
                return result;
            }
            else {
                result = EXCEPTION_DICTIONARY.get(ossEx.getKey());
                result.message(result.getMessage() + (Strings.isEmpty(message) ? "" : message));
                result.path(path);
                result.stackTrace(ex.getStackTrace());
                result.detail(ex.getMessage());
                return result;
            }

        }  else {
            log.error("[OSS]", ex.getMessage());
        }

        return GlobalExceptionHandler.resolveException(ex, path);
    }


}
