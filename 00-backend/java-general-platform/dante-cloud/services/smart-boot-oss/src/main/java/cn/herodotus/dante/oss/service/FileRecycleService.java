/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.oss.service;

import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.dante.oss.entity.FileRecycleEntity;
import cn.herodotus.dante.oss.repository.FileRecycleRepository;
import cn.herodotus.dante.oss.repository.FileRepository;
import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import cn.herodotus.engine.data.core.service.BaseService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.List;


@Service
public class FileRecycleService extends BaseService<FileRecycleEntity, String> {

    private FileRecycleRepository fileRecycleRepository;
    @Resource
    private FileService fileService;

    @Autowired
    public FileRecycleService(FileRecycleRepository fileRecycleRepository) {
        this.fileRecycleRepository = fileRecycleRepository;
    }

    @Override
    public BaseRepository<FileRecycleEntity, String> getRepository() {
        return this.fileRecycleRepository;
    }

    public boolean remove(List<FileEntity> list) {
        for (FileEntity entity : list) {
            List<Object[]> finds
                    = ((FileRepository) fileService.getRepository()).findByRecurise(
                    entity.getId(), super.getTid()
            );
            if (CollectionUtils.isEmpty(finds)) {
                continue;
            }
            fileService.deleteByIds(finds.stream().map(m->m[0].toString()).toList());
        }
        return true;
    }

    public List<FileEntity> myRecycle() {
        String tid = CommonSecurityContextHolder.getTid();
        String uid = CommonSecurityContextHolder.getUid();
        return ((FileRepository) fileService.getRepository()).findByTidAndUidAndState(
                tid, uid, 0
        );
    }

    public Boolean restore(List<FileEntity> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        List<String> ids = list.stream().map(m -> m.getId()).toList();
        List<FileEntity> finds
                = ((FileRepository) fileService.getRepository()).findAllById(ids);
        finds.stream().forEach(v -> v.setState(1));
        ((FileRepository) fileService.getRepository()).saveAll(finds);
        return true;
    }
}
