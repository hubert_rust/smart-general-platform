package cn.herodotus.dante.oss.model;

import lombok.Data;

@Data
public class FileExtInfo {
    private String fileName;
    private String srcFile;
}
