package cn.herodotus.dante.oss.exception;

import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.exception.PlatformException;
import lombok.Data;

@Data
public class OssException extends PlatformException {
    private String key;


    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }
    public OssException(String name, String key) {
        this.key = key;
        setName(name);
    }
    public OssException(String name, String key, String message) {
        super(message);
        this.key = key;
        setName(name);
    }
    public OssException() {
        super();
    }

    public OssException(String key) {
        // super(key);
        this.key = key;
    }

    public OssException(String message, Throwable cause) {
        super(message, cause);
    }

    public OssException(Throwable cause) {
        super(cause);
    }

    protected OssException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public Feedback getFeedback() {
        return super.getFeedback();
    }


}
