package cn.herodotus.dante.oss.entity;

import cn.herodotus.dante.core.tree.Tree;
import cn.herodotus.dante.definition.CommonTenantEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "sys_file")
@Data
@EqualsAndHashCode(callSuper=true)
public class FileEntity extends CommonTenantEntity implements Tree {
    @Column(name = "uid", length = 64)
    private String uid;
    @Column(name = "lib_name", length = 64)
    private String libName;

    @Column(name = "star")
    private Integer star;

    @Column(name = "show_style")
    private String showStyle = "list";

    // 为了区分平台发布的sso和非平台发布的sso
    @Column(name = "parent_id", length = 64)
    private String parentId;

    @Column(name = "file_name", length = 64)
    private String fileName;

    @Column(name = "file_class", length = 16)
    private String fileClass;

    @Column(name = "file_url", length = 255)
    private String fileUrl;

    @Column(name = "file_ext", length = 16)
    private String fileExt;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "digest", length = 64)
    private String digest;

    @Column(name = "src_name", length = 64)
    private String srcName;

    private Integer state = 1;

    @JsonIgnore
    @Override
    @Transient
    public List<Tree> getChildrenNode() {
        return this.children;
    }

    @Override
    @Transient
    public String getNodeCode() {
        return null;
    }

    @Transient
    private List<Tree> children;



    @Transient
    @Override
    public String getNodeId() {
        return this.getId() == null ? null : (this.getId());
    }

    @Transient
    @Override
    public String getNodeParentId() {
        if (this.parentId == null) {
            return null;
        }
        else {
            return (this.parentId);
        }
    }

    @Transient
    @Override
    public void setChildrenNode(List children) {
        this.setChildren(children);
    }


    @Transient
    private Integer commandVisible = 0;

    // 查询父节点，倒序
    @Transient
    private Integer level;
}
