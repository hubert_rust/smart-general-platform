package cn.herodotus.dante.oss.controller;

import cn.herodotus.dante.oss.service.CommonDownloadProcess;
import cn.herodotus.dante.oss.service.CommonUploadProcess;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@Tag(name = "文件上传下载")
@Slf4j
@RestController
@RequestMapping("file")
public class UploadController {

    @Resource
    private CommonUploadProcess commonUploadProcess;
    @Resource
    private CommonDownloadProcess commonDownloadProcess;


    @Operation(summary = "上传文件", description = "上传文件")
    @PostMapping("upload/{parent}")
    public Result upload(HttpServletRequest request,
                         @PathVariable String parent,
                         @RequestParam(value = "fs", required = false) MultipartFile[] fs) {

        return Result.success(Feedback.OK, commonUploadProcess.uploadProcess(request, parent, fs));
    }


    @Operation(summary = "下载文件", description = "下载文件")
    @GetMapping(value = "/download/{id}")
    public void down(@PathVariable("id") String id, HttpServletResponse response) {
        commonDownloadProcess.downloadProcess(id, response);
    }
}
