/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Cloud Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Cloud 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://gitee.com/dromara/dante-cloud
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/dromara/dante-cloud
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.oss.configuration;

import cn.herodotus.dante.oss.service.CommonDownloadProcess;
import cn.herodotus.dante.oss.service.CommonUploadProcess;
import cn.herodotus.dante.sdk.configuration.AppClientAuthInterceptors;
import cn.herodotus.engine.cache.redisson.annotation.EnableHerodotusRedisson;
import com.google.common.collect.Lists;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration(proxyBeanMethods = false)
@EntityScan( basePackages = {
        "cn.herodotus.dante.oss.entity"
})
@EnableJpaRepositories(
        basePackages = "cn.herodotus.dante.oss.repository"
)
@ComponentScan(basePackages = {
        "cn.herodotus.dante.oss.controller",
        "cn.herodotus.dante.oss.service",
        "cn.herodotus.dante.oss.remote",
})
@EnableHerodotusRedisson
@AutoConfiguration
@EnableJpaAuditing
@EnableConfigurationProperties(AuthPermitProperties.class)
public class SmartOssConfiguration implements WebMvcConfigurer {

    private static final Logger log = LoggerFactory.getLogger(SmartOssConfiguration.class);

    @Resource
    private AppClientAuthInterceptors appClientAuthInterceptors;
    @Resource
    AuthPermitProperties authPermitProperties;

    @PostConstruct
    public void postConstruct() {
        log.info("[Herodotus] |- Service [Oss Ability] Auto Configure.");
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptorRegistry = registry.addInterceptor(appClientAuthInterceptors)
                .order(100)
                .addPathPatterns("/**");

        List<String> permits = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(authPermitProperties.getPermit())) {
            permits.addAll(authPermitProperties.getPermit());
        }
        for (String s : permits) {
            interceptorRegistry = ((InterceptorRegistration) interceptorRegistry).excludePathPatterns(s);
        }
    }

    /*@Bean
    public CommonUploadProcess commonUploadProcess() {
        return new CommonUploadProcess();
    }
    @Bean
    public CommonDownloadProcess commonDownloadProcess() {
        return new CommonDownloadProcess();
    }*/

    /*@Bean
    public FinalSessionProperties finalSessionProperties() {
        FinalSessionProperties repository =  new FinalSessionProperties();
        return repository;
    }
    @Bean
    public FinalRedisRepository finalRedisRepository(FinalSessionProperties properties) {
        FinalRedisRepository repository =  new FinalRedisRepository(properties, redissonClient);
        return repository;
    }
    @Bean
    @ConditionalOnMissingBean
    public FilterRegistrationBean<Filter> finalSessionAdapter(FinalSessionProperties properties,
                                                              FinalRedisRepository repository) {
        log.debug("finalSessionRequestFilter init for /* ");
        FinalSessionConfigurerAdapter adapter = new FinalSessionConfigurerAdapter(properties, repository);
        FilterRegistrationBean<Filter> registrationBean
                = new FilterRegistrationBean<Filter>(adapter);
        registrationBean.addUrlPatterns("/*");
        registrationBean.setName("finalSessionRequestFilter");
        registrationBean.setOrder(3);
        return registrationBean;
    }*/

}
