package cn.herodotus.dante.oss.remote;

import com.dtflys.forest.Forest;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Data;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author root
 * @description TODO
 * @date 2023/9/13 8:30
 */
// 如果单独jar包，请使用该接口
/* @FeignClient(name = "smart-auth")
public interface RemoteBackendService {
    @RequestMapping(value = "/open/apis", method = RequestMethod.POST)
    Result<List<OperationModel>> getBackendOperations(
            URI uri, @RequestBody RemoteBaseModel model);
} */

@Data
@Component
public class RemoteServiceContainer implements InitializingBean, DisposableBean {

    private static RemoteServiceContainer INST;

    @Value("${sa-token.sso.sso-addr}")
    private String introspectAddr;

    public Object authCenter(HttpServletRequest request) {
        // Forest.post(introspectAddr).
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        RemoteServiceContainer.INST = this;
    }

    @Override
    public void destroy() throws Exception {
    }
}