package cn.herodotus.dante.oss.controller;

import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.dante.oss.service.FileRecycleService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/recycle")
@Tag(name = "回收站管理")
public class RecycleController {


    @Resource
    private FileRecycleService fileRecycleService;


    @Operation(summary = "彻底删除文件", description = "彻底删除文件")
    @PostMapping("/removeFile")
    public Result<Boolean> removeFile(@RequestBody List<FileEntity> list){
        Boolean ret = fileRecycleService.remove(list);
        return Result.success(Feedback.OK, ret);
    }
    @Operation(summary = "我的回收站列表", description = "我的回收站列表")
    @PostMapping("/myRecycle")
    public Result<List<FileEntity>> myFav(){
        List<FileEntity> list = fileRecycleService.myRecycle();
        return Result.success(Feedback.OK, list);
    }

    @Operation(summary = "还原文件", description = "还原文件")
    @PostMapping("/restore")
    public Result<Boolean> restoreFile(@RequestBody List<FileEntity> list){
        Boolean ret = fileRecycleService.restore(list);
        return Result.success(Feedback.OK, ret);
    }


}
