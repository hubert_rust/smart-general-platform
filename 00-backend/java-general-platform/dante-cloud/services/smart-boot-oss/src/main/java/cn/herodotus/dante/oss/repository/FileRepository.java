/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.dante.oss.repository;


import cn.herodotus.dante.oss.entity.FileEntity;
import cn.herodotus.engine.data.core.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FileRepository extends BaseRepository<FileEntity, String> {

    // 删除的
    List<FileEntity> findByTidAndUidAndState(String tid, String uid, Integer state);
    boolean existsByTidAndParentIdAndUidAndLibName(
            String tid, String parentId, String uid, String libName
    );
    boolean existsByTidAndParentIdAndUidAndFileName(
            String tid, String parentId, String uid, String fileName
    );

    // 我的收藏夹
    List<FileEntity> findByTidAndUidAndStarAndState(
            String tid, String uid, Integer star, Integer state);

    List<FileEntity> findByTidAndUidAndParentIdAndState(
            String tid, String uid, String parentId, Integer state);

    @Query(value = "WITH RECURSIVE cte as ( \n" +
            " select * from sys_file where  id = ?1 and tid = ?2 \n" +
            " UNION all\n" +
            " select  t.* from sys_file  t\n" +
            " join cte on cte.id = t.parent_id\n" +
            " \n" +
            " )\n" +
            "  select * from cte", nativeQuery = true)
    List<Object[]> findByRecurise(String id, String tid);

    @Query(value = "with recursive re (id, parent_id, file_name, file_class, show_style, level) AS ( \n" +
            "  SELECT id, parent_id, file_name, file_class, show_style, 1 level \n" +
            "  FROM sys_file \n" +
            "  WHERE id = ?1 \n" +
            "  union all \n" +
            "  SELECT t.id, t.parent_id, t.file_name, t.file_class, t.show_style, level + 1 \n" +
            "  FROM sys_file t INNER JOIN re pu \n" +
            "  ON t.id = pu.parent_id \n" +
            ")\n" +
            "SELECT * FROM re", nativeQuery = true)
    List<Object[]> findParentById(String id);
}
