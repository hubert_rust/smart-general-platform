package cn.herodotus.dante.oss.constant;

public interface BaseConfigConstant {
    String CONFIG_TYPE_EXPORT = "export";
    String CONFIG_TYPE_SERVER_URL = "server_url";
    String CONFIG_TYPE_UPLOAD_PATH = "upload_path";
}
