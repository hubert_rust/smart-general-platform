package cn.herodotus.dante.oss.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "smart.auth")
public class AuthPermitProperties {
    private List<String> permit;
}
