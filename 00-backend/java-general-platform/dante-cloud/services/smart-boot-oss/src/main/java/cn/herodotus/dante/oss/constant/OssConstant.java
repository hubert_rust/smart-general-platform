package cn.herodotus.dante.oss.constant;

/**
 * @author root
 * @description TODO
 * @date 2023/10/17 8:39
 */
public interface OssConstant {
    String DIR_ASC = "asc";
    String DIR_DESC = "desc";

    String FILE_CLASS_FILE = "file";
    String FILE_CLASS_FOLDER = "folder";
    String FILE_CLASS_LIB = "lib";

    String DEFAULT_PARENTID = "-1";

    String SHOW_STYLE_LIST = "list";
    String SHOW_STYLE_GRID = "grid";
    String SHOW_STYLE_COLUMN = "column";
}
