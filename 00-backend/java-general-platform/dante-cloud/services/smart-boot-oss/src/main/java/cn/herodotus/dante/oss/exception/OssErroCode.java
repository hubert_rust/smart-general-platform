package cn.herodotus.dante.oss.exception;


public interface OssErroCode {

    int BASE = 20000;


    int PARAM_INVALID = BASE + 1;

    int NAME_INVALID = BASE + 2;
    int AUTH_SERVER_INFO = BASE + 3;
}
