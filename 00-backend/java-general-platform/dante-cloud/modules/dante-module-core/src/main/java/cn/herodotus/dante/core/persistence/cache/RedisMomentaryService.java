/*
 * Copyright [2022] [MaxKey of copyright http://www.maxkey.top]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package cn.herodotus.dante.core.persistence.cache;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
public class RedisMomentaryService implements MomentaryService {

    protected int validitySeconds = 60 * 5; //default 5 minutes.

    private RedissonClient redissonClient;

    public static String PREFIX = "SMART_MOMENTARY_";

    public RedisMomentaryService(RedissonClient redissonClient) {
        super();
        this.redissonClient = redissonClient;
    }


    @Override
    public void put(String sessionId, String name, Object value) {
    }

    @Override
    public Object get(String sessionId, String name) {
        log.info(">>> get");
        return null;
    }

    @Override
    public Object remove(String sessionId, String name) {
        //log.trace("key {}, value {}", getSessionKey(sessionId, name), value);
        return null;
    }


    private String getSessionKey(String sessionId, String name) {
        return PREFIX + sessionId + name;
    }


}
