/*
 * Copyright [2020] [MaxKey of copyright http://www.maxkey.top]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 

package cn.herodotus.dante.core.configuration;

import cn.herodotus.dante.core.constants.ConstsConfig;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
@Data
@Component
@ConfigurationProperties(ConstsConfig.CONFIG_ENCODING)
public class CharacterEncodingConfig {

    /**
     * 源字符集.
     */
    String from;

    /**
     * 目标字符集.
     */
    String charset;

    /**
     * 转换标志.
     */
    boolean enabled = false;

    /**
     * 字符集转换.
     * 
     * @param encodingString 源字符串
     * @return encoded目标字符串
     */
    public String encoding(String encodingString) {
        if (!this.enabled || encodingString == null) {
            return encodingString;
        }

        try {
            return new String(encodingString.getBytes(this.from), this.charset);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
