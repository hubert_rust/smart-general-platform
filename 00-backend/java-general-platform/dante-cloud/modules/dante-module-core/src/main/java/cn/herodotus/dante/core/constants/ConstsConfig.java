package cn.herodotus.dante.core.constants;

public interface ConstsConfig {
    String CONFIG_LOGIN = "config.login";
    String CONFIG_EMAIL = "config.email";
    String CONFIG_APPLICATION = "config.server";

    String CONFIG_ENCODING = "server.servlet.encoding";

    String CONFIG_AUTH_JWT = "config.auth.jwt";
}
