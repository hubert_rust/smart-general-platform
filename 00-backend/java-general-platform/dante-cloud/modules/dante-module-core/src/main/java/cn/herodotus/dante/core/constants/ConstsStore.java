package cn.herodotus.dante.core.constants;

public interface ConstsStore {
    String STORE_MOMENT_REDIS = "redis";
    String STORE_MOMENT_MEMORY = "memory";

    String STORE_SESSION_REDIS = "redis";
    String STORE_SESSION_MEMORY = "memory";
}
