/*
 * Copyright [2020] [MaxKey of copyright http://www.maxkey.top]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 

package cn.herodotus.dante.core.autoconfigure;

import cn.herodotus.dante.common.crypto.keystore.KeyStoreLoader;
import cn.herodotus.dante.common.crypto.password.*;
import cn.herodotus.dante.core.configuration.ApplicationContainer;
import cn.herodotus.dante.core.constants.ConstsStore;
import cn.herodotus.dante.core.persistence.cache.InMemoryMomentaryService;
import cn.herodotus.dante.core.persistence.cache.MomentaryService;
import cn.herodotus.dante.core.persistence.cache.RedisMomentaryService;
import cn.herodotus.engine.cache.redisson.annotation.EnableHerodotusRedisson;
import com.nimbusds.jose.JOSEException;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@AutoConfiguration
@EnableHerodotusRedisson
public class ApplicationAutoConfiguration  implements InitializingBean {

    @jakarta.annotation.Resource
    private RedissonClient redissonClient;
    @Bean
    public PasswordReciprocal passwordReciprocal() {
        return new PasswordReciprocal();
    }
    

    /**
     * Authentication Password Encoder .
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        Map<String ,PasswordEncoder > encoders = new HashMap<String ,PasswordEncoder>();
        encoders.put("bcrypt", new BCryptPasswordEncoder());
        encoders.put("plain", NoOpPasswordEncoder.getInstance());
        encoders.put("pbkdf2", Pbkdf2PasswordEncoder.defaultsForSpringSecurity_v5_8());
        encoders.put("scrypt", SCryptPasswordEncoder.defaultsForSpringSecurity_v5_8());
        //md
        encoders.put("md4", new Md4PasswordEncoder());
        encoders.put("md5", new MessageDigestPasswordEncoder("MD5"));
        //sha
        encoders.put("sha1", new StandardPasswordEncoder("SHA-1",""));
        encoders.put("sha256", new StandardPasswordEncoder());
        encoders.put("sha384", new StandardPasswordEncoder("SHA-384",""));
        encoders.put("sha512", new StandardPasswordEncoder("SHA-512",""));
        
        encoders.put("sm3", new SM3PasswordEncoder());
        
        encoders.put("ldap", new LdapShaPasswordEncoder());
        
        //idForEncode is default for encoder
        String encoder = ApplicationContainer.INST.getServerConfig().getPasswordEncoder();
        PasswordEncoder passwordEncoder =
            new DelegatingPasswordEncoder(encoder, encoders);
       
        if(log.isTraceEnabled()) {
        	 log.trace("Password Encoders :");
	        for (String key : encoders.keySet()) {
	            log.trace("{}= {}" ,String.format("%-10s", key), encoders.get(key).getClass().getName());
	        }
        }
        log.debug("{} is default encoder" , encoder);
        return passwordEncoder;
    }

    
    /**
     * keyStoreLoader .
     * @return
     */
    @Bean
    public KeyStoreLoader keyStoreLoader(
            @Value("${maxkey.saml.v20.idp.issuing.entity.id}") String entityName,
            @Value("${maxkey.saml.v20.idp.keystore.password}") String keystorePassword,
            @Value("${maxkey.saml.v20.idp.keystore}") Resource keystoreFile) {
        KeyStoreLoader keyStoreLoader = new KeyStoreLoader();
        keyStoreLoader.setEntityName(entityName);
        keyStoreLoader.setKeystorePassword(keystorePassword);
        keyStoreLoader.setKeystoreFile(keystoreFile);
        return keyStoreLoader;
    }
    

    @Bean
    public MomentaryService momentaryService() throws JOSEException {
    	MomentaryService momentaryService;
        String store = ApplicationContainer.INST.getServerConfig().getMomentStore();
    	if (ConstsStore.STORE_MOMENT_REDIS.equals(store)) {
    		momentaryService = new RedisMomentaryService(redissonClient);
    	}else {
    		momentaryService = new InMemoryMomentaryService();
    	}
    	
    	return momentaryService;
    }
    

    @Override
    public void afterPropertiesSet() throws Exception {
        
    }
}
