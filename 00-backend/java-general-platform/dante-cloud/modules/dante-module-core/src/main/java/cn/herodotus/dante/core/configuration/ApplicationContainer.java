package cn.herodotus.dante.core.configuration;

import jakarta.annotation.Resource;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;


@Component
public class ApplicationContainer implements InitializingBean, DisposableBean {

    public static ApplicationContainer INST;

    @Resource
    private LoginConfig loginConfig;
    @Resource
    private ServerConfig serverConfig;
    @Resource
    private EmailConfig emailConfig;
    @Resource
    private AuthJwkConfig authJwkConfig;
    @Resource
    private CharacterEncodingConfig encodingConfig;

    public LoginConfig getLoginConfig() {
        return loginConfig;
    }

    public ServerConfig getServerConfig() {
        return serverConfig;
    }

    public EmailConfig getEmailConfig() {
        return emailConfig;
    }

    public AuthJwkConfig getAuthJwkConfig() {
        return authJwkConfig;
    }

    public CharacterEncodingConfig getEncodingConfig() {
        return encodingConfig;
    }

    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ApplicationContainer.INST = this;
    }
}
