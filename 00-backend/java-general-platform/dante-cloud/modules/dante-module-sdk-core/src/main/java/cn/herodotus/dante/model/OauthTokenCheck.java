package cn.herodotus.dante.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class OauthTokenCheck implements Serializable {
    private String token;
    private String salt;
    private Long operationId;
    private String requestUrl;
    private String requestMethod;
}
