package cn.herodotus.dante.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/9/13 8:50
 */
@Data
public class RemoteBaseModel implements Serializable {
    private String key;
    private String value;
}
