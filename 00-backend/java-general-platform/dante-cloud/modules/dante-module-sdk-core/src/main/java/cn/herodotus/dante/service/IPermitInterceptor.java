package cn.herodotus.dante.service;

import cn.herodotus.dante.model.AuthTokenCheck;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


public interface IPermitInterceptor {
    //默认放行
    default Boolean permit(HttpServletRequest request,
                           HttpServletResponse response,
                           AuthTokenCheck authTokenCheck, //1: uims自己校验 2: 其他应用到uims通过固定解决校验
                           CommonUserDetails requestBaseUser) {

        return true;
    }
}
