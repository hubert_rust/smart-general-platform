package cn.herodotus.dante.model;

import jakarta.persistence.Column;
import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description 最顶层parentId=-1，id必须有，operationPath必须有
 *              operationPath规则: 父节点operationPath-自己节点ID，
 *              parentId=-1的节点，path就是自己的ID
 * @date 2023/9/12 18:05
 */
@Data
public class OperationModel implements Serializable {
    private String id;
    private String appInnerName;
    private String digest;
    private String operationType;  //catalog: 目录，api
    private String processType;  //open(匿名访问),login(登录可用),permit(权限控制)

    /**
     * 操作父ID
     */
    private String parentId;

    /**
     * 操作路径标识
     */
    private String operationPath;

    /**
     * 操作名称
     */
    private String operationName;

    private String deleted;

    /**
     * 请求 url
     */
    private String requestUrl;

    /**
     * 请求方式
     */
    private String requestMode;

    private String remark;
    private Integer release;
}
