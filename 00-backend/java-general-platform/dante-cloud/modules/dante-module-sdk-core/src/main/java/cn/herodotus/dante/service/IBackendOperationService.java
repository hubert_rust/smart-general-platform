package cn.herodotus.dante.service;

import cn.herodotus.dante.model.OperationModel;

import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/12 18:08
 */
public interface IBackendOperationService {
    List<OperationModel> operations(String appInnerName);

}
