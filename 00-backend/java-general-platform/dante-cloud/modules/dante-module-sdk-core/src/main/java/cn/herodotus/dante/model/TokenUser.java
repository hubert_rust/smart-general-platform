package cn.herodotus.dante.model;

import lombok.Data;

@Data
public class TokenUser {
    private static final long serialVersionUID = 1L;

    private Long cid;   //需要和header中的clientId校验
    private Long tid;
    private Long uid;
    //private String domain; //登陆的时候获取到
    private String agent;  //来源，比如是PC，安卓，iOS还是微信小程序或其它
    private Integer accExp; //access Token过期时间(秒)
    //private Integer rfsExp; //refress Token过期时间(秒)

    public static TokenUser of() {
        return new TokenUser();
    }
}
