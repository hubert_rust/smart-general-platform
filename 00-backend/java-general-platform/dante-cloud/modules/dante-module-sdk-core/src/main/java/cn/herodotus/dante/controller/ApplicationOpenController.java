package cn.herodotus.dante.controller;

import cn.herodotus.dante.model.OperationModel;
import cn.herodotus.dante.model.RemoteBaseModel;
import cn.herodotus.dante.service.IBackendOperationService;
import cn.herodotus.engine.assistant.core.domain.Feedback;
import cn.herodotus.engine.assistant.core.domain.Result;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author root
 * @description TODO
 * @date 2023/9/12 18:03
 */
@RestController
@RequestMapping("/open")
@Tag(name = "开发接口")
public class ApplicationOpenController {
    @Resource
    private IBackendOperationService backendOperationService;
    @PostMapping("/apis")
    public Result<List<OperationModel>> getBackendOperations(@RequestBody RemoteBaseModel model) {
        List<OperationModel> ret = backendOperationService.operations(model.getKey());
        return Result.success(Feedback.OK, ret);
    }
}
