package cn.herodotus.dante.service;


import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import cn.herodotus.engine.assistant.core.domain.Result;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface IAuthCheck {
    default CommonUserDetails tokenCheck(String object,
                                         HttpServletRequest request,
                                         HttpServletResponse response) {


        return null;
    }
    default Result<CommonUserDetails> introspect(HttpServletRequest request) {
        return null;
    }
}
