package cn.herodotus.dante.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class AuthTokenCheck implements Serializable {
    private String token;
    private String salt;
    // 应用ID
    private String clientId;
    private String requestUrl;
    private String requestMethod;
}
