package cn.herodotus.dante.common;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

//pending
@Component
public class TenantIdentifierResolver implements CurrentTenantIdentifierResolver {
    @Override
    public String resolveCurrentTenantIdentifier() {
        return "0";
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return false;
    }
}
