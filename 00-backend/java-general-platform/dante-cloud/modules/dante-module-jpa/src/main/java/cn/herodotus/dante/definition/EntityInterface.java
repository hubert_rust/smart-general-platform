package cn.herodotus.dante.definition;

import cn.herodotus.engine.assistant.core.definition.domain.Entity;

import java.util.Date;

public interface EntityInterface extends Entity {


    String getId();

    void setId(String id);

    Date getCreateTime();

    void setCreateTime(Date createTime);

    String getCreateBy();
    void setCreateBy(String uid);

    Date getUpdateTime();

    void setUpdateTime(Date modifyTime);

    String getUpdateBy();

    void setUpdateBy(String modifyUser);
}
