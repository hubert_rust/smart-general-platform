package cn.herodotus.dante.definition;

public interface TenantEntityInterface extends EntityInterface{
    String getTid();
    void setTid(String tid);
}
