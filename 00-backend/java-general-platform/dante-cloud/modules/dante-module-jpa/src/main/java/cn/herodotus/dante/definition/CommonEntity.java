package cn.herodotus.dante.definition;

import cn.herodotus.dante.audit.CustomAuditingListener;
import cn.herodotus.dante.generator.CommonGeneratorUuid;
import cn.herodotus.engine.assistant.core.definition.constants.DefaultConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;


@Data
@MappedSuperclass
//@EntityListeners(AuditingEntityListener.class)
@EntityListeners(CustomAuditingListener.class)
public abstract class CommonEntity implements EntityInterface {
    @Id
    //@UuidGenerator
    //cn.herodotus.dante.realm.util
    @CommonGeneratorUuid
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Column(name = "id", length = 64)
    private String id;

    @Schema(name = "数据创建时间")
    @Column(name = "create_time", updatable = false)
    @CreatedDate
    @JsonFormat(pattern = DefaultConstants.DATE_TIME_FORMAT)
    private Date createTime = new Date();

    @Schema(name = "数据更新时间")
    @Column(name = "update_time")
    @LastModifiedDate
    @JsonFormat(pattern = DefaultConstants.DATE_TIME_FORMAT)
    private Date updateTime = new Date();

    @Schema(name = "创建人")
    @Column(name = "create_by")
    @CreatedBy
    private String createBy;

    @Schema(name = "最后修改")
    @Column(name = "update_by")
    @LastModifiedBy
    private String updateBy;
}
