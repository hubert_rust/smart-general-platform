package cn.herodotus.dante.security.session.error;

/**
 * @author lingkang
 * Created by 2022/1/26
 */
public class FinalBaseException extends RuntimeException{
    public FinalBaseException(String message) {
        super(message);
    }
}
