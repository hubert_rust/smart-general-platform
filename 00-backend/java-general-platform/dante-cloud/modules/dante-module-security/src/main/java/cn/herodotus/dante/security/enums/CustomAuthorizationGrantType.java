package cn.herodotus.dante.security.enums;

import org.springframework.util.Assert;

import java.io.Serializable;

public class CustomAuthorizationGrantType implements Serializable {
    private static final long serialVersionUID = 610L;
    public static final CustomAuthorizationGrantType AUTHORIZATION_CODE = new CustomAuthorizationGrantType("authorization_code");
    public static final CustomAuthorizationGrantType REFRESH_TOKEN = new CustomAuthorizationGrantType("refresh_token");
    public static final CustomAuthorizationGrantType CLIENT_CREDENTIALS = new CustomAuthorizationGrantType("client_credentials");
    /** @deprecated */
    @Deprecated
    public static final CustomAuthorizationGrantType PASSWORD = new CustomAuthorizationGrantType("password");
    public static final CustomAuthorizationGrantType JWT_BEARER = new CustomAuthorizationGrantType("urn:ietf:params:oauth:grant-type:jwt-bearer");
    public static final CustomAuthorizationGrantType DEVICE_CODE = new CustomAuthorizationGrantType("urn:ietf:params:oauth:grant-type:device_code");
    private final String value;

    public CustomAuthorizationGrantType(String value) {
        Assert.hasText(value, "value cannot be empty");
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj != null && this.getClass() == obj.getClass()) {
            CustomAuthorizationGrantType that = (CustomAuthorizationGrantType)obj;
            return this.getValue().equals(that.getValue());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return this.getValue().hashCode();
    }
}
