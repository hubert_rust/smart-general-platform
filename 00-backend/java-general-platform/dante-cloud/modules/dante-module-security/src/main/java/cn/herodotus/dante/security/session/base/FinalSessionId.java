package cn.herodotus.dante.security.session.base;


import cn.herodotus.dante.security.session.config.FinalSessionProperties;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * 获取和设置会话id类，默认实现类 FinalSessionIdCookie，
 * 可以通过实现该类来定义会话id从cookie、请求头、或者参数中读取
 */
public interface FinalSessionId {
    String getSessionId(HttpServletRequest request, FinalSessionProperties properties);

    void setSessionId(HttpServletRequest request, HttpServletResponse response, FinalSessionProperties properties, String sessionId);
}
