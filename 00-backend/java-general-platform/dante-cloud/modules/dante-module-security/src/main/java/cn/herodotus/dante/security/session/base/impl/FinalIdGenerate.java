package cn.herodotus.dante.security.session.base.impl;


import cn.herodotus.dante.security.session.base.IdGenerate;
import jakarta.servlet.http.HttpServletRequest;

import java.util.UUID;

/**
 * @author lingkang
 * Created by 2022/1/26
 */
public class FinalIdGenerate implements IdGenerate {
    @Override
    public String generateId(HttpServletRequest request) {
        return UUID.randomUUID().toString();
    }
}
