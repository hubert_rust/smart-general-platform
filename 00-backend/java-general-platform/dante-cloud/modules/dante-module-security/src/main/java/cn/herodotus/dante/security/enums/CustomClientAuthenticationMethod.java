package cn.herodotus.dante.security.enums;

import org.springframework.util.Assert;

public class CustomClientAuthenticationMethod {

    private static final long serialVersionUID = 610L;
    public static final CustomClientAuthenticationMethod CLIENT_SECRET_BASIC = new CustomClientAuthenticationMethod("client_secret_basic");
    public static final CustomClientAuthenticationMethod CLIENT_SECRET_POST = new CustomClientAuthenticationMethod("client_secret_post");
    public static final CustomClientAuthenticationMethod CLIENT_SECRET_JWT = new CustomClientAuthenticationMethod("client_secret_jwt");
    public static final CustomClientAuthenticationMethod PRIVATE_KEY_JWT = new CustomClientAuthenticationMethod("private_key_jwt");
    public static final CustomClientAuthenticationMethod NONE = new CustomClientAuthenticationMethod("none");
    private final String value;

    public CustomClientAuthenticationMethod(String value) {
        Assert.hasText(value, "value cannot be empty");
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj != null && this.getClass() == obj.getClass()) {
            CustomClientAuthenticationMethod that = (CustomClientAuthenticationMethod)obj;
            return this.getValue().equals(that.getValue());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return this.getValue().hashCode();
    }
}
