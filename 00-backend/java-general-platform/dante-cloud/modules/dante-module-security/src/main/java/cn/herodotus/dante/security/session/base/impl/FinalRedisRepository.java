package cn.herodotus.dante.security.session.base.impl;

import cn.herodotus.dante.security.session.base.FinalRepository;
import cn.herodotus.dante.security.session.config.FinalSessionProperties;
import cn.herodotus.dante.security.session.wrapper.FinalSession;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;

/**
 * @author lingkang
 * Created by 2022/1/26
 */
@Slf4j
public class FinalRedisRepository implements FinalRepository {

    private RedissonClient redissonClient;
    private FinalSessionProperties properties;

    public FinalRedisRepository(FinalSessionProperties properties, RedissonClient redissonClient) {
        this.properties = properties;
        this.redissonClient = redissonClient;
    }


    public RedissonClient getRedissonClient() {
        return redissonClient;
    }


    @Override
    public void deleteSession(String id, HttpServletRequest request) {
        log.info("deleteSession");
        //redisTemplate.delete(id);
    }

    @Override
    public FinalSession getSession(String id) {
        /*Object o = redisTemplate.opsForValue().get(id);
        if (o != null)
            return (FinalSession) o;*/
        log.info("getSession");
        return null;
    }

    @Override
    public void setSession(String id, FinalSession session) {
        //redisTemplate.opsForValue().set(id, session, properties.getMaxValidTime(), TimeUnit.MILLISECONDS);
        log.info("setSession");
    }

    @Override
    public void setFinalSessionProperties(FinalSessionProperties properties) {
        this.properties = properties;
    }

    @Override
    public void destroy() {

    }
}
