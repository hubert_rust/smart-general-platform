package cn.herodotus.dante.security.session.base.impl;

import cn.herodotus.dante.security.session.base.FinalSessionId;
import cn.herodotus.dante.security.session.config.FinalSessionProperties;
import cn.herodotus.dante.security.session.utils.CookieUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lingkang
 * Created by 2022/2/9
 */
public class FinalSessionIdCookie implements FinalSessionId {
    private static final Logger log= LoggerFactory.getLogger(FinalSessionIdCookie.class);
    @Override
    public String getSessionId(HttpServletRequest request, FinalSessionProperties properties) {
        return CookieUtils.getCookieValue(properties.getCookieName(), request.getCookies());
    }

    @Override
    public void setSessionId(HttpServletRequest request, HttpServletResponse response, FinalSessionProperties properties, String sessionId) {
        try {
            CookieUtils.addSessionIdToCookie(
                    properties.getCookieName(),
                    sessionId,
                    properties.isCookieAge(),
                    properties.getMaxValidTime(),
                    response
            );
        } catch (Exception e) {
            log.error("add cookie error!",e);
        }
    }
}
