package cn.herodotus.dante.security.session.wrapper;


import cn.herodotus.dante.security.session.base.IdGenerate;
import jakarta.servlet.http.HttpServletRequest;

public interface FinalGenerateSession {
    FinalSession generateSession(HttpServletRequest request, IdGenerate idGenerate);
}
