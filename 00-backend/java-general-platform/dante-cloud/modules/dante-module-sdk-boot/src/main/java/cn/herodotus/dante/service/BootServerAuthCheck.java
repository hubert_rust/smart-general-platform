package cn.herodotus.dante.service;

import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.dromara.hutool.crypto.digest.MD5;

/**
 * @author root
 * @description: 应用集成
 * @date 2023/8/13 11:14
 *
 */
public class BootServerAuthCheck implements IAuthCheck{
    @Override
    public CommonUserDetails tokenCheck(String object,
                                        HttpServletRequest request,
                                        HttpServletResponse response) {
        return null;
    }

    public static void main(String[] args) {
        String url = "/reg/resetPwd";
        String digest = MD5.of().digestHex(url);

        System.out.println(digest);
        System.out.println();
    }
}
