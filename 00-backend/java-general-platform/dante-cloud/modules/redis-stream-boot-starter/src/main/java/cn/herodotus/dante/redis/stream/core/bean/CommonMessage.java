package cn.herodotus.dante.redis.stream.core.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author root
 * @description TODO
 * @date 2023/8/16 10:40
 */


@Data
public class CommonMessage implements Serializable {
    private String messageType;
    private Object object;

    public CommonMessage() {
    }

    public CommonMessage(String messageType, Object object) {
        this.messageType = messageType;
        this.object = object;
    }
}
