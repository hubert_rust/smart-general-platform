package cn.herodotus.dante.redis.stream.core;

import cn.herodotus.dante.redis.stream.core.constants.MessageConstants;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import jakarta.annotation.Resource;
import org.redisson.api.RFuture;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MessagePublisher {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedissonClient redissonClient;

    public long publish(String message) {
        return publish(MessageConstants.MESSAGE_TOPIC, message);
    }
    public long publish(String topic, String message) {
        try {
            String top = Strings.isNullOrEmpty(topic) ? MessageConstants.MESSAGE_TOPIC : topic;
            String post = JSONObject.toJSONString(message);
            RTopic rTopic = redissonClient.getTopic(top, new JsonJacksonCodec());
            RFuture<Long> ret = rTopic.publishAsync(post);
            long succ =  ret.get();
            return succ;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

}
