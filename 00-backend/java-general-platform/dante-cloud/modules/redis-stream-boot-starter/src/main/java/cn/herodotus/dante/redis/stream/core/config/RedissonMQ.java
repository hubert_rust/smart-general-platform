package cn.herodotus.dante.redis.stream.core.config;

import cn.herodotus.dante.redis.stream.core.bean.ICommonMessageProcess;
import cn.herodotus.dante.redis.stream.core.constants.MessageConstants;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.api.listener.MessageListener;
import org.redisson.codec.JsonJacksonCodec;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class RedissonMQ implements ApplicationRunner, Ordered {

    @Value("${message.redis.topic}")
    private String topic;
    @Resource
    private RedissonClient redissonClient;
    @Resource
    private ICommonMessageProcess messageProcess;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        RTopic rTopic
                = redissonClient.getTopic(MessageConstants.MESSAGE_TOPIC, new JsonJacksonCodec());
        rTopic.addListener(String.class, new MessageListener<String>() {
            @Override
            public void onMessage(CharSequence charSequence, String message) {
                log.info(">>> onMessage: CharSequence: {}", charSequence);
                messageProcess.messageProc(message);
            }
        });
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
