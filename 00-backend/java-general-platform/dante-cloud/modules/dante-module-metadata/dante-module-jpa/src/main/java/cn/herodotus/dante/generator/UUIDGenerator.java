package cn.herodotus.dante.generator;

import cn.herodotus.dante.container.IDContainer;
import cn.herodotus.dante.definition.EntityInterface;
import com.google.common.base.Strings;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.stereotype.Component;

@Component
public class UUIDGenerator implements IdentifierGenerator {
    @Override
    public Object generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o) {
        EntityInterface entityInterface = (EntityInterface) o;
        String id = entityInterface.getId();
        if (Strings.isNullOrEmpty(id)) {
            return IDContainer.INST.getUUID(0, true);
        }
        return id;
    }
}
