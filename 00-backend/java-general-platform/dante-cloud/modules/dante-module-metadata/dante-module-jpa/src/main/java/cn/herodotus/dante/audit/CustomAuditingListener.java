package cn.herodotus.dante.audit;

import cn.herodotus.dante.definition.CommonEntity;
import cn.herodotus.dante.definition.EntityInterface;
import cn.herodotus.dante.definition.TenantEntity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.aspectj.ConfigurableObject;

//pending
@Slf4j
@Configurable
public class CustomAuditingListener implements ConfigurableObject {

    @PrePersist
    private void prePersist(EntityInterface obj) {
        if (obj instanceof TenantEntity) {
            ((TenantEntity)obj).setTid("0");
            log.info("[JPA Audit], prePersist, TenantEntity");
        }
        else if (obj instanceof CommonEntity) {
            log.info("[JPA Audit], prePersist, CommonEntity");
        }
    }
    @PreUpdate
    private void preUpdate(EntityInterface obj) {
        log.info("[JPA Audit], preUpdate");
    }

}
