package cn.herodotus.dante.container;

import org.dromara.hutool.core.lang.generator.SnowflakeGenerator;
import org.dromara.hutool.crypto.digest.MD5;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Component
public class IDContainer implements InitializingBean, DisposableBean {
    public static IDContainer INST;

    private SnowflakeGenerator snowflakeGenerator = new SnowflakeGenerator();


    public String getSnowFlakeString() {
        return getSnowFlakeLong() + "";
    }

    public Long getSnowFlakeLong() {
        return snowflakeGenerator.next();
    }

    public String getMD5(String input) {
        return MD5.of().digestHex(input, StandardCharsets.UTF_8);
    }

    public String getTid() {
        return getUUID(12, false);
    }

    public String getUUID(int length, boolean dash) {
        String[] val = UUID.randomUUID().toString().split("-");
        if (length == 8) {
            return val[0];
        } else if (length == 12) {
            return dash ? (val[0] + "-" + val[1]) : (val[0] + val[1]);
        } else if (length == 16) {
            return dash ? (val[0] + "-" + val[1] + "-" + val[2]) : (val[0] + val[1] + val[2]);
        } else if (length == 20) {
            return dash ?
                    (val[0] + "-" + val[1] + "-" + val[2] + "-" + val[3]) : (val[0] + val[1] + val[2] + val[3]);
        }

        return UUID.randomUUID().toString();
    }


    @Override
    public void destroy() throws Exception {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        IDContainer.INST = this;

    }
}
