/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.engine.data.core.service;

import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.definition.constants.SymbolConstants;
import cn.herodotus.engine.assistant.core.definition.domain.Entity;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;

import java.io.Serializable;
import java.util.Objects;

/**
 * <p>Description: 通用核心 Service </p>
 *
 * @author : gengwei.zheng
 * @date : 2021/7/14 14:32
 */
public abstract class BaseService< E extends Entity, ID extends Serializable> implements WriteableService<E, ID> {
//    public abstract class BaseService< E extends Entity, ID extends Serializable> implements WriteableService<E, ID> {

    protected String like(String property) {
        return SymbolConstants.PERCENT + property + SymbolConstants.PERCENT;
    }

    public String getUid() {
        CommonUserDetails userDetails = CommonSecurityContextHolder.getUser();
        return Objects.isNull(userDetails) ? null : userDetails.getUid();
    }
    public String getUserTid() {
        CommonUserDetails userDetails = CommonSecurityContextHolder.getUser();
        return Objects.isNull(userDetails) ? null : userDetails.getTid();
    }

    // 正在当前工作空间tid
    public String getTid() {
        return CommonSecurityContextHolder.getTid();
    }
    public E setCreateUser(E domain) {
        CommonUserDetails userDetails = CommonSecurityContextHolder.getUser();
        if(Objects.isNull(userDetails)
                || Objects.isNull(userDetails.getUid())) {
            domain.setCreateBy("0");
        }
        else {
            domain.setCreateBy(userDetails.getUid());
        }
        return domain;
    }
    public E setUpdateUser(E domain) {
        CommonUserDetails userDetails = CommonSecurityContextHolder.getUser();
        if(Objects.isNull(userDetails)) {
            domain.setUpdateBy("0");
        }
        else {
            domain.setUpdateBy(userDetails.getUid());
        }
        return domain;
    }
}
