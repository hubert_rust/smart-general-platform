package cn.herodotus.engine.data.core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Order implements Serializable {
    private String colName;
    //排序类型 = "asc,desc"
    private String orderType;
    @Override
    public String toString() {
        return "Order [colName=" + colName + ", orderType=" + orderType + "]";
    }

}
