package cn.herodotus.engine.data.core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class Relation implements Serializable {
    private String relationType = "AND";
    private List<Condition> conditions = new ArrayList<>();
    private List<Relation> relations = new ArrayList<>();

    @Override
    public String toString() {
        return "Relation{" +
                "relationType='" + relationType + '\'' +
                ", conditions=" + conditions +
                ", relations=" + relations +
                '}';
    }
}
