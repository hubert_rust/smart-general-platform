package cn.herodotus.engine.data.core.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class ExportModel extends CommonQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    //前端传入参数：key表示的字段名称（），value表示中文名字
    private Map<String, String> exportColumn;
    private String sheetName = "sheet1";
    private String exportName;
    private String templateCode;  //模板id
    private String templateName;  //模板名称
}
