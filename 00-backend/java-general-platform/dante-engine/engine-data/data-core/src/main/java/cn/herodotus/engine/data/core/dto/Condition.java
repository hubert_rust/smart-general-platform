package cn.herodotus.engine.data.core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Condition implements Serializable {
    private String colName;
    //ruleType: "EQ, NE, LIKE, LLIKE, RLIKE, GT, LT, GE, LE, IN, NIN"
    private String ruleType;
    private Object value;

    @Override
    public String toString() {
        return "Condition {colName=" + colName + ", ruleType=" + ruleType + ", value=" + value + "}";
    }
}
