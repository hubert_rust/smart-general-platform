package cn.herodotus.engine.data.core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
 * @param:
 * @return: 通用查询接口
 * @Author: admin
 * @Date:  2021/5/7
 * @Description:
 **/
@Data
@NoArgsConstructor
public class CommonQuery extends Query {
    private Relation relation;
    private Page page;
    private List<String> group;
    private List<Order> order;
}
