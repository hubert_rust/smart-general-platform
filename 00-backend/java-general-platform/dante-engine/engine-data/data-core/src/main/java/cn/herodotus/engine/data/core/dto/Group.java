package cn.herodotus.engine.data.core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class Group implements Serializable {
    private List<String> groupName;
}
