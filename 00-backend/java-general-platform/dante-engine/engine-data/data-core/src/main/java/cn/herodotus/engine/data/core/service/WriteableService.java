/*
 * Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
 *
 * Dante Engine licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Dante Engine 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-engine>
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */

package cn.herodotus.engine.data.core.service;

import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.assistant.core.definition.domain.Entity;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>Description: 可读、可写的Service基础接口 </p>
 *
 * @author : gengwei.zheng
 * @date : 2021/7/7 16:47
 */
public interface WriteableService<E extends Entity, ID extends Serializable> extends ReadableService<E, ID> {

    /**
     * 删除数据
     *
     * @param entity 数据对应实体
     */
    default void delete(E entity) {
        getRepository().delete(entity);
    }

    default void deleteByTid(String tid) {
        Specification<E> specification = (root, query, cb) -> {
            return  cb.equal(root.get("tid"), tid);
        };
        getRepository().delete(specification);
    }
    /**
     * 批量全部删除
     */
    default void deleteAllInBatch() {
        getRepository().deleteAllInBatch();
    }

    /**
     * 删除指定多个数据
     *
     * @param entities 数据对应实体集合
     */
    default void deleteAll(Iterable<E> entities) {
        getRepository().deleteAll(entities);
    }

    /**
     * 删除全部数据
     */
    default void deleteAll() {
        getRepository().deleteAll();
    }

    /**
     * 根据ID删除数据
     *
     * @param id 数据对应ID
     */
    default void deleteById(ID id) {
        getRepository().deleteById(id);
    }
    default void deleteByIds(Iterable<ID> ids) {
        getRepository().deleteAllByIdInBatch(ids);
    }

    /*
     * @Author root
     * @Description // 新增和更新调用独立函数
     * @Date 20:28 2023/8/20
     * @Param
     * @return
     **/
    default E insert(E domain) {
        Date date = new Date();
        String tid = CommonSecurityContextHolder.getTid();
        String uid = CommonSecurityContextHolder.getUid();
        domain.setCreateTime(date);
        domain.setUpdateTime(date);
        domain.setCreateBy(uid);
        domain.setUpdateBy(uid);
        if (Objects.isNull(domain.getTid())) {
            domain.setTid(tid);
        }
        return getRepository().save(domain);
    }
    /**
     * 保存或更新数据
     *
     * @param domain 数据对应实体
     * @return 已保存数据
     */
    default E save(E domain) {
        String uid = CommonSecurityContextHolder.getUid();
        domain.setUpdateBy(uid);
        domain.setUpdateTime(new Date());

        return getRepository().save(domain);
    }

    /**
     * 批量保存数据
     *
     * @param entities 实体集合
     * @return 已经保存的实体集合
     */
    default <S extends E> List<S> insertAll(Iterable<S> entities) {
        Date now = new Date();
        String tid = CommonSecurityContextHolder.getTid();
        String uid = CommonSecurityContextHolder.getUid();
        for (S entity : entities) {
            entity.setCreateBy(uid);
            entity.setUpdateBy(uid);
            entity.setCreateTime(now);
            entity.setUpdateTime(now);
            if (Objects.isNull(entity.getTid())) {
                entity.setTid(tid);
            }
        }
        return getRepository().saveAll(entities);
    }
    /**
     * 批量更新数据
     *
     * @param entities 实体集合
     * @return 已经保存的实体集合
     */
    default <S extends E> List<S> saveAll(Iterable<S> entities) {
        Date now = new Date();
        String uid = CommonSecurityContextHolder.getUid();
        for (S entity : entities) {
            entity.setUpdateBy(uid);
            entity.setUpdateTime(now);
        }
        return getRepository().saveAll(entities);
    }
    /**
     * 保存或者更新
     *
     * @param entity 实体
     * @return 保存后实体
     */
    default E saveAndFlush(E entity) {
        return getRepository().saveAndFlush(entity);
    }

    /**
     * 批量保存或者更新
     *
     * @param entities 实体列表
     * @return 保存或更新后的实体
     */
    default List<E> saveAllAndFlush(List<E> entities) {
        return getRepository().saveAllAndFlush(entities);
    }

    /**
     * 刷新实体状态
     */
    default void flush() {
        getRepository().flush();
    }
}
