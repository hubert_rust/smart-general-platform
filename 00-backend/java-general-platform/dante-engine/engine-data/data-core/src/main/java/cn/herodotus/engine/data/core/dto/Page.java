package cn.herodotus.engine.data.core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Page implements Serializable {
    private int page;      //currPage
    private int pageSize;  //pageSize
}
