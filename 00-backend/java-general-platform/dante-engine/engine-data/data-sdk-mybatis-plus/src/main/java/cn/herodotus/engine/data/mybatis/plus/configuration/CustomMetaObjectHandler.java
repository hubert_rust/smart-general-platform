package cn.herodotus.engine.data.mybatis.plus.configuration;

import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.constants.DataConstants;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;

import java.time.LocalDateTime;
import java.util.Objects;

public class CustomMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        LocalDateTime date = LocalDateTime.now();
        //租户信息
        String uid = CommonSecurityContextHolder.getUid();
        String tid = CommonSecurityContextHolder.getTid();
        if (this.getFieldValByName(DataConstants.TENANE_FIELD_NAME, metaObject) == null) {
            if (Objects.nonNull(tid)) {
                this.setFieldValByName(DataConstants.TENANE_FIELD_NAME, tid, metaObject);
            }
            else {
            }
        }
        if (this.getFieldValByName(DataConstants.CU_ENTITY, metaObject) == null) {
            if (Objects.nonNull(uid)) {
                this.setFieldValByName(DataConstants.CU_ENTITY, uid, metaObject);
            }
        }

        if (this.getFieldValByName(DataConstants.MU_ENTITY, metaObject) == null) {
            if (Objects.nonNull(uid)) {
                this.setFieldValByName(DataConstants.MU_ENTITY, uid, metaObject);
            }
        }

        this.setFieldValByName(DataConstants.MT_ENTITY, date, metaObject);
        this.setFieldValByName(DataConstants.CT_ENTITY, date, metaObject);
    }

    /*
     * @param: 
     * @return:
     * @Author: admin 
     * @Date:  2021/4/9
     * @Description: 
     **/
    @Override
    public void updateFill(MetaObject metaObject) {
        String uid = CommonSecurityContextHolder.getUid();
        this.setFieldValByName(DataConstants.MT_ENTITY, LocalDateTime.now(), metaObject);
        this.setFieldValByName(DataConstants.MU_ENTITY, uid, metaObject);
    }
}
