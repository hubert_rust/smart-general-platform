package cn.herodotus.engine.data.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
public abstract class BaseEntity implements IEntityInterface, Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	protected String id;
	//@ApiModelProperty(value = "业务状态: 1 正常, 0 异常")
	//protected Integer state;
	//@ApiModelProperty(value = "是否已删除: 1 已删除, 0 未删除")
	//protected Integer deleted;

	//@DateTimeFormat()
	//@JsonFormat(pattern = "")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@TableField(fill = FieldFill.INSERT)
	protected LocalDateTime createTime;

	@TableField(fill = FieldFill.INSERT)
	protected String createBy;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@TableField(fill = FieldFill.INSERT_UPDATE)
	protected LocalDateTime updateTime;
	@TableField(fill = FieldFill.INSERT_UPDATE)
	protected String updateBy;

	@TableField(exist = false)
	protected Boolean cached = false;

	@Override
	public Boolean getCached() {
		return cached;
	}

	@Override
	public void setCached(Boolean cached) {
		this.cached = cached;
	}
}
