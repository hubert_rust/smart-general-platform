package cn.herodotus.engine.data.mybatis.plus.service;

import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.dto.CommonQuery;
import cn.herodotus.engine.data.core.dto.PageResponse;
import cn.herodotus.engine.data.mybatis.plus.entity.IEntityInterface;
import cn.herodotus.engine.data.mybatis.plus.utils.LambdaUtil;
import cn.herodotus.engine.data.mybatis.plus.utils.SFunction;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

@Scope("prototype")
@Transactional(rollbackFor = Exception.class)
public abstract class AbstractServiceImpl<M extends BaseMapper<T>, T extends IEntityInterface>
        extends ServiceImpl<M, T>
         {
    public List<T> listFromIds(Collection<? extends Serializable> collection) {
        return super.listByIds(collection);
    }

    public List<T> listByFdIds(SFunction<T, ?> fn, Collection<? extends Serializable> collection) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        String fdName = LambdaUtil.getFieldName(fn);
        queryWrapper.in(fdName, collection);
        return super.getBaseMapper().selectList(queryWrapper);
    }


    public AbstractServiceImpl() {
    }

    protected boolean exist(String fdName, Object obj) {
        List<T> list = super.getBaseMapper().selectList(new QueryWrapper<T>().eq(fdName, obj));
        return CollectionUtils.isNotEmpty(list) ? true : false;
    }


    public String getClientId() throws Exception {
        return CommonSecurityContextHolder.getCid();
    }

    public String getUid() {
        return CommonSecurityContextHolder.getUid();
    }

    public String getTid() {
        return CommonSecurityContextHolder.getTid();
    }

    public List<T> selectListByField(SFunction<T, ?> fn, Object obj) {
        try {
            String fdName = LambdaUtil.getFieldName(fn);
            return super.getBaseMapper().selectList(new QueryWrapper<T>().eq(fdName, obj));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public int deleteByField(SFunction<T, ?> fn, Object obj) {
        try {
            String fdName = LambdaUtil.getFieldName(fn);
            return super.getBaseMapper().delete(new QueryWrapper<T>().eq(fdName, obj));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public T selectOneByField(SFunction<T, ?> fn, Object obj) {
        try {
            String fdName = LambdaUtil.getFieldName(fn);
            T t = super.getBaseMapper().selectOne(new QueryWrapper<T>().eq(fdName, obj));
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }


    /*
     * @param:
     * @return: 返回entity中包含了id
     * @Author: admin
     * @Date:  2021/5/7
     * @Description:
     **/
    public T insert(T entity) {
        return save(entity) ? entity : null;
    }

    public T getOne(Serializable id) {
        return super.getById(id);
    }

    public boolean update(T entity, LambdaQueryWrapper<T> query) {
        return super.update(entity, query);
    }

    public boolean update(T entity) throws Exception {
        return super.updateById(entity);
    }

    /*@Override
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }*/

    public boolean deleteById(Serializable id) throws Exception {
        return super.removeById(id);
    }

    public Integer saveBatch(List<T> entityList, int batch) {
        super.saveBatch(entityList);
        return entityList.size();
    }

    public boolean updateBatchById(Collection<T> entityList) {
        return super.updateBatchById(entityList);
    }


    public boolean deleteBatchIds(List<T> entities) throws Exception {
        Set<Serializable> sets = entities.stream().map(v -> v.getId()).collect(Collectors.toSet());
        return super.removeByIds(sets);
    }

    public abstract PageResponse<T> userList(CommonQuery commonQuery) throws Exception;

    public abstract PageResponse<T> tidList(CommonQuery commonQuery) throws Exception;

    public abstract PageResponse<T> commonList(CommonQuery commonQuery) throws Exception;

    public abstract PageResponse<T> permitList(CommonQuery commonQuery) throws Exception;

    public PageResponse<T> list(Integer idx, Integer pageSize, QueryWrapper queryWrapper) {
        Page<T> page = new Page<>(idx, pageSize);
        //IPage<T> records = batchBaseMapper.selectPage(page, queryWrapper);
        IPage<T> records = getBaseMapper().selectPage(page, queryWrapper);
        PageResponse<T> response = new PageResponse<T>();
        response.setData(records.getRecords());
        response.setPageSize((int) records.getSize());
        response.setPageIndex((int) records.getCurrent());
        response.setTotalCount((int) records.getTotal());
        response.setSuccess(true);
        return response;
    }

    public String getTableName() {
        Class cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        Annotation clsAnno = cls.getAnnotation(TableName.class);
        return ((TableName) cls.getAnnotation(TableName.class)).value().toUpperCase();
    }

    //随机查询
    public static <T> List<T> getAny(BaseMapper<T> mapper, QueryWrapper<T> wrapper, Integer limit) {
        Long total = mapper.selectCount(wrapper);
        if (limit == null || limit <= 0 || total == 0) {
            return Collections.emptyList();
        }
        List<T> list = Optional.of(limit).filter(l -> l > total).map(l -> mapper.selectList(wrapper))
                .orElseGet(() -> mapper.selectList(wrapper.last("LIMIT " + new SecureRandom().nextLong(total - (limit - 1)) + "," + limit)));
        //Collections.shuffle(list);
        return list;
    }

    //随机查询
    public List<T> getAny(QueryWrapper<T> wrapper, Integer limit) {
        //LambdaQueryWrapper<T> wrapper = Wrappers.lambdaQuery(condition);
        BaseMapper<T> mapper = super.getBaseMapper();
        Long total = mapper.selectCount(wrapper);
        if (limit == null || limit <= 0 || total == 0) {
            return Collections.emptyList();
        }
        List<T> list = Optional.of(limit).filter(l -> l > total).map(l -> mapper.selectList(wrapper))
                .orElseGet(() -> mapper.selectList(
                        wrapper.last("LIMIT " + new SecureRandom().nextLong(total - (limit - 1)) + "," + limit)
                ));
        //Collections.shuffle(list);
        return list;
    }
}
