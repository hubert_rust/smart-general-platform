package cn.herodotus.engine.data.mybatis.plus.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.Strings;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public interface Expression {
    enum Operator {
        EQ, NE, LIKE, LLIKE, RLIKE, GT, LT, GE, LE, IN, NIN, LAST
    }

    static Operator getOperator(String str) {
        for (Operator operator : Arrays.asList(Operator.values())) {
            if (operator.toString().equals(str.toUpperCase())) {
                return operator;
            }
        }
        return Operator.EQ;
    }

    static Object getQueryWrap(QueryWrapper queryWrapper,
                               Operator operator,
                               Field dataType,
                               String fieldName,
                               Object fieldVal) {
        switch (operator) {
            case EQ:
                return queryWrapper.eq(fieldName, fieldVal);
            case NE:
                return queryWrapper.ne(fieldName, fieldVal);
            case LIKE:
                return queryWrapper.like(fieldName, fieldVal);
            case LLIKE:
                return queryWrapper.likeLeft(fieldName, fieldVal);
            case RLIKE:
                return queryWrapper.likeRight(fieldName, fieldVal);
            case GT:
                return queryWrapper.gt(fieldName, fieldVal);
            case LT:
                return queryWrapper.lt(fieldName, fieldVal);
            case GE:
                return queryWrapper.ge(fieldName, fieldVal);
            case LE:
                return queryWrapper.le(fieldName, fieldVal);
            case IN: {
                getCollection(fieldVal, dataType);
                List<Object> list = (List<Object>) fieldVal;
                return queryWrapper.in(fieldName, list);
            }
            case NIN: {
                List<Object> list = (List<Object>) fieldVal;
                return queryWrapper.notIn(fieldName, list);
            }
            case LAST: {
                //只是针对create_time
                String lastDay = getDate((-1) *Integer.valueOf(fieldVal.toString()));
                return queryWrapper.apply(!Strings.isNullOrEmpty(fieldName),
                        "date_format (create_time,'%Y-%m-%d') <= date_format('" + lastDay + "','%Y-%m-%d')");
            }
            default:
                return queryWrapper.eq(fieldName, fieldVal);
        }
    }

    static String getDate(int i) {
        LocalDateTime dateTime = LocalDateTime.now();
        dateTime = i > 0 ? dateTime.plusDays(i) : dateTime.minusDays(-1*i);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTime.format(formatter);
    }

    static List<?> getCollection(Object object, Field fd) {
        /*JSONObject.parseArray()
        List list = (List) JSONArray.toCollection(jsonArray);
        JSONArray jsonArray = (JSONArray)*/
        List list = JSONObject.parseArray(JSON.toJSONString(object), fd.getType());
        return list;
    }

    enum Logical {
        AND, OR
    }

    static Logical getLogical(String name) {
        return name.toUpperCase().equals("AND") ? Logical.AND : Logical.OR;
    }

    static String getLogicalStr(Logical logical) {
        return logical == Logical.AND ? " AND " : " OR ";
    }

}
