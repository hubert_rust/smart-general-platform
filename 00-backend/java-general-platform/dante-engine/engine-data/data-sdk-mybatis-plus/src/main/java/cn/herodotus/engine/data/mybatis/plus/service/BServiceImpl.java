package cn.herodotus.engine.data.mybatis.plus.service;

import cn.herodotus.engine.assistant.core.context.CommonSecurityContextHolder;
import cn.herodotus.engine.data.core.dto.*;
import cn.herodotus.engine.data.mybatis.plus.entity.IEntityInterface;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * @param:
 * @return: mybatis plus ServiceImpl限制，增加了M
 * @Author: admin
 * @Date:  2021/10/28
 * @Description:
 **/
@Slf4j
public class BServiceImpl<M extends BaseMapper<T>, T extends IEntityInterface> extends AbstractServiceImpl<M, T> {

    /*
     * @param:
     * @return: 对于公共数据(和用户无关的数据)
     * @Author: admin
     * @Date:  2021/6/4
     * @Description:
     **/
    public static void addConditons(CommonQuery cq,
                                    String colName,
                                    String ruleType,
                                    Object val) {
        CommonQuery commonQuery = cq;
        if (commonQuery == null) {
            commonQuery = new CommonQuery();
        }
        if (commonQuery.getRelation() == null) {
            commonQuery.setRelation(new Relation());
        }
        List<Condition> conList = commonQuery.getRelation().getConditions();
        if (conList == null) {
            commonQuery.getRelation().setConditions(Lists.newArrayList());
        }
        conList = commonQuery.getRelation().getConditions();
        Condition condition = new Condition();
        condition.setColName(colName);
        condition.setRuleType(ruleType);
        condition.setValue(val);
        conList.add(condition);
    }
    @Deprecated
    public void addCondition(CommonQuery cq,
                             String colName,
                             String ruleType,
                             Object val) {
        CommonQuery commonQuery = cq;
        if (commonQuery == null) {
            commonQuery = new CommonQuery();
        }
        if (commonQuery.getRelation() == null) {
            commonQuery.setRelation(new Relation());
        }
        List<Condition> conList = commonQuery.getRelation().getConditions();
        if (conList == null) {
            commonQuery.getRelation().setConditions(Lists.newArrayList());
        }
        conList = commonQuery.getRelation().getConditions();
        Condition condition = new Condition();
        condition.setColName(colName);
        condition.setRuleType(ruleType);
        condition.setValue(val);
        conList.add(condition);
    }

    @Override
    public PageResponse<T> tidList(CommonQuery commonQuery) throws Exception {
        if (commonQuery == null) {
            commonQuery = new CommonQuery();
        }

        Page page = getPage(commonQuery);
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("tid", super.getTid());
        PageResponse<T> ret = getPage(commonQuery, page.getPage(), page.getPageSize(), queryWrapper);
        return ret;
    }


    @Override
    public PageResponse<T> userList(CommonQuery commonQuery) throws Exception {
        if (commonQuery == null) {
            commonQuery = new CommonQuery();
        }
        Page page = getPage(commonQuery);
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();


        String uid = CommonSecurityContextHolder.getUid();
        queryWrapper.eq("create_user", uid);
        return getPage(commonQuery, page.getPage(), page.getPageSize(), (QueryWrapper<T>) queryWrapper);

    }
    @Override
    public PageResponse<T> commonList(CommonQuery commonQuery) throws Exception {
        if (commonQuery == null) {
            commonQuery = new CommonQuery();
        }
        Page page = getPage(commonQuery);
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();

        return getPage(commonQuery, page.getPage(), page.getPageSize(), (QueryWrapper<T>) queryWrapper);
    }
    @Override
    public PageResponse<T> permitList(CommonQuery commonQuery) throws Exception {
        return null;
    }

    private Page getPage(CommonQuery commonQuery) {
        Page page = new Page();
        Integer idx = commonQuery.getPage() == null ? 1 :
                (commonQuery.getPage().getPage() <= 0 ? 1 : commonQuery.getPage().getPage());
        Integer pageSize = commonQuery.getPage() == null ? 10 :
                (commonQuery.getPage().getPageSize() > 500 ? 500 : commonQuery.getPage().getPageSize());
        page.setPage(idx);
        page.setPageSize(pageSize);
        return page;
    }

    private PageResponse<T> getPage(CommonQuery commonQuery,
                                    Integer idx,
                                    Integer pageSize,
                                    QueryWrapper<T> queryWrapper) {
        try {
            Class cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
            Map<String, Field> map = getField(cls);

            Consumer<QueryWrapper<T>> consumer = parse(queryWrapper, commonQuery.getRelation(), map);
            if (consumer != null) {
                queryWrapper.nested(consumer);
            }

            //group
            if (commonQuery.getGroup() != null) {
                queryWrapper = queryWrapper.groupBy(commonQuery.getGroup());
            }
            //order by
            if (commonQuery.getOrder() == null) {
               /* List<Order> orders = Lists.newArrayList();
                Order order = new Order();
                order.setColName("create_time");
                order.setOrderType("desc");
                orders.add(order);
                order = new Order();
                order.setColName("id");
                order.setOrderType("desc");
                orders.add(order);
                commonQuery.setOrder(orders);*/
            }
            //order by
            if (commonQuery.getOrder() != null) {
                for (Order order : commonQuery.getOrder()) {
                    if (order.getOrderType().toUpperCase().equals("ASC")) {
                        queryWrapper = queryWrapper.orderByAsc(order.getColName());
                    } else if (order.getOrderType().toUpperCase().equals("DESC")) {
                        queryWrapper = queryWrapper.orderByDesc(order.getColName());
                    }
                }
            }

            return super.list(idx, pageSize, queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(">>> {}, error: {}", this.getClass().getSimpleName(), e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }


    private Map<String, Field> getField(Class<T> cls) {
        Map<String, Field> MAP_FIDLD = Maps.newHashMap();
        Class<?> clazz = cls;
        while (Objects.nonNull(clazz)) {
            Arrays.asList(clazz.getDeclaredFields()).forEach(v -> {
                MAP_FIDLD.put(v.getName(), v);
            });
            clazz = clazz.getSuperclass();
        }
        return MAP_FIDLD;
    }

    private static Pattern linePattern = Pattern.compile("_(\\w)");

    public static String lineToHump(String in) {
        String str = in.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /*private Object conditionDateType(Condition con, String dateType) {
        if (dateType.contains("Date")) {
            return DateSpan.GET_DATE.getDate2Str(con.getValue().toString());
        }
        return con.getValue();
    }*/

    public void conditionExpress(QueryWrapper<T> queryWrapper,
                                 Condition con,
                                 Map<String, Field> fdMap) {
        try {
            String fdName = lineToHump(con.getColName());
            Field fd = fdMap.get(fdName);
            if (Objects.nonNull(fd)) {
                //String dataType = fd.getGenericType().getTypeName();
                String dataType = fd.getType().getSimpleName();
                //Object obj = conditionDateType(con, dataType);
                Expression.Operator operator
                        = Expression.getOperator(con.getRuleType().toUpperCase());
                Expression.getQueryWrap(queryWrapper,
                        operator,
                        fd,
                        con.getColName(),
                        con.getValue());
            }
            else {
                //queryWrapper.eq("1", 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void condtion(QueryWrapper<T> queryWrapper,
                          Condition con,
                          String relationType,
                          Map<String, Field> fdMap) {
        QueryWrapper<T> qw = relationType.toUpperCase().equals("AND") ? queryWrapper : queryWrapper.or();
        conditionExpress(queryWrapper, con, fdMap);
    }

    //
    private Consumer<QueryWrapper<T>> parse(QueryWrapper<T> q,
                                            Relation relation,
                                            Map<String, Field> fdMap) {
        if (relation == null) {
            return null;
        }
        String relationType = relation.getRelationType();
        List<Condition> conditions = relation.getConditions();
        List<Relation> relations = relation.getRelations();
        if (CollectionUtils.isEmpty(conditions)
                && CollectionUtils.isEmpty(relations)) {
            return null;
        }

        Consumer<QueryWrapper<T>> consumer = (qw) -> {
            if (Objects.isNull(conditions) || conditions.isEmpty()) {
                for (Relation rel : relations) {
                    if ("AND".equals(relationType.toUpperCase())) {
                        qw.and(parse(qw, rel, fdMap));
                    } else {
                        qw.or().nested(parse(qw, rel, fdMap));
                    }
                }
            } else {

                if ("OR".equals(relationType.toUpperCase())) {
                    qw.or(i -> {
                        for (Condition con : conditions) {
                            if (Strings.isNullOrEmpty(con.getValue().toString())) {
                                continue;
                            }
                            condtion(i, con, relationType, fdMap);
                            //i.or().eq(con.getColName(), con.getValue());
                        }
                    });
                } else {
                    qw.and(i -> {
                        for (Condition con : conditions) {
                            if (Strings.isNullOrEmpty(con.getValue().toString())) {
                                continue;
                            }
                            condtion(i, con, relationType, fdMap);
                            //i.eq(con.getColName(), con.getValue());
                        }
                    });
                }

                if (Objects.nonNull(relations) && relations.size() > 0) {
                    for (Relation rel : relations) {
                        if ("AND".equals(relationType.toUpperCase())) {
                            qw.and(parse(qw, rel, fdMap));
                        } else {
                            qw.or().nested(parse(qw, rel, fdMap));
                        }
                    }
                }
            }
        };
        return consumer;
    }



}
