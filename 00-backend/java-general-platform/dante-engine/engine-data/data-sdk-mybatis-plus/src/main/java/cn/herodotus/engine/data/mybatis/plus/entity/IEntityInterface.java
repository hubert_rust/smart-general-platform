package cn.herodotus.engine.data.mybatis.plus.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public interface IEntityInterface extends Serializable {
    default String getTid(){return  "0";};
    default void setTid(String tid) {};
    String getId();
    void setId(String id);
    LocalDateTime getCreateTime();
    void setCreateTime(LocalDateTime createTime);
    String getCreateBy();
    void setCreateBy(String uid);
    LocalDateTime getUpdateTime();
    void setUpdateTime(LocalDateTime modifyTime);
    String getUpdateBy();
    void setUpdateBy(String uid);
    void setCached(Boolean cache);
    Boolean getCached();
}
