package cn.herodotus.engine.data.mybatis.plus.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class TenantEntity extends BaseEntity implements IEntityInterface, Serializable {
    protected String tid; //租户id
}
