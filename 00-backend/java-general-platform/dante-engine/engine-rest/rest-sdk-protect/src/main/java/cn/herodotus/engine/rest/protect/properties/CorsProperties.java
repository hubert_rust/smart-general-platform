package cn.herodotus.engine.rest.protect.properties;

import jakarta.annotation.Resource;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "server")
public class CorsProperties  implements InitializingBean, DisposableBean {
    private List<String> allowOrigin;
    public static CorsProperties INST;

    private String origin;

    public void setAllowOrigin(List<String> allowOrigin) {
        this.allowOrigin = allowOrigin;
    }

    public List<String> getAllowOrigin() {
        return allowOrigin;
    }

    @Resource
    private ApplicationContext applicationContext;
    @Override
    public void afterPropertiesSet() throws Exception {
        CorsProperties.INST = this;
        Environment env = applicationContext.getEnvironment();
        // String origin = env.getProperty("server.allow-origin");
        // CorsProperties.INST.allowOrigin = origin;
    }

    @Override
    public void destroy() throws Exception {

    }

}
