package cn.herodotus.engine.rest.protect.secure.interceptor;

import cn.herodotus.engine.rest.protect.properties.CorsProperties;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * 跨域过滤器
 * @author click33 
 */
@Component
@Order(-200)
public class CustomCorsFilter implements Filter {

	private static final Logger log = LoggerFactory.getLogger(CustomCorsFilter.class);
	static final String OPTIONS = "OPTIONS";

	private static String corsPermit(HttpServletRequest request, List<String> origins) {
		if (Strings.isEmpty(request.getHeader("Origin")))  {
			return request.getHeader("Origin");
		}
		for (String origin : origins) {
			String org = request.getHeader("Origin");
			if (org.contains(origin)) {
				return origin;
			}
		}
		return "";
	}
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		List<String> origins = CorsProperties.INST.getAllowOrigin();
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		// 允许指定域访问跨域资源
		String origin = corsPermit(request, origins);
		if (Strings.isNotEmpty(origin)) {
			response.setHeader("Access-Control-Allow-Origin", origin);
		}
		else {
			int index = request.getRequestURL().toString().indexOf(request.getRequestURI());
			String val = request.getRequestURL().toString().substring(0, index);
			response.setHeader("Access-Control-Allow-Origin", val);
		}
		// 允许所有请求方式
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		// 有效时间
		response.setHeader("Access-Control-Max-Age", "3600");
		// 允许的header参数
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with,Cid,App-Type,Apt,Device-Type,Smart-Token,X-Smart-User-Realm,Authorization,Content-Type");

		response.addHeader("Access-Control-Allow-Credentials","true"); // 允许携带验证信息

		// 如果是预检请求，直接返回
		if (OPTIONS.equals(request.getMethod())) {
			log.info("=======================浏览器发来了OPTIONS预检请求==========");
			String requestUrl = request.getHeader("Origin");

			response.getWriter().print("");
			return;
		}

		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig filterConfig) {
	}

	@Override
	public void destroy() {
	}

}
