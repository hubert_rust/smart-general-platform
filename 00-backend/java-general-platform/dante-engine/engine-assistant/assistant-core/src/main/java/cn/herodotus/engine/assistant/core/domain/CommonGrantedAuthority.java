package cn.herodotus.engine.assistant.core.domain;

public interface CommonGrantedAuthority {
    String getAuthority();
}
