package cn.herodotus.engine.assistant.core.domain;

import java.util.Collection;

public interface CommonUserDetails {

    Object getObject();
    Collection<? extends CommonGrantedAuthority> getAuthorities();

    String getPassword();
    String getUid();
    String getClientId();
    String getTid();

    // account
    String getUsername();
    String getMainDeptId();

    boolean isAccountNonExpired();

    boolean isAccountNonLocked();

    boolean isCredentialsNonExpired();

    boolean isEnabled();
}
