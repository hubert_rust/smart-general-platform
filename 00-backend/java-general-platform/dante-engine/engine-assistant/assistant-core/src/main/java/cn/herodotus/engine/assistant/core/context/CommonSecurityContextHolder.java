package cn.herodotus.engine.assistant.core.context;

import cn.herodotus.engine.assistant.core.definition.constants.HttpHeaders;
import cn.herodotus.engine.assistant.core.domain.CommonUserDetails;
import com.alibaba.ttl.TransmittableThreadLocal;
import com.google.common.base.Strings;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Objects;

/**
 * @author root
 * @description TODO
 * @date 2023/8/15 10:16
 */
public class CommonSecurityContextHolder {
    private static final ThreadLocal<CommonUserDetails> CURRENT_CONTEXT
            = new TransmittableThreadLocal<>();


    public static CommonUserDetails getUser() {
        return CURRENT_CONTEXT.get();
    }

    public static String getUid() {
        CommonUserDetails user = CURRENT_CONTEXT.get();
        if (Objects.isNull(user)) {
            return null;
        }
        return user.getUid();
    }

    public static String getTid() {
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        String tid =  request.getHeader(HttpHeaders.HTTP_WKS_HEADER);

        // 用户端，应用可能header中没有tid(例如，小程序)
        return Strings.isNullOrEmpty(tid) ? getUserTid(): tid;
    }

    // 当前用户的tid,
    // 获取当前用户空间的tid, 和当前用户getTid中tid可能不同(比如代管admin)
    public static String getUserTid() {
        CommonUserDetails user = CURRENT_CONTEXT.get();
        if (Objects.isNull(user)) {
            return null;
        }
        return user.getTid();
    }

    public static String getAgent() {
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getHeader(HttpHeaders.HTTP_DEVICE_TYPE);
    }

    // pending

    public static String getCid() {
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getHeader(HttpHeaders.HTTP_CID);
    }
    public static String getApt() {
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getHeader(HttpHeaders.HTTP_APT);
    }
    public static void setUser(CommonUserDetails user) {
        CURRENT_CONTEXT.set(user);
    }

    public static void clearUserDetail() {
        CURRENT_CONTEXT.remove();
    }
}
